import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  InputNumber,
} from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";
import {
  DeleteSelfDevelopmentCategories,
  DeleteSelfDevelopmentCategoriesVariables,
  SelfDevelopmentCategories,
  UpdateSelfDevelopmentCategories,
  UpdateSelfDevelopmentCategoriesVariables,
} from "../globalTypes";
import {
  DELETE_SELF_DEVELOPMENT_CATEGORIES,
  SELF_DEVELOPMENT_CATEGORIES,
  UPDATE_SELF_DEVELOPMENT_CATEGORIES,
} from "./gql/selfDevelopmentCategoriesGql";
import { useMutation } from "@apollo/client";

const { Text } = Typography;

type MutationActionModalProps = {
  record: any;
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const MutationActionModal = (props: MutationActionModalProps) => {
  // Deleting
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };

  const [del, { loading: deleteLoading }] = useMutation<
    DeleteSelfDevelopmentCategories,
    DeleteSelfDevelopmentCategoriesVariables
  >(DELETE_SELF_DEVELOPMENT_CATEGORIES, {
    onCompleted: () => {
      setDeleteModalVisible(false);
    },
    onError: () => {
      setDeleteModalVisible(false);
    },
  });
  const handleDeleteOk = () => {
    del({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<SelfDevelopmentCategories>({
            query: SELF_DEVELOPMENT_CATEGORIES,
          });

          if (existing) {
            const newSelfDevelopmentCategories = existing.self_development_categories.filter(
              (g: any) => g.id !== props.record.id
            );
            cache.writeQuery({
              query: SELF_DEVELOPMENT_CATEGORIES,
              data: {
                self_development_categories: newSelfDevelopmentCategories,
              },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // Editing

  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    form.submit();
  };

  const [update, { loading: editLoading }] = useMutation<
    UpdateSelfDevelopmentCategories,
    UpdateSelfDevelopmentCategoriesVariables
  >(UPDATE_SELF_DEVELOPMENT_CATEGORIES, {
    onCompleted: () => {
      setEditModalVisible(false);
    },
  });
  const handleEditFinish = (values: Store) => {
    update({
      variables: {
        pk_columns: {
          id: props.record.id,
        },
        _set: {
          name: values.name,
          credit: values.credit,
        },
      },
    });
  };

  return (
    <>
      <Modal
        title="Hapus Jenis Pengembangan Diri"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">ID: {props.record.id}</Text>
          <Text type="danger">Nama: {props.record.name}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Jenis Pengembangan Diri"
        visible={editModalvisible}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-selfDevelopmentCategories"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
        >
          <Form.Item
            name={"id"}
            label="ID"
            rules={[{ required: true }]}
            initialValue={props.record.id}
          >
            <Input disabled />
          </Form.Item>
          <Form.Item
            name={"name"}
            label="Nama"
            rules={[{ required: true }]}
            initialValue={props.record.name}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"credit"}
            label="Angka Kredit"
            rules={[{ required: true }]}
            initialValue={props.record.credit}
          >
            <InputNumber min={0} step={0.1} />
          </Form.Item>
        </Form>
      </Modal>

      <Row justify="space-around">
        <Col span={10}>
          <Button block onClick={handleEditClick}>
            <EditOutlined />
          </Button>
        </Col>
        <Col span={10}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined />
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionModal;
