import React from "react";
import { Table, Row, Col } from "antd";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import { HomeOutlined } from "@ant-design/icons";
import ManagementHeading from "../Common/ManagementHeading";
import AddButton from "./AddForm";
import MutationActionsModal from "./MutationActionsModal";
import { useQuery } from "@apollo/client";
import { SELF_DEVELOPMENT_CATEGORIES } from "./gql/selfDevelopmentCategoriesGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import { SelfDevelopmentCategories } from "../globalTypes";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Angka Kredit",
    dataIndex: "credit",
    key: "credit",
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/self-development-categories",
    content: "Pengelolaan Jenis Pengembangan Diri",
  },
];

const SelfDevelopmentListPage = () => {
  const selfDevelopmentCategoriesRes = useQuery<SelfDevelopmentCategories>(
    SELF_DEVELOPMENT_CATEGORIES
  );

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (
    selfDevelopmentCategoriesRes.loading ||
    !selfDevelopmentCategoriesRes.data ||
    !selfDevelopmentCategoriesRes.data.self_development_categories
  ) {
    return <LoadingPlaceholder />;
  }

  const selfDevelopmentCategories =
    selfDevelopmentCategoriesRes.data.self_development_categories;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Jenis Pengembangan Diri"
        breadcrumbs={breadcrumbs}
        rightAddon={<AddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={selfDevelopmentCategories}
          />
        </Col>
      </Row>
    </>
  );
};

export default SelfDevelopmentListPage;
