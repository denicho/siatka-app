import { gql } from "@apollo/client";

export const DELETE_SELF_DEVELOPMENT_CATEGORIES = gql`
  mutation DeleteSelfDevelopmentCategories($id: bigint!) {
    delete_self_development_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const SELF_DEVELOPMENT_CATEGORIES = gql`
  query SelfDevelopmentCategories {
    self_development_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_SELF_DEVELOPMENT_CATEGORIES = gql`
  mutation InsertSelfDevelopmentCategories(
    $object: self_development_categories_insert_input!
  ) {
    insert_self_development_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_SELF_DEVELOPMENT_CATEGORIES = gql`
  mutation UpdateSelfDevelopmentCategories(
    $pk_columns: self_development_categories_pk_columns_input!
    $_set: self_development_categories_set_input
  ) {
    update_self_development_categories_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
