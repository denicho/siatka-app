import { gql } from "@apollo/client";

export const DELETE_SELF_DEVELOPMENT_ACTIVITY_POSITIONS = gql`
  mutation DeleteSelfDevelopmentActivityPositions($id: Int!) {
    delete_self_development_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const SELF_DEVELOPMENT_ACTIVITY_POSITIONS = gql`
  query SelfDevelopmentActivityPositions {
    self_development_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_SELF_DEVELOPMENT_ACTIVITY_POSITIONS = gql`
  mutation InsertSelfDevelopmentActivityPositions(
    $object: self_development_activity_positions_insert_input!
  ) {
    insert_self_development_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_SELF_DEVELOPMENT_ACTIVITY_POSITIONS = gql`
  mutation UpdateSelfDevelopmentActivityPositions(
    $pk_columns: self_development_activity_positions_pk_columns_input!
    $_set: self_development_activity_positions_set_input
  ) {
    update_self_development_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
