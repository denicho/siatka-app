import React, { Children } from "react";
import AuthContext from "./Contexts/AuthContext";

type Props = {
  children: JSX.Element;
};

const AuthorizedRender = (props: Props) => {
  const auth = React.useContext(AuthContext);
  console.log({ auth });

  return props.children;
};

export default AuthorizedRender;
