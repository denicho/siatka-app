import React from "react";
import { Row, Col, Form, Input, Button, Spin, Alert } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";
import AuthContext from "../Contexts/AuthContext";
import styles from "./styles.module.css";
import uajyImg from "../assets/uajy.png";

const LoginPage = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(false);
  const auth = React.useContext(AuthContext);

  const onFinish = (values: Store) => {
    setLoading(true);

    const onError = () => {
      setLoading(false);
      setError(true);
    };

    auth.login(
      {
        username: values.username,
        password: values.password,
      },
      onError
    );
  };

  return (
    <Row align="middle" justify="center" className={styles.row}>
      <Col>
        {error && (
          <Alert
            message="Username atau password anda salah"
            type="error"
            className={styles.alertBox}
          />
        )}
        <Form
          form={form}
          name="normal_login"
          className={styles.loginForm}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <div style={{ display: "flex" }}>
            <img src={uajyImg} className={styles.logoImg} />
            <div>
              <div style={{ fontSize: 22, fontWeight: "bold" }}>
                Selamat Datang di SIATKA
              </div>
              <div style={{ marginBottom: 32 }}>
                Silakan login untuk melanjutkan
              </div>
            </div>
          </div>

          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Mohon isi username anda",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Mohon isi password anda",
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Button
              type={loading ? "default" : "primary"}
              htmlType="submit"
              disabled={loading}
            >
              {loading ? <Spin size="small" /> : "Log In"}
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default LoginPage;
