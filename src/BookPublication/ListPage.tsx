import React from "react";
import { get } from "lodash-es";
import { Table, Row, Col, Space, Button, Input } from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  InfoCircleTwoTone,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

import ManagementHeading from "../Common/ManagementHeading";
// import StaffAddButton from "./AddFormStaff";
// import LecturerAddButton from "./AddFormLecturer";
import stringTableSorter from "../utils/stringTableSorter";
import { useQuery } from "@apollo/client";
import { BOOK_PUBLICATIONS } from "./gql/bookPublicationGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import {
  BookPublications,
  BookPublications_book_publications,
} from "../globalTypes";
import BookPublicationAddButton from "./AddForm";
import numberTableSorter from "../utils/numberTableSorter";

type Props = {
  id: string;
};

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/bookpublications`,
    content: `Pengelolaan Publikasi Buku`,
  },
];

const EmployeeListPage = () => {
  const history = useHistory();
  const bookPublicationsRes = useQuery<BookPublications>(BOOK_PUBLICATIONS);
  const [searchText, setSearchText] = React.useState("");
  const [searchedCol, setSearchedCol] = React.useState("");
  const searchInput = React.useRef<Input>(null);

  if (
    bookPublicationsRes.loading ||
    !bookPublicationsRes.data ||
    !bookPublicationsRes.data.book_publications
  ) {
    return <LoadingPlaceholder />;
  }

  const bookPublications = bookPublicationsRes.data.book_publications;

  // Filtering

  const handleSearch = (
    selectedKeys: string,
    confirm: () => void,
    dataIndex: string
  ) => {
    console.log("handle search", {
      selectedKeys,
      dataIndex,
    });
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedCol(dataIndex);
  };
  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (
    dataIndex: string | string[],
    label: string
  ) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            if (searchInput !== null) {
              // @ts-ignore
              searchInput.current = node;
            }
          }}
          placeholder={`Cari ${label}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(
              selectedKeys,
              confirm,
              Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
            )
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(
                selectedKeys,
                confirm,
                Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
              )
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Cari
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (
      value: string | number | boolean,
      record: BookPublications_book_publications
    ) => {
      const searched = get(
        record,
        Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
      );

      // @ts-ignore
      return searched
        ? // @ts-ignore
          searched
            .toString()
            .toLowerCase()
            // @ts-ignore
            .includes(value.toLowerCase())
        : "";
    },
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => {
          if (searchInput.current) {
            searchInput.current.select();
          }
        }, 100);
      }
    },
    render: (text: string) => {
      const shouldHighlight = Array.isArray(dataIndex)
        ? searchedCol === dataIndex.join(".")
        : searchedCol === dataIndex;

      return shouldHighlight ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      );
    },
  });

  const columns = [
    {
      title: "Judul Publikasi",
      dataIndex: "title",
      key: "title",
      sorter: (
        a: BookPublications_book_publications,
        b: BookPublications_book_publications
      ) => {
        return stringTableSorter(a.title || "", b.title || "");
      },
      ...getColumnSearchProps("title", "Judul"),
    },
    {
      title: "Tahun Terbit",
      dataIndex: "publish_year",
      key: "publish_year",
      sorter: (
        a: BookPublications_book_publications,
        b: BookPublications_book_publications
      ) => {
        return numberTableSorter(a.publish_year || 0, b.publish_year || 0);
      },
      ...getColumnSearchProps("publish_year", "Tahun Terbit"),
    },
    {
      title: "Penerbit",
      dataIndex: "publisher_name",
      key: "publisher_name",
      sorter: (
        a: BookPublications_book_publications,
        b: BookPublications_book_publications
      ) => {
        return stringTableSorter(
          a.publisher_name || "",
          b.publisher_name || ""
        );
      },
      ...getColumnSearchProps("publisher_name", "Tahun Terbit"),
    },
    {
      title: "Kota Penerbit",
      dataIndex: "city",
      key: "city",
      sorter: (
        a: BookPublications_book_publications,
        b: BookPublications_book_publications
      ) => {
        return stringTableSorter(a.city || "", b.city || "");
      },
      ...getColumnSearchProps("city", "Tahun Terbit"),
    },
    {
      title: "Detail",
      key: "detail",
      dataIndex: "detail",
      width: 140,
      render: (text: any, record: any, index: any) => {
        return <InfoCircleTwoTone onClick={() => {}} />;
      },
    },
  ];

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Publikasi Buku"
        breadcrumbs={breadcrumbs}
        rightAddon={
          <BookPublicationAddButton onFinish={bookPublicationsRes.refetch} />
        }
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  history.push(
                    `/dashboard/manage/bookpublications/${record.id}`
                  );
                },
              };
            }}
            size="middle"
            rowKey="id"
            columns={columns}
            dataSource={bookPublications}
          />
        </Col>
      </Row>
    </>
  );
};

export default EmployeeListPage;
