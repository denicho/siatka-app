import React from "react";
import { flatten, debounce, get } from "lodash-es";
import {
  PlusOutlined,
  RightOutlined,
  LeftOutlined,
  MinusCircleOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";
import {
  Button,
  Form,
  Input,
  Divider,
  Spin,
  Modal,
  Row,
  Col,
  DatePicker,
  Upload,
  Typography,
  Select,
  Steps,
  InputNumber,
} from "antd";
import {
  ActivityPositions,
  SearchEmployees,
  SearchEmployeesVariables,
  InsertBookPublications,
  InsertBookPublicationsVariables,
  InsertBookPublicationMember,
  InsertBookPublicationMemberVariables,
  BookPublicationActivityPositions,
  BookPublicationCategories,
} from "../globalTypes";
import { gql, useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { createCustomRequest } from "../Common/uploadFile";
import { SEARCH_EMPLOYEES } from "../Employees/gql/employeesGql";
import {
  INSERT_BOOK_PUBLICATIONS,
  INSERT_BOOK_PUBLICATION_MEMBER,
} from "./gql/bookPublicationGql";
import normalizeMembers from "../utils/normalizeMembers";
import { BOOK_PUBLICATION_ACTIVITY_POSITIONS } from "../BookPublicationCategories/gql/bookPublicationPositionsGql";
import { BOOK_PUBLICATION_CATEGORIES } from "../BookPublicationCategories/gql/bookPublicationCategoriesGql";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

const { Step } = Steps;
const { Option } = Select;

export const normFile = (e: any) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

type AddButtonProps = {
  onFinish: () => void;
};

const BookPublicationAddButton = (props: AddButtonProps) => {
  const [searchEmployees, searchEmployeesRes] = useLazyQuery<
    SearchEmployees,
    SearchEmployeesVariables
  >(SEARCH_EMPLOYEES, {
    onCompleted: (data) => {},
  });
  const categoriesRes = useQuery<BookPublicationCategories>(
    BOOK_PUBLICATION_CATEGORIES
  );
  const activityPosRes = useQuery<BookPublicationActivityPositions>(
    BOOK_PUBLICATION_ACTIVITY_POSITIONS
  );

  const [visible, setVisible] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState(false);
  const [selectedCategoryCredit, setSelectedCategoryCredit] =
    React.useState<number>(0);
  const [isTotalCreditError, setIsTotalCreditError] =
    React.useState<boolean>(true);

  const [form] = Form.useForm();

  const [insertBookPublications] = useMutation<
    InsertBookPublications,
    InsertBookPublicationsVariables
  >(INSERT_BOOK_PUBLICATIONS);
  const [insertBookPublicationMember] = useMutation<
    InsertBookPublicationMember,
    InsertBookPublicationMemberVariables
  >(INSERT_BOOK_PUBLICATION_MEMBER, {
    onCompleted: () => {
      form.resetFields();
      setCurrentStep(0);
      setIsTotalCreditError(true);
      setBookFileList([]);
      setLoading(false);
      setVisible(false);
      props.onFinish();
    },
    onError: () => {
      setLoading(false);
      setVisible(false);
    },
  });

  const categories = categoriesRes?.data?.book_publication_categories || [];
  const activityPoses =
    activityPosRes?.data?.book_publication_activity_positions || [];
  const searchedEmployees = searchEmployeesRes?.data?.employees || [];
  const [selectedEmployeeIDs, setSelectedEmployeeIDs] = React.useState<
    number[]
  >([]);

  const filteredEmployees = searchedEmployees
    ? searchedEmployees.filter(
        (emp) => !selectedEmployeeIDs.includes(emp.value)
      )
    : [];

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    if (isTotalCreditError) {
      return;
    }

    form.submit();
  };
  const onFinish = (values: Store) => {
    setLoading(true);

    const vals = form.getFieldsValue(flatten(validateList));

    const insertBookPublicationsPayload = {
      ...vals,
      category_id: Number(vals.category_id),
      publish_year: vals.publish_year.year(),
      book_file: get(
        vals,
        `book_file[${vals.book_file ? vals.book_file.length - 1 : 0}].url`,
        null
      ),
      credit_multiplier: selectedCategoryCredit,
    };

    insertBookPublications({
      variables: {
        object: insertBookPublicationsPayload,
      },
      update: (cache, { data }) => {
        if (data?.insert_book_publications_one) {
          cache.modify({
            fields: {
              book_publications(existingBookPublications = []) {
                const newBookPublications = cache.writeFragment({
                  data: data.insert_book_publications_one,
                  fragment: gql`
                    fragment book_publications on book_publications {
                      id
                      title
                      created_at
                      updated_at
                      deleted_at
                      book_publication_category {
                        id
                        name
                      }
                      publisher_name
                      publish_year
                      city
                      book_file
                    }
                  `,
                });
                return [...existingBookPublications, newBookPublications];
              },
            },
          });
        }
      },
    }).then((result) => {
      const bookPublicationId = result?.data?.insert_book_publications_one?.id;

      if (bookPublicationId) {
        const insertBookPublicationMembersPayload = normalizeMembers(
          bookPublicationId,
          "book_publication_id",
          values.internal_members,
          values.external_members,
          selectedCategoryCredit
        );

        insertBookPublicationMember({
          variables: {
            objects: insertBookPublicationMembersPayload,
          },
        });
      }
    });
  };

  // Uploads
  const [bookFileList, setBookFileList] = React.useState([]);
  const handleBookUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setBookFileList(fileList);
  };

  const [currentStep, setCurrentStep] = React.useState(0);
  const validateList = [
    ["title", "publisher_name", "publish_year", "city", "category_id"],
    ["book_file"],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 2 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
  const steps = [
    {
      title: "Data Publikasi Buku",
      content: (
        <>
          <Form.Item name={"title"} label="Judul" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"category_id"}
            label="Kategori"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Kategori Publikasi Buku"
              loading={categories.length <= 0}
            >
              {categories.map((ctg) => (
                <Option key={ctg.id} value={ctg?.id?.toString() || ""}>
                  {ctg.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"publisher_name"}
            label="Nama Penerbit"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"publish_year"}
            label="Tahun Terbit"
            rules={[{ required: true }]}
          >
            <DatePicker format="YYYY" picker="year" placeholder="Pilih tahun" />
          </Form.Item>
          <Form.Item
            name={"city"}
            label="Kota Penerbit"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Berkas",
      content: (
        <>
          <Form.Item
            name={"book_file"}
            label="File Buku"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
          >
            <Upload
              customRequest={createCustomRequest("book_file")}
              onChange={handleBookUploadChange}
              fileList={bookFileList}
              onRemove={() => setBookFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {bookFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Penulis",
      content: (
        <>
          <Divider orientation="center" plain>
            Penulis Internal
          </Divider>
          <Row justify="center" style={{ paddingBottom: 16 }}>
            <Typography.Text>Angka Kredit Terbagi:&nbsp;</Typography.Text>
            <Typography.Text strong>{selectedCategoryCredit}</Typography.Text>
          </Row>
          <Form.List
            name="internal_members"
            rules={[
              {
                validator: async (_, names) => {
                  if (!names || names.length < 1) {
                    return Promise.reject(new Error("Minimal 1 penulis"));
                  }
                },
              },
            ]}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <>
                    <div>
                      <Typography.Text type="secondary" underline>
                        Kredit didapatkan:{" "}
                        {Number(
                          form.getFieldValue([
                            "internal_members",
                            field.name,
                            "credit_percentage",
                          ]) * selectedCategoryCredit
                        ) / 100}
                      </Typography.Text>
                    </div>
                    <div
                      key={field.key}
                      style={{
                        display: "flex",
                        alignItems: "baseline",
                        marginBottom: 8,
                      }}
                    >
                      <Form.Item
                        {...field}
                        name={[field.name, "activity_position_id"]}
                        fieldKey={[field.fieldKey, "activity_position_id"]}
                        rules={[
                          { required: true, message: "Posisi wajib diisi" },
                        ]}
                        style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                      >
                        <Select
                          placeholder="Posisi"
                          onChange={() => {}}
                          loading={activityPoses.length <= 0}
                          allowClear
                        >
                          {activityPoses.map((activity) => (
                            <Option
                              key={activity.id}
                              value={activity?.id?.toString() || ""}
                            >
                              {activity.name}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "internal_member"]}
                        fieldKey={[field.fieldKey, "internal_member"]}
                        rules={[
                          { required: true, message: "Nama wajib diisi" },
                        ]}
                        style={{
                          width: "100%",
                          marginRight: 8,
                          marginBottom: 8,
                        }}
                      >
                        <Select
                          maxTagCount={1}
                          labelInValue
                          mode="multiple"
                          value={form.getFieldValue([
                            "internal_members",
                            field.fieldKey,
                            "internal_member",
                          ])}
                          placeholder="Cari nama penulis internal"
                          notFoundContent={
                            searchEmployeesRes.loading ? (
                              <Spin size="small" />
                            ) : null
                          }
                          filterOption={false}
                          onChange={(value) => {
                            const newValue = value[value.length - 1];

                            form.setFieldsValue({
                              internal_members: form
                                .getFieldValue("internal_members")
                                .map(
                                  (
                                    x: Array<{
                                      internal_member: Array<{
                                        value: string | number;
                                        label: string;
                                        key: string;
                                      }>;
                                    }>,
                                    i: number
                                  ) => {
                                    if (i === field.name) {
                                      return {
                                        ...x,
                                        internal_member: newValue
                                          ? [newValue]
                                          : [],
                                      };
                                    }
                                    return x;
                                  }
                                ),
                            });
                          }}
                          onSearch={debounce((value) => {
                            searchEmployees({
                              variables: {
                                term: `%${value}%`,
                              },
                            });
                          }, 400)}
                        >
                          {filteredEmployees.map((d) => (
                            <Option key={`${d.value}a`} value={d.value}>
                              {d.text}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "credit_percentage"]}
                        fieldKey={[field.fieldKey, "credit_percentage"]}
                        initialValue={0}
                        rules={[
                          {
                            required: true,
                            message: "Persentase Angka Kredit wajib diisi",
                          },
                        ]}
                        style={{
                          marginRight: 8,
                          marginBottom: 8,
                          width: 240,
                        }}
                        validateStatus={
                          isTotalCreditError ? "warning" : undefined
                        }
                        help={
                          isTotalCreditError ? "Total harus 100%" : undefined
                        }
                      >
                        <InputNumber
                          min={0}
                          max={100}
                          style={{ width: "100%" }}
                          formatter={(value) => `${value}%`}
                          parser={(value: any) => value.replace("%", "")}
                        />
                      </Form.Item>
                      <MinusCircleOutlined onClick={() => remove(field.name)} />
                    </div>
                  </>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Internal
                  </Button>
                  <Form.ErrorList errors={errors} />
                </Form.Item>
              </>
            )}
          </Form.List>

          {/* external */}
          <Divider orientation="center" plain>
            Penulis Eksternal
          </Divider>
          <Form.List name="external_members">
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <div
                    key={field.key}
                    style={{
                      display: "flex",
                      alignItems: "baseline",
                      marginBottom: 8,
                    }}
                  >
                    <Form.Item
                      {...field}
                      name={[field.name, "activity_position_id"]}
                      fieldKey={[field.fieldKey, "activity_position_id"]}
                      rules={[
                        { required: true, message: "Posisi wajib diisi" },
                      ]}
                      style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                    >
                      <Select
                        placeholder="Posisi"
                        onChange={() => {}}
                        loading={activityPoses.length <= 0}
                        allowClear
                      >
                        {activityPoses.map((activity) => (
                          <Option
                            key={activity.id}
                            value={activity?.id?.toString() || ""}
                          >
                            {activity.name}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      {...field}
                      name={[field.name, "external_members"]}
                      fieldKey={[field.fieldKey, "external_members"]}
                      rules={[{ required: true, message: "Nama wajib diisi" }]}
                      style={{
                        width: "100%",
                        marginRight: 8,
                        marginBottom: 8,
                      }}
                    >
                      <Input placeholder="Nama" />
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                  </div>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Eksternal
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          {stepNavigator}
        </>
      ),
    },
  ];

  return (
    <>
      <Modal
        title="Tambah Publikasi Buku"
        visible={visible}
        onCancel={handleCancel}
        width={768}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            disabled={currentStep !== steps.length - 1}
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={onFinish}
          validateMessages={validateMessages}
          onValuesChange={(changed, all) => {
            if (changed.category_id && all.category_id) {
              const selectedCtg = categories.find(
                (ctg) => ctg.id === Number(changed.category_id)
              );
              setSelectedCategoryCredit(
                selectedCtg?.credit ? selectedCtg?.credit : 0
              );
            }

            if (changed.internal_members && all.internal_members) {
              const truthyInternalMembers =
                all.internal_members.filter(Boolean);

              const creditPercentages = truthyInternalMembers.reduce(
                (acc: number, curr: any) => {
                  return acc + Number(curr.credit_percentage);
                },
                0
              );

              if (creditPercentages !== 100) {
                setIsTotalCreditError(true);
              } else {
                setIsTotalCreditError(false);
              }
            }

            // This will trigger the filters so that user cant pick the same employee twice
            if (changed.internal_members && all.internal_members) {
              const selectedEmployeeIds = all.internal_members
                .filter(Boolean)
                .map((member: any) => {
                  const id =
                    (member.internal_member &&
                      member.internal_member.length > 0 &&
                      member.internal_member[0]?.value) ||
                    0;

                  return id;
                })
                .filter(Boolean);

              setSelectedEmployeeIDs(selectedEmployeeIds);
            }
          }}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>
      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Publikasi Buku
      </Button>
    </>
  );
};

export default BookPublicationAddButton;
