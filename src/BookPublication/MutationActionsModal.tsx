import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  Upload,
  Select,
  Steps,
  InputNumber,
  Divider,
  Spin,
} from "antd";
import moment from "moment";
import { flatten, debounce } from "lodash-es";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  MinusCircleOutlined,
  RightOutlined,
  UploadOutlined,
  LeftOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";

import { createCustomRequest } from "../Common/uploadFile";
import {
  ActivityPositions,
  BookPublicationByPK_book_publications_by_pk,
  BookPublicationByPK_book_publications_by_pk_book_publication_members,
  BookPublications,
  SearchEmployees,
  SearchEmployeesVariables,
  SoftDeleteBookPublicationByPK,
  SoftDeleteBookPublicationByPKVariables,
  UpdateBookPublication,
  UpdateBookPublicationVariables,
  DeleteBookPublicationMembers,
  DeleteBookPublicationMembersVariables,
  InsertBookPublicationMember,
  InsertBookPublicationMemberVariables,
  BookPublicationActivityPositions,
  BookPublicationCategories,
} from "../globalTypes";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";

import { useHistory } from "react-router-dom";
import {
  DELETE_BOOK_PUBLICATION_MEMBERS,
  INSERT_BOOK_PUBLICATION_MEMBER,
  BOOK_PUBLICATIONS,
  SOFT_DELETE_BOOK_PUBLICATION_BY_PK,
  UPDATE_BOOK_PUBLICATION,
} from "./gql/bookPublicationGql";
import { normFile } from "./AddForm";
import { SEARCH_EMPLOYEES } from "../Employees/gql/employeesGql";
import normalizeMembers from "../utils/normalizeMembers";
import { BOOK_PUBLICATION_ACTIVITY_POSITIONS } from "../BookPublicationCategories/gql/bookPublicationPositionsGql";
import { BOOK_PUBLICATION_CATEGORIES } from "../BookPublicationCategories/gql/bookPublicationCategoriesGql";

const { Text } = Typography;
const { Step } = Steps;
const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

type MutationActionModalProps = {
  record: BookPublicationByPK_book_publications_by_pk;
  onEditFinish?: (id: number) => void;
};

const getExternalMemberInitialValues = (
  members: BookPublicationByPK_book_publications_by_pk_book_publication_members[]
) => {
  return members
    .filter((member) => !member.employee)
    .map((extMember) => ({
      activity_position_id:
        extMember.book_publication_activity_position?.id.toString(),
      external_members: extMember.external_author,
    }));
};

const getInternalMemberInitialValues = (
  members: BookPublicationByPK_book_publications_by_pk_book_publication_members[]
) => {
  return members
    .filter((mem) => Boolean(mem.employee))
    .map((intMember) => ({
      activity_position_id:
        intMember.book_publication_activity_position?.id.toString(),
      internal_member: [
        {
          value: intMember.employee?.id,
          key: intMember.employee?.id,
          label: intMember.employee?.name,
        },
      ],
    }));
};

const MutationActionsModalBookPublication = (
  props: MutationActionModalProps
) => {
  console.log(props.record);

  const history = useHistory();

  // deleting
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const [deleteLoading, setDeleteLoading] = React.useState(false);
  const [del] = useMutation<
    SoftDeleteBookPublicationByPK,
    SoftDeleteBookPublicationByPKVariables
  >(SOFT_DELETE_BOOK_PUBLICATION_BY_PK, {
    onCompleted: () => {
      setDeleteLoading(false);
      history.push("/dashboard/manage/bookpublications");
    },
  });
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };
  const handleDeleteOk = () => {
    setDeleteLoading(true);
    del({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<BookPublications>({
            query: BOOK_PUBLICATIONS,
          });

          if (existing) {
            const newBookPublications = existing.book_publications.filter(
              (data) => data.id !== props.record.id
            );

            cache.writeQuery({
              query: BOOK_PUBLICATIONS,
              data: { book_publications: newBookPublications },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // editing
  const [searchEmployees, searchEmployeesRes] = useLazyQuery<
    SearchEmployees,
    SearchEmployeesVariables
  >(SEARCH_EMPLOYEES, {
    onCompleted: (data) => {},
  });
  const activityPosRes = useQuery<BookPublicationActivityPositions>(
    BOOK_PUBLICATION_ACTIVITY_POSITIONS
  );

  const activityPoses =
    activityPosRes?.data?.book_publication_activity_positions || [];

  const categoriesRes = useQuery<BookPublicationCategories>(
    BOOK_PUBLICATION_CATEGORIES
  );
  const categories = categoriesRes?.data?.book_publication_categories || [];

  const searchedEmployees = searchEmployeesRes?.data?.employees || [];
  const [selectedEmployeeIDs, setSelectedEmployeeIDs] = React.useState<
    number[]
  >(
    props.record.book_publication_members
      .map((mem) => mem.employee?.id)
      .filter(Boolean)
  );
  const filteredEmployees = searchedEmployees
    ? searchedEmployees.filter(
        (emp) => !selectedEmployeeIDs.includes(emp.value)
      )
    : [];

  const selectedCategoryCredit = props.record?.credit_multiplier || 0;
  const [isTotalCreditError, setIsTotalCreditError] =
    React.useState<boolean>(true);

  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const [editLoading, setEditLoading] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    if (isTotalCreditError) {
      return;
    }

    form.submit();
  };

  const validateList = [
    ["title", "publisher_name", "publish_year", "city"],
    ["book_file"],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const [currentStep, setCurrentStep] = React.useState(0);
  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 2 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );

  // Uploads
  const [bookFileList, setBookFileList] = React.useState([
    {
      name: "cert.jpg",
      type: "image/jpeg",
      uid: "1",
      url: props.record.book_file || "",
      size: 0,
    },
  ]);
  const handleBookUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setBookFileList(fileList);
  };

  const steps = [
    {
      title: "Data Publikasi Book",
      content: (
        <>
          <Form.Item
            name={"title"}
            label="Judul Publikasi"
            rules={[{ required: true }]}
            initialValue={props.record.title}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"category_id"}
            label="Kategori"
            rules={[{ required: true }]}
            initialValue={props.record.book_publication_category?.id.toString()}
            extra="Kategori tidak dapat diubah karena akan memengaruhi angka kredit."
          >
            <Select
              placeholder="Pilih Kategori Penelitian"
              loading={categories.length <= 0}
              disabled
            >
              {categories.map((ctg) => (
                <Option key={ctg.id} value={ctg?.id?.toString() || ""}>
                  {ctg.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"publisher_name"}
            label="Nama Penerbit"
            rules={[{ required: true }]}
            initialValue={props.record.publisher_name}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"publish_year"}
            label="Tahun Terbit"
            rules={[{ required: true }]}
            initialValue={moment(
              new Date(`${props.record.publish_year}-01-01`),
              "YYYY"
            )}
          >
            <DatePicker format="YYYY" picker="year" placeholder="Pilih tahun" />
          </Form.Item>
          <Form.Item
            name={"city"}
            label="Kota Penerbit"
            rules={[{ required: true }]}
            initialValue={props.record.city}
          >
            <Input />
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Berkas",
      content: (
        <>
          <Form.Item
            name={"book_file"}
            label="File Buku"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
            initialValue={[
              {
                name: "File",
                type: "image.jpg",
                uid: "1",
                // @ts-ignore
                status: "done",
                url: props.record.book_file,
                size: 0,
              },
            ]}
          >
            <Upload
              customRequest={createCustomRequest("book_file")}
              onChange={handleBookUploadChange}
              fileList={bookFileList}
              onRemove={() => setBookFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {bookFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Penulis",
      content: (
        <>
          <Divider orientation="center" plain>
            Penulis Internal
          </Divider>
          <Row justify="center" style={{ paddingBottom: 16 }}>
            <Typography.Text>Angka Kredit Terbagi:&nbsp;</Typography.Text>
            <Typography.Text strong>{selectedCategoryCredit}</Typography.Text>
          </Row>
          <Form.List
            name="internal_members"
            rules={[
              {
                validator: async (_, names) => {
                  if (!names || names.length < 1) {
                    return Promise.reject(new Error("Minimal 1 penulis"));
                  }
                },
              },
            ]}
            initialValue={getInternalMemberInitialValues(
              props.record.book_publication_members
            )}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <>
                    <div>
                      <Typography.Text type="secondary" underline>
                        Kredit didapatkan:{" "}
                        {Number(
                          form.getFieldValue([
                            "internal_members",
                            field.name,
                            "credit_percentage",
                          ]) * selectedCategoryCredit
                        ) / 100}
                      </Typography.Text>
                    </div>
                    <div
                      key={field.key}
                      style={{
                        display: "flex",
                        alignItems: "baseline",
                        marginBottom: 8,
                      }}
                    >
                      <Form.Item
                        {...field}
                        name={[field.name, "activity_position_id"]}
                        fieldKey={[field.fieldKey, "activity_position_id"]}
                        rules={[
                          { required: true, message: "Posisi wajib diisi" },
                        ]}
                        style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                      >
                        <Select
                          placeholder="Posisi"
                          onChange={() => {}}
                          loading={activityPoses.length <= 0}
                          allowClear
                        >
                          {activityPoses.map((activity) => (
                            <Option
                              key={activity.id}
                              value={activity?.id?.toString() || ""}
                            >
                              {activity.name}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "internal_member"]}
                        fieldKey={[field.fieldKey, "internal_member"]}
                        rules={[
                          { required: true, message: "Nama wajib diisi" },
                        ]}
                        style={{
                          width: "100%",
                          marginRight: 8,
                          marginBottom: 8,
                        }}
                      >
                        <Select
                          maxTagCount={1}
                          labelInValue
                          mode="multiple"
                          value={form.getFieldValue([
                            "internal_members",
                            field.fieldKey,
                            "internal_member",
                          ])}
                          placeholder="Cari nama penulis internal"
                          notFoundContent={
                            searchEmployeesRes.loading ? (
                              <Spin size="small" />
                            ) : null
                          }
                          filterOption={false}
                          onChange={(value) => {
                            const newValue = value[value.length - 1];

                            form.setFieldsValue({
                              internal_members: form
                                .getFieldValue("internal_members")
                                .map(
                                  (
                                    x: Array<{
                                      internal_member: Array<{
                                        value: string | number;
                                        label: string;
                                        key: string;
                                      }>;
                                    }>,
                                    i: number
                                  ) => {
                                    if (i === field.name) {
                                      return {
                                        ...x,
                                        internal_member: newValue
                                          ? [newValue]
                                          : [],
                                      };
                                    }
                                    return x;
                                  }
                                ),
                            });
                          }}
                          onSearch={debounce((value) => {
                            searchEmployees({
                              variables: {
                                term: `%${value}%`,
                              },
                            });
                          }, 400)}
                        >
                          {filteredEmployees.map((d) => (
                            <Option key={`${d.value}a`} value={d.value}>
                              {d.text}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "credit_percentage"]}
                        fieldKey={[field.fieldKey, "credit_percentage"]}
                        initialValue={0}
                        rules={[
                          {
                            required: true,
                            message: "Persentase Angka Kredit wajib diisi",
                          },
                        ]}
                        style={{
                          marginRight: 8,
                          marginBottom: 8,
                          width: 240,
                        }}
                        validateStatus={
                          isTotalCreditError ? "warning" : undefined
                        }
                        help={
                          isTotalCreditError ? "Total harus 100%" : undefined
                        }
                      >
                        <InputNumber
                          min={0}
                          max={100}
                          style={{ width: "100%" }}
                          formatter={(value) => `${value}%`}
                          parser={(value: any) => value.replace("%", "")}
                        />
                      </Form.Item>
                      <MinusCircleOutlined onClick={() => remove(field.name)} />
                    </div>
                  </>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Internal
                  </Button>
                  <Form.ErrorList errors={errors} />
                </Form.Item>
              </>
            )}
          </Form.List>

          {/* external */}
          <Divider orientation="center" plain>
            Penulis Eksternal
          </Divider>
          <Form.List
            name="external_members"
            initialValue={getExternalMemberInitialValues(
              props.record.book_publication_members
            )}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <div
                    key={field.key}
                    style={{
                      display: "flex",
                      alignItems: "baseline",
                      marginBottom: 8,
                    }}
                  >
                    <Form.Item
                      {...field}
                      name={[field.name, "activity_position_id"]}
                      fieldKey={[field.fieldKey, "activity_position_id"]}
                      rules={[
                        { required: true, message: "Posisi wajib diisi" },
                      ]}
                      style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                    >
                      <Select
                        placeholder="Posisi"
                        onChange={() => {}}
                        loading={activityPoses.length <= 0}
                        allowClear
                      >
                        {activityPoses.map((activity) => (
                          <Option
                            key={activity.id}
                            value={activity?.id?.toString() || ""}
                          >
                            {activity.name}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      {...field}
                      name={[field.name, "external_members"]}
                      fieldKey={[field.fieldKey, "external_members"]}
                      rules={[{ required: true, message: "Nama wajib diisi" }]}
                      style={{
                        width: "100%",
                        marginRight: 8,
                        marginBottom: 8,
                      }}
                    >
                      <Input placeholder="Nama" />
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                  </div>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Eksternal
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          {stepNavigator}
        </>
      ),
    },
  ];

  const [updateBookPublication, { loading: updateBookPublicationLoading }] =
    useMutation<UpdateBookPublication, UpdateBookPublicationVariables>(
      UPDATE_BOOK_PUBLICATION
    );
  const [deleteBookPublicationMembers, { loading: deleteResMemLoading }] =
    useMutation<
      DeleteBookPublicationMembers,
      DeleteBookPublicationMembersVariables
    >(DELETE_BOOK_PUBLICATION_MEMBERS);
  const [insertBookPublicationMember, { loading: insertResMemLoading }] =
    useMutation<
      InsertBookPublicationMember,
      InsertBookPublicationMemberVariables
    >(INSERT_BOOK_PUBLICATION_MEMBER);
  const handleEditFinish = (values: Store) => {
    const vals = form.getFieldsValue(flatten(validateList));

    const bookFileVal = vals.book_file;

    let bookFile = "";

    try {
      if (bookFileVal && bookFileVal.length > 0) {
        // berarti ganti file
        bookFile = bookFileVal[bookFileVal.length - 1].url;
      }
    } catch (err) {
      console.log("err", err);
    }

    const updateBookPublicationPayload = {
      ...vals,
      publish_year: vals.publish_year.year(),
      book_file: bookFile,
    };

    Promise.all([
      updateBookPublication({
        variables: {
          id: props.record.id,
          _set: {
            ...updateBookPublicationPayload,
          },
        },
      }),
      new Promise((resolve, reject) => {
        deleteBookPublicationMembers({
          variables: {
            book_publication_id: props.record.id,
          },
        })
          .then(() => {
            const insertBookPublicationMembersPayload = normalizeMembers(
              props.record.id,
              "book_publication_id",
              values.internal_members,
              values.external_members,
              selectedCategoryCredit
            );

            insertBookPublicationMember({
              variables: {
                objects: insertBookPublicationMembersPayload,
              },
            }).then(() => {
              resolve(true);
            });
          })
          .catch((err) => {
            reject(err);
          });
      }),
    ])
      .then(() => {
        setCurrentStep(0);
        form.resetFields();
        setIsTotalCreditError(true);
        setEditLoading(false);
        setEditModalVisible(false);
        props.onEditFinish && props.onEditFinish(props.record.id);
      })
      .catch((err) => {
        console.log({
          err,
        });
        setEditLoading(false);
        setEditModalVisible(false);
      });
  };

  return (
    <>
      <Modal
        title="Hapus Publikasi Jurnal"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">Judul: {props.record.title}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Publikasi Buku"
        visible={editModalvisible}
        width={768}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            disabled={
              currentStep !== steps.length - 1 ||
              updateBookPublicationLoading ||
              deleteResMemLoading ||
              insertResMemLoading
            }
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
          onValuesChange={(changed, all) => {
            if (changed.internal_members && all.internal_members) {
              const truthyInternalMembers =
                all.internal_members.filter(Boolean);

              const creditPercentages = truthyInternalMembers.reduce(
                (acc: number, curr: any) => {
                  return acc + Number(curr.credit_percentage);
                },
                0
              );

              if (creditPercentages !== 100) {
                setIsTotalCreditError(true);
              } else {
                setIsTotalCreditError(false);
              }
            }

            // This will trigger the filters so that user cant pick the same employee twice
            if (changed.internal_members && all.internal_members) {
              const selectedEmployeeIds = all.internal_members
                .filter(Boolean)
                .map((member: any) => {
                  const id =
                    (member.internal_member &&
                      member.internal_member.length > 0 &&
                      member.internal_member[0]?.value) ||
                    0;

                  return id;
                })
                .filter(Boolean);

              setSelectedEmployeeIDs(selectedEmployeeIds);
            }
          }}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>

      <Row justify="space-around" style={{ marginTop: 16 }}>
        <Col span={14}>
          <Button block onClick={handleEditClick}>
            <EditOutlined /> Ubah
          </Button>
        </Col>
        <Col span={14} style={{ marginTop: 16 }}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined /> Hapus
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionsModalBookPublication;
