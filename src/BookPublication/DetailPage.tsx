import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import {
  BookPublicationByPK,
  BookPublicationByPKVariables,
} from "../globalTypes";
import { BOOK_PUBLICATION_BY_ID } from "./gql/bookPublicationGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalBookPublication from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/bookpublications`,
    content: `Pengelolaan Publikasi Buku`,
  },
  {
    to: `/dashboard/manage/bookpublications/${id}`,
    content: `Detail Publikasi Buku`,
  },
];

const membersColumns = [
  {
    title: "Posisi",
    dataIndex: ["book_publication_activity_position", "name"],
  },
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const BookPublicationDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const bookPublicationRes = useQuery<
    BookPublicationByPK,
    BookPublicationByPKVariables
  >(BOOK_PUBLICATION_BY_ID, {
    variables: {
      id: Number(id),
    },
  });

  const bookPublication = bookPublicationRes?.data?.book_publications_by_pk;

  if (!bookPublicationRes.loading && bookPublicationRes.error) {
    // error handling
    return null;
  }
  if (!bookPublication) {
    if (bookPublicationRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  return (
    <>
      <ManagementHeading
        title="Detail Publikasi Buku"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>File Buku</Text>
              </Col>
              <Col span={24}>
                {bookPublication?.book_file ? (
                  <Typography.Link
                    href={bookPublication?.book_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalBookPublication
            record={bookPublication}
            onEditFinish={(id) => {
              bookPublicationRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Buku" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label="Judul" value={bookPublication.title} />
                    <FieldInfo
                      label="Jenis Publikasi Buku"
                      value={bookPublication?.book_publication_category?.name}
                    />
                    <FieldInfo
                      label="Angka Kredit Terbagi"
                      value={bookPublication?.credit_multiplier}
                    />
                    <FieldInfo
                      label="Penerbit"
                      value={bookPublication.publisher_name}
                    />
                    <FieldInfo
                      label="Tahun Terbit"
                      value={bookPublication.publish_year}
                    />
                    <FieldInfo
                      label="Kota Penerbit"
                      value={bookPublication.city}
                    />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical"></Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Penulis" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={bookPublication.book_publication_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default BookPublicationDetailPage;
