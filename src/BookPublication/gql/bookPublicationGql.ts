import { gql } from "@apollo/client";

export const BOOK_PUBLICATIONS = gql`
  query BookPublications {
    book_publications(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      book_publication_category {
        id
        credit
        name
      }
      book_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        book_publication_activity_position {
          id
          name
        }
      }
      publisher_name
      publish_year
      city
      book_file
    }
  }
`;

export const BOOK_PUBLICATION_BY_ID = gql`
  query BookPublicationByPK($id: bigint!) {
    book_publications_by_pk(id: $id) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      book_publication_category {
        id
        credit
        name
      }
      book_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        book_publication_activity_position {
          id
          name
        }
      }
      publisher_name
      publish_year
      city
      book_file
    }
  }
`;

export const INSERT_BOOK_PUBLICATIONS = gql`
  mutation InsertBookPublications($object: book_publications_insert_input!) {
    insert_book_publications_one(object: $object) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      book_publication_category {
        id
        credit
        name
      }
      book_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        book_publication_activity_position {
          id
          name
        }
      }
      publisher_name
      publish_year
      city
      book_file
    }
  }
`;

export const INSERT_BOOK_PUBLICATION_MEMBER = gql`
  mutation InsertBookPublicationMember(
    $objects: [book_publication_members_insert_input!]!
  ) {
    insert_book_publication_members(objects: $objects) {
      affected_rows
      returning {
        id
        credit_gain
        book_publication_id
        employee {
          id
          name
          employee_type
        }
        external_author
        book_publication_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_BOOK_PUBLICATION = gql`
  mutation UpdateBookPublication(
    $id: bigint!
    $_set: book_publications_set_input
  ) {
    update_book_publications_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      book_publication_category {
        id
        credit
        name
      }
      book_file
    }
  }
`;

export const DELETE_BOOK_PUBLICATION_MEMBERS = gql`
  mutation DeleteBookPublicationMembers($book_publication_id: bigint!) {
    delete_book_publication_members(
      where: { book_publication_id: { _eq: $book_publication_id } }
    ) {
      affected_rows
    }
  }
`;

export const DELETE_BOOK_PUBLICATION_BY_PK = gql`
  mutation DeleteBookPublicationByPK($id: bigint!) {
    delete_book_publications_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_BOOK_PUBLICATION_BY_PK = gql`
  mutation SoftDeleteBookPublicationByPK($id: bigint!) {
    update_book_publications_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
