import { gql } from "@apollo/client";

export const DELETE_COMMITTEE_ACTIVITY_POSITIONS = gql`
  mutation DeleteCommitteeActivityPositions($id: Int!) {
    delete_committee_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const COMMITTEE_ACTIVITY_POSITIONS = gql`
  query CommitteeActivityPositions {
    committee_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_COMMITTEE_ACTIVITY_POSITIONS = gql`
  mutation InsertCommitteeActivityPositions(
    $object: committee_activity_positions_insert_input!
  ) {
    insert_committee_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_COMMITTEE_ACTIVITY_POSITIONS = gql`
  mutation UpdateCommitteeActivityPositions(
    $pk_columns: committee_activity_positions_pk_columns_input!
    $_set: committee_activity_positions_set_input
  ) {
    update_committee_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
