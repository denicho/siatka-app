import { gql } from "@apollo/client";

export const DELETE_COMMITTEE_CATEGORIES = gql`
  mutation DeleteCommitteeCategories($id: bigint!) {
    delete_committee_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const COMMITTEE_CATEGORIES = gql`
  query CommitteeCategories {
    committee_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_COMMITTEE_CATEGORIES = gql`
  mutation InsertCommitteeCategories(
    $object: committee_categories_insert_input!
  ) {
    insert_committee_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_COMMITTEE_CATEGORIES = gql`
  mutation UpdateCommitteeCategories(
    $pk_columns: committee_categories_pk_columns_input!
    $_set: committee_categories_set_input
  ) {
    update_committee_categories_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
