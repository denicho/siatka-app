import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, Modal } from "antd";

import { Store } from "antd/lib/form/interface";
import { gql, useMutation } from "@apollo/client";
import { INSERT_COMMITTEE_ACTIVITY_POSITIONS } from "./gql/committeePositionsGql";
import {
  InsertCommitteeActivityPositions,
  InsertCommitteeActivityPositionsVariables,
} from "../globalTypes";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const ReseearchPositionsAddForm = () => {
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    form.submit();
  };
  const [create] = useMutation<
    InsertCommitteeActivityPositions,
    InsertCommitteeActivityPositionsVariables
  >(INSERT_COMMITTEE_ACTIVITY_POSITIONS, {
    onCompleted: () => {
      form.resetFields();
      setLoading(false);
      setVisible(false);
    },
    onError: () => {
      setLoading(false);
      setVisible(false);
    },
  });

  const onFinish = (values: Store) => {
    setLoading(true);

    create({
      variables: {
        object: {
          name: values.name,
        },
      },
      update: (cache, { data }) => {
        if (data?.insert_committee_activity_positions_one) {
          cache.modify({
            fields: {
              committee_activity_positions(existingActivityPositions = []) {
                const newActivityPositions = cache.writeFragment({
                  data: data.insert_committee_activity_positions_one,
                  fragment: gql`
                    fragment committee_activity_positions on committee_activity_positions {
                      id
                      name
                      created_at
                      updated_at
                    }
                  `,
                });
                return [...existingActivityPositions, newActivityPositions];
              },
            },
          });
        }
      },
    });
  };

  return (
    <>
      <Modal
        title="Tambah Posisi Kepanitiaan"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-activityPositions"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Posisi Kepanitiaan
      </Button>
    </>
  );
};

export default ReseearchPositionsAddForm;
