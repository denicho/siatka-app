import { gql } from "@apollo/client";

export const DELETE_BOOK_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation DeleteBookPublicationActivityPositions($id: Int!) {
    delete_book_publication_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const BOOK_PUBLICATION_ACTIVITY_POSITIONS = gql`
  query BookPublicationActivityPositions {
    book_publication_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_BOOK_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation InsertBookPublicationActivityPositions(
    $object: book_publication_activity_positions_insert_input!
  ) {
    insert_book_publication_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_BOOK_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation UpdateBookPublicationActivityPositions(
    $pk_columns: book_publication_activity_positions_pk_columns_input!
    $_set: book_publication_activity_positions_set_input
  ) {
    update_book_publication_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
