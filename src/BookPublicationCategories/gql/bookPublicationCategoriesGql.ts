import { gql } from "@apollo/client";

export const DELETE_BOOK_PUBLICATION_CATEGORIES = gql`
  mutation DeleteBookPublicationCategories($id: Int!) {
    delete_book_publication_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const BOOK_PUBLICATION_CATEGORIES = gql`
  query BookPublicationCategories {
    book_publication_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_BOOK_PUBLICATION_CATEGORIES = gql`
  mutation InsertBookPublicationCategories(
    $object: book_publication_categories_insert_input!
  ) {
    insert_book_publication_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_BOOK_PUBLICATION_CATEGORIES = gql`
  mutation UpdateBookPublicationCategories(
    $pk_columns: book_publication_categories_pk_columns_input!
    $_set: book_publication_categories_set_input
  ) {
    update_book_publication_categories_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
