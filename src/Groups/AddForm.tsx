import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, Modal } from "antd";
import { useMutation, gql } from "@apollo/client";
import { Store } from "antd/lib/form/interface";
import { INSERT_GROUPS } from "./gql/groupGql";
import { InsertGroups, InsertGroupsVariables } from "../globalTypes";

// import GroupResource from "../resources/group";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const GroupAddButton = () => {
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    form.submit();
  };

  const [create] = useMutation<InsertGroups, InsertGroupsVariables>(
    INSERT_GROUPS,
    {
      onCompleted: () => {
        form.resetFields();
        setLoading(false);
        setVisible(false);
      },
      onError: () => {
        setLoading(false);
        setVisible(false);
      },
    }
  );

  const onFinish = (values: Store) => {
    setLoading(true);

    create({
      variables: {
        object: {
          name: values.name,
        },
      },
      update: (cache, { data }) => {
        if (data?.insert_groups_one) {
          cache.modify({
            fields: {
              groups(existingGroups = []) {
                const newGroups = cache.writeFragment({
                  data: data.insert_groups_one,
                  fragment: gql`
                    fragment groups on groups {
                      id
                      name
                      created_at
                      updated_at
                    }
                  `,
                });
                return [...existingGroups, newGroups];
              },
            },
          });
        }
      },
    });
  };

  return (
    <>
      <Modal
        title="Tambah Golongan"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-groups"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Golongan
      </Button>
    </>
  );
};

export default GroupAddButton;
