import React from "react";
import { Table, Row, Col } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import { useQuery } from "@apollo/client";

import { GROUPS } from "./gql/groupGql";
import { Groups } from "../globalTypes";
// import GroupResource from "../resources/group";
import ManagementHeading from "../Common/ManagementHeading";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import GroupAddButton from "./AddForm";
import MutationActionsModal from "./MutationActionsModal";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/groups",
    content: "Pengelolaan Golongan",
  },
];

const GroupListPage = () => {
  const groupsRes = useQuery<Groups>(GROUPS);

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (groupsRes.loading || !groupsRes.data || !groupsRes.data.groups) {
    return <LoadingPlaceholder />;
  }

  const groups = groupsRes.data.groups;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Golongan"
        breadcrumbs={breadcrumbs}
        rightAddon={<GroupAddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={groups}
          />
        </Col>
      </Row>
    </>
  );
};

export default GroupListPage;
