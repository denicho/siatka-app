import { gql } from "@apollo/client";

export const DELETE_GROUPS = gql`
  mutation DeleteGroups($id: bigint!) {
    delete_groups_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const GROUPS = gql`
  query Groups {
    groups(order_by: {created_at: asc}) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_GROUPS = gql`
  mutation InsertGroups($object: groups_insert_input!) {
    insert_groups_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_GROUPS = gql`
  mutation UpdateGroups(
    $pk_columns: groups_pk_columns_input!
    $_set: groups_set_input
  ) {
    update_groups_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      created_at
      updated_at
    }
  }
`;
