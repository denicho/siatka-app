import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import { ServiceByPK, ServiceByPKVariables } from "../globalTypes";
import { SERVICE_BY_ID } from "./gql/serviceGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalService from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/services`,
    content: `Pengelolaan Pengabdian`,
  },
  {
    to: `/dashboard/manage/services/${id}`,
    content: `Detail Pengabdian`,
  },
];

const membersColumns = [
  {
    title: "Posisi",
    dataIndex: ["service_activity_position", "name"],
  },
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const ServiceDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const serviceRes = useQuery<ServiceByPK, ServiceByPKVariables>(
    SERVICE_BY_ID,
    {
      variables: {
        id: Number(id),
      },
    }
  );

  const service = serviceRes?.data?.services_by_pk;

  if (!serviceRes.loading && serviceRes.error) {
    // error handling
    return null;
  }
  if (!service) {
    if (serviceRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  const startDateAsDate = new Date(service.start_date);
  const endDateAtAsDate = new Date(service.end_date);

  return (
    <>
      <ManagementHeading
        title="Detail Pengabdian"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Proposal</Text>
              </Col>
              <Col span={24}>
                {service?.proposal_file ? (
                  <Typography.Link
                    href={service?.proposal_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Laporan Akhir</Text>
              </Col>
              <Col span={24}>
                {service?.final_report_file ? (
                  <Typography.Link
                    href={service?.final_report_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Sertifikat</Text>
              </Col>
              <Col span={24}>
                {service?.certificate_file ? (
                  <Typography.Link
                    href={service?.certificate_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>SK / Surat Tugas</Text>
              </Col>
              <Col span={24}>
                {service?.decree_file ? (
                  <Typography.Link
                    href={service?.decree_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalService
            record={service}
            onEditFinish={(id) => {
              serviceRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Pengabdian" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label="Judul" value={service.title} />
                    <FieldInfo
                      label="Jenis Pengabdian"
                      value={service?.service_category?.name}
                    />
                    <FieldInfo
                      label="Angka Kredit Terbagi"
                      value={service?.credit_multiplier}
                    />
                    <FieldInfo
                      label="Besar Dana"
                      value={`Rp${formatMoney(
                        service?.fund_amount || 0,
                        0,
                        ",",
                        "."
                      )}`}
                    />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Tanggal Mulai"
                      value={format(startDateAsDate, "eeee, dd MMMM yyyy", {
                        locale: localeId,
                      })}
                    />
                    <FieldInfo
                      label="Tanggal Selesai"
                      value={
                        service.end_date
                          ? format(endDateAtAsDate, "eeee, dd MMMM yyyy", {
                              locale: localeId,
                            })
                          : "-"
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Penulis" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={service.service_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default ServiceDetailPage;
