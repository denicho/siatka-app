import { gql } from "@apollo/client";

export const SERVICES = gql`
  query Services {
    services(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      title
      fund_amount
      created_at
      updated_at
      deleted_at
      start_date
      end_date
      credit_multiplier
      service_category {
        id
        name
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
      service_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        service_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const SERVICE_BY_ID = gql`
  query ServiceByPK($id: bigint!) {
    services_by_pk(id: $id) {
      id
      title
      fund_amount
      created_at
      updated_at
      deleted_at
      credit_multiplier
      start_date
      end_date
      service_category {
        id
        name
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
      service_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        service_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const INSERT_SERVICES = gql`
  mutation InsertServices($object: services_insert_input!) {
    insert_services_one(object: $object) {
      id
      title
      fund_amount
      credit_multiplier
      created_at
      updated_at
      deleted_at
      start_date
      end_date
      service_category {
        id
        name
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
    }
  }
`;

export const INSERT_SERVICE_MEMBER = gql`
  mutation InsertServiceMember($objects: [service_members_insert_input!]!) {
    insert_service_members(objects: $objects) {
      affected_rows
      returning {
        id
        credit_gain
        service_id
        employee {
          id
          name
          employee_type
        }
        external_author
        service_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_SERVICE = gql`
  mutation UpdateService($id: bigint!, $_set: services_set_input) {
    update_services_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      title
      fund_amount
      credit_multiplier
      created_at
      updated_at
      deleted_at
      start_date
      end_date
      service_category {
        id
        name
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
    }
  }
`;

export const DELETE_SERVICE_MEMBERS = gql`
  mutation DeleteServiceMembers($serviceId: bigint!) {
    delete_service_members(where: { service_id: { _eq: $serviceId } }) {
      affected_rows
    }
  }
`;

export const DELETE_SERVICE_BY_PK = gql`
  mutation DeleteServiceByPK($id: bigint!) {
    delete_services_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_SERVICE_BY_PK = gql`
  mutation SoftDeleteServiceByPK($id: bigint!) {
    update_services_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
