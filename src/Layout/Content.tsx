import * as React from "react";
import { Layout } from "antd";
import styles from "./layout.module.css";

const { Content } = Layout;

type Props = {
  children: React.ReactNode;
};

function NavHeader(props: Props) {
  return <Content className={styles.content}>{props.children}</Content>;
}

export default NavHeader;
