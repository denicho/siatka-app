const siderMap = [
  {
    title: "Beranda",
    path: "/dashboard",
    adminOnly: false,
  },
  {
    submenu: true,
    title: "Pengelolaan Universitas",
    path: "/manage",
    adminOnly: false,
    menus: [
      {
        title: "Dosen",
        path: "/dashboard/manage/lecturers",
        adminOnly: false,
      },
      {
        title: "Staf",
        path: "/dashboard/manage/employees",
        adminOnly: false,
      },
      {
        title: "Fakultas",
        path: "/dashboard/manage/faculties",
        adminOnly: true,
      },
      {
        title: "Program Studi",
        path: "/dashboard/manage/departments",
        adminOnly: true,
      },
      {
        title: "Jabatan Struktural",
        path: "/dashboard/manage/structural-positions",
        adminOnly: true,
      },
      {
        title: "Jabatan Fungsional",
        path: "/dashboard/manage/functional-positions",
        adminOnly: true,
      },
      {
        title: "Golongan",
        path: "/dashboard/manage/groups",
        adminOnly: true,
      },
    ],
  },
  {
    submenu: true,
    title: "Pengelolaan Aktivitas",
    path: "/manage-activity",
    adminOnly: false,
    menus: [
      {
        title: "Penelitian",
        path: "/dashboard/manage/researches",
        adminOnly: false,
      },
      {
        title: "Pengabdian",
        path: "/dashboard/manage/services",
        adminOnly: false,
      },
      {
        title: "Publikasi Jurnal",
        path: "/dashboard/manage/journalpublications",
        adminOnly: false,
      },
      {
        title: "Publikasi Seminar",
        path: "/dashboard/manage/seminarpublications",
        adminOnly: false,
      },
      {
        title: "Publikasi Buku",
        path: "/dashboard/manage/bookpublications",
        adminOnly: false,
      },
      {
        title: "Publikasi Paten",
        path: "/dashboard/manage/patentpublications",
        adminOnly: false,
      },
      {
        title: "Pengembangan Diri",
        path: "/dashboard/manage/selfdevelopments",
        adminOnly: false,
      },
      {
        title: "Kepanitiaan",
        path: "/dashboard/manage/committees",
        adminOnly: false,
      },
    ],
  },
  {
    submenu: true,
    title: "Manajemen Aktivitas",
    path: "/manage-activity-type",
    adminOnly: true,
    menus: [
      {
        group: true,
        title: "Manajemen Penelitian",
        menus: [
          {
            title: "Jenis Penelitian",
            path: "/dashboard/manage/research-categories",
            adminOnly: true,
          },
          {
            title: "Posisi Penelitian",
            path: "/dashboard/manage/research-positions",
            adminOnly: true,
          },
        ],
      },
      {
        group: true,
        title: "Manajemen Pengabdian",
        menus: [
          {
            title: "Jenis Pengabdian",
            path: "/dashboard/manage/service-categories",
            adminOnly: true,
          },
          {
            title: "Posisi Pengabdian",
            path: "/dashboard/manage/service-positions",
            adminOnly: true,
          },
        ],
      },
      {
        group: true,
        title: "Manajemen Publikasi Jurnal",
        menus: [
          {
            title: "Jenis Publikasi Jurnal",
            path: "/dashboard/manage/journal-publication-categories",
            adminOnly: true,
          },
          {
            title: "Posisi Publikasi Jurnal",
            path: "/dashboard/manage/journal-publication-positions",
            adminOnly: true,
          },
        ],
      },
      {
        group: true,
        title: "Manajemen Publikasi Seminar",
        menus: [
          {
            title: "Jenis Publikasi Seminar",
            path: "/dashboard/manage/seminar-publication-categories",
            adminOnly: true,
          },
          {
            title: "Posisi Publikasi Seminar",
            path: "/dashboard/manage/seminar-publication-positions",
            adminOnly: true,
          },
        ],
      },
      {
        group: true,
        title: "Manajemen Publikasi Buku",
        menus: [
          {
            title: "Jenis Publikasi Buku",
            path: "/dashboard/manage/book-publication-categories",
            adminOnly: true,
          },
          {
            title: "Posisi Publikasi Buku",
            path: "/dashboard/manage/book-publication-positions",
            adminOnly: true,
          },
        ],
      },
      {
        group: true,
        title: "Manajemen Publikasi Paten",
        menus: [
          {
            title: "Jenis Publikasi Paten",
            path: "/dashboard/manage/patent-publication-categories",
            adminOnly: true,
          },
          {
            title: "Posisi Publikasi Paten",
            path: "/dashboard/manage/patent-publication-positions",
            adminOnly: true,
          },
        ],
      },
      {
        group: true,
        title: "Manajemen Pengembangan Diri",
        menus: [
          {
            title: "Jenis Pengembangan Diri",
            path: "/dashboard/manage/self-development-categories",
            adminOnly: true,
          },
          // {
          //   title: "Posisi Pengembangan Diri",
          //   path: "/dashboard/manage/self-development-positions",
          //   adminOnly: true,
          // },
        ],
      },
      {
        group: true,
        title: "Manajemen Kepanitiaan",
        menus: [
          {
            title: "Jenis Kepanitiaan",
            path: "/dashboard/manage/committee-categories",
            adminOnly: true,
          },
          //   {
          //     title: "Posisi Kepanitiaan",
          //     path: "/dashboard/manage/committee-positions",
          //     adminOnly: true,
          //   },
        ],
      },
    ],
  },
];

export default siderMap;
