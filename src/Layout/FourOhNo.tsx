import React from "react";
import { Result, Button } from "antd";
import { Link } from "react-router-dom";

export const FourOhThree = () => {
  return (
    <Result
      status="403"
      title="403"
      subTitle="Maaf, anda tidak punya akses ke halaman ini."
      extra={
        <Link to="/dashboard">
          <Button type="primary">Kembali ke Beranda</Button>
        </Link>
      }
    />
  );
};

export const FourOhFour = () => {
  return (
    <Result
      status="404"
      title="404"
      subTitle="Maaf, halaman tidak ada :("
      extra={
        <Link to="/dashboard">
          <Button type="primary">Kembali ke Beranda</Button>
        </Link>
      }
    />
  );
};
