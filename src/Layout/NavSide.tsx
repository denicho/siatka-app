import React from "react";
import { Layout, Menu } from "antd";
import styles from "./layout.module.css";
import { Link, useLocation } from "react-router-dom";
import siderMap from "./siderMap";
import useIsAdmin from "../hooks/useIsAdmin";
import uajyImg from "../assets/uajy.png";

const { SubMenu } = Menu;
const { Sider } = Layout;

function NavSide() {
  const { pathname } = useLocation();

  // this magic handles so even if we're in detail page, the sider will be highlighted
  const splittedPathname = pathname.split("/");
  const isDynamicIdLast =
    splittedPathname.length > 0 &&
    !isNaN(Number(splittedPathname[splittedPathname.length - 1]));
  let selectedKeys;
  if (isDynamicIdLast) {
    splittedPathname.pop();
    selectedKeys = splittedPathname.join("/");
  } else {
    selectedKeys = pathname;
  }

  const isAdmin = useIsAdmin();

  return (
    <Sider
      className={styles.sider}
      breakpoint="md"
      width="18rem"
      collapsedWidth="0"
      theme="light"
    >
      <div className={styles.logo}>
        <img src={uajyImg} className={styles.logoImg} />
        Sistem Informasi Aktivitas Tenaga Kependidikan
      </div>

      <Menu
        defaultSelectedKeys={[selectedKeys]}
        mode="inline"
        className={styles.menu}
        selectedKeys={[selectedKeys]}
        theme="light"
      >
        {siderMap.map((menu, i) => {
          if (menu.adminOnly && !isAdmin) {
            return null;
          }

          if (menu.submenu) {
            return (
              <SubMenu key={menu.path} title={menu.title}>
                {/* @ts-ignore */}
                {menu.menus.map((sub) => {
                  if (sub?.adminOnly && !isAdmin) {
                    return null;
                  }

                  if (sub.group) {
                    return (
                      <Menu.SubMenu key={sub.title} title={sub.title}>
                        {/* @ts-ignore */}
                        {sub.menus.map((groupChildren) => {
                          if (groupChildren?.adminOnly && !isAdmin) {
                            return null;
                          }

                          return (
                            <Menu.Item key={groupChildren.path}>
                              <Link to={groupChildren.path}>
                                {groupChildren.title}
                              </Link>
                            </Menu.Item>
                          );
                        })}
                      </Menu.SubMenu>
                    );
                  }

                  return (
                    <Menu.Item key={sub.path}>
                      {/* @ts-ignore */}
                      <Link to={sub.path}>{sub.title}</Link>
                    </Menu.Item>
                  );
                })}
              </SubMenu>
            );
          }

          return (
            <Menu.Item key={menu.path}>
              <Link to={menu.path}>{menu.title}</Link>
            </Menu.Item>
          );
        })}
      </Menu>
    </Sider>
  );
}

export default NavSide;
