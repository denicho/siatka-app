import React from "react";
import {
  Layout,
  Menu,
  Row,
  Col,
  Modal,
  Button,
  Form,
  Input,
  message,
} from "antd";
import { UserOutlined, LogoutOutlined, LockOutlined } from "@ant-design/icons";
import { get } from "lodash-es";
import styles from "./layout.module.css";
import AuthContext from "../Contexts/AuthContext";
import { employeeChangePassword } from "../Employees/gql/employeeSignup";

const { Header } = Layout;
const { SubMenu } = Menu;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

function NavHeader() {
  const { data } = React.useContext(AuthContext);
  const [current] = React.useState<string>("");
  const auth = React.useContext(AuthContext);
  const [form] = Form.useForm();
  const { whoami } = data;
  const username = get(whoami, "data.username", "");
  const [
    changePassModalVisible,
    setChangePassModalVisible,
  ] = React.useState<boolean>(false);
  const [changePassLoading, setChangePassLoading] = React.useState<boolean>(
    false
  );

  const handleChangePassCancel = () => {
    setChangePassModalVisible(false);
  };

  const handleFinishChangePasswordForm = (values: any) => {
    if (values.password && values.confirmPassword) {
      setChangePassLoading(true);
      employeeChangePassword({
        id: auth?.data?.whoami?.data.id,
        password: values.password,
        confirmPassword: values.confirmPassword,
      })
        .then(() => {
          message.success("Berhasil merubah password");
          setChangePassLoading(false);
          form.resetFields();
          setChangePassModalVisible(false);
        })
        .catch((err) => {
          message.error("Gagal mengubah password");
          console.log("Failed changing password, ", err);
          setChangePassLoading(false);
          form.resetFields();
          setChangePassModalVisible(false);
        });
    }
  };

  const handleChangePassOk = () => {
    form.submit();
  };

  const handleLogout = () => {
    auth.logout();
  };

  const handleClick = (data: any) => {
    if (data.key === "logout") {
      handleLogout();
    }
  };

  return (
    <>
      <Modal
        title="Ubah Password"
        visible={changePassModalVisible}
        onCancel={handleChangePassCancel}
        footer={[
          <Button key="back" onClick={handleChangePassCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={changePassLoading}
            onClick={handleChangePassOk}
          >
            Ubah
          </Button>,
        ]}
      >
        <Form
          {...layout}
          form={form}
          name="register"
          onFinish={handleFinishChangePasswordForm}
        >
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: "Mohon masukkan password baru!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirmPassword"
            label="Konfirmasi Password"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Mohon konfirmasi password baru!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    "Dua password yang dimasukkan berbeda!"
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </Modal>
      <Header className={styles.header}>
        {/* menus */}
        <Row style={{ height: "64px", overflow: "hidden" }} justify="end">
          <Col>
            <Menu
              onClick={handleClick}
              selectedKeys={[current]}
              mode="horizontal"
            >
              <SubMenu key="SubMenu" icon={<UserOutlined />} title={username}>
                <Menu.Item
                  key="Ubah Password"
                  onClick={() => {
                    setChangePassModalVisible(true);
                  }}
                >
                  <LockOutlined />
                  Ubah Password
                </Menu.Item>
                <Menu.Item key="logout">
                  <LogoutOutlined />
                  Keluar
                </Menu.Item>
              </SubMenu>
            </Menu>
          </Col>
        </Row>
      </Header>
    </>
  );
}

export default NavHeader;
