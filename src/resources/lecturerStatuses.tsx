import cookie from "cookie";

export const lecturerStatusFetcher = () => {
  let token = cookie.parse(document.cookie)["access-token"];

  return fetch("https://api.siatka.xyz/lecturer-statuses", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then((raw) => raw.json());
};
