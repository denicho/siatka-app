import { Resource } from "rest-hooks";
import cookie from "cookie";

export default class UserResource extends Resource {
  readonly id: number | undefined;
  readonly username: string = "";

  pk() {
    return this.id?.toString();
  }

  static loginShape<T extends typeof Resource>(this: T) {
    return {
      ...this.detailShape(),
      getFetchKey: () => {
        return "/auth";
      },
      fetch: (params: {}, body: { username: string; password: string }) => {
        return this.fetch("post", `https://api.siatka.xyz/auth`, body);
      },
      schema: { data: this.asSchema() },
    };
  }

  static currentShape<T extends typeof Resource>(this: T) {
    return {
      ...this.detailShape(),
      getFetchKey: () => {
        return "/auth/whoami";
      },
      fetch: (params: {}, body?: Readonly<object | string>) => {
        return this.fetch("get", `https://api.siatka.xyz/auth/whoami`);
      },
      schema: { data: this.asSchema() },
    };
  }

  static fetchOptionsPlugin = (options: RequestInit) => {
    let token = cookie.parse(document.cookie)["access-token"];

    return {
      ...options,

      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
  };

  static urlRoot = "https://api.siatka.xyz/auth";
}
