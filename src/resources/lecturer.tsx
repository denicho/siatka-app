import BaseResource from "./BaseResource";
import DepartmentResource from "./department";
import FunctionalPositionsResource from "./functionalPositions";
import StructuralPositionsResource from "./structuralPositions";
import GroupResource from "./group";
import UserResource from "./user";

export default class LecturerResource extends BaseResource {
  readonly id: number | undefined = undefined;
  readonly npp: string = "";
  readonly nidn: string = "";
  readonly name: string = "";
  readonly picture_file: string = "";
  readonly status: string = "";
  readonly initial: string = "";
  readonly place_of_birth: string = "";
  readonly dateOfBirth: string = "";
  readonly address: string = "";
  readonly phone: string = "";
  readonly email: string = "";
  readonly lecturerCertificationNumber: string = "";
  readonly lecturerCertificationUrl: string = "";
  readonly department: string = "";
  readonly functionalPosition: number = 0;
  readonly structuralPosition: number = 0;
  readonly group: number = 0;
  readonly user: {
    id: number;
    username: string;
  } = {
    id: 0,
    username: "",
  };

  static schema = {
    department: DepartmentResource.asSchema(),
    functionalPosition: FunctionalPositionsResource.asSchema(),
    structuralPosition: StructuralPositionsResource.asSchema(),
    group: GroupResource.asSchema(),
  };

  pk() {
    return this.id?.toString();
  }

  static urlRoot = "https://api.siatka.xyz/lecturers";
}
