import { Resource } from "rest-hooks";
import cookie from "cookie";

export default class AuthenticatedResource extends Resource {
  readonly id: number | string | undefined;

  pk() {
    return this.id?.toString();
  }

  static fetchOptionsPlugin = (options: RequestInit) => {
    let token = cookie.parse(document.cookie)["access-token"];

    return {
      ...options,

      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
  };

  static detailShape<T extends typeof Resource>(this: T) {
    return {
      ...super.detailShape(),
      schema: { data: this.asSchema() },
    };
  }

  static listShape<T extends typeof Resource>(this: T) {
    return {
      ...super.listShape(),
      schema: { data: [this.asSchema()] },
    };
  }

  static createShape<T extends typeof Resource>(this: T) {
    return {
      ...super.createShape(),
      schema: { data: this.asSchema() },
    };
  }

  static updateShape<T extends typeof Resource>(this: T) {
    return {
      ...super.updateShape(),
      schema: { data: this.asSchema() },
    };
  }

  static partialUpdateShape<T extends typeof Resource>(this: T) {
    return {
      ...super.partialUpdateShape(),
      schema: { data: this.asSchema() },
    };
  }
}
