import BaseResource from "./BaseResource";

export default class JournalPublicationCategoriesResource extends BaseResource {
  readonly id: number | undefined;
  readonly name: string = "";
  readonly created_at: string = "";
  readonly updated_at: string = "";

  static urlRoot = "https://api.siatka.xyz/journal-publication-categories";
}
