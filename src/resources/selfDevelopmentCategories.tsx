import BaseResource from "./BaseResource";

export default class SelfDevelopmentCategoriesResource extends BaseResource {
  readonly id: number | undefined;
  readonly name: string = "";
  readonly created_at: string = "";
  readonly updated_at: string = "";

  static urlRoot = "https://api.siatka.xyz/self-development-categories";
}
