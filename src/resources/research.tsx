import BaseResource from "./BaseResource";

export default class ResearchResource extends BaseResource {
  readonly id: number | undefined;

  static urlRoot = "https://api.siatka.xyz/researches";
}
