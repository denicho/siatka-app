import React from "react";
import { flatten, debounce, get } from "lodash-es";
import moment from "moment";
import {
  PlusOutlined,
  RightOutlined,
  LeftOutlined,
  MinusCircleOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";
import {
  Button,
  Form,
  Input,
  Divider,
  Spin,
  Modal,
  Row,
  Col,
  DatePicker,
  Upload,
  Select,
  Steps,
  Typography,
  InputNumber,
} from "antd";
import {
  ActivityPositions,
  SearchEmployees,
  SearchEmployeesVariables,
  InsertCommittees,
  InsertCommitteesVariables,
  InsertCommitteeMember,
  InsertCommitteeMemberVariables,
  CommitteeLevel,
  CommitteeCategories,
  CommitteeActivityPositions,
} from "../globalTypes";
import { gql, useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { createCustomRequest } from "../Common/uploadFile";
import { SEARCH_EMPLOYEES } from "../Employees/gql/employeesGql";
import {
  INSERT_COMMITTEES,
  INSERT_COMMITTEE_MEMBER,
  COMMITTEE_LEVEL,
} from "./gql/committeeGql";
import normalizeMembers from "../utils/normalizeMembers";
import { COMMITTEE_CATEGORIES } from "../CommitteeCategories/gql/committeeCategoriesGql";
import { COMMITTEE_ACTIVITY_POSITIONS } from "../CommitteeCategories/gql/committeePositionsGql";
import AuthContext from "../Contexts/AuthContext";

const layout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 24 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

const { Step } = Steps;
const { Option } = Select;

export function disabledDate(current: any) {
  // Can not select days after today and today
  return current && current > moment().endOf("day");
}

export const normFile = (e: any) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

const CommitteeAddButton = () => {
  const [searchEmployees, searchEmployeesRes] = useLazyQuery<
    SearchEmployees,
    SearchEmployeesVariables
  >(SEARCH_EMPLOYEES, {
    onCompleted: (data) => {},
  });
  const categoriesRes = useQuery<CommitteeCategories>(COMMITTEE_CATEGORIES);
  const activityPosRes = useQuery<CommitteeActivityPositions>(
    COMMITTEE_ACTIVITY_POSITIONS
  );
  const selfDevLevelsRes = useQuery<CommitteeLevel>(COMMITTEE_LEVEL);

  const [visible, setVisible] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState(false);

  const [form] = Form.useForm();

  const [insertCommittees] = useMutation<
    InsertCommittees,
    InsertCommitteesVariables
  >(INSERT_COMMITTEES);
  const [insertCommitteeMember] = useMutation<
    InsertCommitteeMember,
    InsertCommitteeMemberVariables
  >(INSERT_COMMITTEE_MEMBER, {
    onCompleted: () => {
      form.resetFields();
      setCurrentStep(0);
      setDecreeFileList([]);
      setBookFileList([]);
      setLoading(false);
      setVisible(false);
    },
    onError: () => {
      setLoading(false);
      setVisible(false);
    },
  });

  // const activityPoses =
  //   activityPosRes?.data?.committee_activity_positions || [];
  const selfDevLevels = selfDevLevelsRes?.data?.committee_level || [];
  const categories = categoriesRes?.data?.committee_categories || [];
  const searchedEmployees = searchEmployeesRes?.data?.employees || [];
  const [selectedEmployeeIDs, setSelectedEmployeeIDs] = React.useState<
    number[]
  >([]);
  const [
    selectedCategoryCredit,
    setSelectedCategoryCredit,
  ] = React.useState<number>(0);

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    form.submit();
  };
  const onFinish = (values: Store) => {
    // setLoading(true);

    const vals = form.getFieldsValue(flatten(validateList));

    console.log(vals);

    const insertCommitteesPayload = {
      ...vals,
      category_id: Number(vals.category_id),
      start_date: vals.start_date.format("YYYY-MM-DD"),
      end_date: vals.end_date ? vals.end_date.format("YYYY-MM-DD") : undefined,
      certificate_file: get(
        vals,
        `certificate_file[${
          vals.certificate_file ? vals.certificate_file.length - 1 : 0
        }].url`,
        null
      ),
      decree_file: get(
        vals,
        `decree_file[${
          vals.decree_file ? vals.decree_file.length - 1 : 0
        }].url`,
        null
      ),
    };

    delete insertCommitteesPayload.member;

    insertCommittees({
      variables: {
        object: insertCommitteesPayload,
      },
      update: (cache, { data }) => {
        if (data?.insert_committees_one) {
          cache.modify({
            fields: {
              committees(existingCommittees = []) {
                const newCommittees = cache.writeFragment({
                  data: data.insert_committees_one,
                  fragment: gql`
                    fragment committees on committees {
                      id
                      name
                      created_at
                      updated_at
                      deleted_at
                      venue
                      start_date
                      end_date
                      decree_file
                      level
                      certificate_file
                    }
                  `,
                });
                return [...existingCommittees, newCommittees];
              },
            },
          });
        }
      },
    }).then((result) => {
      const committeeId = result?.data?.insert_committees_one?.id;

      if (committeeId) {
        // const insertCommitteeMembersPayload = normalizeMembers(
        //   committeeId,
        //   "committee_id",
        //   vals.members,
        //   [],
        //   100
        // );
        const insertCommitteeMembersPayload =
          vals.member && vals.member.length > 0
            ? [
                {
                  employee_id: vals.member[0].value,
                  committee_id: committeeId,
                  credit_gain: Number(selectedCategoryCredit),
                },
              ]
            : [];

        insertCommitteeMember({
          variables: {
            objects: insertCommitteeMembersPayload,
          },
        });
      }
    });
  };

  // Uploads
  const [certificateFileList, setBookFileList] = React.useState([]);
  const handleBookUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setBookFileList(fileList);
  };
  const [decreeFileList, setDecreeFileList] = React.useState([]);
  const handleDecreeUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setDecreeFileList(fileList);
  };

  const [currentStep, setCurrentStep] = React.useState(0);
  const validateList = [
    [
      "name",
      "member",
      "category_id",
      "start_date",
      "end_date",
      "venue",
      "level",
    ],
    ["certificate_file", "decree_file"],
    // ["member_activity_position_id", "member"],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 1 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
  const steps = [
    {
      title: "Data  Kepanitiaan",
      content: (
        <>
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"category_id"}
            label="Kategori"
            extra={
              <Typography.Text>
                Angka kredit didapat: <strong>{selectedCategoryCredit}</strong>
              </Typography.Text>
            }
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Kategori Kepanitiaan"
              loading={categories.length <= 0}
            >
              {categories.map((ctg) => (
                <Option key={ctg.id} value={ctg?.id?.toString() || ""}>
                  {ctg.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"member"}
            label="Pelaku"
            rules={[{ required: true, message: "Pelaku wajib diisi" }]}
            style={{
              width: "100%",
              marginRight: 8,
            }}
          >
            <Select
              maxTagCount={1}
              labelInValue
              mode="multiple"
              value={form.getFieldValue(["member"])}
              placeholder="Cari nama"
              notFoundContent={
                searchEmployeesRes.loading ? <Spin size="small" /> : null
              }
              filterOption={false}
              onChange={(value) => {
                const newValue = value[value.length - 1];

                form.setFieldsValue({
                  member: newValue ? [newValue] : [],
                });
              }}
              onSearch={debounce((value) => {
                searchEmployees({
                  variables: {
                    term: `%${value}%`,
                  },
                });
              }, 400)}
            >
              {searchedEmployees.map((d) => (
                <Option key={`${d.value}a`} value={d.value}>
                  {d.text}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"start_date"}
            label="Tanggal Mulai"
            rules={[{ required: true }]}
          >
            <DatePicker
              format="DD/MM/YYYY"
              showToday={false}
              onChange={(startDateValue) => {
                const endDate: moment.Moment = form.getFieldValue("end_date");
                if (startDateValue && endDate) {
                  if (endDate.isBefore(startDateValue)) {
                    form.setFieldsValue({ end_date: startDateValue });
                  }
                }
              }}
            />
          </Form.Item>
          <Form.Item name={"end_date"} label="Tanggal Selesai">
            <DatePicker
              format="DD/MM/YYYY"
              disabledDate={(current) => {
                const startedAt = form.getFieldValue("start_date");
                // Can not select days after today and today
                if (startedAt) {
                  return current.isBefore(startedAt);
                } else {
                  return false;
                }
              }}
              showToday={false}
              onChange={(endDateValue) => {
                const startDate: moment.Moment = form.getFieldValue(
                  "start_date"
                );
                if (endDateValue && startDate) {
                  if (startDate.isAfter(endDateValue)) {
                    form.setFieldsValue({ start_date: endDateValue });
                  }
                }
              }}
            />
          </Form.Item>
          <Form.Item name={"venue"} label="Tempat" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"level"}
            label="Tingkat"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Tingkat"
              loading={selfDevLevels.length <= 0}
            >
              {selfDevLevels.map((ctg) => (
                <Option key={ctg.level} value={ctg?.level}>
                  {ctg.level}
                </Option>
              ))}
            </Select>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Berkas",
      content: (
        <>
          <Form.Item
            name={"certificate_file"}
            label="Sertifikat"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
          >
            <Upload
              customRequest={createCustomRequest("committee_certificate_file")}
              onChange={handleBookUploadChange}
              fileList={certificateFileList}
              onRemove={() => setBookFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {certificateFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"decree_file"}
            label="SK / Surat Tugas"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Upload
              customRequest={createCustomRequest("committee_decree_file")}
              onChange={handleDecreeUploadChange}
              fileList={decreeFileList}
              onRemove={() => setDecreeFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {decreeFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    // {
    //   title: "Data Anggota",
    //   content: (
    //     <>
    //       <div
    //         style={{
    //           display: "flex",
    //           alignItems: "baseline",
    //           marginBottom: 8,
    //         }}
    //       >
    //         <Form.Item
    //           name={"member_activity_position_id"}
    //           rules={[{ required: true, message: "Posisi wajib diisi" }]}
    //           style={{ marginRight: 8, marginBottom: 8, width: 200 }}
    //         >
    //           <Select
    //             placeholder="Posisi"
    //             onChange={(value) => {
    //               form.setFieldsValue({
    //                 member_activity_position_id: value,
    //               });
    //             }}
    //             loading={activityPoses.length <= 0}
    //             allowClear
    //           >
    //             {activityPoses.map((activity) => (
    //               <Option
    //                 key={activity.id}
    //                 value={activity?.id?.toString() || ""}
    //               >
    //                 {activity.name}
    //               </Option>
    //             ))}
    //           </Select>
    //         </Form.Item>
    //         <Form.Item
    //           name={"member"}
    //           rules={[{ required: true, message: "Nama wajib diisi" }]}
    //           style={{
    //             width: "100%",
    //             marginRight: 8,
    //             marginBottom: 8,
    //           }}
    //         >
    //           <Select
    //             maxTagCount={1}
    //             labelInValue
    //             mode="multiple"
    //             value={form.getFieldValue(["member"])}
    //             placeholder="Cari nama"
    //             notFoundContent={
    //               searchEmployeesRes.loading ? <Spin size="small" /> : null
    //             }
    //             filterOption={false}
    //             onChange={(value) => {
    //               const newValue = value[value.length - 1];

    //               form.setFieldsValue({
    //                 member: newValue ? [newValue] : [],
    //               });
    //             }}
    //             onSearch={debounce((value) => {
    //               searchEmployees({
    //                 variables: {
    //                   term: `%${value}%`,
    //                 },
    //               });
    //             }, 400)}
    //           >
    //             {searchedEmployees.map((d) => (
    //               <Option key={`${d.value}a`} value={d.value}>
    //                 {d.text}
    //               </Option>
    //             ))}
    //           </Select>
    //         </Form.Item>
    //       </div>

    //       {stepNavigator}
    //     </>
    //   ),
    // },
  ];

  return (
    <>
      <Modal
        title="Tambah  Kepanitiaan"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            disabled={currentStep !== steps.length - 1}
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={onFinish}
          validateMessages={validateMessages}
          onValuesChange={(changed, all) => {
            if (changed.category_id && all.category_id) {
              const selectedCtg = categories.find(
                (ctg) => ctg.id === Number(changed.category_id)
              );
              setSelectedCategoryCredit(
                selectedCtg?.credit ? selectedCtg?.credit : 0
              );
            }

            // // This will trigger the filters so that user cant pick the same employee twice
            // if (changed.internal_members && all.internal_members) {
            //   const selectedEmployeeIds = all.internal_members
            //     .filter(Boolean)
            //     .map((member: any) => {
            //       const id =
            //         (member.internal_member &&
            //           member.internal_member.length > 0 &&
            //           member.internal_member[0]?.value) ||
            //         0;

            //       return id;
            //     })
            //     .filter(Boolean);

            //   setSelectedEmployeeIDs(selectedEmployeeIds);
            // }
          }}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>
      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Kepanitiaan
      </Button>
    </>
  );
};

export default CommitteeAddButton;
