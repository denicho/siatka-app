import React from "react";
import { get } from "lodash-es";
import { Table, Row, Col, Space, Button, Input } from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  InfoCircleTwoTone,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

import ManagementHeading from "../Common/ManagementHeading";
// import StaffAddButton from "./AddFormStaff";
// import LecturerAddButton from "./AddFormLecturer";
import stringTableSorter from "../utils/stringTableSorter";
import { useQuery } from "@apollo/client";
import { COMMITTEES } from "./gql/committeeGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import { Committees, Committees_committees } from "../globalTypes";
import CommitteeAddButton from "./AddForm";
import numberTableSorter from "../utils/numberTableSorter";

type Props = {
  id: string;
};

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/committees`,
    content: `Pengelolaan  Kepanitiaan`,
  },
];

const EmployeeListPage = () => {
  const history = useHistory();
  const committeesRes = useQuery<Committees>(COMMITTEES);
  const [searchText, setSearchText] = React.useState("");
  const [searchedCol, setSearchedCol] = React.useState("");
  const searchInput = React.useRef<Input>(null);

  if (
    committeesRes.loading ||
    !committeesRes.data ||
    !committeesRes.data.committees
  ) {
    return <LoadingPlaceholder />;
  }

  const committees = committeesRes.data.committees;

  // Filtering

  const handleSearch = (
    selectedKeys: string,
    confirm: () => void,
    dataIndex: string
  ) => {
    console.log("handle search", {
      selectedKeys,
      dataIndex,
    });
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedCol(dataIndex);
  };
  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (
    dataIndex: string | string[],
    label: string
  ) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            if (searchInput !== null) {
              // @ts-ignore
              searchInput.current = node;
            }
          }}
          placeholder={`Cari ${label}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(
              selectedKeys,
              confirm,
              Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
            )
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(
                selectedKeys,
                confirm,
                Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
              )
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Cari
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (
      value: string | number | boolean,
      record: Committees_committees
    ) => {
      const searched = get(
        record,
        Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
      );

      // @ts-ignore
      return searched
        ? // @ts-ignore
          searched
            .toString()
            .toLowerCase()
            // @ts-ignore
            .includes(value.toLowerCase())
        : "";
    },
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => {
          if (searchInput.current) {
            searchInput.current.select();
          }
        }, 100);
      }
    },
    render: (text: string) => {
      const shouldHighlight = Array.isArray(dataIndex)
        ? searchedCol === dataIndex.join(".")
        : searchedCol === dataIndex;

      return shouldHighlight ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      );
    },
  });

  const columns = [
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
      sorter: (a: Committees_committees, b: Committees_committees) => {
        return stringTableSorter(a.name || "", b.name || "");
      },
      ...getColumnSearchProps("name", "Nama"),
    },
    {
      title: "Tanggal Mulai",
      dataIndex: "start_date",
      key: "start_date",
      sorter: (a: Committees_committees, b: Committees_committees) => {
        return stringTableSorter(a.name || "", b.name || "");
      },
      ...getColumnSearchProps("start_date", "Tanggal Mulai"),
    },
    {
      title: "Tempat",
      dataIndex: "venue",
      key: "venue",
      sorter: (a: Committees_committees, b: Committees_committees) => {
        return stringTableSorter(a.venue || "", b.venue || "");
      },
      ...getColumnSearchProps("venue", "Tahun Terbit"),
    },
    {
      title: "Tingkat",
      dataIndex: "level",
      key: "level",
      sorter: (a: Committees_committees, b: Committees_committees) => {
        return stringTableSorter(a.level || "", b.level || "");
      },
      ...getColumnSearchProps("level", "Tahun Terbit"),
    },
    {
      title: "Detail",
      key: "detail",
      dataIndex: "detail",
      width: 140,
      render: (text: any, record: any, index: any) => {
        return <InfoCircleTwoTone onClick={() => {}} />;
      },
    },
  ];

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Kepanitiaan"
        breadcrumbs={breadcrumbs}
        rightAddon={<CommitteeAddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  history.push(`/dashboard/manage/committees/${record.id}`);
                },
              };
            }}
            size="middle"
            rowKey="id"
            columns={columns}
            dataSource={committees}
          />
        </Col>
      </Row>
    </>
  );
};

export default EmployeeListPage;
