import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import { CommitteeByPK, CommitteeByPKVariables } from "../globalTypes";
import { COMMITTEE_BY_ID } from "./gql/committeeGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalCommittee from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/selfdevelopments`,
    content: `Pengelolaan  Kepanitiaan`,
  },
  {
    to: `/dashboard/manage/selfdevelopments/${id}`,
    content: `Detail  Kepanitiaan`,
  },
];

const membersColumns = [
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const CommitteeDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const committeeRes = useQuery<CommitteeByPK, CommitteeByPKVariables>(
    COMMITTEE_BY_ID,
    {
      variables: {
        id: Number(id),
      },
    }
  );

  const committee = committeeRes?.data?.committees_by_pk;

  if (!committeeRes.loading && committeeRes.error) {
    // error handling
    return null;
  }
  if (!committee) {
    if (committeeRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  const startAsDate = new Date(committee.start_date);
  const endAsDate = new Date(committee.end_date);

  return (
    <>
      <ManagementHeading
        title="Detail Kepanitiaan"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>File Sertifikat</Text>
              </Col>
              <Col span={24}>
                {committee?.certificate_file ? (
                  <Typography.Link
                    href={committee?.certificate_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>File SK / Surat Tugas</Text>
              </Col>
              <Col span={24}>
                {committee?.decree_file ? (
                  <Typography.Link
                    href={committee?.decree_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalCommittee
            record={committee}
            onEditFinish={(id) => {
              committeeRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Kepanitiaan" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label="Nama" value={committee.name} />
                    <FieldInfo
                      label="Tempat Kegiatan"
                      value={committee.venue}
                    />
                    <FieldInfo label="Tingkat" value={committee.level} />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Tanggal Mulai"
                      value={
                        committee.start_date
                          ? format(startAsDate, "eeee, dd MMMM yyyy", {
                              locale: localeId,
                            })
                          : "-"
                      }
                    />
                    <FieldInfo
                      label="Tanggal Selesai"
                      value={
                        committee.end_date
                          ? format(endAsDate, "eeee, dd MMMM yyyy", {
                              locale: localeId,
                            })
                          : "-"
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Pelaku" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={committee.committee_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default CommitteeDetailPage;
