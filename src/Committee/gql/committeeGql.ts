import { gql } from "@apollo/client";

export const COMMITTEE_LEVEL = gql`
  query CommitteeLevel {
    committee_level {
      level
    }
  }
`;

export const COMMITTEES = gql`
  query Committees {
    committees(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      created_at
      updated_at
      deleted_at
      committee_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
      committee_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const COMMITTEE_BY_ID = gql`
  query CommitteeByPK($id: bigint!) {
    committees_by_pk(id: $id) {
      id
      created_at
      updated_at
      deleted_at
      committee_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
      committee_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const INSERT_COMMITTEES = gql`
  mutation InsertCommittees($object: committees_insert_input!) {
    insert_committees_one(object: $object) {
      id
      created_at
      updated_at
      deleted_at
      committee_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
      committee_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const INSERT_COMMITTEE_MEMBER = gql`
  mutation InsertCommitteeMember($objects: [committee_members_insert_input!]!) {
    insert_committee_members(objects: $objects) {
      affected_rows
      returning {
        id
        committee_id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
    }
  }
`;

export const UPDATE_COMMITTEE = gql`
  mutation UpdateCommittee($id: bigint!, $_set: committees_set_input) {
    update_committees_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      created_at
      updated_at
      deleted_at
      committee_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const DELETE_COMMITTEE_MEMBERS = gql`
  mutation DeleteCommitteeMembers($committee_id: bigint!) {
    delete_committee_members(where: { committee_id: { _eq: $committee_id } }) {
      affected_rows
    }
  }
`;

export const DELETE_COMMITTEE_BY_PK = gql`
  mutation DeleteCommitteeByPK($id: bigint!) {
    delete_committees_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_COMMITTEE_BY_PK = gql`
  mutation SoftDeleteCommitteeByPK($id: bigint!) {
    update_committees_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
