import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import {
  PatentPublicationByPK,
  PatentPublicationByPKVariables,
} from "../globalTypes";
import { PATENT_PUBLICATION_BY_ID } from "./gql/patentPublicationGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalPatentPublication from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/patentpublications`,
    content: `Pengelolaan Publikasi Paten`,
  },
  {
    to: `/dashboard/manage/patentpublications/${id}`,
    content: `Detail Publikasi Paten`,
  },
];

const membersColumns = [
  {
    title: "Posisi",
    dataIndex: ["patent_publication_activity_position", "name"],
  },
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const PatentPublicationDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const patentPublicationRes = useQuery<
    PatentPublicationByPK,
    PatentPublicationByPKVariables
  >(PATENT_PUBLICATION_BY_ID, {
    variables: {
      id: Number(id),
    },
  });

  const patentPublication =
    patentPublicationRes?.data?.patent_publications_by_pk;

  if (!patentPublicationRes.loading && patentPublicationRes.error) {
    // error handling
    return null;
  }
  if (!patentPublication) {
    if (patentPublicationRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  return (
    <>
      <ManagementHeading
        title="Detail Publikasi Paten"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>File Sertifikat Paten</Text>
              </Col>
              <Col span={24}>
                {patentPublication?.certificate_file ? (
                  <Typography.Link
                    href={patentPublication?.certificate_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalPatentPublication
            record={patentPublication}
            onEditFinish={(id) => {
              patentPublicationRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Paten" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label="Judul" value={patentPublication.title} />
                    <FieldInfo
                      label="Kategori Paten"
                      value={
                        patentPublication.patent_publication_category?.name
                      }
                    />
                    <FieldInfo
                      label="Angka Kredit Terbagi"
                      value={patentPublication?.credit_multiplier}
                    />
                    <FieldInfo
                      label="No. Sertifikat"
                      value={patentPublication.certificate_no}
                    />
                    <FieldInfo
                      label="Tanggal Resmi"
                      value={patentPublication.first_announcement_date}
                    />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical"></Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Penulis" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={patentPublication.patent_publication_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default PatentPublicationDetailPage;
