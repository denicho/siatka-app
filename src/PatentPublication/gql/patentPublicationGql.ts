import { gql } from "@apollo/client";

export const PATENT_PUBLICATIONS = gql`
  query PatentPublications {
    patent_publications(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      patent_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        patent_publication_activity_position {
          id
          name
        }
      }
      patent_publication_category {
        id
        credit
        name
      }
      certificate_no
      first_announcement_date
      certificate_file
    }
  }
`;

export const PATENT_PUBLICATION_BY_ID = gql`
  query PatentPublicationByPK($id: bigint!) {
    patent_publications_by_pk(id: $id) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      patent_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        patent_publication_activity_position {
          id
          name
        }
      }
      patent_publication_category {
        id
        credit
        name
      }
      certificate_no
      first_announcement_date
      certificate_file
    }
  }
`;

export const INSERT_PATENT_PUBLICATIONS = gql`
  mutation InsertPatentPublications(
    $object: patent_publications_insert_input!
  ) {
    insert_patent_publications_one(object: $object) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      patent_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        patent_publication_activity_position {
          id
          name
        }
      }
      patent_publication_category {
        id
        credit
        name
      }
      certificate_no
      first_announcement_date
      certificate_file
    }
  }
`;

export const INSERT_PATENT_PUBLICATION_MEMBER = gql`
  mutation InsertPatentPublicationMember(
    $objects: [patent_publication_members_insert_input!]!
  ) {
    insert_patent_publication_members(objects: $objects) {
      affected_rows
      returning {
        id
        patent_publication_id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        patent_publication_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_PATENT_PUBLICATION = gql`
  mutation UpdatePatentPublication(
    $id: bigint!
    $_set: patent_publications_set_input
  ) {
    update_patent_publications_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      title
      credit_multiplier
      created_at
      updated_at
      deleted_at
    }
  }
`;

export const DELETE_PATENT_PUBLICATION_MEMBERS = gql`
  mutation DeletePatentPublicationMembers($patent_publication_id: bigint!) {
    delete_patent_publication_members(
      where: { patent_publication_id: { _eq: $patent_publication_id } }
    ) {
      affected_rows
    }
  }
`;

export const DELETE_PATENT_PUBLICATION_BY_PK = gql`
  mutation DeletePatentPublicationByPK($id: bigint!) {
    delete_patent_publications_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_PATENT_PUBLICATION_BY_PK = gql`
  mutation SoftDeletePatentPublicationByPK($id: bigint!) {
    update_patent_publications_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
