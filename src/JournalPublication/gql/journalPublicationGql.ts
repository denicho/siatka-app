import { gql } from "@apollo/client";

export const JOURNAL_PUBLICATIONS = gql`
  query JournalPublications {
    journal_publications(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      journal_publication_category {
        id
        name
      }
      journal_cover_file
      journal_toc_file
      certificate_file
      paper_file
      decree_file
      journal_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        journal_publication_activity_position {
          id
          name
        }
      }
      isbn
      issn
      journal_no
      journal_name
      journal_volume
      journal_year
      journal_page_no
    }
  }
`;

export const JOURNAL_PUBLICATION_BY_ID = gql`
  query JournalPublicationByPK($id: bigint!) {
    journal_publications_by_pk(id: $id) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      journal_publication_category {
        id
        name
      }
      certificate_file
      journal_cover_file
      journal_toc_file
      paper_file
      decree_file
      journal_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        journal_publication_activity_position {
          id
          name
        }
      }
      isbn
      issn
      journal_no
      journal_name
      journal_volume
      journal_year
      journal_page_no
    }
  }
`;

export const INSERT_JOURNAL_PUBLICATIONS = gql`
  mutation InsertJournalPublications(
    $object: journal_publications_insert_input!
  ) {
    insert_journal_publications_one(object: $object) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      journal_publication_category {
        id
        name
      }
      certificate_file
      journal_cover_file
      journal_toc_file
      paper_file
      decree_file
    }
  }
`;

export const INSERT_JOURNAL_PUBLICATION_MEMBER = gql`
  mutation InsertJournalPublicationMember(
    $objects: [journal_publication_members_insert_input!]!
  ) {
    insert_journal_publication_members(objects: $objects) {
      affected_rows
      returning {
        id
        credit_gain
        journal_publication_id
        employee {
          id
          name
          employee_type
        }
        external_author
        journal_publication_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_JOURNAL_PUBLICATION = gql`
  mutation UpdateJournalPublication(
    $id: bigint!
    $_set: journal_publications_set_input
  ) {
    update_journal_publications_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      title
      credit_multiplier
      created_at
      updated_at
      deleted_at
      journal_publication_category {
        id
        name
      }
      certificate_file
      journal_cover_file
      journal_toc_file
      paper_file
      decree_file
    }
  }
`;

export const DELETE_JOURNAL_PUBLICATION_MEMBERS = gql`
  mutation DeleteJournalPublicationMembers($journal_publication_id: bigint!) {
    delete_journal_publication_members(
      where: { journal_publication_id: { _eq: $journal_publication_id } }
    ) {
      affected_rows
    }
  }
`;

export const DELETE_JOURNAL_PUBLICATION_BY_PK = gql`
  mutation DeleteJournalPublicationByPK($id: bigint!) {
    delete_journal_publications_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_JOURNAL_PUBLICATION_BY_PK = gql`
  mutation SoftDeleteJournalPublicationByPK($id: bigint!) {
    update_journal_publications_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
