import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import {
  JournalPublicationByPK,
  JournalPublicationByPKVariables,
} from "../globalTypes";
import { JOURNAL_PUBLICATION_BY_ID } from "./gql/journalPublicationGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalJournalPublication from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/journalpublications`,
    content: `Pengelolaan Publikasi Jurnal`,
  },
  {
    to: `/dashboard/manage/journalpublications/${id}`,
    content: `Detail Publikasi Jurnal`,
  },
];

const membersColumns = [
  {
    title: "Posisi",
    dataIndex: ["journal_publication_activity_position", "name"],
  },
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const JournalPublicationDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const journalPublicationRes = useQuery<
    JournalPublicationByPK,
    JournalPublicationByPKVariables
  >(JOURNAL_PUBLICATION_BY_ID, {
    variables: {
      id: Number(id),
    },
  });

  const journalPublication =
    journalPublicationRes?.data?.journal_publications_by_pk;

  if (!journalPublicationRes.loading && journalPublicationRes.error) {
    // error handling
    return null;
  }
  if (!journalPublication) {
    if (journalPublicationRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  return (
    <>
      <ManagementHeading
        title="Detail Publikasi Jurnal"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Cover Prosiding</Text>
              </Col>
              <Col span={24}>
                {journalPublication?.journal_cover_file ? (
                  <Typography.Link
                    href={journalPublication?.journal_cover_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Daftar Isi Prosiding</Text>
              </Col>
              <Col span={24}>
                {journalPublication?.journal_toc_file ? (
                  <Typography.Link
                    href={journalPublication?.journal_toc_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Paper</Text>
              </Col>
              <Col span={24}>
                {journalPublication?.paper_file ? (
                  <Typography.Link
                    href={journalPublication?.paper_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Sertifikat</Text>
              </Col>
              <Col span={24}>
                {journalPublication?.certificate_file ? (
                  <Typography.Link
                    href={journalPublication?.certificate_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>SK / Surat Tugas</Text>
              </Col>
              <Col span={24}>
                {journalPublication?.decree_file ? (
                  <Typography.Link
                    href={journalPublication?.decree_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalJournalPublication
            record={journalPublication}
            onEditFinish={(id) => {
              journalPublicationRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Publikasi Jurnal" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Judul Publikasi"
                      value={journalPublication.title}
                    />
                    <FieldInfo
                      label="Jenis Publikasi Jurnal"
                      value={
                        journalPublication?.journal_publication_category?.name
                      }
                    />
                    <FieldInfo
                      label="Angka Kredit Terbagi"
                      value={journalPublication?.credit_multiplier}
                    />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Nama Jurnal"
                      value={journalPublication?.journal_name}
                    />
                    <FieldInfo
                      label="ISBN"
                      value={journalPublication?.isbn || "-"}
                    />
                    <FieldInfo
                      label="ISSN"
                      value={journalPublication?.issn || "-"}
                    />
                    <FieldInfo
                      label="Volume Jurnal"
                      value={journalPublication?.journal_volume}
                    />
                    <FieldInfo
                      label="Nomor Jurnal"
                      value={journalPublication?.journal_no}
                    />
                    <FieldInfo
                      label="Tahun"
                      value={journalPublication?.journal_year}
                    />
                    <FieldInfo
                      label="Halaman"
                      value={journalPublication?.journal_page_no}
                    />
                  </Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Penulis" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={journalPublication.journal_publication_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default JournalPublicationDetailPage;
