import { gql } from "@apollo/client";

export const DELETE_SEMINAR_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation DeleteSeminarPublicationActivityPositions($id: Int!) {
    delete_seminar_publication_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const SEMINAR_PUBLICATION_ACTIVITY_POSITIONS = gql`
  query SeminarPublicationActivityPositions {
    seminar_publication_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_SEMINAR_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation InsertSeminarPublicationActivityPositions(
    $object: seminar_publication_activity_positions_insert_input!
  ) {
    insert_seminar_publication_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_SEMINAR_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation UpdateSeminarPublicationActivityPositions(
    $pk_columns: seminar_publication_activity_positions_pk_columns_input!
    $_set: seminar_publication_activity_positions_set_input
  ) {
    update_seminar_publication_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
