import { gql } from "@apollo/client";

export const DELETE_SEMINAR_PUBLICATION_CATEGORIES = gql`
  mutation DeleteSeminarPublicationCategories($id: bigint!) {
    delete_seminar_publication_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const SEMINAR_PUBLICATION_CATEGORIES = gql`
  query SeminarPublicationCategories {
    seminar_publication_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_SEMINAR_PUBLICATION_CATEGORIES = gql`
  mutation InsertSeminarPublicationCategories(
    $object: seminar_publication_categories_insert_input!
  ) {
    insert_seminar_publication_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_SEMINAR_PUBLICATION_CATEGORIES = gql`
  mutation UpdateSeminarPublicationCategories(
    $pk_columns: seminar_publication_categories_pk_columns_input!
    $_set: seminar_publication_categories_set_input
  ) {
    update_seminar_publication_categories_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
