import { gql } from "@apollo/client";

export const DELETE_RESEARCH_ACTIVITY_POSITIONS = gql`
  mutation DeleteResearchActivityPositions($id: Int!) {
    delete_research_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const RESEARCH_ACTIVITY_POSITIONS = gql`
  query ResearchActivityPositions {
    research_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_RESEARCH_ACTIVITY_POSITIONS = gql`
  mutation InsertResearchActivityPositions(
    $object: research_activity_positions_insert_input!
  ) {
    insert_research_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_RESEARCH_ACTIVITY_POSITIONS = gql`
  mutation UpdateResearchActivityPositions(
    $pk_columns: research_activity_positions_pk_columns_input!
    $_set: research_activity_positions_set_input
  ) {
    update_research_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
