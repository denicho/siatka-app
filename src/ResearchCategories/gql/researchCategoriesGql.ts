import { gql } from "@apollo/client";

export const DELETE_RESEARCH_CATEGORIES = gql`
  mutation DeleteResearchCategories($id: bigint!) {
    delete_research_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const RESEARCH_CATEGORIES = gql`
  query ResearchCategories {
    research_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_RESEARCH_CATEGORIES = gql`
  mutation InsertResearchCategories(
    $object: research_categories_insert_input!
  ) {
    insert_research_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_RESEARCH_CATEGORIES = gql`
  mutation UpdateResearchCategories(
    $pk_columns: research_categories_pk_columns_input!
    $_set: research_categories_set_input
  ) {
    update_research_categories_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
