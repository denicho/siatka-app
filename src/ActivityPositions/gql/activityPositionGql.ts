import { gql } from "@apollo/client";

export const DELETE_ACTIVITY_POSITIONS = gql`
  mutation DeleteActivityPositions($id: bigint!) {
    delete_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const ACTIVITY_POSITIONS = gql`
  query ActivityPositions {
    activity_positions(order_by: {created_at: asc}) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_ACTIVITY_POSITIONS = gql`
  mutation InsertActivityPositions($object: activity_positions_insert_input!) {
    insert_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_ACTIVITY_POSITIONS = gql`
  mutation UpdateActivityPositions(
    $pk_columns: activity_positions_pk_columns_input!
    $_set: activity_positions_set_input
  ) {
    update_activity_positions_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      created_at
      updated_at
    }
  }
`;
