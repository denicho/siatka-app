import cookie from "cookie";
import { message } from "antd";

export const uploadFile = (file: File): Promise<any> => {
  return new Promise((resolve, reject) => {
    if (!file) {
      reject("File required");
    }

    const formData = new FormData();
    formData.append("file", file);

    let token = cookie.parse(document.cookie)["access-token"];

    fetch("https://api.siatka.xyz/files", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    })
      .then((raw) => raw.json())
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export type UploadPresets =
  | "employee_profpic"
  | "employee_certification_file"
  | "research_proposal_file"
  | "research_final_report_file"
  | "research_certification_file"
  | "research_decree_file"
  | "service_proposal_file"
  | "service_final_report_file"
  | "service_certification_file"
  | "service_decree_file"
  | "journal_cover_file"
  | "journal_toc_file"
  | "journal_paper_file"
  | "journal_certificate_file"
  | "journal_decree_file"
  | "seminar_cover_file"
  | "seminar_toc_file"
  | "seminar_paper_file"
  | "seminar_certificate_file"
  | "seminar_decree_file"
  | "book_file"
  | "patent_certificate_file"
  | "self_development_certificate_file"
  | "self_development_decree_file"
  | "committee_certificate_file"
  | "committee_decree_file";

export const limitFileSize = (file: File) => {
  const isLt5M = file.size / 1024 / 1024 < 5;

  if (!isLt5M) {
    message.error("File harus lebih kecil dari 2MB!");
  }

  return isLt5M;
};

export const createCustomRequest = (uploadPreset: UploadPresets): any => (
  options: any
) => {
  const formData = new FormData();
  formData.append("file", options.file);

  fetch(
    `https://api.cloudinary.com/v1_1/siatka/auto/upload?upload_preset=${uploadPreset}`,
    {
      method: "POST",
      body: formData,
    }
  )
    .then((raw) => raw.json())
    .then((response) => {
      options.onSuccess(response, options.file);
    })
    .catch(options.onError);
};
