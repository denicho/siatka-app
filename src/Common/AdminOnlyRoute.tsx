import * as React from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import AuthContext from "../Contexts/AuthContext";
import useIsAdmin from "../hooks/useIsAdmin";
import { FourOhThree } from "../Layout/FourOhNo";

interface Props extends RouteProps {
  children: React.ReactNode;
}

function AdminOnlyRoute({ children, ...rest }: Props) {
  const isAdmin = useIsAdmin();

  return (
    <Route
      {...rest}
      render={({ location }) => (isAdmin ? children : <FourOhThree />)}
    />
  );
}

export default AdminOnlyRoute;
