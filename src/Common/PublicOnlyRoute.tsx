import * as React from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import AuthContext from "../Contexts/AuthContext";

interface Props extends RouteProps {
  children: React.ReactNode;
}

function PublicOnlyRoute({ children, ...rest }: Props) {
  const auth = React.useContext(AuthContext);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        Boolean(auth.data.session) ? (
          <Redirect
            to={{
              pathname: "/dashboard",
              state: { from: location },
            }}
          />
        ) : (
          children
        )
      }
    />
  );
}

export default PublicOnlyRoute;
