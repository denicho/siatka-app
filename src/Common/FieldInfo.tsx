import React from "react";
import { Row, Col, Typography } from "antd";

const { Text } = Typography;

const FieldInfo = (props: { label: string; value: React.ReactNode }) => {
  return (
    <Row>
      <Col span={24}>
        <Text strong>{props.label}</Text>
      </Col>
      <Col span={24}>
        <Text>{props.value}</Text>
      </Col>
    </Row>
  );
};

export default FieldInfo;
