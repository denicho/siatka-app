import React from "react";
import { Row, Typography, Breadcrumb, Col } from "antd";
import { Link } from "react-router-dom";

const { Title } = Typography;

type Props = {
  title: string;
  breadcrumbs: Array<{ to: string; content: React.ReactNode }>;
  rightAddon: React.ReactNode;
};

const ManagementHeading = (props: Props) => {
  return (
    <>
      <Row>
        <Title>{props.title}</Title>
      </Row>
      <Row>
        <Col span={12}>
          <Breadcrumb>
            {props.breadcrumbs.map((crumb) => (
              <Breadcrumb.Item key={crumb.to}>
                <Link to={crumb.to}>{crumb.content}</Link>
              </Breadcrumb.Item>
            ))}
          </Breadcrumb>
        </Col>
        <Col span={12}>
          <Row justify="end">{props.rightAddon}</Row>
        </Col>
      </Row>
    </>
  );
};

ManagementHeading.defaultProps = {
  rightAddon: null,
};

export default ManagementHeading;
