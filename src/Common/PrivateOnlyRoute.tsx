import * as React from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import AuthContext from "../Contexts/AuthContext";

interface Props extends RouteProps {
  children: React.ReactNode;
}

function PrivateOnlyRoute({ children, ...rest }: Props) {
  const auth = React.useContext(AuthContext);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        Boolean(auth.data.session) ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

export default PrivateOnlyRoute;
