import React from "react";
import { Spin, Row } from "antd";
import "moment/locale/id";

const rowLoadingStyle = {
  height: "100%",
};

const LoadingPlaceholder = () => (
  <Row justify="center" align="middle" style={rowLoadingStyle}>
    <Spin size="large" />
  </Row>
);

export default LoadingPlaceholder;
