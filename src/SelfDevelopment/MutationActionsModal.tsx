import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  Upload,
  Select,
  Steps,
  InputNumber,
  Divider,
  Spin,
} from "antd";
import moment from "moment";
import { flatten, debounce } from "lodash-es";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  MinusCircleOutlined,
  RightOutlined,
  UploadOutlined,
  LeftOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";

import { createCustomRequest } from "../Common/uploadFile";
import {
  ActivityPositions,
  SelfDevelopmentByPK_self_developments_by_pk,
  SelfDevelopmentByPK_self_developments_by_pk_self_development_members,
  SelfDevelopments,
  SearchEmployees,
  SearchEmployeesVariables,
  SoftDeleteSelfDevelopmentByPK,
  SoftDeleteSelfDevelopmentByPKVariables,
  UpdateSelfDevelopment,
  UpdateSelfDevelopmentVariables,
  DeleteSelfDevelopmentMembers,
  DeleteSelfDevelopmentMembersVariables,
  InsertSelfDevelopmentMember,
  InsertSelfDevelopmentMemberVariables,
  SelfDevelopmentCategories,
  SelfDevelopmentLevel,
  SelfDevelopmentActivityPositions,
} from "../globalTypes";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";

import { useHistory } from "react-router-dom";
import {
  DELETE_SELF_DEVELOPMENT_MEMBERS,
  INSERT_SELF_DEVELOPMENT_MEMBER,
  SELF_DEVELOPMENTS,
  SELF_DEVELOPMENT_LEVEL,
  SOFT_DELETE_SELF_DEVELOPMENT_BY_PK,
  UPDATE_SELF_DEVELOPMENT,
} from "./gql/selfDevelopmentGql";
import { normFile } from "./AddForm";
import { SEARCH_EMPLOYEES } from "../Employees/gql/employeesGql";
import normalizeMembers from "../utils/normalizeMembers";
import { SELF_DEVELOPMENT_CATEGORIES } from "../SelfDevelopmentCategories/gql/selfDevelopmentCategoriesGql";
import { SELF_DEVELOPMENT_ACTIVITY_POSITIONS } from "../SelfDevelopmentCategories/gql/selfDevelopmentPositionsGql";

const { Text } = Typography;
const { Step } = Steps;
const { Option } = Select;

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 24 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

type MutationActionModalProps = {
  record: SelfDevelopmentByPK_self_developments_by_pk;
  onEditFinish?: (id: number) => void;
};

const MutationActionsModalSelfDevelopment = (
  props: MutationActionModalProps
) => {
  const history = useHistory();
  const categoriesRes = useQuery<SelfDevelopmentCategories>(
    SELF_DEVELOPMENT_CATEGORIES
  );
  const selfDevLevelsRes = useQuery<SelfDevelopmentLevel>(
    SELF_DEVELOPMENT_LEVEL
  );
  const categories = categoriesRes?.data?.self_development_categories || [];
  const selfDevLevels = selfDevLevelsRes?.data?.self_development_level || [];
  const selectedCategoryCredit =
    props.record?.self_development_members &&
    props.record?.self_development_members.length > 0
      ? props.record?.self_development_members[0].credit_gain
      : 0;

  // deleting
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const [deleteLoading, setDeleteLoading] = React.useState(false);
  const [del] = useMutation<
    SoftDeleteSelfDevelopmentByPK,
    SoftDeleteSelfDevelopmentByPKVariables
  >(SOFT_DELETE_SELF_DEVELOPMENT_BY_PK, {
    onCompleted: () => {
      setDeleteLoading(false);
      history.push("/dashboard/manage/selfdevelopments");
    },
  });
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };
  const handleDeleteOk = () => {
    setDeleteLoading(true);
    del({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<SelfDevelopments>({
            query: SELF_DEVELOPMENTS,
          });

          if (existing) {
            const newSelfDevelopments = existing.self_developments.filter(
              (data) => data.id !== props.record.id
            );

            cache.writeQuery({
              query: SELF_DEVELOPMENTS,
              data: { self_developments: newSelfDevelopments },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // editing
  const [searchEmployees, searchEmployeesRes] = useLazyQuery<
    SearchEmployees,
    SearchEmployeesVariables
  >(SEARCH_EMPLOYEES, {
    onCompleted: (data) => {},
  });
  const activityPosRes = useQuery<SelfDevelopmentActivityPositions>(
    SELF_DEVELOPMENT_ACTIVITY_POSITIONS
  );

  const searchedEmployees = searchEmployeesRes?.data?.employees || [];
  const [selectedEmployeeIDs, setSelectedEmployeeIDs] = React.useState<
    number[]
  >(
    props.record.self_development_members
      .map((mem) => mem.employee?.id)
      .filter(Boolean)
  );
  const filteredEmployees = searchedEmployees
    ? searchedEmployees.filter(
        (emp) => !selectedEmployeeIDs.includes(emp.value)
      )
    : [];

  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const [editLoading, setEditLoading] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    form.submit();
  };

  const validateList = [
    [
      "name",
      "category_id",
      "member",
      "start_date",
      "end_date",
      "venue",
      "level",
    ],
    ["certificate_file", "decree_file"],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const [currentStep, setCurrentStep] = React.useState(0);
  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 1 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );

  // Uploads
  const [certificationFileList, setCertificationFileList] = React.useState(
    props.record.certificate_file
      ? [
          {
            name: "cert.jpg",
            type: "image/jpeg",
            uid: "1",
            url: props.record.certificate_file || "",
            size: 0,
          },
        ]
      : []
  );
  const handleCertificationUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setCertificationFileList(fileList);
  };

  const [decreeFileList, setDecreeFileList] = React.useState(
    props.record.decree_file
      ? [
          {
            name: "cert.jpg",
            type: "image/jpeg",
            uid: "1",
            url: props.record.decree_file || "",
            size: 0,
          },
        ]
      : []
  );
  const handleDecreeUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setDecreeFileList(fileList);
  };
  const steps = [
    {
      title: "Data  Book",
      content: (
        <>
          <Form.Item
            name={"name"}
            label="Nama"
            rules={[{ required: true }]}
            initialValue={props.record.name}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"category_id"}
            label="Kategori"
            rules={[{ required: true }]}
            extra={
              <Typography.Text>
                Angka kredit didapat: <strong>{selectedCategoryCredit}</strong>
              </Typography.Text>
            }
            initialValue={props.record.self_development_category?.id.toString()}
          >
            <Select
              placeholder="Pilih Kategori Pengembangan Diri"
              loading={categories.length <= 0}
              disabled
            >
              {categories.map((ctg) => (
                <Option key={ctg.id} value={ctg?.id?.toString() || ""}>
                  {ctg.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"member"}
            label="Pelaku"
            rules={[{ required: true, message: "Pelaku wajib diisi" }]}
            style={{
              width: "100%",
              marginRight: 8,
            }}
            initialValue={
              props.record.self_development_members &&
              props.record.self_development_members.length > 0 && [
                {
                  value: props.record.self_development_members[0].employee?.id,
                  key: props.record.self_development_members[0].employee?.id,
                  label:
                    props.record.self_development_members[0].employee?.name ||
                    "",
                },
              ]
            }
          >
            <Select
              maxTagCount={1}
              labelInValue
              mode="multiple"
              value={form.getFieldValue(["member"])}
              placeholder="Cari nama"
              notFoundContent={
                searchEmployeesRes.loading ? <Spin size="small" /> : null
              }
              filterOption={false}
              onChange={(value) => {
                const newValue = value[value.length - 1];

                form.setFieldsValue({
                  member: newValue ? [newValue] : [],
                });
              }}
              onSearch={debounce((value) => {
                searchEmployees({
                  variables: {
                    term: `%${value}%`,
                  },
                });
              }, 400)}
            >
              {searchedEmployees.map((d) => (
                <Option key={`${d.value}a`} value={d.value}>
                  {d.text}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"start_date"}
            label="Tanggal Mulai"
            rules={[{ required: true }]}
            initialValue={moment(props.record.start_date, "YYYY-MM-DD")}
          >
            <DatePicker
              format="DD/MM/YYYY"
              showToday={false}
              onChange={(startDateValue) => {
                const endDate: moment.Moment = form.getFieldValue("end_date");
                if (startDateValue && endDate) {
                  if (endDate.isBefore(startDateValue)) {
                    form.setFieldsValue({ end_date: startDateValue });
                  }
                }
              }}
            />
          </Form.Item>
          <Form.Item
            name={"end_date"}
            label="Tanggal Selesai"
            initialValue={
              props.record.end_date
                ? moment(props.record.end_date, "YYYY-MM-DD")
                : undefined
            }
          >
            <DatePicker
              format="DD/MM/YYYY"
              disabledDate={(current) => {
                const startedAt = form.getFieldValue("start_date");
                // Can not select days after today and today
                if (startedAt) {
                  return current.isBefore(startedAt);
                } else {
                  return false;
                }
              }}
              showToday={false}
              onChange={(endDateValue) => {
                const startDate: moment.Moment = form.getFieldValue(
                  "start_date"
                );
                if (endDateValue && startDate) {
                  if (startDate.isAfter(endDateValue)) {
                    form.setFieldsValue({ start_date: endDateValue });
                  }
                }
              }}
            />
          </Form.Item>
          <Form.Item
            name={"venue"}
            label="Tempat"
            rules={[{ required: true }]}
            initialValue={props.record.venue}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"level"}
            label="Tingkat"
            rules={[{ required: true }]}
            initialValue={props.record.level}
          >
            <Select
              placeholder="Pilih Tingkat"
              loading={selfDevLevels.length <= 0}
            >
              {selfDevLevels.map((ctg) => (
                <Option key={ctg.level} value={ctg?.level}>
                  {ctg.level}
                </Option>
              ))}
            </Select>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Berkas",
      content: (
        <>
          <Form.Item
            name={"certificate_file"}
            label="Sertifikat"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            initialValue={
              props.record.certificate_file
                ? [
                    {
                      name: "File",
                      type: "image.jpg",
                      uid: "1",
                      // @ts-ignore
                      status: "done",
                      url: props.record.certificate_file,
                      size: 0,
                    },
                  ]
                : undefined
            }
          >
            <Upload
              customRequest={createCustomRequest(
                "self_development_certificate_file"
              )}
              onChange={handleCertificationUploadChange}
              fileList={certificationFileList}
              onRemove={() => setCertificationFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {certificationFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"decree_file"}
            label="SK / Surat Tugas"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            initialValue={
              props.record.decree_file
                ? [
                    {
                      name: "File",
                      type: "image.jpg",
                      uid: "1",
                      // @ts-ignore
                      status: "done",
                      url: props.record.decree_file,
                      size: 0,
                    },
                  ]
                : undefined
            }
          >
            <Upload
              customRequest={createCustomRequest(
                "self_development_decree_file"
              )}
              onChange={handleDecreeUploadChange}
              fileList={decreeFileList}
              onRemove={() => setDecreeFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {decreeFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    // {
    //   title: "Data Penulis",
    //   content: (
    //     <>
    //       <Divider orientation="center" plain>
    //         Penulis Internal
    //       </Divider>
    //       <Form.List
    //         name="internal_members"
    //         rules={[
    //           {
    //             validator: async (_, names) => {
    //               if (!names || names.length < 1) {
    //                 return Promise.reject(new Error("Minimal 1 penulis"));
    //               }
    //             },
    //           },
    //         ]}
    //         initialValue={getInternalMemberInitialValues(
    //           props.record.self_development_members
    //         )}
    //       >
    //         {(fields, { add, remove }, { errors }) => (
    //           <>
    //             {fields.map((field) => (
    //               <div
    //                 key={field.key}
    //                 style={{
    //                   display: "flex",
    //                   alignItems: "baseline",
    //                   marginBottom: 8,
    //                 }}
    //               >
    //                 <Form.Item
    //                   {...field}
    //                   name={[field.name, "activity_position_id"]}
    //                   fieldKey={[field.fieldKey, "activity_position_id"]}
    //                   rules={[
    //                     { required: true, message: "Posisi wajib diisi" },
    //                   ]}
    //                   style={{ marginRight: 8, marginBottom: 8, width: 200 }}
    //                 >
    //                   <Select
    //                     placeholder="Posisi"
    //                     onChange={() => {}}
    //                     loading={activityPoses.length <= 0}
    //                     allowClear
    //                   >
    //                     {activityPoses.map((activity) => (
    //                       <Option
    //                         key={activity.id}
    //                         value={activity?.id?.toString() || ""}
    //                       >
    //                         {activity.name}
    //                       </Option>
    //                     ))}
    //                   </Select>
    //                 </Form.Item>
    //                 <Form.Item
    //                   {...field}
    //                   name={[field.name, "internal_member"]}
    //                   fieldKey={[field.fieldKey, "internal_member"]}
    //                   rules={[{ required: true, message: "Nama wajib diisi" }]}
    //                   style={{
    //                     width: "100%",
    //                     marginRight: 8,
    //                     marginBottom: 8,
    //                   }}
    //                 >
    //                   <Select
    //                     maxTagCount={1}
    //                     labelInValue
    //                     mode="multiple"
    //                     value={form.getFieldValue([
    //                       "internal_members",
    //                       field.fieldKey,
    //                       "internal_member",
    //                     ])}
    //                     placeholder="Cari nama penulis internal"
    //                     notFoundContent={
    //                       searchEmployeesRes.loading ? (
    //                         <Spin size="small" />
    //                       ) : null
    //                     }
    //                     filterOption={false}
    //                     onChange={(value) => {
    //                       const newValue = value[value.length - 1];

    //                       form.setFieldsValue({
    //                         internal_members: form
    //                           .getFieldValue("internal_members")
    //                           .map(
    //                             (
    //                               x: Array<{
    //                                 internal_member: Array<{
    //                                   value: string | number;
    //                                   label: string;
    //                                   key: string;
    //                                 }>;
    //                               }>,
    //                               i: number
    //                             ) => {
    //                               if (i === field.name) {
    //                                 return {
    //                                   ...x,
    //                                   internal_member: newValue
    //                                     ? [newValue]
    //                                     : [],
    //                                 };
    //                               }
    //                               return x;
    //                             }
    //                           ),
    //                       });
    //                     }}
    //                     onSearch={debounce((value) => {
    //                       searchEmployees({
    //                         variables: {
    //                           term: `%${value}%`,
    //                         },
    //                       });
    //                     }, 400)}
    //                   >
    //                     {filteredEmployees.map((d) => (
    //                       <Option key={`${d.value}a`} value={d.value}>
    //                         {d.text}
    //                       </Option>
    //                     ))}
    //                   </Select>
    //                 </Form.Item>
    //                 <MinusCircleOutlined onClick={() => remove(field.name)} />
    //               </div>
    //             ))}
    //             <Form.Item>
    //               <Button
    //                 type="dashed"
    //                 onClick={() => add()}
    //                 block
    //                 icon={<PlusOutlined />}
    //               >
    //                 Tambah Penulis Internal
    //               </Button>
    //               <Form.ErrorList errors={errors} />
    //             </Form.Item>
    //           </>
    //         )}
    //       </Form.List>

    //       {/* external */}
    //       <Divider orientation="center" plain>
    //         Penulis Eksternal
    //       </Divider>
    //       <Form.List
    //         name="external_members"
    //         initialValue={getExternalMemberInitialValues(
    //           props.record.self_development_members
    //         )}
    //       >
    //         {(fields, { add, remove }, { errors }) => (
    //           <>
    //             {fields.map((field) => (
    //               <div
    //                 key={field.key}
    //                 style={{
    //                   display: "flex",
    //                   alignItems: "baseline",
    //                   marginBottom: 8,
    //                 }}
    //               >
    //                 <Form.Item
    //                   {...field}
    //                   name={[field.name, "activity_position_id"]}
    //                   fieldKey={[field.fieldKey, "activity_position_id"]}
    //                   rules={[
    //                     { required: true, message: "Posisi wajib diisi" },
    //                   ]}
    //                   style={{ marginRight: 8, marginBottom: 8, width: 200 }}
    //                 >
    //                   <Select
    //                     placeholder="Posisi"
    //                     onChange={() => {}}
    //                     loading={activityPoses.length <= 0}
    //                     allowClear
    //                   >
    //                     {activityPoses.map((activity) => (
    //                       <Option
    //                         key={activity.id}
    //                         value={activity?.id?.toString() || ""}
    //                       >
    //                         {activity.name}
    //                       </Option>
    //                     ))}
    //                   </Select>
    //                 </Form.Item>
    //                 <Form.Item
    //                   {...field}
    //                   name={[field.name, "external_members"]}
    //                   fieldKey={[field.fieldKey, "external_members"]}
    //                   rules={[{ required: true, message: "Nama wajib diisi" }]}
    //                   style={{
    //                     width: "100%",
    //                     marginRight: 8,
    //                     marginBottom: 8,
    //                   }}
    //                 >
    //                   <Input placeholder="Nama" />
    //                 </Form.Item>
    //                 <MinusCircleOutlined onClick={() => remove(field.name)} />
    //               </div>
    //             ))}
    //             <Form.Item>
    //               <Button
    //                 type="dashed"
    //                 onClick={() => add()}
    //                 block
    //                 icon={<PlusOutlined />}
    //               >
    //                 Tambah Penulis Eksternal
    //               </Button>
    //             </Form.Item>
    //           </>
    //         )}
    //       </Form.List>
    //       {stepNavigator}
    //     </>
    //   ),
    // },
  ];

  const [
    updateSelfDevelopment,
    { loading: updateSelfDevelopmentLoading },
  ] = useMutation<UpdateSelfDevelopment, UpdateSelfDevelopmentVariables>(
    UPDATE_SELF_DEVELOPMENT
  );
  const [
    deleteSelfDevelopmentMembers,
    { loading: deleteResMemLoading },
  ] = useMutation<
    DeleteSelfDevelopmentMembers,
    DeleteSelfDevelopmentMembersVariables
  >(DELETE_SELF_DEVELOPMENT_MEMBERS);
  const [
    insertSelfDevelopmentMember,
    { loading: insertResMemLoading },
  ] = useMutation<
    InsertSelfDevelopmentMember,
    InsertSelfDevelopmentMemberVariables
  >(INSERT_SELF_DEVELOPMENT_MEMBER);
  const handleEditFinish = (values: Store) => {
    const vals = form.getFieldsValue(flatten(validateList));

    const certificateFileVal = vals.certificate_file;
    const decreeFileVal = vals.decree_file;
    let certFile = "";
    let decreeFile = "";

    try {
      if (certificateFileVal && certificateFileVal.length > 0) {
        // berarti ganti file
        certFile = certificateFileVal[certificateFileVal.length - 1].url;
      }
      if (decreeFileVal && decreeFileVal.length > 0) {
        // berarti ganti file
        decreeFile = decreeFileVal[decreeFileVal.length - 1].url;
      }
    } catch (err) {
      console.log("err", err);
    }

    const updateSelfDevelopmentPayload = {
      ...vals,
      category_id: Number(vals.category_id),
      start_date: vals.start_date.format("YYYY-MM-DD"),
      end_date: vals.end_date ? vals.end_date.format("YYYY-MM-DD") : undefined,
      certificate_file: certFile,
      decree_file: decreeFile,
    };

    delete updateSelfDevelopmentPayload.member;

    Promise.all([
      updateSelfDevelopment({
        variables: {
          id: props.record.id,
          _set: {
            ...updateSelfDevelopmentPayload,
          },
        },
      }),
      new Promise((resolve, reject) => {
        deleteSelfDevelopmentMembers({
          variables: {
            self_development_id: props.record.id,
          },
        })
          .then(() => {
            const insertSelfDevelopmentMembersPayload =
              vals.member && vals.member.length > 0
                ? [
                    {
                      employee_id: vals.member[0].value,
                      self_development_id: props.record.id,
                      credit_gain: Number(selectedCategoryCredit),
                    },
                  ]
                : [];

            insertSelfDevelopmentMember({
              variables: {
                objects: insertSelfDevelopmentMembersPayload,
              },
            }).then(() => {
              resolve(true);
            });
          })
          .catch((err) => {
            reject(err);
          });
      }),
    ])
      .then(() => {
        setCurrentStep(0);
        form.resetFields();
        setEditLoading(false);
        setEditModalVisible(false);
        props.onEditFinish && props.onEditFinish(props.record.id);
      })
      .catch((err) => {
        setEditLoading(false);
        setEditModalVisible(false);
      });
  };

  return (
    <>
      <Modal
        title="Hapus Pengembangan Diri"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">Nama: {props.record.name}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Pengembangan Diri"
        visible={editModalvisible}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            disabled={
              currentStep !== steps.length - 1 ||
              updateSelfDevelopmentLoading ||
              deleteResMemLoading ||
              insertResMemLoading
            }
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
          onValuesChange={(changed, all) => {
            // This will trigger the filters so that user cant pick the same employee twice
            if (changed.internal_members && all.internal_members) {
              const selectedEmployeeIds = all.internal_members
                .filter(Boolean)
                .map((member: any) => {
                  const id =
                    (member.internal_member &&
                      member.internal_member.length > 0 &&
                      member.internal_member[0]?.value) ||
                    0;

                  return id;
                })
                .filter(Boolean);

              setSelectedEmployeeIDs(selectedEmployeeIds);
            }
          }}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>

      <Row justify="space-around" style={{ marginTop: 16 }}>
        <Col span={14}>
          <Button block onClick={handleEditClick}>
            <EditOutlined /> Ubah
          </Button>
        </Col>
        <Col span={14} style={{ marginTop: 16 }}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined /> Hapus
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionsModalSelfDevelopment;
