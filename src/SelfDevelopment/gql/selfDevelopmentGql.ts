import { gql } from "@apollo/client";

export const SELF_DEVELOPMENT_LEVEL = gql`
  query SelfDevelopmentLevel {
    self_development_level {
      level
    }
  }
`;

export const SELF_DEVELOPMENTS = gql`
  query SelfDevelopments {
    self_developments(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      created_at
      updated_at
      deleted_at
      self_development_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
      self_development_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const SELF_DEVELOPMENT_BY_ID = gql`
  query SelfDevelopmentByPK($id: bigint!) {
    self_developments_by_pk(id: $id) {
      id
      created_at
      updated_at
      deleted_at
      self_development_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
      self_development_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const INSERT_SELF_DEVELOPMENTS = gql`
  mutation InsertSelfDevelopments($object: self_developments_insert_input!) {
    insert_self_developments_one(object: $object) {
      id
      created_at
      updated_at
      deleted_at
      self_development_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
      self_development_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const INSERT_SELF_DEVELOPMENT_MEMBER = gql`
  mutation InsertSelfDevelopmentMember(
    $objects: [self_development_members_insert_input!]!
  ) {
    insert_self_development_members(objects: $objects) {
      affected_rows
      returning {
        id
        self_development_id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
      }
    }
  }
`;

export const UPDATE_SELF_DEVELOPMENT = gql`
  mutation UpdateSelfDevelopment(
    $id: bigint!
    $_set: self_developments_set_input
  ) {
    update_self_developments_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      created_at
      updated_at
      deleted_at
      self_development_category {
        id
        name
      }
      name
      start_date
      end_date
      venue
      decree_file
      certificate_file
      level
    }
  }
`;

export const DELETE_SELF_DEVELOPMENT_MEMBERS = gql`
  mutation DeleteSelfDevelopmentMembers($self_development_id: bigint!) {
    delete_self_development_members(
      where: { self_development_id: { _eq: $self_development_id } }
    ) {
      affected_rows
    }
  }
`;

export const DELETE_SELF_DEVELOPMENT_BY_PK = gql`
  mutation DeleteSelfDevelopmentByPK($id: bigint!) {
    delete_self_developments_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_SELF_DEVELOPMENT_BY_PK = gql`
  mutation SoftDeleteSelfDevelopmentByPK($id: bigint!) {
    update_self_developments_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
