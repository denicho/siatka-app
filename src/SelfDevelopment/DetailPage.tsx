import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import {
  SelfDevelopmentByPK,
  SelfDevelopmentByPKVariables,
} from "../globalTypes";
import { SELF_DEVELOPMENT_BY_ID } from "./gql/selfDevelopmentGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalSelfDevelopment from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/selfdevelopments`,
    content: `Pengelolaan  Pengembangan Diri`,
  },
  {
    to: `/dashboard/manage/selfdevelopments/${id}`,
    content: `Detail  Pengembangan Diri`,
  },
];

const membersColumns = [
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const SelfDevelopmentDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const selfDevelopmentRes = useQuery<
    SelfDevelopmentByPK,
    SelfDevelopmentByPKVariables
  >(SELF_DEVELOPMENT_BY_ID, {
    variables: {
      id: Number(id),
    },
  });

  const selfDevelopment = selfDevelopmentRes?.data?.self_developments_by_pk;

  if (!selfDevelopmentRes.loading && selfDevelopmentRes.error) {
    // error handling
    return null;
  }
  if (!selfDevelopment) {
    if (selfDevelopmentRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  const startAsDate = new Date(selfDevelopment.start_date);
  const endAsDate = new Date(selfDevelopment.end_date);

  return (
    <>
      <ManagementHeading
        title="Detail Pengembangan Diri"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>File Sertifikat</Text>
              </Col>
              <Col span={24}>
                {selfDevelopment?.certificate_file ? (
                  <Typography.Link
                    href={selfDevelopment?.certificate_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>File SK / Surat Tugas</Text>
              </Col>
              <Col span={24}>
                {selfDevelopment?.decree_file ? (
                  <Typography.Link
                    href={selfDevelopment?.decree_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalSelfDevelopment
            record={selfDevelopment}
            onEditFinish={(id) => {
              selfDevelopmentRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Pengembangan Diri" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label="Nama" value={selfDevelopment.name} />
                    <FieldInfo
                      label="Tempat Kegiatan"
                      value={selfDevelopment.venue}
                    />
                    <FieldInfo label="Tingkat" value={selfDevelopment.level} />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Tanggal Mulai"
                      value={
                        selfDevelopment.start_date
                          ? format(startAsDate, "eeee, dd MMMM yyyy", {
                              locale: localeId,
                            })
                          : "-"
                      }
                    />
                    <FieldInfo
                      label="Tanggal Selesai"
                      value={
                        selfDevelopment.end_date
                          ? format(endAsDate, "eeee, dd MMMM yyyy", {
                              locale: localeId,
                            })
                          : "-"
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Pelaku" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={selfDevelopment.self_development_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default SelfDevelopmentDetailPage;
