import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import AdminOnlyRoute from "./Common/AdminOnlyRoute";

import EmployeeListPage from "./Employees/ListPage";
import EmployeeDetailPage from "./Employees/DetailPage";
import FacultyListPage from "./Faculties/ListPage";
import DepartmentListPage from "./Departments/ListPage";
import GroupListPage from "./Groups/ListPage";
import StructuralPositionsListPage from "./StructuralPositions/ListPage";
import FunctionalPositionsListPage from "./FunctionalPositions/ListPage";
// import UserListPage from "./User/ListPage";

import ActivityPositionsListPage from "./ActivityPositions/ListPage";

import ResearchCategoriesListPage from "./ResearchCategories/ListPage";
import ResearchPositionsListPage from "./ResearchCategories/PositionListPage";
import ResearchListPage from "./Research/ListPage";
import ResearchDetailPage from "./Research/DetailPage";

import ServiceCategoriesListPage from "./ServiceCategories/ListPage";
import ServicePositionsListPage from "./ServiceCategories/PositionListPage";
import ServiceListPage from "./Service/ListPage";
import ServiceDetailPage from "./Service/DetailPage";

import JournalPublicationCategoriesListPage from "./JournalPublicationCategories/ListPage";
import JournalPublicationPositionsListPage from "./JournalPublicationCategories/PositionListPage";
import JournalPublicationListPage from "./JournalPublication/ListPage";
import JournalPublicationDetailPage from "./JournalPublication/DetailPage";

import SeminarPublicationCategoriesListPage from "./SeminarPublicationCategories/ListPage";
import SeminarPublicationPositionsListPage from "./SeminarPublicationCategories/PositionListPage";
import SeminarPublicationListPage from "./SeminarPublication/ListPage";
import SeminarPublicationDetailPage from "./SeminarPublication/DetailPage";

import BookPublicationCategoriesListPage from "./BookPublicationCategories/ListPage";
import BookPublicationListPage from "./BookPublication/ListPage";
import BookPublicationDetailPage from "./BookPublication/DetailPage";
import BookPublicationPositionsListPage from "./BookPublicationCategories/PositionListPage";

import PatentPublicationCategoriesListPage from "./PatentPublicationCategories/ListPage";
import PatentPublicationPositionsListPage from "./PatentPublicationCategories/PositionListPage";

import PatentPublicationListPage from "./PatentPublication/ListPage";
import PatentPublicationDetailPage from "./PatentPublication/DetailPage";

import SelfDevelopmentCategoriesListPage from "./SelfDevelopmentCategories/ListPage";
import SelfDevelopmentPositionsListPage from "./SelfDevelopmentCategories/PositionListPage";
import SelfDevelopmentListPage from "./SelfDevelopment/ListPage";
import SelfDevelopmentDetailPage from "./SelfDevelopment/DetailPage";

import CommitteeCategoriesListPage from "./CommitteeCategories/ListPage";
import CommitteeListPage from "./Committee/ListPage";
import CommitteeDetailPage from "./Committee/DetailPage";
import CommitteePositionsListPage from "./CommitteeCategories/PositionListPage";
import HomePage from "./HomePage";

function Dashboard() {
  return (
    <Switch>
      <Route exact path="/dashboard">
        <HomePage />
      </Route>
      <Route exact path="/dashboard/manage/lecturers">
        <EmployeeListPage type="lecturer" />
      </Route>
      <Route exact path="/dashboard/manage/employees">
        <EmployeeListPage type="staff" />
      </Route>
      <Route exact path="/dashboard/manage/lecturers/:id">
        <EmployeeDetailPage type="lecturer" />
      </Route>
      <Route exact path="/dashboard/manage/employees/:id">
        <EmployeeDetailPage type="staff" />
      </Route>

      <AdminOnlyRoute exact path="/dashboard/manage/faculties">
        <FacultyListPage />
      </AdminOnlyRoute>
      <AdminOnlyRoute exact path="/dashboard/manage/departments">
        <DepartmentListPage />
      </AdminOnlyRoute>
      <AdminOnlyRoute exact path="/dashboard/manage/structural-positions">
        <StructuralPositionsListPage />
      </AdminOnlyRoute>
      <AdminOnlyRoute exact path="/dashboard/manage/functional-positions">
        <FunctionalPositionsListPage />
      </AdminOnlyRoute>
      <AdminOnlyRoute exact path="/dashboard/manage/groups">
        <GroupListPage />
      </AdminOnlyRoute>

      <Route exact path="/dashboard/manage/researches">
        <ResearchListPage />
      </Route>
      <Route exact path="/dashboard/manage/researches/:id">
        <ResearchDetailPage />
      </Route>
      <AdminOnlyRoute exact path="/dashboard/manage/research-categories">
        <ResearchCategoriesListPage />
      </AdminOnlyRoute>
      <AdminOnlyRoute exact path="/dashboard/manage/research-positions">
        <ResearchPositionsListPage />
      </AdminOnlyRoute>

      <AdminOnlyRoute exact path="/dashboard/manage/service-categories">
        <ServiceCategoriesListPage />
      </AdminOnlyRoute>
      <Route exact path="/dashboard/manage/services">
        <ServiceListPage />
      </Route>
      <Route exact path="/dashboard/manage/services/:id">
        <ServiceDetailPage />
      </Route>
      <AdminOnlyRoute exact path="/dashboard/manage/service-positions">
        <ServicePositionsListPage />
      </AdminOnlyRoute>

      <AdminOnlyRoute
        exact
        path="/dashboard/manage/journal-publication-categories"
      >
        <JournalPublicationCategoriesListPage />
      </AdminOnlyRoute>
      <Route exact path="/dashboard/manage/journalpublications">
        <JournalPublicationListPage />
      </Route>
      <Route exact path="/dashboard/manage/journalpublications/:id">
        <JournalPublicationDetailPage />
      </Route>
      <AdminOnlyRoute
        exact
        path="/dashboard/manage/journal-publication-positions"
      >
        <JournalPublicationPositionsListPage />
      </AdminOnlyRoute>

      <AdminOnlyRoute
        exact
        path="/dashboard/manage/seminar-publication-categories"
      >
        <SeminarPublicationCategoriesListPage />
      </AdminOnlyRoute>
      <Route exact path="/dashboard/manage/seminarpublications">
        <SeminarPublicationListPage />
      </Route>
      <Route exact path="/dashboard/manage/seminarpublications/:id">
        <SeminarPublicationDetailPage />
      </Route>
      <AdminOnlyRoute
        exact
        path="/dashboard/manage/seminar-publication-positions"
      >
        <SeminarPublicationPositionsListPage />
      </AdminOnlyRoute>

      <AdminOnlyRoute
        exact
        path="/dashboard/manage/book-publication-categories"
      >
        <BookPublicationCategoriesListPage />
      </AdminOnlyRoute>
      <Route exact path="/dashboard/manage/bookpublications">
        <BookPublicationListPage />
      </Route>
      <Route exact path="/dashboard/manage/bookpublications/:id">
        <BookPublicationDetailPage />
      </Route>
      <AdminOnlyRoute exact path="/dashboard/manage/book-publication-positions">
        <BookPublicationPositionsListPage />
      </AdminOnlyRoute>

      <AdminOnlyRoute
        exact
        path="/dashboard/manage/patent-publication-categories"
      >
        <PatentPublicationCategoriesListPage />
      </AdminOnlyRoute>
      <Route exact path="/dashboard/manage/patentpublications">
        <PatentPublicationListPage />
      </Route>
      <Route exact path="/dashboard/manage/patentpublications/:id">
        <PatentPublicationDetailPage />
      </Route>
      <AdminOnlyRoute
        exact
        path="/dashboard/manage/patent-publication-positions"
      >
        <PatentPublicationPositionsListPage />
      </AdminOnlyRoute>

      <AdminOnlyRoute
        exact
        path="/dashboard/manage/self-development-categories"
      >
        <SelfDevelopmentCategoriesListPage />
      </AdminOnlyRoute>
      <Route exact path="/dashboard/manage/selfdevelopments">
        <SelfDevelopmentListPage />
      </Route>
      <Route exact path="/dashboard/manage/selfdevelopments/:id">
        <SelfDevelopmentDetailPage />
      </Route>
      <AdminOnlyRoute exact path="/dashboard/manage/self-development-positions">
        <SelfDevelopmentPositionsListPage />
      </AdminOnlyRoute>

      <AdminOnlyRoute exact path="/dashboard/manage/committee-categories">
        <CommitteeCategoriesListPage />
      </AdminOnlyRoute>
      <Route exact path="/dashboard/manage/committees">
        <CommitteeListPage />
      </Route>
      <Route exact path="/dashboard/manage/committees/:id">
        <CommitteeDetailPage />
      </Route>
      <AdminOnlyRoute exact path="/dashboard/manage/committee-positions">
        <CommitteePositionsListPage />
      </AdminOnlyRoute>

      <Redirect to="/dashboard" />
    </Switch>
  );
}

export default Dashboard;
