import { gql } from "@apollo/client";

export const DELETE_DEPARTMENTS = gql`
  mutation DeleteDepartments($id: String!) {
    delete_departments_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const DEPARTMENTS = gql`
  query Departments {
    departments(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
      faculty {
        id
        name
      }
    }
  }
`;

export const INSERT_DEPARTMENTS = gql`
  mutation InsertDepartments($object: departments_insert_input!) {
    insert_departments_one(object: $object) {
      id
      name
      created_at
      updated_at
      faculty {
        id
        name
      }
    }
  }
`;

export const UPDATE_DEPARTMENTS = gql`
  mutation UpdateDepartments(
    $pk_columns: departments_pk_columns_input!
    $_set: departments_set_input
  ) {
    update_departments_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      created_at
      updated_at
      faculty {
        id
        name
      }
    }
  }
`;
