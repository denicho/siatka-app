import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, Modal, Select } from "antd";
import { Store } from "antd/lib/form/interface";
import {
  InsertDepartments,
  InsertDepartmentsVariables,
  Faculties,
} from "../globalTypes";
import { gql, useMutation, useQuery } from "@apollo/client";
import { INSERT_DEPARTMENTS } from "./gql/departmentGql";
import { FACULTIES } from "../Faculties/gql/facultyGql";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const { Option } = Select;

const DepartmentAddButton = () => {
  const facultiesRes = useQuery<Faculties>(FACULTIES);
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const faculties = facultiesRes?.data?.faculties || [];

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    form.submit();
  };

  const [create] = useMutation<InsertDepartments, InsertDepartmentsVariables>(
    INSERT_DEPARTMENTS,
    {
      onCompleted: () => {
        form.resetFields();
        setLoading(false);
        setVisible(false);
      },
      onError: () => {
        setLoading(false);
        setVisible(false);
      },
    }
  );

  const onFinish = (values: Store) => {
    setLoading(true);

    create({
      variables: {
        object: {
          id: values.id,
          name: values.name,
          faculty_id: values.faculty_id,
        },
      },
      update: (cache, { data }) => {
        if (data?.insert_departments_one) {
          cache.modify({
            fields: {
              departments(existingDepartments = []) {
                const newDepartments = cache.writeFragment({
                  data: data.insert_departments_one,
                  fragment: gql`
                    fragment departments on departments {
                      id
                      name
                      created_at
                      updated_at
                    }
                  `,
                });
                return [...existingDepartments, newDepartments];
              },
            },
          });
        }
      },
    });
  };

  const onFacultyChange = (value: string) => {
    form.setFieldsValue({ faculty_id: value || null });
  };

  return (
    <>
      <Modal
        title="Tambah Program Studi"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-departments"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name={"id"} label="ID" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"faculty_id"}
            label="Fakultas"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Fakultas"
              onChange={onFacultyChange}
              loading={faculties.length <= 0}
              allowClear
            >
              {faculties.map((faculty) => (
                <Option key={faculty.id} value={faculty?.id?.toString() || ""}>
                  {faculty.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Form>
      </Modal>

      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Program Studi
      </Button>
    </>
  );
};

export default DepartmentAddButton;
