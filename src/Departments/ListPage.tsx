import React from "react";
import { Table, Row, Col } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import ManagementHeading from "../Common/ManagementHeading";
import DepartmentAddButton from "./AddForm";
import MutationActionsModal from "./MutationActionsModal";
import { useQuery } from "@apollo/client";
import { DEPARTMENTS } from "./gql/departmentGql";
import { Departments } from "../globalTypes";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Fakultas",
    dataIndex: ["faculty", "name"],
    key: "faculty.name",
    width: 400,
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/departments",
    content: "Pengelolaan Program Studi",
  },
];

const DepartmentListPage = () => {
  const depsRes = useQuery<Departments>(DEPARTMENTS);

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (depsRes.loading || !depsRes.data || !depsRes.data.departments) {
    return <LoadingPlaceholder />;
  }

  const departments = depsRes.data.departments;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Program Studi"
        breadcrumbs={breadcrumbs}
        rightAddon={<DepartmentAddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={departments}
          />
        </Col>
      </Row>
    </>
  );
};

export default DepartmentListPage;
