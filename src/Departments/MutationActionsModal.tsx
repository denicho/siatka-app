import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  Select,
} from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";

import { useMutation, useQuery } from "@apollo/client";
import {
  DELETE_DEPARTMENTS,
  DEPARTMENTS,
  UPDATE_DEPARTMENTS,
} from "./gql/departmentGql";
import {
  DeleteDepartments,
  DeleteDepartmentsVariables,
  Departments,
  Faculties,
  UpdateDepartments,
  UpdateDepartmentsVariables,
} from "../globalTypes";
import { FACULTIES } from "../Faculties/gql/facultyGql";

const { Text } = Typography;
const { Option } = Select;

type MutationActionModalProps = {
  record: any;
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const MutationActionModal = (props: MutationActionModalProps) => {
  const facultiesRes = useQuery<Faculties>(FACULTIES);
  const faculties = facultiesRes?.data?.faculties || [];
  const onFacultyChange = (value: string) => {
    form.setFieldsValue({ faculty_id: value || null });
  };
  // Deleting
  const [deleteDepartment, { loading: deleteLoading }] = useMutation<
    DeleteDepartments,
    DeleteDepartmentsVariables
  >(DELETE_DEPARTMENTS, {
    onCompleted: () => {
      setDeleteModalVisible(false);
    },
    onError: () => {
      setDeleteModalVisible(false);
    },
  });
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };
  const handleDeleteOk = () => {
    deleteDepartment({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<Departments>({ query: DEPARTMENTS });

          if (existing) {
            const newDeps = existing.departments.filter(
              (g: any) => g.id !== props.record.id
            );
            cache.writeQuery({
              query: DEPARTMENTS,
              data: { departments: newDeps },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // Editing
  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    form.submit();
  };
  const [update, { loading: editLoading }] = useMutation<
    UpdateDepartments,
    UpdateDepartmentsVariables
  >(UPDATE_DEPARTMENTS, {
    onCompleted: () => {
      setEditModalVisible(false);
    },
  });
  const handleEditFinish = (values: Store) => {
    update({
      variables: {
        pk_columns: {
          id: props.record.id,
        },
        _set: {
          name: values.name,
          faculty_id: values.faculty_id,
        },
      },
    });
  };

  return (
    <>
      <Modal
        title="Hapus Program Studi"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">ID: {props.record.id}</Text>
          <Text type="danger">Nama: {props.record.name}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Program Studi"
        visible={editModalvisible}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-departments"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
        >
          <Form.Item
            name={"id"}
            label="ID"
            rules={[{ required: true }]}
            initialValue={props.record.id}
          >
            <Input disabled />
          </Form.Item>
          <Form.Item
            name={"name"}
            label="Nama"
            rules={[{ required: true }]}
            initialValue={props.record.name}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"faculty_id"}
            label="Fakultas"
            rules={[{ required: true }]}
            initialValue={props.record.faculty?.id?.toString() || ""}
          >
            <Select
              placeholder="Pilih Fakultas"
              onChange={onFacultyChange}
              loading={faculties.length <= 0}
              allowClear
            >
              {faculties.map((faculty) => (
                <Option key={faculty.id} value={faculty?.id?.toString() || ""}>
                  {faculty.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Form>
      </Modal>

      <Row justify="space-around">
        <Col span={10}>
          <Button block onClick={handleEditClick}>
            <EditOutlined />
          </Button>
        </Col>
        <Col span={10}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined />
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionModal;
