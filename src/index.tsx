import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { Alert, Spin } from "antd";
import cookie from "cookie";
import { CacheProvider, NetworkErrorBoundary } from "rest-hooks";
import "antd/dist/antd.css";
import {
  ApolloLink,
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  createHttpLink,
} from "@apollo/client";
import "./index.less";
import App from "./App";
import { AuthContextProvider } from "./Contexts/AuthContext";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, locations, path }) => {
      if (message.includes("Foreign key violation")) {
        alert(
          "Tidak dapat menghapus data, pastikan data tidak terhubung dengan entitas lain."
        );
      }
    });
  }

  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const httpLink = createHttpLink({
  uri: `${process.env.REACT_APP_GQL_HOST}/v1/graphql`,
});

const authLink = setContext((_, context) => {
  const cookies = cookie.parse(document.cookie);
  const token = cookies["access-token"];

  if (context.skipAuth) {
    return {
      headers: {
        ...context.headers,
      },
    };
  }

  return {
    headers: {
      ...context.headers,
      Authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const client = new ApolloClient({
  link: errorLink.concat(authLink.concat(httpLink)),
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <Suspense fallback={<Spin />}>
    <ApolloProvider client={client}>
      <AuthContextProvider>
        <NetworkErrorBoundary fallbackComponent={() => <div>error</div>}>
          <CacheProvider>
            <App />
          </CacheProvider>
        </NetworkErrorBoundary>
      </AuthContextProvider>
    </ApolloProvider>
  </Suspense>,
  document.getElementById("root")
);
