import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  Upload,
  InputNumber,
  Select,
  Steps,
  message,
} from "antd";
import { flatten } from "lodash-es";
import moment from "moment";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  LoadingOutlined,
  RightOutlined,
  LeftOutlined,
  RetweetOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";

import { createCustomRequest, limitFileSize } from "../Common/uploadFile";
import { disabledDate, normFile } from "./AddFormStaff";
import {
  DeleteEmployeesAndUser,
  DeleteEmployeesAndUserVariables,
  Employees,
  EmployeesByPK_employees_by_pk,
  employee_type_enum,
  UpdateEmployeesAndUser,
  UpdateEmployeesAndUserVariables,
} from "../globalTypes";
import { useMutation, useQuery } from "@apollo/client";
import {
  Departments,
  FunctionalPositions,
  Groups,
  StructuralPositions,
  EmployeeStatuses,
  Roles,
} from "../globalTypes";
import { DEPARTMENTS } from "../Departments/gql/departmentGql";
import { GROUPS } from "../Groups/gql/groupGql";
import { FUNCTIONAL_POS } from "../FunctionalPositions/gql/functionalPositionGql";
import { STRUCTURAL_POS } from "../StructuralPositions/gql/structuralPositionGql";
import {
  DELETE_EMPLOYEES_AND_USER,
  EMPLOYEES,
  EMPLOYEE_STATUSES,
  UPDATE_EMPLOYEES_AND_USER,
} from "./gql/employeesGql";
import { ROLES } from "./gql/rolesGql";
import { employeeChangePassword } from "./gql/employeeSignup";
import { useHistory } from "react-router-dom";
import useIsAdmin from "../hooks/useIsAdmin";
import AuthContext from "../Contexts/AuthContext";

const { Text } = Typography;
const { Step } = Steps;
const { Option } = Select;

type MutationActionModalProps = {
  record: EmployeesByPK_employees_by_pk;
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const MutationActionModal = (props: MutationActionModalProps) => {
  const auth = React.useContext(AuthContext);
  const isAdmin = useIsAdmin();
  // Deleting
  // const del = useFetcher(EmployeeResource.deleteShape());
  const history = useHistory();
  const [del] = useMutation<
    DeleteEmployeesAndUser,
    DeleteEmployeesAndUserVariables
  >(DELETE_EMPLOYEES_AND_USER, {
    onCompleted: () => {
      setDeleteLoading(false);
      history.push("/dashboard/manage/employees");
    },
  });
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const [deleteLoading, setDeleteLoading] = React.useState(false);
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };
  const handleDeleteOk = () => {
    setDeleteLoading(true);
    del({
      variables: {
        employee_id: props.record.id,
        user_id: props.record.user.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<Employees>({
            query: EMPLOYEES,
            variables: {
              employee_type: employee_type_enum.STAFF,
            },
          });

          console.log(existing);

          if (existing) {
            const newEmployees = existing.employees.filter(
              (g: any) => g.id !== props.record.id
            );
            cache.writeQuery({
              query: EMPLOYEES,
              variables: {
                employee_type: employee_type_enum.STAFF,
              },
              data: { employees: newEmployees },
            });
          }
        } catch (err) {
          console.log("Failed updating cache");
        }
      },
    });
  };
  const [selectedFacultyName, setSelectedFacultyName] = React.useState<string>(
    props.record.department?.faculty?.name || ""
  );

  // Editing
  // const update = useFetcher(EmployeeResource.updateShape());
  const [update, { loading: updateLoading }] = useMutation<
    UpdateEmployeesAndUser,
    UpdateEmployeesAndUserVariables
  >(UPDATE_EMPLOYEES_AND_USER, {
    onCompleted: () => {
      setEditModalVisible(false);
      setEditLoading(false);
      form.resetFields();
      setCurrentStep(0);
    },
    onError: (err) => {
      if (err.message.includes("Unique")) {
        message.error("Gagal membuat data, pastikan NPP belum terpakai.");
      } else {
        message.error("Gagal membuat data.");
      }

      setEditLoading(false);
    },
  });
  const [form] = Form.useForm();

  const depsRes = useQuery<Departments>(DEPARTMENTS);
  const groupsRes = useQuery<Groups>(GROUPS);
  const funcPosRes = useQuery<FunctionalPositions>(FUNCTIONAL_POS);
  const structPosRes = useQuery<StructuralPositions>(STRUCTURAL_POS);
  const rolesRes = useQuery<Roles>(ROLES);
  const emplStatusesRes = useQuery<EmployeeStatuses>(EMPLOYEE_STATUSES);

  const deps = depsRes?.data?.departments || [];
  const groups = groupsRes?.data?.groups || [];
  const functionalPoses = funcPosRes?.data?.functional_positions || [];
  const structuralPoses = structPosRes?.data?.structural_positions || [];
  const statuses = emplStatusesRes?.data?.employee_status || [];
  const roles = rolesRes?.data?.roles || [];

  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const [editLoading, setEditLoading] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    form.submit();
  };

  const [currentStep, setCurrentStep] = React.useState(0);
  const validateList = [
    [
      "name",
      "address",
      "place_of_birth",
      "date_of_birth",
      "phone",
      "picture_file",
    ],
    [
      "initial",
      "department_id",
      "nidn",
      "npp",
      "functional_position_id",
      "structural_position_id",
      "group_id",
      "status",
      "base_credit",
    ],
    ["email", ...(isAdmin ? ["role_id"] : [])],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const handleResetPassword = () => {
    employeeChangePassword({
      id: props.record?.user.id,
      password: props.record?.npp || "",
      confirmPassword: props.record?.npp || "",
    })
      .then(() => {
        message.success("Berhasil mereset password");
      })
      .catch((err) => {
        message.error("Gagal mengubah password");
        console.log("Failed changing password, ", err);
      });
  };

  const handleEditFinish = (values: Store) => {
    setEditLoading(true);

    const vals = form.getFieldsValue(flatten(validateList));

    const pFile = vals.picture_file;

    let pictureFile = "";

    try {
      if (pFile && pFile.length > 1) {
        console.log(pFile);
        // ganti file
        pictureFile = pFile[pFile.length - 1].response.secure_url;
      } else {
        pictureFile = profPicFileRes.url || "";
      }
    } catch (err) {
      console.log("err", err);
    }

    const empPayload = {
      ...vals,
      date_of_birth: vals.date_of_birth.format("YYYY-MM-DD"),
      functional_position_id: Number(vals.functional_position_id),
      structural_position_id: vals.structural_position_id
        ? Number(vals.structural_position_id)
        : null,
      group_id: Number(vals.group_id),
      picture_file: pictureFile,
    };
    const usPayload = {
      username: empPayload.npp,
      role_id: empPayload.role_id,
    };

    delete empPayload.username;
    delete empPayload.role_id;

    update({
      variables: {
        emp_pk_columns: {
          id: props.record.id,
        },
        us_pk_columns: {
          id: props.record.user.id,
        },
        emp_set: {
          ...empPayload,
        },
        us_set: {
          ...usPayload,
        },
      },
    }).catch((err) => {
      console.log(err);
    });
  };

  //   File Uploading
  const [profPicFileRes, setProfPicFileRes] = React.useState({
    file: "test",
    url: props.record.picture_file,
  });
  const [profPicFileLoading, setProfPicFileLoading] = React.useState(false);
  const handleProfileUploadChange = (info: any) => {
    if (info.file.status === "uploading") {
      setProfPicFileLoading(true);
      return;
    } else if (info.file.status === "done") {
      setProfPicFileLoading(false);
      setProfPicFileRes(info.fileList[info.fileList.length - 1].response);
      return;
    }
  };
  const profileUploadButton = (
    <div>
      {profPicFileLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className="ant-upload-text">Upload Gambar</div>
    </div>
  );

  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 2 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
  const onDepartmentChange = (value: string) => {
    const dep = deps.find((department) => department.id === value);
    const faculty = dep?.faculty;

    setSelectedFacultyName(faculty?.name || "");
    form.setFieldsValue({ departmentId: value });
  };
  const onFuncPosChange = (value: string) => {
    form.setFieldsValue({ functionalPositionId: value });
  };
  const onStrucPosChange = (value: string) => {
    form.setFieldsValue({ structuralPositionId: value || null });
  };
  const onStatusChange = (value: string) => {
    form.setFieldsValue({ status: value });
  };
  const onGroupChange = (value: string) => {
    form.setFieldsValue({ groupId: value });
  };
  const onRoleChange = (value: string) => {
    form.setFieldsValue({ roleId: value });
  };

  const steps = [
    {
      title: "Data Pribadi",
      content: (
        <>
          <Form.Item
            name={"name"}
            label="Nama"
            rules={[{ required: true }]}
            initialValue={props.record.name}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"address"}
            label="Alamat"
            rules={[{ required: true }]}
            initialValue={props.record.address}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"place_of_birth"}
            label="Tempat Lahir"
            rules={[{ required: true }]}
            initialValue={props.record.place_of_birth}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"date_of_birth"}
            label="Tanggal Lahir"
            rules={[{ required: true }]}
            initialValue={moment(props.record.date_of_birth, "YYYY-MM-DD")}
          >
            <DatePicker
              format="DD/MM/YYYY"
              disabledDate={disabledDate}
              showToday={false}
            />
          </Form.Item>
          <Form.Item
            name={"phone"}
            label="Nomor Telepon"
            rules={[{ required: true }]}
            initialValue={props.record.phone}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"picture_file"}
            label="Foto Profil"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
            initialValue={[{ profPicFileRes }]}
          >
            <Upload
              listType="picture-card"
              showUploadList={false}
              className="avatar-uploader"
              beforeUpload={limitFileSize}
              customRequest={createCustomRequest("employee_profpic")}
              onChange={handleProfileUploadChange}
              accept=".png,.jpeg,.jpg"
            >
              {profPicFileRes.url ? (
                <img
                  src={profPicFileRes.url}
                  alt="avatar"
                  style={{ width: "100%" }}
                />
              ) : (
                profileUploadButton
              )}
            </Upload>
          </Form.Item>
          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Universitas",
      content: (
        <>
          <Form.Item
            name={"npp"}
            label="NPP"
            rules={[{ required: true }]}
            initialValue={props.record.npp}
            extra="Apabila dirubah, data login juga akan terubah."
          >
            <Input />
          </Form.Item>
          <Row className="ant-form-item">
            <Col span="6" className="ant-form-item-label">
              <label>
                <Text>Fakultas</Text>
              </label>
            </Col>
            <Col
              span="16"
              className="ant-form-item-control"
              style={{ display: "flex", justifyContent: "center" }}
            >
              <Text disabled>{selectedFacultyName || "-"}</Text>
            </Col>
          </Row>
          <Form.Item
            name={"department_id"}
            label="Program Studi"
            rules={[{ required: true }]}
            initialValue={props.record.department?.id}
          >
            <Select
              placeholder="Pilih Program Studi"
              onChange={onDepartmentChange}
              loading={deps.length <= 0}
            >
              {deps.map((dep) => (
                <Option key={dep.id} value={dep?.id || ""}>
                  {dep.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"functional_position_id"}
            label="Jb. Fungsional"
            rules={[{ required: true }]}
            initialValue={props.record.functional_position?.id || ""}
          >
            <Select
              placeholder="Pilih Jabatan Fungsional"
              onChange={onFuncPosChange}
              loading={functionalPoses.length <= 0}
            >
              {functionalPoses.map((funcPos) => (
                <Option key={funcPos.id} value={funcPos?.id || ""}>
                  {funcPos.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"structural_position_id"}
            label="Jb. Struktural"
            initialValue={props.record.structural_position?.id || ""}
          >
            <Select
              placeholder="Pilih Jabatan Struktural"
              onChange={onStrucPosChange}
              loading={structuralPoses.length <= 0}
              allowClear
            >
              {structuralPoses.map((strucPos) => (
                <Option key={strucPos.id} value={strucPos?.id || ""}>
                  {strucPos.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"group_id"}
            label="Golongan"
            rules={[{ required: true }]}
            initialValue={props.record.group?.id || ""}
          >
            <Select
              placeholder="Pilih Golongan"
              onChange={onGroupChange}
              loading={groups.length <= 0}
            >
              {groups.map((group) => (
                <Option key={group.id} value={group?.id || ""}>
                  {group.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"status"}
            label="Status"
            rules={[{ required: true }]}
            initialValue={props.record.status}
          >
            <Select placeholder="Pilih Status" onChange={onStatusChange}>
              {statuses.map((status) => (
                <Option key={status.status} value={status.status}>
                  {status.status}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"base_credit"}
            label="Kredit Awal"
            rules={[{ required: true }]}
            initialValue={props.record.base_credit}
          >
            <InputNumber min={0} step={0.1} />
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Akun",
      content: (
        <>
          {isAdmin && (
            <Form.Item
              name={"role_id"}
              label="Role"
              rules={[{ required: true }]}
              initialValue={props.record.user.role?.id}
            >
              <Select
                placeholder="Pilih Role"
                onChange={onRoleChange}
                loading={roles.length <= 0}
              >
                {roles.map((role) => (
                  <Option key={role.id} value={role?.id || ""}>
                    {role.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          )}
          <Form.Item
            name={"email"}
            label="Email"
            rules={[{ required: true }]}
            initialValue={props.record.email}
          >
            <Input />
          </Form.Item>
          {stepNavigator}
        </>
      ),
    },
  ];

  return (
    <>
      <Modal
        title="Hapus Staf"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">Nama: {props.record.name}</Text>
          <Text type="danger">NPP: {props.record.npp}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Staf"
        visible={editModalvisible}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            disabled={currentStep !== steps.length - 1 || updateLoading}
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>

      <Row justify="space-around" style={{ marginTop: 16 }}>
        {(String(auth.data.whoami?.data?.id) === String(props.record.user.id) ||
          isAdmin) && (
          <Col span={14}>
            <Button block onClick={handleEditClick}>
              <EditOutlined /> Ubah
            </Button>
          </Col>
        )}
        {isAdmin && (
          <Col span={14} style={{ marginTop: 16 }}>
            <Button danger block onClick={handleDeleteClick}>
              <DeleteOutlined /> Hapus
            </Button>
          </Col>
        )}

        {isAdmin && (
          <Col span={14} style={{ marginTop: 16 }}>
            <Button
              danger
              block
              onClick={() => {
                Modal.confirm({
                  title: "Reset Password",
                  icon: <ExclamationCircleOutlined />,
                  content: (
                    <>
                      <Text>Yakin mereset password </Text>
                      <Text strong>{props.record.name}</Text>
                      <Text>?</Text>
                    </>
                  ),
                  okText: "Reset",
                  cancelText: "Batal",
                  onOk: handleResetPassword,
                });
              }}
            >
              <RetweetOutlined /> Reset Pass
            </Button>
          </Col>
        )}
      </Row>
    </>
  );
};

export default MutationActionModal;
