import { gql } from "@apollo/client";

export const EMPLOYEE_GAINS = gql`
  query EmployeeGains(
    $where: employees_gains_bool_exp
    $limit: Int
    $order_by: [employees_gains_order_by!]
  ) {
    employee_gains(where: $where, limit: $limit, order_by: $order_by) {
      employee {
        id
        employee_type
        name
        department {
          id
          name
          faculty {
            id
            name
          }
        }
      }
      id
      name
      all_gains
      research_gains
      service_gains
      journal_publication_gains
      seminar_publication_gains
      book_publication_gains
      patent_publication_gains
      self_development_gains
      committee_gains
    }
  }
`;

export const EMPLOYEES_YEARLY_GAINS = gql`
  query EmployeeYearlyGains($year: bigint) {
    employees {
      id
      name
      employee_type
      department {
        id
        name
        faculty {
          id
          name
        }
      }
      yearly_research_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
      yearly_service_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
      yearly_journal_publication_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
      yearly_seminar_publication_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
      yearly_book_publication_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
      yearly_patent_publication_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
      yearly_self_development_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
      yearly_committee_gains(where: { year: { _eq: $year } }) {
        gains
        year
      }
    }
  }
`;

export const EMPLOYEE_RESEARCH_CREDIT_TOP10 = gql`
  query Top10EmployeesByResearchCredit {
    employees(
      order_by: { research_members_aggregate: { sum: { credit_gain: desc } } }
      where: { research_members: {} }
      limit: 10
    ) {
      id
      name
      research_members {
        id
      }
      research_members_aggregate {
        aggregate {
          sum {
            credit_gain
          }
        }
      }
    }
  }
`;

export const EMPLOYEE_SERVICE_CREDIT_TOP10 = gql`
  query Top10EmployeesByServiceCredit {
    employees(
      order_by: { service_members_aggregate: { sum: { credit_gain: desc } } }
      where: { service_members: {} }
      limit: 10
    ) {
      id
      name
      service_members {
        id
      }
      service_members_aggregate {
        aggregate {
          sum {
            credit_gain
          }
        }
      }
    }
  }
`;

export const EMPLOYEE_JOURPUB_CREDIT_TOP10 = gql`
  query Top10EmployeesByJournalPublicationCredit {
    employees(
      order_by: {
        journal_publication_members_aggregate: { sum: { credit_gain: desc } }
      }
      where: { journal_publication_members: {} }
      limit: 10
    ) {
      id
      name
      journal_publication_members {
        id
      }
      journal_publication_members_aggregate {
        aggregate {
          sum {
            credit_gain
          }
        }
      }
    }
  }
`;
