import { gql } from "@apollo/client";

export const DELETE_EMPLOYEES_AND_USER = gql`
  mutation DeleteEmployeesAndUser($employee_id: bigint!, $user_id: bigint!) {
    delete_employees_by_pk(id: $employee_id) {
      employee_type
      id
      n_id_n
      npp
      email
      name
      address
      date_of_birth
      place_of_birth
      status
      base_credit
      initial
      phone
      picture_file
      lecturer_certification_number
      lecturer_certification_file
      department {
        id
        name
        faculty {
          id
          name
        }
      }
      functional_position {
        id
        name
      }
      structural_position {
        id
        name
      }
      group {
        id
        name
      }
      user {
        id
        username
        role {
          id
          name
          key
        }
      }
    }
    delete_users_by_pk(id: $user_id) {
      id
      username
      role {
        id
        name
        key
      }
    }
  }
`;

export const EMPLOYEE_STATUSES = gql`
  query EmployeeStatuses {
    employee_status {
      description
      status
    }
  }
`;

export const EMPLOYEES = gql`
  query Employees($employee_type: employee_type_enum) {
    employees(
      order_by: { created_at: asc }
      where: { employee_type: { _eq: $employee_type } }
    ) {
      employee_type
      id
      n_id_n
      npp
      email
      name
      address
      date_of_birth
      place_of_birth
      status
      base_credit
      initial
      phone
      picture_file
      lecturer_certification_file
      lecturer_certification_number
      department {
        id
        name
        faculty {
          id
          name
        }
      }
      functional_position {
        id
        name
      }
      structural_position {
        id
        name
      }
      group {
        id
        name
      }
      user {
        id
        username
        role {
          id
          name
          key
        }
      }
    }
  }
`;

export const SEARCH_EMPLOYEES = gql`
  query SearchEmployees($term: String) {
    employees(where: { name: { _ilike: $term } }) {
      value: id
      text: name
    }
  }
`;

export const EMPLOYEE_BY_ID = gql`
  query EmployeesByPK($id: bigint!) {
    employees_by_pk(id: $id) {
      employee_type
      id
      n_id_n
      npp
      email
      name
      address
      date_of_birth
      place_of_birth
      status
      base_credit
      initial
      phone
      picture_file
      lecturer_certification_file
      lecturer_certification_number
      department {
        id
        name
        faculty {
          id
          name
        }
      }
      functional_position {
        id
        name
      }
      structural_position {
        id
        name
      }
      group {
        id
        name
      }
      user {
        id
        username
        role {
          id
          name
          key
        }
      }
    }
  }
`;

export const EMPLOYEE_CREDIT_GAIN_GRAPH = gql`
  query EmployeeCreditGainsGraph(
    $id: bigint!
    $start: timestamptz
    $end: timestamptz
    $startAsDate: date
    $endAsDate: date
  ) {
    employees_by_pk(id: $id) {
      base_credit
      research_members_aggregate(
        where: {
          research: {
            deleted_at: { _is_null: true }
            started_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          research {
            id
            title
            started_at
          }
        }
      }

      service_members_aggregate(
        where: {
          service: {
            deleted_at: { _is_null: true }
            start_date: { _gte: $startAsDate, _lte: $endAsDate }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          service {
            id
            title
            start_date
          }
        }
      }

      journal_publication_members_aggregate(
        where: {
          journal_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          journal_publication {
            id
            title
            created_at
          }
        }
      }

      seminar_publication_members_aggregate(
        where: {
          seminar_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          seminar_publication {
            id
            title
            created_at
          }
        }
      }

      book_publication_members_aggregate(
        where: {
          book_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          book_publication {
            id
            title
            created_at
          }
        }
      }

      patent_publication_members_aggregate(
        where: {
          patent_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          patent_publication {
            id
            title
            created_at
          }
        }
      }

      self_development_members_aggregate(
        where: {
          self_development: {
            deleted_at: { _is_null: true }
            start_date: { _gte: $startAsDate, _lte: $endAsDate }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          self_development {
            id
            name
            start_date
          }
        }
      }

      committee_members_aggregate(
        where: {
          committee: {
            deleted_at: { _is_null: true }
            start_date: { _gte: $startAsDate, _lte: $endAsDate }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
        nodes {
          credit_gain
          committee {
            id
            name
            start_date
          }
        }
      }
    }
  }
`;

export const EMPLOYEE_CREDIT_GAIN = gql`
  query EmployeeCreditGains(
    $id: bigint!
    $start: timestamptz
    $end: timestamptz
    $startAsDate: date
    $endAsDate: date
  ) {
    employees_by_pk(id: $id) {
      base_credit
      research_members_aggregate(
        where: {
          research: {
            deleted_at: { _is_null: true }
            started_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }

      service_members_aggregate(
        where: {
          service: {
            deleted_at: { _is_null: true }
            start_date: { _gte: $startAsDate, _lte: $endAsDate }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }

      journal_publication_members_aggregate(
        where: {
          journal_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }

      seminar_publication_members_aggregate(
        where: {
          seminar_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }

      book_publication_members_aggregate(
        where: {
          book_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }

      patent_publication_members_aggregate(
        where: {
          patent_publication: {
            deleted_at: { _is_null: true }
            created_at: { _gte: $start, _lte: $end }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }

      self_development_members_aggregate(
        where: {
          self_development: {
            deleted_at: { _is_null: true }
            start_date: { _gte: $startAsDate, _lte: $endAsDate }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }

      committee_members_aggregate(
        where: {
          committee: {
            deleted_at: { _is_null: true }
            start_date: { _gte: $startAsDate, _lte: $endAsDate }
          }
        }
      ) {
        aggregate {
          sum {
            credit_gain
          }
        }
      }
    }
  }
`;

export const INSERT_EMPLOYEES = gql`
  mutation InsertEmployees($object: employees_insert_input!) {
    insert_employees_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_EMPLOYEES = gql`
  mutation UpdateEmployees(
    $pk_columns: employees_pk_columns_input!
    $_set: employees_set_input
  ) {
    update_employees_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_EMPLOYEES_AND_USER = gql`
  mutation UpdateEmployeesAndUser(
    $emp_pk_columns: employees_pk_columns_input!
    $emp_set: employees_set_input
    $us_pk_columns: users_pk_columns_input!
    $us_set: users_set_input
  ) {
    update_employees_by_pk(pk_columns: $emp_pk_columns, _set: $emp_set) {
      employee_type
      id
      n_id_n
      npp
      email
      name
      address
      date_of_birth
      place_of_birth
      status
      base_credit
      initial
      phone
      picture_file
      lecturer_certification_file
      lecturer_certification_number
      department {
        id
        name
        faculty {
          id
          name
        }
      }
      functional_position {
        id
        name
      }
      structural_position {
        id
        name
      }
      group {
        id
        name
      }
      user {
        id
        username
        role {
          id
          name
          key
        }
      }
    }
    update_users_by_pk(pk_columns: $us_pk_columns, _set: $us_set) {
      id
      username
      role {
        id
        name
        key
      }
    }
  }
`;
