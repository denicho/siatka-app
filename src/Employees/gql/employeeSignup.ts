import cookie from "cookie";

type Payload = {
  username: string;
  password: string;
  role_id: 6;
  department_id: "TF";
  functional_position_id: 4;
  structural_position_id: 3;
  group_id: 1;
  status: string;
  npp: string;
  n_id_n?: string | null;
  initial?: string | null;
  name: string;
  place_of_birth: string;
  date_of_birth: string;
  address: string;
  phone: string;
  email: string;
  base_credit: number;
  picture_file: string;
  lecturer_certification_number?: string | null;
  lecturer_certification_file?: string | null;
  employee_type: "LECTURER" | "STAFF";
};

export const employeeSignup = (data: Payload) =>
  new Promise((resolve, reject) => {
    let token = cookie.parse(document.cookie)["access-token"];

    fetch(`${process.env.REACT_APP_AUTH_HOST}/employees` || "", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => {
        if (res.status === 200) {
          return res.json();
        } else {
          if (res.status === 409) {
            throw Error("NPPUnique");
          } else {
            throw Error("Failed to register employee");
          }
        }
      })
      .then((json) => {
        if (json.id) {
          resolve(json.id);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });

export const employeeChangePassword = (data: {
  id: number;
  password: string;
  confirmPassword: string;
}) =>
  new Promise((resolve, reject) => {
    let token = cookie.parse(document.cookie)["access-token"];

    fetch(`${process.env.REACT_APP_AUTH_HOST}/change-password` || "", {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => {
        if (res.status === 200) {
          resolve();
        } else {
          throw Error("Failed to change password");
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
