import { gql } from "@apollo/client";
import { EmployeeActivites } from "../../globalTypes";

export const EMPLOYEE_ACTIVITIES = gql`
  query EmployeeActivites($employeeId: bigint!) {
    researches(
      where: { research_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      title
    }
    services(
      where: { service_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      title
    }
    journal_publications(
      where: {
        journal_publication_members: { employee_id: { _eq: $employeeId } }
      }
    ) {
      id
      title
    }
    seminar_publications(
      where: {
        seminar_publication_members: { employee_id: { _eq: $employeeId } }
      }
    ) {
      id
      title
    }
    book_publications(
      where: { book_publication_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      title
    }
    patent_publications(
      where: {
        patent_publication_members: { employee_id: { _eq: $employeeId } }
      }
    ) {
      id
      title
    }
    self_developments(
      where: { self_development_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      name
    }
    committees(
      where: { committee_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      name
    }
  }
`;

export const keyMap = {
  researches: "Penelitian",
  services: "Pengabdian",
  journal_publications: "Publikasi Jurnal",
  seminar_publications: "Publikasi Seminar",
  book_publications: "Publikasi Buku",
  patent_publications: "Publikasi Paten",
  self_developments: "Pengembangan Diri",
  committees: "Kepanitiaan",
};

type ResultElement = {
  type: string;
  id: number;
  title: string;
  url: string;
};

export const mapEmployeeActivites = (
  data?: EmployeeActivites,
  type?: string
) => {
  if (!data) {
    return [];
  }

  let result: ResultElement[] = [];

  for (const prop in data) {
    // @ts-ignore
    data[prop].map((el) => {
      result.push({
        // @ts-ignore
        type: keyMap[prop],
        id: el.id,
        title: el.title ? el.title : el.name,
        url: `/dashboard/manage/${prop.split("_").join("")}/${el.id}`,
      });
    });
  }
  console.log({
    result,
    type,
  });
  if (type) {
    return result.filter((res) => res.type === type);
  }

  return result;
};
