import {
  EmployeeCreditGains_employees_by_pk,
  EmployeeCreditGainsGraph_employees_by_pk,
  EmployeeYearlyGains_employees,
  order_by,
} from "../globalTypes";
import moment from "moment";

type CreditRow = {
  sourceName: string;
  value: number;
};

function round2Decimals(num: number): number {
  return Math.round((num + Number.EPSILON) * 100) / 100;
}

function calculateCreditData(
  employee?: EmployeeCreditGains_employees_by_pk | null
): CreditRow[] {
  let rows = [];
  let total = 0;

  if (!employee) {
    return [];
  }

  rows.push({
    sourceName: "Kredit Awal",
    value: round2Decimals(Number(employee.base_credit)),
  });
  total += round2Decimals(Number(employee.base_credit));

  rows.push({
    sourceName: "Kredit Penelitian",
    value: round2Decimals(
      Number(employee.research_members_aggregate.aggregate?.sum?.credit_gain)
    ),
  });
  total += Number(
    employee.research_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Kredit Pengabdian",
    value: round2Decimals(
      Number(employee.service_members_aggregate.aggregate?.sum?.credit_gain)
    ),
  });
  total += Number(
    employee.service_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Kredit Publikasi Jurnal",
    value: round2Decimals(
      Number(
        employee.journal_publication_members_aggregate.aggregate?.sum
          ?.credit_gain
      )
    ),
  });
  total += Number(
    employee.journal_publication_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Kredit Publikasi Seminar",
    value: round2Decimals(
      Number(
        employee.seminar_publication_members_aggregate.aggregate?.sum
          ?.credit_gain
      )
    ),
  });
  total += Number(
    employee.seminar_publication_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Kredit Publikasi Buku",
    value: round2Decimals(
      Number(
        employee.book_publication_members_aggregate.aggregate?.sum?.credit_gain
      )
    ),
  });
  total += Number(
    employee.book_publication_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Kredit Publikasi Paten",
    value: round2Decimals(
      Number(
        employee.patent_publication_members_aggregate.aggregate?.sum
          ?.credit_gain
      )
    ),
  });
  total += Number(
    employee.patent_publication_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Kredit Pengembangan Diri",
    value: round2Decimals(
      Number(
        employee.self_development_members_aggregate.aggregate?.sum?.credit_gain
      )
    ),
  });
  total += Number(
    employee.self_development_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Kredit Kepanitiaan",
    value: round2Decimals(
      Number(employee.committee_members_aggregate.aggregate?.sum?.credit_gain)
    ),
  });
  total += Number(
    employee.committee_members_aggregate.aggregate?.sum?.credit_gain
  );

  rows.push({
    sourceName: "Total",
    value: round2Decimals(total),
  });

  return rows;
}

export function calculateCreditGraphData(
  employee?: EmployeeCreditGainsGraph_employees_by_pk | null
): any {
  let rows = [];
  let total = 0;

  const data = [
    { Tahun: "Januari", "Angka Kredit": 0 },
    { Tahun: "Februari", "Angka Kredit": 0 },
    { Tahun: "Maret", "Angka Kredit": 0 },
    { Tahun: "April", "Angka Kredit": 0 },
    { Tahun: "Mei", "Angka Kredit": 0 },
    { Tahun: "Juni", "Angka Kredit": 0 },
    { Tahun: "Juli", "Angka Kredit": 0 },
    { Tahun: "Agustus", "Angka Kredit": 0 },
    { Tahun: "September", "Angka Kredit": 0 },
    { Tahun: "Oktober", "Angka Kredit": 0 },
    { Tahun: "November", "Angka Kredit": 0 },
    { Tahun: "Desember", "Angka Kredit": 0 },
  ];

  if (!employee) {
    return [];
  }

  for (const property in employee) {
    // @ts-ignore
    if (employee[property] && employee[property].nodes) {
      // @ts-ignore
      const nodes = employee[property].nodes;

      if (Array.isArray(nodes)) {
        nodes.forEach((el) => {
          const fieldKey = el.__typename.replace("_members", "");
          const node = el[fieldKey];

          const startDate = node.start_date
            ? node.start_date
            : node.created_at
            ? node.created_at
            : node.started_at
            ? node.started_at
            : null;

          const momentDate = moment(startDate);

          if (momentDate) {
            const month = momentDate.format("M");
            const monthAsIndex = Number(month) - 1;

            if (data[monthAsIndex]) {
              data[monthAsIndex]["Angka Kredit"] =
                data[monthAsIndex]["Angka Kredit"] + el.credit_gain;
            }
          }
        });
      }
    }
  }

  return data;
}

export type GainType =
  | "research_gains"
  | "service_gains"
  | "journal_publication_gains"
  | "seminar_publication_gains"
  | "book_publication_gains"
  | "patent_publication_gains"
  | "self_development_gains"
  | "committee_gains";

export const computeEmployeeYearlyGains = (
  sort: order_by,
  sortActivity: GainType,
  year: number | null,
  employees?: EmployeeYearlyGains_employees[]
) => {
  if (!employees || !year) {
    return [];
  }

  return employees
    .map((e) => {
      const research_gains =
        e.yearly_research_gains && e.yearly_research_gains.length > 0
          ? e.yearly_research_gains[0].gains
          : 0;
      const service_gains =
        e.yearly_service_gains && e.yearly_service_gains.length > 0
          ? e.yearly_service_gains[0].gains
          : 0;
      const journal_publication_gains =
        e.yearly_journal_publication_gains &&
        e.yearly_journal_publication_gains.length > 0
          ? e.yearly_journal_publication_gains[0].gains
          : 0;
      const seminar_publication_gains =
        e.yearly_seminar_publication_gains &&
        e.yearly_seminar_publication_gains.length > 0
          ? e.yearly_seminar_publication_gains[0].gains
          : 0;
      const book_publication_gains =
        e.yearly_book_publication_gains &&
        e.yearly_book_publication_gains.length > 0
          ? e.yearly_book_publication_gains[0].gains
          : 0;
      const patent_publication_gains =
        e.yearly_patent_publication_gains &&
        e.yearly_patent_publication_gains.length > 0
          ? e.yearly_patent_publication_gains[0].gains
          : 0;
      const self_development_gains =
        e.yearly_self_development_gains &&
        e.yearly_self_development_gains.length > 0
          ? e.yearly_self_development_gains[0].gains
          : 0;
      const committee_gains =
        e.yearly_committee_gains && e.yearly_committee_gains.length > 0
          ? e.yearly_committee_gains[0].gains
          : 0;

      return {
        employee: {
          id: e.id,
          name: e.name,
          department: {
            name: e.department?.name || "",
            faculty: {
              name: e.department?.faculty?.name || "",
            },
          },
        },
        research_gains,
        service_gains,
        journal_publication_gains,
        seminar_publication_gains,
        book_publication_gains,
        patent_publication_gains,
        self_development_gains,
        committee_gains,
        all_gains: round2Decimals(
          research_gains +
            service_gains +
            journal_publication_gains +
            seminar_publication_gains +
            book_publication_gains +
            patent_publication_gains +
            self_development_gains +
            committee_gains
        ),
      };
    })
    .sort((a, b) => {
      console.log("sort", sort);
      if (sort === order_by.asc_nulls_first) {
        return a[sortActivity] - b[sortActivity];
      } else {
        return b[sortActivity] - a[sortActivity];
      }
    });
};

export default calculateCreditData;
