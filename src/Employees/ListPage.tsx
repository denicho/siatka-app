import React from "react";
import { Table, Row, Col, Space, Button, Input } from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  InfoCircleTwoTone,
} from "@ant-design/icons";
import { get } from "lodash-es";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

import ManagementHeading from "../Common/ManagementHeading";
import StaffAddButton from "./AddFormStaff";
import LecturerAddButton from "./AddFormLecturer";
import stringTableSorter from "../utils/stringTableSorter";
import { useQuery } from "@apollo/client";
import { EMPLOYEES } from "./gql/employeesGql";
import {
  Employees,
  Employees_employees,
  EmployeesVariables,
  employee_type_enum,
} from "../globalTypes";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";

const breadcrumbs = (isLecturer: boolean) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/${isLecturer ? "lecturers" : "employees"}`,
    content: `Pengelolaan ${isLecturer ? "Dosen" : "Staf"}`,
  },
];

type ListPageProps = {
  type: "lecturer" | "staff";
};

const EmployeeListPage = (props: ListPageProps) => {
  const { type } = props;
  const isLecturer = type === "lecturer";
  const history = useHistory();
  const employeesRes = useQuery<Employees, EmployeesVariables>(EMPLOYEES, {
    variables: {
      employee_type: isLecturer
        ? employee_type_enum.LECTURER
        : employee_type_enum.STAFF,
    },
  });
  const [searchText, setSearchText] = React.useState("");
  const [searchedCol, setSearchedCol] = React.useState("");
  const searchInput = React.useRef<Input>(null);

  if (
    employeesRes.loading ||
    !employeesRes.data ||
    !employeesRes.data.employees
  ) {
    return <LoadingPlaceholder />;
  }

  const employees = employeesRes.data.employees;

  // Filtering

  const handleSearch = (
    selectedKeys: string,
    confirm: () => void,
    dataIndex: string
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedCol(dataIndex);
  };
  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (
    dataIndex: string | string[],
    label: string
  ) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            if (searchInput !== null) {
              // @ts-ignore
              searchInput.current = node;
            }
          }}
          placeholder={`Cari ${label}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(
              selectedKeys,
              confirm,
              Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
            )
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(
                selectedKeys,
                confirm,
                Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
              )
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Cari
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (
      value: string | number | boolean,
      record: Employees_employees
    ) => {
      const searched = get(
        record,
        Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
      );

      // @ts-ignore
      return searched
        ? // @ts-ignore
          searched
            .toString()
            .toLowerCase()
            // @ts-ignore
            .includes(value.toLowerCase())
        : "";
    },
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => {
          if (searchInput.current) {
            searchInput.current.select();
          }
        }, 100);
      }
    },
    render: (text: string) => {
      const shouldHighlight = Array.isArray(dataIndex)
        ? searchedCol === dataIndex.join(".")
        : searchedCol === dataIndex;

      return shouldHighlight ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      );
    },
  });

  const columns = [
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
      sorter: (a: Employees_employees, b: Employees_employees) => {
        return stringTableSorter(a.name || "", b.name || "");
      },
      ...getColumnSearchProps("name", "Nama"),
    },
    {
      title: "Program Studi",
      dataIndex: ["department", "name"],
      key: "department.name",
      sorter: (a: Employees_employees, b: Employees_employees) => {
        return stringTableSorter(
          a.department?.name || "",
          b.department?.name || ""
        );
      },
      ...getColumnSearchProps(["department", "name"], "Nama"),
    },
    {
      // nidn / npp
      title: "NPP",
      dataIndex: "npp",
      key: "npp",
      sorter: (a: Employees_employees, b: Employees_employees) => {
        return stringTableSorter(a.npp || "", b.npp || "");
      },
      ...getColumnSearchProps("npp", "NPP"),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      sorter: (a: Employees_employees, b: Employees_employees) => {
        return stringTableSorter(a.status || "", b.status || "");
      },
      ...getColumnSearchProps("status", "Status"),
    },
    {
      title: "Detail",
      key: "detail",
      dataIndex: "detail",
      width: 140,
      render: (text: any, record: any, index: any) => (
        <InfoCircleTwoTone onClick={() => {}} />
      ),
    },
  ];

  const handleCreateFinish = () => {
    employeesRes.refetch();
  };

  return (
    <>
      <ManagementHeading
        title={`Pengelolaan ${isLecturer ? "Dosen" : "Staf"}`}
        breadcrumbs={breadcrumbs(isLecturer)}
        rightAddon={
          isLecturer ? (
            <LecturerAddButton onCreateFinish={handleCreateFinish} />
          ) : (
            <StaffAddButton onCreateFinish={handleCreateFinish} />
          )
        }
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  history.push(
                    `/dashboard/manage/${
                      isLecturer ? "lecturers" : "employees"
                    }/${record.id}`
                  );
                },
              };
            }}
            size="middle"
            rowKey="id"
            columns={columns}
            dataSource={employees}
          />
        </Col>
      </Row>
    </>
  );
};

export default EmployeeListPage;
