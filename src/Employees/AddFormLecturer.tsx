import React from "react";
import moment from "moment";
import { flatten, get } from "lodash-es";
import {
  PlusOutlined,
  LoadingOutlined,
  RightOutlined,
  LeftOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import {
  Button,
  Form,
  Input,
  Modal,
  Row,
  Col,
  DatePicker,
  Upload,
  Select,
  Steps,
  message,
  Typography,
  InputNumber,
} from "antd";
import { Store } from "antd/lib/form/interface";

import { createCustomRequest, limitFileSize } from "../Common/uploadFile";
import { useQuery } from "@apollo/client";
import {
  Departments,
  FunctionalPositions,
  Groups,
  StructuralPositions,
  EmployeeStatuses,
  Roles,
  Faculties,
} from "../globalTypes";
import { DEPARTMENTS } from "../Departments/gql/departmentGql";
import { GROUPS } from "../Groups/gql/groupGql";
import { FUNCTIONAL_POS } from "../FunctionalPositions/gql/functionalPositionGql";
import { STRUCTURAL_POS } from "../StructuralPositions/gql/structuralPositionGql";
import { EMPLOYEE_STATUSES } from "./gql/employeesGql";
import { ROLES } from "./gql/rolesGql";
import { employeeSignup } from "./gql/employeeSignup";
import useIsAdmin from "../hooks/useIsAdmin";

const { Step } = Steps;
const { Option } = Select;
const { Text } = Typography;

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

export const normFile = (e: any) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

// @ts-ignore
export function disabledDate(current) {
  // Can not select days after today and today
  return current && current > moment().endOf("day");
}

type Props = {
  onCreateFinish: () => void;
};

const EmployeeAddButton = (props: Props) => {
  const isAdmin = useIsAdmin();
  const depsRes = useQuery<Departments>(DEPARTMENTS);
  const groupsRes = useQuery<Groups>(GROUPS);
  const funcPosRes = useQuery<FunctionalPositions>(FUNCTIONAL_POS);
  const structPosRes = useQuery<StructuralPositions>(STRUCTURAL_POS);
  const rolesRes = useQuery<Roles>(ROLES);
  const emplStatusesRes = useQuery<EmployeeStatuses>(EMPLOYEE_STATUSES);
  const [selectedFacultyName, setSelectedFacultyName] = React.useState<string>(
    ""
  );

  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const deps = depsRes?.data?.departments || [];
  const groups = groupsRes?.data?.groups || [];
  const functionalPoses = funcPosRes?.data?.functional_positions || [];
  const structuralPoses = structPosRes?.data?.structural_positions || [];
  const statuses = emplStatusesRes?.data?.employee_status || [];
  const roles = rolesRes?.data?.roles || [];

  //   File Uploading
  const [profPicFileRes, setProfPicFileRes] = React.useState({
    file: "",
    secure_url: "",
  });
  const resetProfPicFileRes = () => {
    setProfPicFileRes({
      file: "",
      secure_url: "",
    });
  };
  const [profPicFileLoading, setProfPicFileLoading] = React.useState(false);
  const handleProfileUploadChange = (info: any) => {
    if (info.file.status === "uploading") {
      setProfPicFileLoading(true);
      return;
    } else if (info.file.status === "done") {
      setProfPicFileLoading(false);
      setProfPicFileRes(info.fileList[info.fileList.length - 1].response);
      return;
    }
  };
  const profileUploadButton = (
    <div>
      {profPicFileLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className="ant-upload-text">Upload Gambar</div>
    </div>
  );

  // certification
  const [certificationFileList, setCertificationFileList] = React.useState([]);

  const handleCertificationUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setCertificationFileList(fileList);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const onDepartmentChange = (value: string) => {
    const dep = deps.find((department) => department.id === value);
    const faculty = dep?.faculty;

    setSelectedFacultyName(faculty?.name || "");

    form.setFieldsValue({ departmentId: value });
  };
  const onFuncPosChange = (value: string) => {
    form.setFieldsValue({ functionalPositionId: value });
  };
  const onStrucPosChange = (value: string) => {
    form.setFieldsValue({ structuralPositionId: value || null });
  };
  const onStatusChange = (value: string) => {
    form.setFieldsValue({ status: value });
  };
  const onGroupChange = (value: string) => {
    form.setFieldsValue({ groupId: value });
  };
  const onRoleChange = (value: string) => {
    form.setFieldsValue({ roleId: value });
  };

  const [currentStep, setCurrentStep] = React.useState(0);
  const validateList = [
    [
      "name",
      "initial",
      "address",
      "place_of_birth",
      "date_of_birth",
      "phone",
      "picture_file",
    ],
    [
      "department_id",
      "n_id_n",
      "npp",
      "functional_position_id",
      "structural_position_id",
      "group_id",
      "status",
      "base_credit",
      "lecturer_certification_number",
      "lecturer_certification_file",
    ],
    ["email", "role_id"],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };

  const handleOk = () => {
    form.submit();
  };
  const resetAllFormState = () => {
    form.resetFields();
    setCurrentStep(0);
    resetProfPicFileRes();
    setCertificationFileList([]);
  };
  const onFinish = (values: Store) => {
    setLoading(true);
    const vals = form.getFieldsValue(flatten(validateList));

    const payload = {
      ...vals,
      date_of_birth: vals.date_of_birth.format("YYYY-MM-DD"),
      functional_position_id: Number(vals.functional_position_id),
      structural_position_id: vals.structural_position_id
        ? Number(vals.structural_position_id)
        : null,
      group_id: Number(vals.group_id),
      base_credit: Number(vals.base_credit),

      picture_file: get(
        vals,
        `picture_file[${vals.picture_file.length - 1}].response.secure_url`,
        null
      ),

      lecturer_certification_file: get(
        vals,
        `lecturer_certification_file[${
          vals.lecturer_certification_file
            ? vals.lecturer_certification_file.length - 1
            : 0
        }].url`,
        null
      ),

      // When creating, the username and password will be the NPP
      username: vals.npp,
      password: vals.npp,
      employee_type: "LECTURER",
    };

    employeeSignup(payload)
      .then((id) => {
        if (id) {
          resetAllFormState();
          setLoading(false);
          setVisible(false);
          props.onCreateFinish();
        }
      })
      .catch((err) => {
        if (err.message === "NPPUnique") {
          message.error("Gagal membuat data, pastikan NPP belum terpakai.");
        } else {
          message.error("Gagal membuat data.");
        }

        setLoading(false);
      });
  };

  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 2 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
  const steps = [
    {
      title: "Data Pribadi",
      content: (
        <>
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"initial"}
            label="Inisial"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"address"}
            label="Alamat"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"place_of_birth"}
            label="Tempat Lahir"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"date_of_birth"}
            label="Tanggal Lahir"
            rules={[{ required: true }]}
          >
            <DatePicker
              format="DD/MM/YYYY"
              disabledDate={disabledDate}
              showToday={false}
            />
          </Form.Item>
          <Form.Item
            name={"phone"}
            label="Nomor Telepon"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"picture_file"}
            label="Foto Profil"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
          >
            <Upload
              listType="picture-card"
              showUploadList={false}
              className="avatar-uploader"
              beforeUpload={limitFileSize}
              customRequest={createCustomRequest("employee_profpic")}
              onChange={handleProfileUploadChange}
              accept=".png,.jpeg,.jpg"
            >
              {profPicFileRes.secure_url ? (
                <img
                  src={profPicFileRes.secure_url}
                  alt="avatar"
                  style={{ width: "100%" }}
                />
              ) : (
                profileUploadButton
              )}
            </Upload>
          </Form.Item>
          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Universitas",
      content: (
        <>
          <Form.Item name={"n_id_n"} label="NIDN" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"npp"}
            label="NPP"
            rules={[{ required: true }]}
            extra="Akan digunakan sebagai username login."
          >
            <Input />
          </Form.Item>
          <Row className="ant-form-item">
            <Col span="6" className="ant-form-item-label">
              <label>
                <Text>Fakultas</Text>
              </label>
            </Col>
            <Col
              span="16"
              className="ant-form-item-control"
              style={{ display: "flex", justifyContent: "center" }}
            >
              <Text disabled>{selectedFacultyName || "-"}</Text>
            </Col>
          </Row>
          <Form.Item
            name={"department_id"}
            label="Program Studi"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Program Studi"
              onChange={onDepartmentChange}
              loading={deps.length <= 0}
            >
              {deps.map((dep) => (
                <Option key={dep.id} value={dep?.id?.toString() || ""}>
                  {dep.name}
                </Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            name={"functional_position_id"}
            label="Jb. Fungsional"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Jabatan Fungsional"
              onChange={onFuncPosChange}
              loading={functionalPoses.length <= 0}
            >
              {functionalPoses.map((funcPos) => (
                <Option key={funcPos.id} value={funcPos?.id?.toString() || ""}>
                  {funcPos.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"structural_position_id"}
            label="Jb. Struktural"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Jabatan Struktural"
              onChange={onStrucPosChange}
              loading={structuralPoses.length <= 0}
              allowClear
            >
              {structuralPoses.map((strucPos) => (
                <Option
                  key={strucPos.id}
                  value={strucPos?.id?.toString() || ""}
                >
                  {strucPos.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"group_id"}
            label="Golongan"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Golongan"
              onChange={onGroupChange}
              loading={groups.length <= 0}
            >
              {groups.map((group) => (
                <Option key={group.id} value={group?.id?.toString() || ""}>
                  {group.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"status"}
            label="Status"
            rules={[{ required: true }]}
          >
            <Select placeholder="Pilih Status" onChange={onStatusChange}>
              {statuses.map((status) => (
                <Option key={status.status} value={status.status}>
                  {status.status}
                </Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            name={"base_credit"}
            label="Kredit Awal"
            rules={[{ required: true }]}
            initialValue={0}
          >
            <InputNumber min={0} step={0.1} />
          </Form.Item>

          <Form.Item
            name={"lecturer_certification_number"}
            label="No. Sertifikasi"
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"lecturer_certification_file"}
            label="File Sertifikasi"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Upload
              customRequest={createCustomRequest("employee_certification_file")}
              onChange={handleCertificationUploadChange}
              fileList={certificationFileList}
              onRemove={() => setCertificationFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {certificationFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Akun",
      content: (
        <>
          <Form.Item name={"role_id"} label="Role" rules={[{ required: true }]}>
            <Select
              placeholder="Pilih Role"
              onChange={onRoleChange}
              loading={roles.length <= 0}
            >
              {roles.map((role) => (
                <Option key={role.id} value={role?.id?.toString() || ""}>
                  {role.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name={"email"} label="Email" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          {stepNavigator}
        </>
      ),
    },
  ];

  return (
    <>
      <Modal
        title="Tambah Dosen"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            disabled={currentStep !== steps.length - 1}
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>
      {isAdmin && (
        <Button type="primary" onClick={handleAddClick}>
          <PlusOutlined />
          Tambah Dosen
        </Button>
      )}
    </>
  );
};

export default EmployeeAddButton;
