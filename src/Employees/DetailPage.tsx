import React, { useContext } from "react";
import ManagementHeading from "../Common/ManagementHeading";
import { HomeOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/client";
import {
  EMPLOYEE_BY_ID,
  EMPLOYEE_CREDIT_GAIN,
  EMPLOYEE_CREDIT_GAIN_GRAPH,
} from "./gql/employeesGql";
import { Line } from "@ant-design/charts";
import { useParams } from "react-router-dom";
import moment from "moment";
import {
  EmployeesByPK,
  EmployeesByPKVariables,
  EmployeeCreditGains,
  EmployeeCreditGainsVariables,
  EmployeeCreditGainsGraph,
  EmployeeCreditGainsGraphVariables,
} from "../globalTypes";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import {
  Col,
  Image,
  Row,
  Space,
  Tabs,
  Typography,
  Table,
  DatePicker,
} from "antd";
import FieldInfo from "../Common/FieldInfo";
import { format } from "date-fns";
import { id as localeId } from "date-fns/locale";
import MutationActionsModalStaff from "./MutationActionsModalStaff";
import MutationActionsModalLecturer from "./MutationActionsModalLecturer";
import AuthorizedRender from "../AuthorizedRender";
import useIsAdmin from "../hooks/useIsAdmin";
import calculateCreditData, {
  calculateCreditGraphData,
} from "./calculateCreditData";
import ActivityList from "./ActivityList";

const { Link, Text } = Typography;
const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

const breadcrumbs = (id: string, isLecturer: boolean) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/${isLecturer ? "lecturers" : "employees"}`,
    content: `Pengelolaan ${isLecturer ? "Dosen" : "Staf"}`,
  },
  {
    to: `/dashboard/manage/${isLecturer ? "lecturers" : "employees"}/${id}`,
    content: `Detail ${isLecturer ? "Dosen" : "Staf"}`,
  },
];

type DetailPageProps = {
  type: "lecturer" | "staff";
};

const creditColumns = [
  {
    title: "Nama",
    dataIndex: ["sourceName"],
    render: (text: any, record: any, index: any) => {
      return <Text strong={record.sourceName === "Total"}>{text}</Text>;
    },
  },
  {
    title: "Kredit",
    dataIndex: ["value"],
    render: (text: any, record: any, index: any) => (
      <Text strong={record.sourceName === "Total"}>{text}</Text>
    ),
  },
];

const EmployeeDetailPage = (props: DetailPageProps) => {
  const { type } = props;
  const isLecturer = type === "lecturer";
  const isAdmin = useIsAdmin();
  const params = useParams<{ id: string }>();
  const id = params?.id || "";
  const [creditGainFilterDate, setCreditGainFilterDate] = React.useState<any>();
  const [graphicYear, setGraphicYear] = React.useState<any>(moment());

  const creditGainFilterStart = creditGainFilterDate
    ? creditGainFilterDate[0].format("YYYY-MM-DD")
    : undefined;
  const creditGainFilterEnd = creditGainFilterDate
    ? creditGainFilterDate[1].format("YYYY-MM-DD")
    : undefined;

  const employeeRes = useQuery<EmployeesByPK, EmployeesByPKVariables>(
    EMPLOYEE_BY_ID,
    {
      variables: {
        id: Number(id),
      },
      fetchPolicy: "cache-and-network",
    }
  );

  const employeeCreditGainsRes = useQuery<
    EmployeeCreditGains,
    EmployeeCreditGainsVariables
  >(EMPLOYEE_CREDIT_GAIN, {
    variables: {
      id: Number(id),
      start: creditGainFilterStart,
      end: creditGainFilterEnd,
      startAsDate: creditGainFilterStart,
      endAsDate: creditGainFilterEnd,
    },
  });

  const graphStartYear = graphicYear
    ? graphicYear.startOf("year").format("YYYY-MM-DD")
    : undefined;
  const graphEndYear = graphicYear
    ? graphicYear.endOf("year").format("YYYY-MM-DD")
    : undefined;

  const employeeCreditGainsGraph = useQuery<
    EmployeeCreditGainsGraph,
    EmployeeCreditGainsGraphVariables
  >(EMPLOYEE_CREDIT_GAIN_GRAPH, {
    variables: {
      id: Number(id),
      start: graphStartYear,
      end: graphEndYear,
      startAsDate: graphStartYear,
      endAsDate: graphEndYear,
    },
  });

  console.log({
    employeeCreditGainsGraph,
  });

  const creditGains = employeeCreditGainsRes?.data?.employees_by_pk;
  const creditGraph = employeeCreditGainsGraph?.data?.employees_by_pk;
  const employee = employeeRes?.data?.employees_by_pk;

  if (!employeeRes.loading && employeeRes.error) {
    // error handling
    return null;
  }
  if (!employee) {
    if (employeeRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  const dobAsDate = new Date(employee.date_of_birth);

  const creditData = calculateCreditData(creditGains);
  const creditGraphData = calculateCreditGraphData(creditGraph);

  const config = {
    data: creditGraphData,
    height: 400,
    xField: "Tahun",
    yField: "Angka Kredit",
    point: {
      size: 5,
      shape: "diamond",
    },
    label: {
      style: {
        fill: "#aaa",
      },
    },
  };

  return (
    <>
      <ManagementHeading
        title={`Detail ${isLecturer ? "Dosen" : "Staf"}`}
        breadcrumbs={breadcrumbs(id, isLecturer)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Image
            src={employee.picture_file || ""}
            width="100%"
            style={{
              boxShadow:
                "rgba(0, 0, 0, 0.2) 0px 3px 3px -2px, rgba(0, 0, 0, 0.14) 0px 3px 4px 0px, rgba(0, 0, 0, 0.12) 0px 1px 5px 0px",
            }}
          />
          {/* @ts-ignore */}
          {isLecturer ? (
            <MutationActionsModalLecturer record={employee} />
          ) : (
            <MutationActionsModalStaff record={employee} />
          )}
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Universitas" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label="Nama" value={employee.name} />
                    {isLecturer && (
                      <FieldInfo label="Inisial" value={employee.initial} />
                    )}
                    <FieldInfo label="Alamat Email" value={employee.email} />
                    <FieldInfo label="Alamat" value={employee.address} />
                    <FieldInfo
                      label="Tempat & Tanggal Lahir"
                      value={`${employee.place_of_birth}, ${format(
                        dobAsDate,
                        "dd MMMM yyyy",
                        {
                          locale: localeId,
                        }
                      )}`}
                    />
                    <FieldInfo label="No. HP" value={employee.phone} />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label={"NPP"} value={employee.npp} />

                    {isLecturer && (
                      <FieldInfo label={"NIDN"} value={employee.n_id_n} />
                    )}

                    <FieldInfo
                      label="Fakultas"
                      value={employee?.department?.faculty?.name || "-"}
                    />
                    <FieldInfo
                      label="Program Studi"
                      value={employee?.department?.name || "-"}
                    />
                    <FieldInfo
                      label="Jabatan Struktural"
                      value={employee?.structural_position?.name || "-"}
                    />
                    <FieldInfo
                      label="Jabatan Fungsional"
                      value={employee?.functional_position?.name}
                    />
                    <FieldInfo label="Golongan" value={employee?.group?.name} />
                    {isLecturer && (
                      <FieldInfo
                        label="No. Sertifikat"
                        value={employee?.lecturer_certification_number || "-"}
                      />
                    )}
                    {isLecturer && (
                      <FieldInfo
                        label="File Sertifikat"
                        value={
                          employee?.lecturer_certification_file ? (
                            <Link
                              href={employee?.lecturer_certification_file || ""}
                              target="_blank"
                            >
                              Unduh
                            </Link>
                          ) : (
                            "-"
                          )
                        }
                      />
                    )}
                  </Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Informasi Angka Kredit" key="2">
              <Tabs type="card">
                <TabPane tab="Tabel Angka Kredit" key="1">
                  <Row style={{ paddingBottom: 16 }} justify="end">
                    <RangePicker
                      format="DD/MM/YYYY"
                      onChange={(val) => {
                        if (!val) {
                          setCreditGainFilterDate(null);
                        }

                        setCreditGainFilterDate(val);
                      }}
                    />
                  </Row>
                  <Row>
                    <Col flex={1}>
                      <Table
                        columns={creditColumns}
                        dataSource={creditData}
                        size="small"
                      />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tab="Grafik" key="2">
                  <Row justify="end" style={{ paddingBottom: 16 }}>
                    <DatePicker
                      onChange={(value) => {
                        setGraphicYear(value);
                      }}
                      allowClear={false}
                      value={graphicYear}
                      placeholder="Pilih tahun"
                      picker="year"
                    />
                  </Row>
                  <Line {...config} />
                </TabPane>
              </Tabs>
            </TabPane>
            <TabPane tab="Daftar Aktivitas">
              <Row>
                <Col flex={1}>
                  <ActivityList employeeId={employee.id} />
                </Col>
              </Row>
            </TabPane>
            {isAdmin && (
              <TabPane tab="Informasi Login" key="3">
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Username"
                      value={employee.user.username}
                    />
                    <FieldInfo
                      label="Role Name"
                      value={employee.user.role?.name}
                    />
                    <FieldInfo
                      label="Role Key"
                      value={employee.user.role?.key}
                    />
                  </Space>
                </Col>
              </TabPane>
            )}
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default EmployeeDetailPage;
