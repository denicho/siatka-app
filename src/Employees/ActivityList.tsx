import { useQuery } from "@apollo/client";
import React from "react";
import {
  EMPLOYEE_ACTIVITIES,
  mapEmployeeActivites,
} from "./gql/employeeActivities";
import { EmployeeActivites, EmployeeActivitesVariables } from "../globalTypes";
import {
  Col,
  Image,
  Row,
  Space,
  Tabs,
  Typography,
  Table,
  DatePicker,
  Select,
} from "antd";
import { Link } from "react-router-dom";
import { keyMap } from "./gql/employeeActivities";

const { Text } = Typography;
const { Option } = Select;

type Props = {
  employeeId: number;
};

const activityColumns = [
  {
    title: "Jenis",
    dataIndex: ["type"],
    width: 200,
  },
  {
    title: "Judul",
    dataIndex: ["title"],
    render: (text: any, record: any, index: any) => {
      return <Link to={record.url}>{text}</Link>;
    },
  },
];

const ActivityList = (props: Props) => {
  const [selectedCategory, setSelectedCategory] = React.useState<any>();
  const employeeActivites = useQuery<
    EmployeeActivites,
    EmployeeActivitesVariables
  >(EMPLOYEE_ACTIVITIES, {
    variables: {
      employeeId: props.employeeId,
    },
  });

  const filterOptions = Object.keys(keyMap).map((key) => {
    return {
      // @ts-ignore
      name: keyMap[key],
      value: key,
    };
  });

  const employeeActivityData = mapEmployeeActivites(
    employeeActivites?.data,
    selectedCategory
  );

  return (
    <>
      <Row justify="end" style={{ paddingBottom: 16 }}>
        <Select
          placeholder="Pilih Kategori Aktivitas"
          loading={false}
          style={{ width: 300 }}
          allowClear
          onChange={(val) => {
            setSelectedCategory(val);
          }}
        >
          {filterOptions.map((filter) => (
            <Option key={filter.value} value={filter?.name?.toString() || ""}>
              {filter.name}
            </Option>
          ))}
        </Select>
      </Row>
      <Table
        columns={activityColumns}
        dataSource={employeeActivityData}
        size="small"
      />
    </>
  );
};

export default ActivityList;
