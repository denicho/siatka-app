import React from "react";
import AuthContext from "../Contexts/AuthContext";

const useIsAdmin = () => {
  const auth = React.useContext(AuthContext);

  return auth?.data?.whoami?.data?.role?.key === "admin";
};

export default useIsAdmin;
