import React from "react";
import { Modal as AntdModal, Button } from "antd";

type Params = {
  modalProps: any;
};

type ModalProps = {
  onOk: (e: Event | React.MouseEvent<HTMLElement, MouseEvent>) => void;
  onCancel: (e: Event | React.MouseEvent<HTMLElement, MouseEvent>) => void;
  children: React.ReactNode;
};

const useModal = (params: Params) => {
  const [modal, contextHolder] = AntdModal.useModal();
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const Modal = (props: ModalProps) => (
    <AntdModal
      visible={visible}
      footer={[
        <Button key="back" onClick={props.onCancel}>
          Return
        </Button>,
        <Button
          key="submit"
          type="primary"
          loading={loading}
          onClick={props.onOk}
        >
          Submit
        </Button>,
      ]}
      {...params.modalProps}
    >
      {props.children}
    </AntdModal>
  );

  return {
    visible,
    setVisible,
    Modal,
    modal,
    contextHolder,
    loading,
    setLoading,
  };
};

export default useModal;
