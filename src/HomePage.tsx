import React from "react";
import {
  Typography,
  Row,
  Col,
  Card,
  Table,
  Select,
  DatePicker,
  Space,
} from "antd";
import { useLazyQuery, useQuery } from "@apollo/client";
import { Link } from "react-router-dom";

import useIsAdmin from "./hooks/useIsAdmin";
import uajyImg from "./assets/uajy.png";
import { valuesIn } from "lodash";
import {
  EMPLOYEE_RESEARCH_CREDIT_TOP10,
  EMPLOYEE_JOURPUB_CREDIT_TOP10,
  EMPLOYEE_SERVICE_CREDIT_TOP10,
  EMPLOYEE_GAINS,
  EMPLOYEES_YEARLY_GAINS,
} from "./Employees/gql/employeeReportHomepage";
import {
  EmployeeGains,
  EmployeeGainsVariables,
  order_by,
  EmployeeYearlyGains,
  EmployeeYearlyGainsVariables,
} from "./globalTypes";
import { stringify } from "postcss";
import Text from "antd/lib/typography/Text";
import {
  computeEmployeeYearlyGains,
  GainType,
} from "./Employees/calculateCreditData";

const { Title } = Typography;
const { Option } = Select;

const top10options = [
  {
    label: "Semua",
    value: "all",
  },
  {
    label: "Penelitian",
    value: "research",
  },
  {
    label: "Pengabdian",
    value: "service",
  },
  {
    label: "Publikasi Jurnal",
    value: "journal_publication",
  },
  {
    label: "Publikasi Seminar",
    value: "seminar_publication",
  },
  {
    label: "Publikasi Buku",
    value: "book_publication",
  },
  {
    label: "Publikasi Paten",
    value: "patent_publication",
  },
  {
    label: "Pengembangan Diri",
    value: "self_development",
  },
  {
    label: "Kepanitiaan",
    value: "committee",
  },
];

const orderOptions = [
  {
    label: "Kredit Tertinggi",
    value: order_by.desc_nulls_last,
  },
  {
    label: "Kredit Terendah",
    value: order_by.asc_nulls_first,
  },
];

const HomePage = () => {
  const isAdmin = useIsAdmin();
  const [activityOptions, setActivityOptions] = React.useState<string>(
    top10options[0].value
  );
  const [sortOptions, setSortOptions] = React.useState<order_by>(
    orderOptions[0].value
  );
  const [year, setYear] = React.useState<number | null>(null);

  const employeeGainsResult = useQuery<EmployeeGains, EmployeeGainsVariables>(
    EMPLOYEE_GAINS,
    {
      variables: {
        order_by: [
          {
            [`${activityOptions}_gains`]: sortOptions,
          },
        ],
      },
      fetchPolicy: "cache-and-network",
    }
  );

  const employeeYearlyGainsResult = useQuery<
    EmployeeYearlyGains,
    EmployeeYearlyGainsVariables
  >(EMPLOYEES_YEARLY_GAINS, {
    variables: {
      year,
    },
    fetchPolicy: "cache-and-network",
  });

  const employeeGains = employeeGainsResult?.data?.employee_gains || [];
  const computedYearlyEmployeeGains = computeEmployeeYearlyGains(
    sortOptions,
    // @ts-ignore
    `${activityOptions}_gains`,
    year,
    employeeYearlyGainsResult?.data?.employees
  );

  const columns = [
    {
      title: "Nama",
      dataIndex: ["employee", "name"],
      key: "name",
      render: (text: any, record: any, index: any) => (
        <Link
          to={`/dashboard/manage/${
            record.employee_type === "LECTUREER" ? "lecturers" : "employees"
          }/${record.employee.id}`}
        >
          {text}
        </Link>
      ),
    },
    {
      title: "Fakultas",
      dataIndex: ["employee", "department", "faculty", "name"],
      key: "faculty",
    },
    {
      title: "Program Studi",
      dataIndex: ["employee", "department", "name"],
      key: "department",
    },
    {
      title: "Angka Kredit",
      dataIndex: `${activityOptions}_gains`,
      key: "value",
      render: (text: any, record: any, index: any) => (
        <Text>{Number(text)}</Text>
      ),
    },
  ];

  console.log({
    isAdmin,
  });

  return (
    <Col style={{ height: "100%" }}>
      <Row
        align={isAdmin ? "top" : "middle"}
        justify="center"
        style={{ height: isAdmin ? "auto" : "100%", paddingTop: 32 }}
      >
        <Col
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <img src={uajyImg} style={{ width: 50 }} />

          <Title style={{ marginLeft: 32 }} level={2}>
            Selamat Datang di Sistem Informasi Aktivitas Tenaga Kependidikan
          </Title>
        </Col>
      </Row>
      {isAdmin && (
        <Row gutter={[16, 16]} style={{ marginTop: 32 }}>
          <Col span={24}>
            <Card
              title="Laporan Performa Angka Kredit"
              bordered={false}
              style={{ width: "100%" }}
            >
              <Row justify="space-between" style={{ marginBottom: 16 }}>
                <Col>
                  <Space>
                    <Select
                      placeholder="Pilih Jenis Aktivitas"
                      value={activityOptions}
                      style={{ width: 200 }}
                      onChange={(val) => {
                        // @ts-ignore
                        setActivityOptions(val);
                      }}
                    >
                      {top10options.map((opt) => (
                        <Option key={opt.value} value={opt.value}>
                          {opt.label}
                        </Option>
                      ))}
                    </Select>

                    <Select
                      placeholder="Urutkan"
                      onChange={(val) => {
                        // @ts-ignore
                        setSortOptions(val);
                      }}
                      value={sortOptions}
                    >
                      {orderOptions.map((opt) => (
                        <Option key={opt.value} value={opt.value}>
                          {opt.label}
                        </Option>
                      ))}
                    </Select>
                  </Space>
                </Col>
                <Col>
                  <DatePicker
                    onChange={(value) => {
                      setYear(value ? value.year() : null);
                    }}
                    picker="year"
                    placeholder="Pilih tahun"
                  />
                </Col>
              </Row>

              <Table
                size="middle"
                rowKey="id"
                columns={columns}
                pagination={{
                  pageSize: 10,
                }}
                // @ts-ignore
                dataSource={year ? computedYearlyEmployeeGains : employeeGains}
              />
            </Card>
          </Col>
        </Row>
      )}
    </Col>
  );
};

export default HomePage;
