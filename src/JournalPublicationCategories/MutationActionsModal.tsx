import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  InputNumber,
} from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";

import { useMutation } from "@apollo/client";
import {
  DeleteJournalPublicationCategories,
  DeleteJournalPublicationCategoriesVariables,
  JournalPublicationCategories,
  UpdateJournalPublicationCategories,
  UpdateJournalPublicationCategoriesVariables,
} from "../globalTypes";
import {
  DELETE_JOURNAL_PUBLICATION_CATEGORIES,
  JOURNAL_PUBLICATION_CATEGORIES,
  UPDATE_JOURNAL_PUBLICATION_CATEGORIES,
} from "./gql/journalPublicationCategoriesGql";

const { Text } = Typography;

type MutationActionModalProps = {
  record: any;
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const MutationActionModal = (props: MutationActionModalProps) => {
  // Deleting
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };
  const [del, { loading: deleteLoading }] = useMutation<
    DeleteJournalPublicationCategories,
    DeleteJournalPublicationCategoriesVariables
  >(DELETE_JOURNAL_PUBLICATION_CATEGORIES, {
    onCompleted: () => {
      setDeleteModalVisible(false);
    },
    onError: () => {
      setDeleteModalVisible(false);
    },
  });
  const handleDeleteOk = () => {
    del({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<JournalPublicationCategories>({
            query: JOURNAL_PUBLICATION_CATEGORIES,
          });

          if (existing) {
            const filtered = existing.journal_publication_categories.filter(
              (g: any) => g.id !== props.record.id
            );
            cache.writeQuery({
              query: JOURNAL_PUBLICATION_CATEGORIES,
              data: { journal_publication_categories: filtered },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // Editing
  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    form.submit();
  };
  const [update, { loading: editLoading }] = useMutation<
    UpdateJournalPublicationCategories,
    UpdateJournalPublicationCategoriesVariables
  >(UPDATE_JOURNAL_PUBLICATION_CATEGORIES, {
    onCompleted: () => {
      setEditModalVisible(false);
    },
  });
  const handleEditFinish = (values: Store) => {
    update({
      variables: {
        pk_columns: {
          id: props.record.id,
        },
        _set: {
          name: values.name,
          credit: values.credit,
        },
      },
    });
  };

  return (
    <>
      <Modal
        title="Hapus Jenis Publikasi Jurnal"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">ID: {props.record.id}</Text>
          <Text type="danger">Nama: {props.record.name}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Jenis Publikasi Jurnal"
        visible={editModalvisible}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-journalPublicationCategories"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
        >
          <Form.Item
            name={"id"}
            label="ID"
            rules={[{ required: true }]}
            initialValue={props.record.id}
          >
            <Input disabled />
          </Form.Item>
          <Form.Item
            name={"name"}
            label="Nama"
            rules={[{ required: true }]}
            initialValue={props.record.name}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"credit"}
            label="Angka Kredit"
            rules={[{ required: true }]}
            initialValue={props.record.credit}
          >
            <InputNumber min={0} step={0.1} />
          </Form.Item>
        </Form>
      </Modal>

      <Row justify="space-around">
        <Col span={10}>
          <Button block onClick={handleEditClick}>
            <EditOutlined />
          </Button>
        </Col>
        <Col span={10}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined />
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionModal;
