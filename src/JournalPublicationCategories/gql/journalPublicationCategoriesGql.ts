import { gql } from "@apollo/client";

export const DELETE_JOURNAL_PUBLICATION_CATEGORIES = gql`
  mutation DeleteJournalPublicationCategories($id: bigint!) {
    delete_journal_publication_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const JOURNAL_PUBLICATION_CATEGORIES = gql`
  query JournalPublicationCategories {
    journal_publication_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_JOURNAL_PUBLICATION_CATEGORIES = gql`
  mutation InsertJournalPublicationCategories(
    $object: journal_publication_categories_insert_input!
  ) {
    insert_journal_publication_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_JOURNAL_PUBLICATION_CATEGORIES = gql`
  mutation UpdateJournalPublicationCategories(
    $pk_columns: journal_publication_categories_pk_columns_input!
    $_set: journal_publication_categories_set_input
  ) {
    update_journal_publication_categories_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
