import { gql } from "@apollo/client";

export const DELETE_JOURNAL_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation DeleteJournalPublicationActivityPositions($id: Int!) {
    delete_journal_publication_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const JOURNAL_PUBLICATION_ACTIVITY_POSITIONS = gql`
  query JournalPublicationActivityPositions {
    journal_publication_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_JOURNAL_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation InsertJournalPublicationActivityPositions(
    $object: journal_publication_activity_positions_insert_input!
  ) {
    insert_journal_publication_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_JOURNAL_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation UpdateJournalPublicationActivityPositions(
    $pk_columns: journal_publication_activity_positions_pk_columns_input!
    $_set: journal_publication_activity_positions_set_input
  ) {
    update_journal_publication_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
