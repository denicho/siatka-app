import React from "react";
import { Table, Row, Col } from "antd";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import { HomeOutlined } from "@ant-design/icons";
import ManagementHeading from "../Common/ManagementHeading";
import AddButton from "./PositionAddForm";
import MutationActionsModal from "./PositionMutationActionsModal";
import { useQuery } from "@apollo/client";
import { JOURNAL_PUBLICATION_ACTIVITY_POSITIONS } from "./gql/journalPublicationPositionsGql";
import {
  ActivityPositions,
  JournalPublicationActivityPositions,
} from "../globalTypes";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/activity-positions",
    content: "Pengelolaan Posisi Publikasi Jurnal",
  },
];

const JournalPublicationPositionsListPage = () => {
  const activityPositionsRes = useQuery<JournalPublicationActivityPositions>(
    JOURNAL_PUBLICATION_ACTIVITY_POSITIONS
  );

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (
    activityPositionsRes.loading ||
    !activityPositionsRes.data ||
    !activityPositionsRes.data.journal_publication_activity_positions
  ) {
    return <LoadingPlaceholder />;
  }

  const activityPositions =
    activityPositionsRes.data.journal_publication_activity_positions;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Posisi Publikasi Jurnal"
        breadcrumbs={breadcrumbs}
        rightAddon={<AddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={activityPositions}
          />
        </Col>
      </Row>
    </>
  );
};

export default JournalPublicationPositionsListPage;
