import { gql } from "@apollo/client";

export const DELETE_PATENT_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation DeletePatentPublicationActivityPositions($id: Int!) {
    delete_patent_publication_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const PATENT_PUBLICATION_ACTIVITY_POSITIONS = gql`
  query PatentPublicationActivityPositions {
    patent_publication_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_PATENT_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation InsertPatentPublicationActivityPositions(
    $object: patent_publication_activity_positions_insert_input!
  ) {
    insert_patent_publication_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_PATENT_PUBLICATION_ACTIVITY_POSITIONS = gql`
  mutation UpdatePatentPublicationActivityPositions(
    $pk_columns: patent_publication_activity_positions_pk_columns_input!
    $_set: patent_publication_activity_positions_set_input
  ) {
    update_patent_publication_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
