import { gql } from "@apollo/client";

export const DELETE_PATENT_PUBLICATION_CATEGORIES = gql`
  mutation DeletePatentPublicationCategories($id: bigint!) {
    delete_patent_publication_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const PATENT_PUBLICATION_CATEGORIES = gql`
  query PatentPublicationCategories {
    patent_publication_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_PATENT_PUBLICATION_CATEGORIES = gql`
  mutation InsertPatentPublicationCategories(
    $object: patent_publication_categories_insert_input!
  ) {
    insert_patent_publication_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_PATENT_PUBLICATION_CATEGORIES = gql`
  mutation UpdatePatentPublicationCategories(
    $pk_columns: patent_publication_categories_pk_columns_input!
    $_set: patent_publication_categories_set_input
  ) {
    update_patent_publication_categories_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
