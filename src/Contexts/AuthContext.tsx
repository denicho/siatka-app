import React from "react";
import { useCookies } from "react-cookie";

type ContextType = {
  data: {
    session: string;
    whoami: any;
  };
  login: (body: any, onError: (err: Error) => void) => void;
  logout: () => void;
};

const AuthContext = React.createContext<ContextType>({
  data: {
    session: "",
    whoami: null,
  },
  login: () => {},
  logout: () => {},
});

type Props = {
  children: React.ReactNode;
};

export const AuthContextProvider = (props: Props) => {
  const [cookies, setCookie, removeCookie] = useCookies(["access-token"]);
  const accessToken = cookies["access-token"];
  const [user, setUser] = React.useState(null);

  const login = React.useCallback(
    (body, onError: (err: Error) => void) => {
      fetch(`${process.env.REACT_APP_AUTH_HOST}/login`, {
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
      })
        .then((res) => res.json())
        .then((res) => {
          if (res.token) {
            setCookie("access-token", res.token);
          } else {
            throw Error("Invalid");
          }
        })
        .catch((err) => {
          onError(err);
        });
    },
    [setCookie]
  );

  const logout = React.useCallback(() => {
    removeCookie("access-token");

    window.location.href = `${window.location.origin}/login`;
  }, [removeCookie]);

  const fetchWhoAmI = React.useCallback(() => {
    fetch(`${process.env.REACT_APP_AUTH_HOST}/whoami`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setUser(res);
      })
      .catch((err) => {
        removeCookie("access-token");
      });
  }, [accessToken, removeCookie]);

  React.useEffect(() => {
    fetchWhoAmI();
  }, [accessToken, fetchWhoAmI]);

  return (
    <AuthContext.Provider
      value={{
        data: {
          session: accessToken,
          whoami: user,
        },
        login,
        logout,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
