import { gql } from "@apollo/client";

export const DELETE_FACULTIES = gql`
  mutation DeleteFaculties($id: Int!) {
    delete_faculties_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const FACULTIES = gql`
  query Faculties {
    faculties(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_FACULTIES = gql`
  mutation InsertFaculties($object: faculties_insert_input!) {
    insert_faculties_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_FACULTIES = gql`
  mutation UpdateFaculties(
    $pk_columns: faculties_pk_columns_input!
    $_set: faculties_set_input
  ) {
    update_faculties_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      created_at
      updated_at
    }
  }
`;
