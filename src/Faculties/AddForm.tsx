import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, Modal } from "antd";
import { Store } from "antd/lib/form/interface";
import { InsertFaculties, InsertFacultiesVariables } from "../globalTypes";
import { gql, useMutation } from "@apollo/client";
import { INSERT_FACULTIES } from "./gql/facultyGql";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const FacultyAddButton = () => {
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    form.submit();
  };

  const [create] = useMutation<InsertFaculties, InsertFacultiesVariables>(
    INSERT_FACULTIES,
    {
      onCompleted: () => {
        form.resetFields();
        setLoading(false);
        setVisible(false);
      },
      onError: () => {
        setLoading(false);
        setVisible(false);
      },
    }
  );

  const onFinish = (values: Store) => {
    setLoading(true);

    create({
      variables: {
        object: {
          name: values.name,
        },
      },
      update: (cache, { data }) => {
        if (data?.insert_faculties_one) {
          cache.modify({
            fields: {
              faculties(existingFaculties = []) {
                const newFaculties = cache.writeFragment({
                  data: data.insert_faculties_one,
                  fragment: gql`
                    fragment faculties on faculties {
                      id
                      name
                      created_at
                      updated_at
                    }
                  `,
                });
                return [...existingFaculties, newFaculties];
              },
            },
          });
        }
      },
    });
  };

  return (
    <>
      <Modal
        title="Tambah Fakultas"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-faculties"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Fakultas
      </Button>
    </>
  );
};

export default FacultyAddButton;
