import React from "react";
import { Table, Row, Col } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import ManagementHeading from "../Common/ManagementHeading";
import FacultyAddButton from "./AddForm";
import MutationActionsModal from "./MutationActionsModal";
import { useQuery } from "@apollo/client";
import { FACULTIES } from "./gql/facultyGql";
import { Faculties } from "../globalTypes";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/faculties",
    content: "Pengelolaan Fakultas",
  },
];

const FacultyListPage = () => {
  const depsRes = useQuery<Faculties>(FACULTIES);

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (depsRes.loading || !depsRes.data || !depsRes.data.faculties) {
    return <LoadingPlaceholder />;
  }

  const faculties = depsRes.data.faculties;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Fakultas"
        breadcrumbs={breadcrumbs}
        rightAddon={<FacultyAddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={faculties}
          />
        </Col>
      </Row>
    </>
  );
};

export default FacultyListPage;
