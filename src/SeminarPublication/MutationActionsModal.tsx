import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  Upload,
  Select,
  Steps,
  InputNumber,
  Divider,
  Spin,
} from "antd";
import moment from "moment";
import { flatten, debounce } from "lodash-es";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  MinusCircleOutlined,
  RightOutlined,
  UploadOutlined,
  LeftOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";

import { createCustomRequest } from "../Common/uploadFile";
import {
  ActivityPositions,
  SeminarPublicationByPK_seminar_publications_by_pk,
  SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members,
  SeminarPublicationCategories,
  SeminarPublications,
  SearchEmployees,
  SearchEmployeesVariables,
  SoftDeleteSeminarPublicationByPK,
  SoftDeleteSeminarPublicationByPKVariables,
  UpdateSeminarPublication,
  UpdateSeminarPublicationVariables,
  DeleteSeminarPublicationMembers,
  DeleteSeminarPublicationMembersVariables,
  InsertSeminarPublicationMember,
  InsertSeminarPublicationMemberVariables,
  SeminarPublicationActivityPositions,
} from "../globalTypes";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";

import { useHistory } from "react-router-dom";
import {
  DELETE_SEMINAR_PUBLICATION_MEMBERS,
  INSERT_SEMINAR_PUBLICATION_MEMBER,
  SEMINAR_PUBLICATIONS,
  SOFT_DELETE_SEMINAR_PUBLICATION_BY_PK,
  UPDATE_SEMINAR_PUBLICATION,
} from "./gql/seminarPublicationGql";
import { SEMINAR_PUBLICATION_CATEGORIES } from "../SeminarPublicationCategories/gql/seminarPublicationCategoriesGql";
import { normFile } from "./AddForm";
import { SEARCH_EMPLOYEES } from "../Employees/gql/employeesGql";
import normalizeMembers from "../utils/normalizeMembers";
import { SEMINAR_PUBLICATION_ACTIVITY_POSITIONS } from "../SeminarPublicationCategories/gql/seminarPublicationPositionsGql";

const { Text } = Typography;
const { Step } = Steps;
const { Option } = Select;

const layout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 24 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

type MutationActionModalProps = {
  record: SeminarPublicationByPK_seminar_publications_by_pk;
  onEditFinish?: (id: number) => void;
};

const getExternalMemberInitialValues = (
  members: SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members[]
) => {
  return members
    .filter((member) => !member.employee)
    .map((extMember) => ({
      activity_position_id:
        extMember.seminar_publication_activity_position?.id.toString(),
      external_members: extMember.external_author,
    }));
};

const getInternalMemberInitialValues = (
  members: SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members[]
) => {
  return members
    .filter((mem) => Boolean(mem.employee))
    .map((intMember) => ({
      activity_position_id:
        intMember.seminar_publication_activity_position?.id.toString(),
      internal_member: [
        {
          value: intMember.employee?.id,
          key: intMember.employee?.id,
          label: intMember.employee?.name,
        },
      ],
    }));
};

const MutationActionsModalSeminarPublication = (
  props: MutationActionModalProps
) => {
  console.log(props.record);

  const history = useHistory();

  // deleting
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const [deleteLoading, setDeleteLoading] = React.useState(false);
  const [del] = useMutation<
    SoftDeleteSeminarPublicationByPK,
    SoftDeleteSeminarPublicationByPKVariables
  >(SOFT_DELETE_SEMINAR_PUBLICATION_BY_PK, {
    onCompleted: () => {
      setDeleteLoading(false);
      history.push("/dashboard/manage/seminarpublications");
    },
  });
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };
  const handleDeleteOk = () => {
    setDeleteLoading(true);
    del({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<SeminarPublications>({
            query: SEMINAR_PUBLICATIONS,
          });

          if (existing) {
            const newSeminarPublications = existing.seminar_publications.filter(
              (data) => data.id !== props.record.id
            );

            cache.writeQuery({
              query: SEMINAR_PUBLICATIONS,
              data: { seminar_publications: newSeminarPublications },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // editing
  const [searchEmployees, searchEmployeesRes] = useLazyQuery<
    SearchEmployees,
    SearchEmployeesVariables
  >(SEARCH_EMPLOYEES, {
    onCompleted: (data) => {},
  });
  const activityPosRes = useQuery<SeminarPublicationActivityPositions>(
    SEMINAR_PUBLICATION_ACTIVITY_POSITIONS
  );

  const categoriesRes = useQuery<SeminarPublicationCategories>(
    SEMINAR_PUBLICATION_CATEGORIES
  );
  const categories = categoriesRes?.data?.seminar_publication_categories || [];
  const activityPoses =
    activityPosRes?.data?.seminar_publication_activity_positions || [];
  const searchedEmployees = searchEmployeesRes?.data?.employees || [];
  const [selectedEmployeeIDs, setSelectedEmployeeIDs] = React.useState<
    number[]
  >(
    props.record.seminar_publication_members
      .map((mem) => mem.employee?.id)
      .filter(Boolean)
  );
  const filteredEmployees = searchedEmployees
    ? searchedEmployees.filter(
        (emp) => !selectedEmployeeIDs.includes(emp.value)
      )
    : [];

  const selectedCategoryCredit = props.record?.credit_multiplier || 0;
  const [isTotalCreditError, setIsTotalCreditError] =
    React.useState<boolean>(true);

  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const [editLoading, setEditLoading] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    if (isTotalCreditError) {
      return;
    }

    form.submit();
  };

  const validateList = [
    [
      "title",
      "category_id",
      "seminar_name",
      "seminar_no",
      "seminar_volume",
      "seminar_year",
      "seminar_page_no",
      "isbn",
      "issn",
    ],
    [
      "proceeding_cover_file",
      "proceeding_toc_file",
      "certificate_file",
      "paper_file",
      "decree_file",
    ],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const [currentStep, setCurrentStep] = React.useState(0);
  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 2 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );

  // Uploads
  const [coverFileList, setCoverFileList] = React.useState([
    {
      name: "cert.jpg",
      type: "image/jpeg",
      uid: "1",
      url: props.record.proceeding_cover_file || "",
      size: 0,
    },
  ]);
  const handleCoverUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setCoverFileList(fileList);
  };

  const [tocFileList, setToCFileList] = React.useState([
    {
      name: "cert.jpg",
      type: "image/jpeg",
      uid: "1",
      url: props.record.proceeding_cover_file || "",
      size: 0,
    },
  ]);
  const handleToCUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setToCFileList(fileList);
  };

  const [paperFileList, setPaperFileList] = React.useState([
    {
      name: "cert.jpg",
      type: "image/jpeg",
      uid: "1",
      url: props.record.proceeding_cover_file || "",
      size: 0,
    },
  ]);
  const handlePaperUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setPaperFileList(fileList);
  };

  const [certificateFileList, setCertificateFileList] = React.useState(
    props.record.certificate_file
      ? [
          {
            name: "cert.jpg",
            type: "image/jpeg",
            uid: "1",
            url: props.record.certificate_file || "",
            size: 0,
          },
        ]
      : []
  );
  const handleCertificateUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setCertificateFileList(fileList);
  };

  const [decreeFileList, setDecreeFileList] = React.useState(
    props.record.decree_file
      ? [
          {
            name: "cert.jpg",
            type: "image/jpeg",
            uid: "1",
            url: props.record.decree_file || "",
            size: 0,
          },
        ]
      : []
  );
  const handleDecreeUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setDecreeFileList(fileList);
  };
  const steps = [
    {
      title: "Data Publikasi Seminar",
      content: (
        <>
          <Form.Item
            name={"title"}
            label="Judul Publikasi"
            rules={[{ required: true }]}
            initialValue={props.record.title}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"category_id"}
            label="Kategori"
            rules={[{ required: true }]}
            extra="Kategori tidak dapat diubah karena akan memengaruhi angka kredit."
            initialValue={props.record.seminar_publication_category?.id.toString()}
          >
            <Select
              placeholder="Pilih Kategori Publikasi Seminar"
              loading={categories.length <= 0}
              disabled
            >
              {categories.map((ctg) => (
                <Option key={ctg.id} value={ctg?.id?.toString() || ""}>
                  {ctg.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"seminar_name"}
            label="Nama Seminar"
            rules={[{ required: true }]}
            initialValue={props.record.seminar_name}
          >
            <Input />
          </Form.Item>
          {/* <Form.Item
            name={"seminar_no"}
            label="Nomor Seminar"
            rules={[{ required: true }]}
            initialValue={props.record.seminar_no}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"seminar_volume"}
            label="Volume Seminar"
            rules={[{ required: true }]}
            initialValue={props.record.seminar_volume}
          >
            <Input />
          </Form.Item> */}
          <Form.Item
            name={"seminar_year"}
            label="Tahun"
            rules={[{ required: true }]}
            initialValue={moment(
              new Date(`${props.record.seminar_year}-01-01`),
              "YYYY"
            )}
          >
            <DatePicker format="YYYY" picker="year" />
          </Form.Item>
          <Form.Item
            name={"seminar_page_no"}
            label="No. Halaman"
            rules={[{ required: true }]}
            initialValue={props.record.seminar_page_no}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"isbn"}
            label="ISBN"
            initialValue={props.record.isbn}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"issn"}
            label="ISSN"
            initialValue={props.record.issn}
          >
            <Input />
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Berkas",
      content: (
        <>
          <Form.Item
            name={"proceeding_cover_file"}
            label="Cover"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
            initialValue={[
              {
                name: "File",
                type: "image.jpg",
                uid: "1",
                // @ts-ignore
                status: "done",
                url: props.record.proceeding_cover_file,
                size: 0,
              },
            ]}
          >
            <Upload
              customRequest={createCustomRequest("seminar_cover_file")}
              onChange={handleCoverUploadChange}
              fileList={coverFileList}
              onRemove={() => setCoverFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {coverFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"proceeding_toc_file"}
            label="Daftar Isi"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
            initialValue={[
              {
                name: "File",
                type: "image.jpg",
                uid: "1",
                // @ts-ignore
                status: "done",
                url: props.record.proceeding_toc_file,
                size: 0,
              },
            ]}
          >
            <Upload
              customRequest={createCustomRequest("seminar_toc_file")}
              onChange={handleToCUploadChange}
              fileList={tocFileList}
              onRemove={() => setToCFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {tocFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"paper_file"}
            label="Paper"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
            initialValue={[
              {
                name: "File",
                type: "image.jpg",
                uid: "1",
                // @ts-ignore
                status: "done",
                url: props.record.paper_file,
                size: 0,
              },
            ]}
          >
            <Upload
              customRequest={createCustomRequest("seminar_paper_file")}
              onChange={handlePaperUploadChange}
              fileList={paperFileList}
              onRemove={() => setPaperFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {paperFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"certificate_file"}
            label="Sertifikat"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            initialValue={
              props.record.certificate_file
                ? [
                    {
                      name: "File",
                      type: "image.jpg",
                      uid: "1",
                      // @ts-ignore
                      status: "done",
                      url: props.record.certificate_file,
                      size: 0,
                    },
                  ]
                : undefined
            }
          >
            <Upload
              customRequest={createCustomRequest("seminar_certificate_file")}
              onChange={handleCertificateUploadChange}
              fileList={certificateFileList}
              onRemove={() => setCertificateFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {certificateFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"decree_file"}
            label="SPPD / Surat Tugas"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            initialValue={
              props.record.decree_file
                ? [
                    {
                      name: "File",
                      type: "image.jpg",
                      uid: "1",
                      // @ts-ignore
                      status: "done",
                      url: props.record.decree_file,
                      size: 0,
                    },
                  ]
                : undefined
            }
          >
            <Upload
              customRequest={createCustomRequest("seminar_decree_file")}
              onChange={handleDecreeUploadChange}
              fileList={decreeFileList}
              onRemove={() => setDecreeFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {decreeFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Penulis",
      content: (
        <>
          <Divider orientation="center" plain>
            Penulis Internal
          </Divider>
          <Row justify="center" style={{ paddingBottom: 16 }}>
            <Typography.Text>Angka Kredit Terbagi:&nbsp;</Typography.Text>
            <Typography.Text strong>{selectedCategoryCredit}</Typography.Text>
          </Row>
          <Form.List
            name="internal_members"
            rules={[
              {
                validator: async (_, names) => {
                  if (!names || names.length < 1) {
                    return Promise.reject(new Error("Minimal 1 penulis"));
                  }
                },
              },
            ]}
            initialValue={getInternalMemberInitialValues(
              props.record.seminar_publication_members
            )}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <>
                    <div>
                      <Typography.Text type="secondary" underline>
                        Kredit didapatkan:{" "}
                        {Number(
                          form.getFieldValue([
                            "internal_members",
                            field.name,
                            "credit_percentage",
                          ]) * selectedCategoryCredit
                        ) / 100}
                      </Typography.Text>
                    </div>
                    <div
                      key={field.key}
                      style={{
                        display: "flex",
                        alignItems: "baseline",
                        marginBottom: 8,
                      }}
                    >
                      <Form.Item
                        {...field}
                        name={[field.name, "activity_position_id"]}
                        fieldKey={[field.fieldKey, "activity_position_id"]}
                        rules={[
                          { required: true, message: "Posisi wajib diisi" },
                        ]}
                        style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                      >
                        <Select
                          placeholder="Posisi"
                          onChange={() => {}}
                          loading={activityPoses.length <= 0}
                          allowClear
                        >
                          {activityPoses.map((activity) => (
                            <Option
                              key={activity.id}
                              value={activity?.id?.toString() || ""}
                            >
                              {activity.name}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "internal_member"]}
                        fieldKey={[field.fieldKey, "internal_member"]}
                        rules={[
                          { required: true, message: "Nama wajib diisi" },
                        ]}
                        style={{
                          width: "100%",
                          marginRight: 8,
                          marginBottom: 8,
                        }}
                      >
                        <Select
                          maxTagCount={1}
                          labelInValue
                          mode="multiple"
                          value={form.getFieldValue([
                            "internal_members",
                            field.fieldKey,
                            "internal_member",
                          ])}
                          placeholder="Cari nama penulis internal"
                          notFoundContent={
                            searchEmployeesRes.loading ? (
                              <Spin size="small" />
                            ) : null
                          }
                          filterOption={false}
                          onChange={(value) => {
                            const newValue = value[value.length - 1];

                            form.setFieldsValue({
                              internal_members: form
                                .getFieldValue("internal_members")
                                .map(
                                  (
                                    x: Array<{
                                      internal_member: Array<{
                                        value: string | number;
                                        label: string;
                                        key: string;
                                      }>;
                                    }>,
                                    i: number
                                  ) => {
                                    if (i === field.name) {
                                      return {
                                        ...x,
                                        internal_member: newValue
                                          ? [newValue]
                                          : [],
                                      };
                                    }
                                    return x;
                                  }
                                ),
                            });
                          }}
                          onSearch={debounce((value) => {
                            searchEmployees({
                              variables: {
                                term: `%${value}%`,
                              },
                            });
                          }, 400)}
                        >
                          {filteredEmployees.map((d) => (
                            <Option key={`${d.value}a`} value={d.value}>
                              {d.text}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "credit_percentage"]}
                        fieldKey={[field.fieldKey, "credit_percentage"]}
                        initialValue={0}
                        rules={[
                          {
                            required: true,
                            message: "Persentase Angka Kredit wajib diisi",
                          },
                        ]}
                        style={{
                          marginRight: 8,
                          marginBottom: 8,
                          width: 240,
                        }}
                        validateStatus={
                          isTotalCreditError ? "warning" : undefined
                        }
                        help={
                          isTotalCreditError ? "Total harus 100%" : undefined
                        }
                      >
                        <InputNumber
                          min={0}
                          max={100}
                          style={{ width: "100%" }}
                          formatter={(value) => `${value}%`}
                          parser={(value: any) => value.replace("%", "")}
                        />
                      </Form.Item>
                      <MinusCircleOutlined onClick={() => remove(field.name)} />
                    </div>
                  </>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Internal
                  </Button>
                  <Form.ErrorList errors={errors} />
                </Form.Item>
              </>
            )}
          </Form.List>

          {/* external */}
          <Divider orientation="center" plain>
            Penulis Eksternal
          </Divider>
          <Form.List
            name="external_members"
            initialValue={getExternalMemberInitialValues(
              props.record.seminar_publication_members
            )}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <div
                    key={field.key}
                    style={{
                      display: "flex",
                      alignItems: "baseline",
                      marginBottom: 8,
                    }}
                  >
                    <Form.Item
                      {...field}
                      name={[field.name, "activity_position_id"]}
                      fieldKey={[field.fieldKey, "activity_position_id"]}
                      rules={[
                        { required: true, message: "Posisi wajib diisi" },
                      ]}
                      style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                    >
                      <Select
                        placeholder="Posisi"
                        onChange={() => {}}
                        loading={activityPoses.length <= 0}
                        allowClear
                      >
                        {activityPoses.map((activity) => (
                          <Option
                            key={activity.id}
                            value={activity?.id?.toString() || ""}
                          >
                            {activity.name}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      {...field}
                      name={[field.name, "external_members"]}
                      fieldKey={[field.fieldKey, "external_members"]}
                      rules={[{ required: true, message: "Nama wajib diisi" }]}
                      style={{
                        width: "100%",
                        marginRight: 8,
                        marginBottom: 8,
                      }}
                    >
                      <Input placeholder="Nama" />
                    </Form.Item>

                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                  </div>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Eksternal
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          {stepNavigator}
        </>
      ),
    },
  ];

  const [
    updateSeminarPublication,
    { loading: updateSeminarPublicationLoading },
  ] = useMutation<UpdateSeminarPublication, UpdateSeminarPublicationVariables>(
    UPDATE_SEMINAR_PUBLICATION
  );
  const [deleteSeminarPublicationMembers, { loading: deleteResMemLoading }] =
    useMutation<
      DeleteSeminarPublicationMembers,
      DeleteSeminarPublicationMembersVariables
    >(DELETE_SEMINAR_PUBLICATION_MEMBERS);
  const [insertSeminarPublicationMember, { loading: insertResMemLoading }] =
    useMutation<
      InsertSeminarPublicationMember,
      InsertSeminarPublicationMemberVariables
    >(INSERT_SEMINAR_PUBLICATION_MEMBER);
  const handleEditFinish = (values: Store) => {
    const vals = form.getFieldsValue(flatten(validateList));

    const coverFileVal = vals.proceeding_cover_file;
    const tocFileVal = vals.proceeding_toc_file;
    const paperFileVal = vals.paper_file;
    const certificateFileVal = vals.certificate_file;
    const decreeFileVal = vals.decree_file;

    let coverFile = "";
    let tocFile = "";
    let paperFile = "";
    let certificateFile = "";
    let decreeFile = "";

    try {
      if (coverFileVal && coverFileVal.length > 0) {
        // berarti ganti file
        coverFile = coverFileVal[coverFileVal.length - 1].url;
      }
      if (tocFileVal && tocFileVal.length > 0) {
        // berarti ganti file
        tocFile = tocFileVal[tocFileVal.length - 1].url;
      }
      if (paperFileVal && paperFileVal.length > 0) {
        // berarti ganti file
        paperFile = paperFileVal[paperFileVal.length - 1].url;
      }
      if (certificateFileVal && certificateFileVal.length > 0) {
        // berarti ganti file
        certificateFile = certificateFileVal[certificateFileVal.length - 1].url;
      }
      if (decreeFileVal && decreeFileVal.length > 0) {
        // berarti ganti file
        decreeFile = decreeFileVal[decreeFileVal.length - 1].url;
      }
    } catch (err) {
      console.log("err", err);
    }

    console.log({
      vals,
      values,
    });

    const updateSeminarPublicationPayload = {
      ...vals,
      category_id: Number(vals.category_id),
      seminar_year: vals.seminar_year.year(),
      proceeding_cover_file: coverFile,
      proceeding_toc_file: tocFile,
      paper_file: paperFile,
      certificate_file: certificateFile,
      decree_file: decreeFile,
    };

    Promise.all([
      updateSeminarPublication({
        variables: {
          id: props.record.id,
          _set: {
            ...updateSeminarPublicationPayload,
          },
        },
      }),
      new Promise((resolve, reject) => {
        deleteSeminarPublicationMembers({
          variables: {
            seminar_publication_id: props.record.id,
          },
        })
          .then(() => {
            const insertSeminarPublicationMembersPayload = normalizeMembers(
              props.record.id,
              "seminar_publication_id",
              values.internal_members,
              values.external_members,
              selectedCategoryCredit
            );

            insertSeminarPublicationMember({
              variables: {
                objects: insertSeminarPublicationMembersPayload,
              },
            }).then(() => {
              resolve(true);
            });
          })
          .catch((err) => {
            reject(err);
          });
      }),
    ])
      .then(() => {
        setCurrentStep(0);
        form.resetFields();
        setIsTotalCreditError(true);
        setEditLoading(false);
        setEditModalVisible(false);
        props.onEditFinish && props.onEditFinish(props.record.id);
      })
      .catch((err) => {
        setEditLoading(false);
        setEditModalVisible(false);
      });
  };

  return (
    <>
      <Modal
        title="Hapus Publikasi Seminar"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">Judul: {props.record.title}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Publikasi Seminar"
        visible={editModalvisible}
        width={768}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            disabled={
              currentStep !== steps.length - 1 ||
              updateSeminarPublicationLoading ||
              deleteResMemLoading ||
              insertResMemLoading
            }
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
          onValuesChange={(changed, all) => {
            // Commented bcs category id cant be changed
            // if (changed.category_id && all.category_id) {
            //   const selectedCtg = categories.find(
            //     (ctg) => ctg.id === Number(changed.category_id)
            //   );
            //   setSelectedCategoryCredit(
            //     selectedCtg?.credit ? selectedCtg?.credit : 0
            //   );
            // }
            if (changed.internal_members && all.internal_members) {
              const truthyInternalMembers =
                all.internal_members.filter(Boolean);

              const creditPercentages = truthyInternalMembers.reduce(
                (acc: number, curr: any) => {
                  return acc + Number(curr.credit_percentage);
                },
                0
              );

              if (creditPercentages !== 100) {
                setIsTotalCreditError(true);
              } else {
                setIsTotalCreditError(false);
              }
            }

            // This will trigger the filters so that user cant pick the same employee twice
            if (changed.internal_members && all.internal_members) {
              const selectedEmployeeIds = all.internal_members
                .filter(Boolean)
                .map((member: any) => {
                  const id =
                    (member.internal_member &&
                      member.internal_member.length > 0 &&
                      member.internal_member[0]?.value) ||
                    0;

                  return id;
                })
                .filter(Boolean);

              setSelectedEmployeeIDs(selectedEmployeeIds);
            }
          }}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>

      <Row justify="space-around" style={{ marginTop: 16 }}>
        <Col span={14}>
          <Button block onClick={handleEditClick}>
            <EditOutlined /> Ubah
          </Button>
        </Col>
        <Col span={14} style={{ marginTop: 16 }}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined /> Hapus
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionsModalSeminarPublication;
