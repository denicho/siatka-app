import React from "react";
import { get } from "lodash-es";
import { Table, Row, Col, Space, Button, Input } from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  InfoCircleTwoTone,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

import ManagementHeading from "../Common/ManagementHeading";
// import StaffAddButton from "./AddFormStaff";
// import LecturerAddButton from "./AddFormLecturer";
import stringTableSorter from "../utils/stringTableSorter";
import { useQuery } from "@apollo/client";
import { SEMINAR_PUBLICATIONS } from "./gql/seminarPublicationGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import {
  SeminarPublications,
  SeminarPublications_seminar_publications,
} from "../globalTypes";
import SeminarPublicationAddButton from "./AddForm";
import numberTableSorter from "../utils/numberTableSorter";

type Props = {
  id: string;
};

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/seminarpublications`,
    content: `Pengelolaan Publikasi Seminar`,
  },
];

const EmployeeListPage = () => {
  const history = useHistory();
  const seminarPublicationsRes = useQuery<SeminarPublications>(
    SEMINAR_PUBLICATIONS
  );
  const [searchText, setSearchText] = React.useState("");
  const [searchedCol, setSearchedCol] = React.useState("");
  const searchInput = React.useRef<Input>(null);

  if (
    seminarPublicationsRes.loading ||
    !seminarPublicationsRes.data ||
    !seminarPublicationsRes.data.seminar_publications
  ) {
    return <LoadingPlaceholder />;
  }

  const seminarPublications = seminarPublicationsRes.data.seminar_publications;

  // Filtering

  const handleSearch = (
    selectedKeys: string,
    confirm: () => void,
    dataIndex: string
  ) => {
    console.log("handle search", {
      selectedKeys,
      dataIndex,
    });
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedCol(dataIndex);
  };
  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (
    dataIndex: string | string[],
    label: string
  ) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            if (searchInput !== null) {
              // @ts-ignore
              searchInput.current = node;
            }
          }}
          placeholder={`Cari ${label}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(
              selectedKeys,
              confirm,
              Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
            )
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(
                selectedKeys,
                confirm,
                Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
              )
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Cari
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (
      value: string | number | boolean,
      record: SeminarPublications_seminar_publications
    ) => {
      const searched = get(
        record,
        Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
      );

      // @ts-ignore
      return searched
        ? // @ts-ignore
          searched
            .toString()
            .toLowerCase()
            // @ts-ignore
            .includes(value.toLowerCase())
        : "";
    },
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => {
          if (searchInput.current) {
            searchInput.current.select();
          }
        }, 100);
      }
    },
    render: (text: string) => {
      const shouldHighlight = Array.isArray(dataIndex)
        ? searchedCol === dataIndex.join(".")
        : searchedCol === dataIndex;

      return shouldHighlight ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      );
    },
  });

  const columns = [
    {
      title: "Judul Publikasi",
      dataIndex: "title",
      key: "title",
      sorter: (
        a: SeminarPublications_seminar_publications,
        b: SeminarPublications_seminar_publications
      ) => {
        return stringTableSorter(a.title || "", b.title || "");
      },
      ...getColumnSearchProps("title", "Judul"),
    },
    {
      title: "Kategori",
      dataIndex: ["seminar_publication_category", "name"],
      key: "seminar_publication_category.name",
      sorter: (
        a: SeminarPublications_seminar_publications,
        b: SeminarPublications_seminar_publications
      ) => {
        return stringTableSorter(
          a.seminar_publication_category?.name || "",
          b.seminar_publication_category?.name || ""
        );
      },
      ...getColumnSearchProps(
        ["seminar_publication_category", "name"],
        "Kategori"
      ),
    },
    {
      title: "Nama Seminar",
      dataIndex: "seminar_name",
      key: "seminar_name",
      sorter: (
        a: SeminarPublications_seminar_publications,
        b: SeminarPublications_seminar_publications
      ) => {
        return stringTableSorter(a.seminar_name || "", b.seminar_name || "");
      },
      ...getColumnSearchProps("seminar_name", "Nama Seminar"),
    },
    {
      title: "Tahun",
      dataIndex: "seminar_year",
      key: "seminar_year",
      sorter: (
        a: SeminarPublications_seminar_publications,
        b: SeminarPublications_seminar_publications
      ) => {
        return numberTableSorter(a.seminar_year || 0, b.seminar_year || 0);
      },
      ...getColumnSearchProps("seminar_year", "Tahun"),
    },
    {
      title: "Detail",
      key: "detail",
      dataIndex: "detail",
      width: 140,
      render: (text: any, record: any, index: any) => {
        return <InfoCircleTwoTone onClick={() => {}} />;
      },
    },
  ];

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Publikasi Seminar"
        breadcrumbs={breadcrumbs}
        rightAddon={
          <SeminarPublicationAddButton
            onFinish={seminarPublicationsRes.refetch}
          />
        }
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  history.push(
                    `/dashboard/manage/seminarpublications/${record.id}`
                  );
                },
              };
            }}
            size="middle"
            rowKey="id"
            columns={columns}
            dataSource={seminarPublications}
          />
        </Col>
      </Row>
    </>
  );
};

export default EmployeeListPage;
