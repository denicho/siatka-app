import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import {
  SeminarPublicationByPK,
  SeminarPublicationByPKVariables,
} from "../globalTypes";
import { SEMINAR_PUBLICATION_BY_ID } from "./gql/seminarPublicationGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalSeminarPublication from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/seminarpublications`,
    content: `Pengelolaan Publikasi Seminar`,
  },
  {
    to: `/dashboard/manage/seminarpublications/${id}`,
    content: `Detail Publikasi Seminar`,
  },
];

const membersColumns = [
  {
    title: "Posisi",
    dataIndex: ["seminar_publication_activity_position", "name"],
  },
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const SeminarPublicationDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const seminarPublicationRes = useQuery<
    SeminarPublicationByPK,
    SeminarPublicationByPKVariables
  >(SEMINAR_PUBLICATION_BY_ID, {
    variables: {
      id: Number(id),
    },
  });

  const seminarPublication =
    seminarPublicationRes?.data?.seminar_publications_by_pk;

  if (!seminarPublicationRes.loading && seminarPublicationRes.error) {
    // error handling
    return null;
  }
  if (!seminarPublication) {
    if (seminarPublicationRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  const seminarDateAsDate = new Date(seminarPublication.seminar_date);

  return (
    <>
      <ManagementHeading
        title="Detail Publikasi Seminar"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Cover Prosiding</Text>
              </Col>
              <Col span={24}>
                {seminarPublication?.proceeding_cover_file ? (
                  <Typography.Link
                    href={seminarPublication?.proceeding_cover_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Daftar Isi Prosiding</Text>
              </Col>
              <Col span={24}>
                {seminarPublication?.proceeding_toc_file ? (
                  <Typography.Link
                    href={seminarPublication?.proceeding_toc_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Paper</Text>
              </Col>
              <Col span={24}>
                {seminarPublication?.paper_file ? (
                  <Typography.Link
                    href={seminarPublication?.paper_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Sertifikat</Text>
              </Col>
              <Col span={24}>
                {seminarPublication?.certificate_file ? (
                  <Typography.Link
                    href={seminarPublication?.certificate_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>SK / Surat Tugas</Text>
              </Col>
              <Col span={24}>
                {seminarPublication?.decree_file ? (
                  <Typography.Link
                    href={seminarPublication?.decree_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalSeminarPublication
            record={seminarPublication}
            onEditFinish={(id) => {
              seminarPublicationRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Publikasi Seminar" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Judul Publikasi"
                      value={seminarPublication.title}
                    />
                    <FieldInfo
                      label="Jenis Publikasi Seminar"
                      value={
                        seminarPublication?.seminar_publication_category?.name
                      }
                    />
                    <FieldInfo
                      label="Angka Kredit Terbagi"
                      value={seminarPublication?.credit_multiplier}
                    />
                    <FieldInfo
                      label="Nama Seminar"
                      value={seminarPublication?.seminar_name}
                    />
                    <FieldInfo
                      label="Tempat"
                      value={seminarPublication?.seminar_venue}
                    />
                    <FieldInfo
                      label="Tanggal Penyelenggaraan"
                      value={format(seminarDateAsDate, "eeee, dd MMMM yyyy", {
                        locale: localeId,
                      })}
                    />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="ISBN"
                      value={seminarPublication?.isbn || "-"}
                    />
                    <FieldInfo
                      label="ISSN"
                      value={seminarPublication?.issn || "-"}
                    />
                    <FieldInfo
                      label="Tahun"
                      value={seminarPublication?.seminar_year}
                    />
                    <FieldInfo
                      label="No. Halaman"
                      value={seminarPublication?.seminar_page_no || "-"}
                    />
                  </Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Penulis" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={seminarPublication.seminar_publication_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default SeminarPublicationDetailPage;
