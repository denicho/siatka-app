import { gql } from "@apollo/client";

export const SEMINAR_PUBLICATIONS = gql`
  query SeminarPublications {
    seminar_publications(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      seminar_publication_category {
        id
        name
      }
      proceeding_cover_file
      proceeding_toc_file
      certificate_file
      paper_file
      decree_file
      seminar_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        seminar_publication_activity_position {
          id
          name
        }
      }
      isbn
      issn
      seminar_name
      seminar_venue
      seminar_year
      seminar_page_no
      seminar_date
    }
  }
`;

export const SEMINAR_PUBLICATION_BY_ID = gql`
  query SeminarPublicationByPK($id: bigint!) {
    seminar_publications_by_pk(id: $id) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      seminar_publication_category {
        id
        name
      }
      certificate_file
      proceeding_cover_file
      proceeding_toc_file
      paper_file
      decree_file
      seminar_publication_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        seminar_publication_activity_position {
          id
          name
        }
      }
      isbn
      issn
      seminar_name
      seminar_venue
      seminar_year
      seminar_page_no
      seminar_date
    }
  }
`;

export const INSERT_SEMINAR_PUBLICATIONS = gql`
  mutation InsertSeminarPublications(
    $object: seminar_publications_insert_input!
  ) {
    insert_seminar_publications_one(object: $object) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      seminar_publication_category {
        id
        name
      }
      certificate_file
      proceeding_cover_file
      proceeding_toc_file
      paper_file
      decree_file
    }
  }
`;

export const INSERT_SEMINAR_PUBLICATION_MEMBER = gql`
  mutation InsertSeminarPublicationMember(
    $objects: [seminar_publication_members_insert_input!]!
  ) {
    insert_seminar_publication_members(objects: $objects) {
      affected_rows
      returning {
        id
        credit_gain
        seminar_publication_id
        employee {
          id
          name
          employee_type
        }
        external_author
        seminar_publication_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_SEMINAR_PUBLICATION = gql`
  mutation UpdateSeminarPublication(
    $id: bigint!
    $_set: seminar_publications_set_input
  ) {
    update_seminar_publications_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      title
      created_at
      updated_at
      deleted_at
      credit_multiplier
      seminar_publication_category {
        id
        name
      }
      certificate_file
      proceeding_cover_file
      proceeding_toc_file
      paper_file
      decree_file
    }
  }
`;

export const DELETE_SEMINAR_PUBLICATION_MEMBERS = gql`
  mutation DeleteSeminarPublicationMembers($seminar_publication_id: bigint!) {
    delete_seminar_publication_members(
      where: { seminar_publication_id: { _eq: $seminar_publication_id } }
    ) {
      affected_rows
    }
  }
`;

export const DELETE_SEMINAR_PUBLICATION_BY_PK = gql`
  mutation DeleteSeminarPublicationByPK($id: bigint!) {
    delete_seminar_publications_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_SEMINAR_PUBLICATION_BY_PK = gql`
  mutation SoftDeleteSeminarPublicationByPK($id: bigint!) {
    update_seminar_publications_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
