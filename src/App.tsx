import React, { Suspense } from "react";
import { Layout, Spin, Row, ConfigProvider } from "antd";
import moment from "moment";
import "moment/locale/id";
import loadable from "@loadable/component";
import locale from "antd/es/locale/id_ID";
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
} from "react-router-dom";
import NavHeader from "./Layout/NavHeader";
import NavSide from "./Layout/NavSide";
import Content from "./Layout/Content";
import LoginPage from "./User/LoginPage";
import PrivateRoute from "./Common/PrivateOnlyRoute";
import PublicOnlyRoute from "./Common/PublicOnlyRoute";

import styles from "./App.module.css";
import Dashboard from "./Dashboard";

const rowLoadingStyle = {
  height: "100%",
};
const LoadingPlaceholder = () => (
  <Row justify="center" align="middle" style={rowLoadingStyle}>
    <Spin size="large" />
  </Row>
);

const ProfileLandingLazy = loadable(() => import("./Profiles/Landing"));
const ProfileDetailLazy = loadable(() => import("./Profiles/Detail"));

moment.locale("id");

function App() {
  return (
    <div className={styles.main}>
      <ConfigProvider locale={locale}>
        <Router>
          <Switch>
            <Route exact path="/profile">
              <ProfileLandingLazy />
            </Route>
            <Route path="/profile/:id">
              <ProfileDetailLazy />
            </Route>
            <PublicOnlyRoute path="/login">
              <LoginPage />
            </PublicOnlyRoute>
            <PrivateRoute path="/dashboard">
              <Layout className={styles.containerLayout}>
                <NavSide />
                <Layout>
                  <NavHeader />

                  <Content>
                    <Suspense fallback={<LoadingPlaceholder />}>
                      <Dashboard />
                    </Suspense>
                  </Content>
                </Layout>
              </Layout>
            </PrivateRoute>
            <Redirect to="/dashboard" />
          </Switch>
        </Router>
      </ConfigProvider>
    </div>
  );
}

export default App;
