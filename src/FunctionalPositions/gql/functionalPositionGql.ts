import { gql } from "@apollo/client";

export const DELETE_FUNCTIONAL_POS = gql`
  mutation DeleteFunctionalPosition($id: bigint!) {
    delete_functional_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const FUNCTIONAL_POS = gql`
  query FunctionalPositions {
    functional_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_FUNCTIONAL_POS = gql`
  mutation InsertFunctionalPosition(
    $object: functional_positions_insert_input!
  ) {
    insert_functional_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_FUNCTIONAL_POS = gql`
  mutation UpdateFunctionalPosition(
    $pk_columns: functional_positions_pk_columns_input!
    $_set: functional_positions_set_input
  ) {
    update_functional_positions_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      created_at
      updated_at
    }
  }
`;
