import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, Modal } from "antd";
import { useMutation, gql } from "@apollo/client";
import { Store } from "antd/lib/form/interface";

import { INSERT_FUNCTIONAL_POS } from "./gql/functionalPositionGql";
import {
  InsertFunctionalPosition,
  InsertFunctionalPositionVariables,
} from "../globalTypes";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const FunctionalPositionsAddButton = () => {
  const [form] = Form.useForm();
  const [create] = useMutation<
    InsertFunctionalPosition,
    InsertFunctionalPositionVariables
  >(INSERT_FUNCTIONAL_POS, {
    onCompleted: () => {
      form.resetFields();
      setLoading(false);
      setVisible(false);
    },
    onError: () => {
      setLoading(false);
      setVisible(false);
    },
  });
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    form.submit();
  };
  const onFinish = (values: Store) => {
    setLoading(true);

    create({
      variables: {
        object: {
          name: values.name,
        },
      },
      update: (cache, { data }) => {
        if (data?.insert_functional_positions_one) {
          cache.modify({
            fields: {
              functional_positions(existingFuncPos = []) {
                const newFuncPos = cache.writeFragment({
                  data: data.insert_functional_positions_one,
                  fragment: gql`
                    fragment functional_positions on functional_positions {
                      id
                      name
                      created_at
                      updated_at
                    }
                  `,
                });
                return [...existingFuncPos, newFuncPos];
              },
            },
          });
        }
      },
    });
  };

  return (
    <>
      <Modal
        title="Tambah Jabatan Fungsional"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-functionalPositions"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Jabatan Fungsional
      </Button>
    </>
  );
};

export default FunctionalPositionsAddButton;
