import React from "react";
import { useQuery } from "@apollo/client";
import { Table, Row, Col } from "antd";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import { HomeOutlined } from "@ant-design/icons";
import ManagementHeading from "../Common/ManagementHeading";
import DepartmentAddButton from "./AddForm";
import MutationActionsModal from "./MutationActionsModal";
import { FunctionalPositions } from "../globalTypes";
import { FUNCTIONAL_POS } from "./gql/functionalPositionGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/functional-positions",
    content: "Pengelolaan Jabatan Fungsional",
  },
];

const FunctionalPositionsListPage = () => {
  const funcPosRes = useQuery<FunctionalPositions>(FUNCTIONAL_POS);

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (
    funcPosRes.loading ||
    !funcPosRes.data ||
    !funcPosRes.data.functional_positions
  ) {
    return <LoadingPlaceholder />;
  }

  const funcPoses = funcPosRes.data.functional_positions;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Jabatan Fungsional"
        breadcrumbs={breadcrumbs}
        rightAddon={<DepartmentAddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={funcPoses}
          />
        </Col>
      </Row>
    </>
  );
};

export default FunctionalPositionsListPage;
