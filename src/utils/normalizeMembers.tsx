import { research_members_insert_input } from "../globalTypes";

export type InternalMembers = {
  activity_position_id: string;
  credit_percentage: number;
  internal_member: Array<{
    key: number;
    label: string;
    value: number;
  }>;
};

export type ExternalMembers = {
  activity_position_id: string;
  external_members: string;
};

export type NormalizeMembersResult = research_members_insert_input[];

const normalizeMembers = (
  id: number,
  id_field_name: string,
  internal_members?: Array<InternalMembers>,
  external_members?: Array<ExternalMembers>,
  creditMultiplier?: number
): NormalizeMembersResult => {
  const internals =
    internal_members && internal_members.length > 0
      ? internal_members.map((im) => ({
          activity_position_id: im.activity_position_id,
          employee_id: im.internal_member[0].value,
          credit_gain:
            (Number(im.credit_percentage) *
              (creditMultiplier ? creditMultiplier : 0)) /
            100,
          [id_field_name]: id,
        }))
      : [];
  const externals =
    external_members && external_members.length > 0
      ? external_members.map((em) => ({
          activity_position_id: em.activity_position_id,
          external_author: em.external_members,
          [id_field_name]: id,
        }))
      : [];

  return [...internals, ...externals];
};

export default normalizeMembers;
