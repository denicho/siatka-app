import React from "react";
import { LeftOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import "./index.css";
import { useQuery } from "@apollo/client";
import {
  PublicEmployeesByPK,
  PublicEmployeesByPKVariables,
  PublicEmployeeActivites,
  PublicEmployeeActivitesVariables,
} from "../globalTypes";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import {
  PUBLIC_EMPLOYEE_BY_ID,
  PUBLIC_EMPLOYEE_ACTIVITIES,
} from "./gql/profileGql";
import { mapPublicEmployeeActivites } from "./utils";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import { Pagination } from "antd";

function paginate(array: any[], page_size: number, page_number: number) {
  // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
  return array.slice((page_number - 1) * page_size, page_number * page_size);
}

const PAGE_SIZE = 5;

const ProfileDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "1";
  const [page, setPage] = React.useState<number>(1);

  const employeeRes = useQuery<
    PublicEmployeesByPK,
    PublicEmployeesByPKVariables
  >(PUBLIC_EMPLOYEE_BY_ID, {
    variables: {
      id: Number(id),
    },
    context: {
      skipAuth: true,
    },
    fetchPolicy: "cache-and-network",
  });
  const employeeActivites = useQuery<
    PublicEmployeeActivites,
    PublicEmployeeActivitesVariables
  >(PUBLIC_EMPLOYEE_ACTIVITIES, {
    variables: {
      employeeId: Number(id),
    },
    context: {
      skipAuth: true,
    },
  });

  const employeeActivityData = mapPublicEmployeeActivites(
    employeeActivites?.data
  );

  const employee = employeeRes?.data?.employees_by_pk;
  if (!employeeRes.loading && employeeRes.error) {
    // error handling
    return null;
  }

  if (!employee) {
    if (employeeRes.loading) {
      return (
        <div className="h-screen">
          <LoadingPlaceholder />
        </div>
      );
    }

    // error handling
    return null;
  }

  const handlePageChange = (page: number) => {
    setPage(page);
  };

  return (
    <div className="bg-gray-50 h-screen  overflow-y-scroll">
      <div className="container mx-auto box-border bg-white min-h-screen max-w-screen-xl shadow-lg">
        <div className="px-4 pt-4 pb-8 xl:px-16 bg-gradient-to-b from-blue-600 to-blue-800">
          <Link to="/profile">
            <div className="text-white flex flex-row items-end leading-4 pb-8 cursor-pointer hover:text-yellow-500">
              <div style={{ marginBottom: 1 }}>
                <LeftOutlined />
              </div>
              <div className="pl-2">Kembali</div>
            </div>
          </Link>
          <div className="flex flex-col items-center">
            <div className=" w-64 xl:w-96 overflow-hidden mb-6 shadow-md border-solid border-white border-2 xl:border-4">
              <img
                className="w-full h-full object-cover "
                src={employee.picture_file || ""}
              />
            </div>
            <h2 className="font-bold text-xl xl:text-4xl text-white text-center">
              {employee.name}
            </h2>
            <h6 className="xl:text-lg text-white text-center">
              {employee.department?.name}, {employee.department?.faculty?.name}
            </h6>
            <a
              href={`mailto:${employee.email}`}
              className="text-yellow-300 hover:text-yellow-500"
            >
              {employee.email}
            </a>
          </div>
        </div>
        <div className="flex flex-col ">
          <div className="xl:px-16 xl:py-8 px-8 py-6 flex-1">
            <h3 className="text-xl xl:text-3xl font-semibold">Profil</h3>
            <div className="flex flex-row mt-4 xl:leading-6 xl:text-lg">
              <div>
                <div className="py-2 xl:flex xl:flex-row">
                  <div className="w-32">
                    <p className="font-semibold flex-1 ">Inisial</p>
                  </div>
                  <div className="flex-1">
                    <p>{employee.initial}</p>
                  </div>
                </div>
                <div className="py-2 xl:flex xl:flex-row">
                  <div className="w-32">
                    <p className="font-semibold flex-1 ">NIDN</p>
                  </div>
                  <div className="flex-1">
                    <p>{employee.n_id_n}</p>
                  </div>
                </div>
                <div className="py-2 xl:flex xl:flex-row">
                  <div className="w-32">
                    <p className="font-semibold flex-1 ">Alamat</p>
                  </div>
                  <div className="flex-1">
                    <p>{employee.address}</p>
                  </div>
                </div>
                <div className="py-2 xl:flex xl:flex-row">
                  <div className="w-32">
                    <p className="font-semibold flex-1">TTL</p>
                  </div>
                  <div className="flex-1">
                    <p>
                      {employee.place_of_birth},{" "}
                      {format(new Date(employee.date_of_birth), "dd MMMM yyyy")}
                    </p>
                  </div>
                </div>
                <div className="py-2 xl:flex xl:flex-row">
                  <div className="w-32">
                    <p className="font-semibold flex-1">No. HP</p>
                  </div>
                  <div className="flex-1">
                    <p>{employee.phone}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="xl:px-16 xl:pb-8 px-8 pb-6 flex-1">
            <h3 className="text-xl xl:text-3xl font-semibold">Aktivitas</h3>
            <div className="py-4">
              {paginate(employeeActivityData, PAGE_SIZE, page).map((act) => (
                <>
                  <div className="py-2">
                    <p className="font-semibold xl:text-lg">{act.type}</p>
                    <div>{act.title}</div>
                    <div className="text-gray-500 italic text-sm">
                      {act.date}
                    </div>
                  </div>
                  <div className="border-solid border-t border-gray-100"></div>
                </>
              ))}
            </div>
            {employeeActivityData.length > 0 ? (
              <Pagination
                size="small"
                total={employeeActivityData.length}
                pageSize={PAGE_SIZE}
                onChange={handlePageChange}
                current={page}
              />
            ) : (
              <p>Belum ada data</p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileDetailPage;
