import { gql } from "@apollo/client";

export const PUBLIC_EMPLOYEES_LIST = gql`
  query PublicEmployeesList(
    $limit: Int
    $offset: Int
    $where: employees_bool_exp
  ) {
    employees(limit: $limit, offset: $offset, where: $where) {
      id
      name
      picture_file
      department {
        id
        name
        faculty {
          id
          name
        }
      }
    }
    employees_aggregate(where: $where) {
      aggregate {
        count
      }
    }
  }
`;

export const PUBLIC_DEPARTMENTS = gql`
  query PublicDepartments {
    departments(order_by: { created_at: asc }) {
      id
      name
      faculty {
        id
        name
      }
    }
  }
`;

export const PUBLIC_EMPLOYEE_BY_ID = gql`
  query PublicEmployeesByPK($id: bigint!) {
    employees_by_pk(id: $id) {
      employee_type
      id
      n_id_n
      npp
      email
      name
      address
      date_of_birth
      place_of_birth
      status
      initial
      phone
      picture_file
      department {
        id
        name
        faculty {
          id
          name
        }
      }
      functional_position {
        id
        name
      }
      structural_position {
        id
        name
      }
      group {
        id
        name
      }
    }
  }
`;

export const PUBLIC_EMPLOYEE_ACTIVITIES = gql`
  query PublicEmployeeActivites($employeeId: bigint!) {
    researches(
      where: { research_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      title
      finished_at
    }
    services(
      where: { service_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      title
      end_date
    }
    journal_publications(
      where: {
        journal_publication_members: { employee_id: { _eq: $employeeId } }
      }
    ) {
      id
      title
      journal_year
    }
    seminar_publications(
      where: {
        seminar_publication_members: { employee_id: { _eq: $employeeId } }
      }
    ) {
      id
      title
      seminar_date
    }
    book_publications(
      where: { book_publication_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      title
      publish_year
    }
    patent_publications(
      where: {
        patent_publication_members: { employee_id: { _eq: $employeeId } }
      }
    ) {
      id
      title
      first_announcement_date
    }
    self_developments(
      where: { self_development_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      name
      end_date
    }
    committees(
      where: { committee_members: { employee_id: { _eq: $employeeId } } }
    ) {
      id
      name
      end_date
    }
  }
`;
