import React from "react";
import "./index.css";
import uajyImg from "../assets/uajy.png";
import { debounce } from "lodash-es";
import { Pagination } from "antd";
import { Input, Select } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/client";
import { PUBLIC_EMPLOYEES_LIST, PUBLIC_DEPARTMENTS } from "./gql/profileGql";
import { Link } from "react-router-dom";
import {
  PublicDepartments,
  PublicEmployeesList,
  PublicEmployeesListVariables,
} from "../globalTypes";

type ListItemProps = {
  name: string;
  department: string;
  avatarUrl: string;
  id: number;
};

const EmployeeListItem = (props: ListItemProps) => {
  return (
    <Link to={`/profile/${props.id}`}>
      <div className="flex flex-row h-20 py-4 cursor-pointer xl:px-4 hover:bg-gray-50">
        <div>
          <div className="rounded-full h-12 w-12 overflow-hidden border-red border-solid border-2 overflow-hidden">
            <img
              src={props.avatarUrl
                .split("siatka/image/upload/")
                .join("siatka/image/upload/w_128/")}
              className="w-full h-full object-cover"
            />
          </div>
        </div>
        <div className="pl-4 overflow-hidden">
          <p className="font-semibold text-lg truncate w-full">{props.name}</p>
          <p className="text-gray-500 italic">{props.department}</p>
        </div>
      </div>
      <div className="border-solid border-t border-gray-100"></div>
    </Link>
  );
};

const PER_PAGE = 10;

const Landing = () => {
  const [page, setPage] = React.useState<number>(1);
  const [term, setTerm] = React.useState<string>("");
  const [ilike, setIlike] = React.useState<string>("");
  const [departmentFilter, setDepartmentFilter] =
    React.useState<string | undefined>(undefined);

  const handlePageChange = (pageNumber: number) => {
    setPage(pageNumber);
  };

  const handleChangeTerm = (e: any) => {
    setTerm(e.target.value);
  };

  const depsRes = useQuery<PublicDepartments>(PUBLIC_DEPARTMENTS, {
    context: {
      skipAuth: true,
    },
  });

  const departments = depsRes?.data?.departments || [];

  const publicEmployeesList = useQuery<
    PublicEmployeesList,
    PublicEmployeesListVariables
  >(PUBLIC_EMPLOYEES_LIST, {
    context: {
      skipAuth: true,
    },
    variables: {
      limit: PER_PAGE,
      offset: PER_PAGE * (page - 1),
      where: {
        employee_type: {
          // @ts-ignore
          _eq: "LECTURER",
        },
        status: {
          // @ts-ignore
          _eq: "Aktif",
        },
        name: {
          _ilike: `%${ilike}%`,
        },
        department_id: {
          _eq: departmentFilter ? departmentFilter : undefined,
        },
      },
    },
  });

  const handleSearch = React.useCallback(
    debounce((term: string) => {
      setIlike(term);
      setPage(1);
    }, 500),
    []
  );

  React.useEffect(() => {
    handleSearch(term);
  }, [term]);

  const employees = publicEmployeesList?.data?.employees || [];
  const count =
    publicEmployeesList?.data?.employees_aggregate?.aggregate?.count || 10;

  return (
    <div className="bg-gray-50 h-screen">
      <div className="container mx-auto px-4 xl:px-16 box-border bg-white h-screen max-w-screen-xl shadow-lg">
        <div>
          <div className="flex flex-col xl:flex-row xl:justify-between">
            <div className="header flex flex-row py-4 items-center justify-center">
              <div className="flex justify-center items-center">
                <img src={uajyImg} className="w-12" />
              </div>
              <h2 className="md:text-xl text-base ml-4 font-bold">
                Daftar Aktivitas Dosen Universitas Atma Jaya Yogyakarta
              </h2>
            </div>
            <div className="border-solid border-t border-gray-100 xl:hidden"></div>
            <div className="content py-4 xl:flex xl:items-center ">
              <div className="w-full flex flex flex-col xl:flex-row xl:pl-12">
                <Input
                  placeholder="Cari Nama"
                  prefix={<SearchOutlined />}
                  value={term}
                  onChange={handleChangeTerm}
                />
                <div className="xl:ml-2 mt-2 xl:mt-0 xl:w-64">
                  <Select
                    placeholder="Pilih Prodi"
                    allowClear
                    loading={departments.length <= 0}
                    style={{ width: "100%" }}
                    value={departmentFilter}
                    onChange={(depId) => {
                      setDepartmentFilter(depId);
                    }}
                  >
                    {departments.map((dept) => (
                      <Select.Option
                        key={dept.id}
                        value={dept?.id?.toString() || ""}
                      >
                        {dept.name}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
            </div>
          </div>

          <div className="border-solid border-t border-gray-100"></div>

          {employees.map((employee) => (
            <EmployeeListItem
              key={employee.id}
              id={employee.id}
              name={employee.name || ""}
              department={employee?.department?.name || ""}
              avatarUrl={employee?.picture_file || ""}
            />
          ))}

          <div className="flex justify-center py-4">
            <Pagination
              current={page}
              defaultCurrent={1}
              total={count}
              pageSize={PER_PAGE}
              onChange={handlePageChange}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Landing;
