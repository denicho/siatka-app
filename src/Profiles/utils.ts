import { PublicEmployeeActivites } from "../globalTypes";
import { format } from "date-fns";

const keyMap = {
  researches: "Penelitian",
  services: "Pengabdian",
  journal_publications: "Publikasi Jurnal",
  seminar_publications: "Publikasi Seminar",
  book_publications: "Publikasi Buku",
  patent_publications: "Publikasi Paten",
  self_developments: "Pengembangan Diri",
  committees: "Kepanitiaan",
};

const dateMap = {
  researches: "finished_at",
  services: "end_date",
  journal_publications: "journal_year",
  seminar_publications: "seminar_date",
  book_publications: "publish_year",
  patent_publications: "first_announcement_date",
  self_developments: "end_date",
  committees: "end_date",
};

type ResultElement = {
  type: string;
  id: number;
  title: string;
  url: string;
  date: any;
};

export const mapPublicEmployeeActivites = (data?: PublicEmployeeActivites) => {
  if (!data) {
    return [];
  }

  let result: ResultElement[] = [];

  for (const prop in data) {
    // @ts-ignore
    data[prop].map((el) => {
      let mapped = {
        // @ts-ignore
        type: keyMap[prop],
        id: el.id,
        title: el.title ? el.title : el.name,
        url: `/dashboard/manage/${prop.split("_").join("")}/${el.id}`,
        date: "-",
      };

      try {
        // @ts-ignore
        if (el[dateMap[prop]]) {
          // @ts-ignore
          const date = el[dateMap[prop]];

          if (typeof date === "string") {
            mapped.date = format(new Date(date), "dd MMMM yyyy");
          } else {
            mapped.date = format(new Date(date.toString()), "yyyy");
          }
        }
      } catch (err) {
        console.error("Invalid date");
      }
      result.push(mapped);
    });
  }

  return result;
};
