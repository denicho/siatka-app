import { gql } from "@apollo/client";

export const RESEARCHES = gql`
  query Researches {
    researches(
      order_by: { created_at: desc }
      where: { deleted_at: { _is_null: true } }
    ) {
      id
      title
      fund_amount
      created_at
      updated_at
      deleted_at
      started_at
      finished_at
      credit_multiplier
      research_category {
        id
        credit
        name
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
      research_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        research_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const RESEARCH_BY_ID = gql`
  query ResearchByPK($id: bigint!) {
    researches_by_pk(id: $id) {
      id
      title
      fund_amount
      created_at
      updated_at
      deleted_at
      started_at
      finished_at
      credit_multiplier
      research_category {
        id
        credit
        name
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
      research_members(order_by: { activity_position_id: asc }) {
        id
        credit_gain
        employee {
          id
          name
          employee_type
        }
        external_author
        research_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const INSERT_RESEARCHES = gql`
  mutation InsertResearches($object: researches_insert_input!) {
    insert_researches_one(object: $object) {
      id
      title
      fund_amount
      credit_multiplier
      created_at
      updated_at
      deleted_at
      started_at
      finished_at
      research_category {
        id
        credit
        name
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
    }
  }
`;

export const INSERT_RESEARCH_MEMBER = gql`
  mutation InsertResearchMember($objects: [research_members_insert_input!]!) {
    insert_research_members(objects: $objects) {
      affected_rows
      returning {
        id
        credit_gain
        research_id
        employee {
          id
          name
          employee_type
        }
        external_author
        research_activity_position {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_RESEARCH = gql`
  mutation UpdateResearch($id: bigint!, $_set: researches_set_input) {
    update_researches_by_pk(pk_columns: { id: $id }, _set: $_set) {
      id
      title
      fund_amount
      credit_multiplier
      created_at
      updated_at
      deleted_at
      started_at
      finished_at
      research_category {
        id
        name
        credit
      }
      proposal_file
      final_report_file
      certificate_file
      decree_file
    }
  }
`;

export const DELETE_RESEARCH_MEMBERS = gql`
  mutation DeleteResearchMembers($researchId: bigint!) {
    delete_research_members(where: { research_id: { _eq: $researchId } }) {
      affected_rows
    }
  }
`;

export const DELETE_RESEARCH_BY_PK = gql`
  mutation DeleteResearchByPK($id: bigint!) {
    delete_researches_by_pk(id: $id) {
      id
    }
  }
`;

export const SOFT_DELETE_RESEARCH_BY_PK = gql`
  mutation SoftDeleteResearchByPK($id: bigint!) {
    update_researches_by_pk(
      pk_columns: { id: $id }
      _set: { deleted_at: "now()" }
    ) {
      id
    }
  }
`;
