import { useQuery } from "@apollo/client";
import { Col, Row, Space, Table, Tabs, Typography } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import ManagementHeading from "../Common/ManagementHeading";
import { id as localeId } from "date-fns/locale";
import { ResearchByPK, ResearchByPKVariables } from "../globalTypes";
import { RESEARCH_BY_ID } from "./gql/researchGql";
import FieldInfo from "../Common/FieldInfo";
import formatMoney from "../utils/formatMoney";
import { format } from "date-fns";
import MutationActionsModalResearch from "./MutationActionsModal";

const { Text, Title } = Typography;
const { TabPane } = Tabs;

const breadcrumbs = (id: string) => [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/researches`,
    content: `Pengelolaan Penelitian`,
  },
  {
    to: `/dashboard/manage/researches/${id}`,
    content: `Detail Penelitian`,
  },
];

const membersColumns = [
  {
    title: "Posisi",
    dataIndex: ["research_activity_position", "name"],
  },
  {
    title: "Nama",
    dataIndex: "name",
    render: (text: any, record: any, index: any) => {
      const isExternal = !record.employee;

      if (isExternal) {
        return <Text>{record.external_author}</Text>;
      }

      return (
        <Link
          to={`/dashboard/manage/${
            record.employee.employee_type === "LECTURER"
              ? "lecturers"
              : "employees"
          }/${record.employee.id}`}
        >
          {record.employee.name}
        </Link>
      );
    },
  },
  {
    title: "Angka Kredit",
    dataIndex: ["credit_gain"],
  },
];

const ResearchDetailPage = () => {
  const params = useParams<{ id: string }>();
  const id = params?.id || "";

  const researchRes = useQuery<ResearchByPK, ResearchByPKVariables>(
    RESEARCH_BY_ID,
    {
      variables: {
        id: Number(id),
      },
    }
  );

  const research = researchRes?.data?.researches_by_pk;

  if (!researchRes.loading && researchRes.error) {
    // error handling
    return null;
  }
  if (!research) {
    if (researchRes.loading) {
      return <LoadingPlaceholder />;
    }

    // error handling
    return null;
  }

  const startedAtAsDate = new Date(research.started_at);
  const finishedAtAtAsDate = new Date(research.finished_at);

  return (
    <>
      <ManagementHeading
        title="Detail Penelitian"
        breadcrumbs={breadcrumbs(id)}
      />
      <Row
        style={{
          marginTop: 24,
          background: "#fff",
          padding: 20,
        }}
      >
        <Col span={6}>
          <Space direction="vertical">
            <Row>
              <Col span={24}>
                <Title level={3}>Daftar Berkas</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Proposal</Text>
              </Col>
              <Col span={24}>
                {research?.proposal_file ? (
                  <Typography.Link
                    href={research?.proposal_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Laporan Akhir</Text>
              </Col>
              <Col span={24}>
                {research?.final_report_file ? (
                  <Typography.Link
                    href={research?.final_report_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>Sertifikat</Text>
              </Col>
              <Col span={24}>
                {research?.certificate_file ? (
                  <Typography.Link
                    href={research?.certificate_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Text strong>SK / Surat Tugas</Text>
              </Col>
              <Col span={24}>
                {research?.decree_file ? (
                  <Typography.Link
                    href={research?.decree_file || ""}
                    target="_blank"
                  >
                    Unduh
                  </Typography.Link>
                ) : (
                  "-"
                )}
              </Col>
            </Row>
          </Space>
          <MutationActionsModalResearch
            record={research}
            onEditFinish={(id) => {
              researchRes.refetch();
            }}
          />
        </Col>
        <Col span={18} style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Tabs type="card">
            <TabPane tab="Informasi Penelitian" key="1">
              <Row>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo label="Judul" value={research.title} />
                    <FieldInfo
                      label="Jenis Penelitian"
                      value={research?.research_category?.name}
                    />
                    <FieldInfo
                      label="Angka Kredit Terbagi"
                      value={research?.credit_multiplier}
                    />
                    <FieldInfo
                      label="Besar Dana"
                      value={`Rp${formatMoney(
                        research.fund_amount,
                        0,
                        ",",
                        "."
                      )}`}
                    />
                  </Space>
                </Col>
                <Col flex={1}>
                  <Space direction="vertical">
                    <FieldInfo
                      label="Tanggal Mulai"
                      value={format(startedAtAsDate, "eeee, dd MMMM yyyy", {
                        locale: localeId,
                      })}
                    />
                    <FieldInfo
                      label="Tanggal Selesai"
                      value={
                        research.finished_at
                          ? format(finishedAtAtAsDate, "eeee, dd MMMM yyyy", {
                              locale: localeId,
                            })
                          : "-"
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Daftar Penulis" key="2">
              <Col flex={1}>
                <Table
                  columns={membersColumns}
                  dataSource={research.research_members}
                  size="small"
                />
              </Col>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </>
  );
};

export default ResearchDetailPage;
