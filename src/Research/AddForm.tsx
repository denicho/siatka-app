import React from "react";
import { flatten, debounce, get } from "lodash-es";
import {
  PlusOutlined,
  RightOutlined,
  LeftOutlined,
  MinusCircleOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";
import {
  Button,
  Form,
  Input,
  Divider,
  Spin,
  Modal,
  Row,
  Col,
  DatePicker,
  Upload,
  Select,
  Steps,
  InputNumber,
  Typography,
} from "antd";
import {
  ActivityPositions,
  ResearchCategories,
  SearchEmployees,
  SearchEmployeesVariables,
  InsertResearches,
  InsertResearchesVariables,
  InsertResearchMember,
  InsertResearchMemberVariables,
  ResearchActivityPositions,
} from "../globalTypes";
import { gql, useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { RESEARCH_CATEGORIES } from "../ResearchCategories/gql/researchCategoriesGql";
import { createCustomRequest } from "../Common/uploadFile";
import { SEARCH_EMPLOYEES } from "../Employees/gql/employeesGql";
import { INSERT_RESEARCHES, INSERT_RESEARCH_MEMBER } from "./gql/researchGql";
import normalizeMembers from "../utils/normalizeMembers";
import { RESEARCH_ACTIVITY_POSITIONS } from "../ResearchCategories/gql/researchPositionsGql";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

const { Step } = Steps;
const { Option } = Select;

export const normFile = (e: any) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

type ResearchAddButtonProps = {
  onFinish: () => void;
};

const ResearchAddButton = (props: ResearchAddButtonProps) => {
  const [searchEmployees, searchEmployeesRes] = useLazyQuery<
    SearchEmployees,
    SearchEmployeesVariables
  >(SEARCH_EMPLOYEES, {
    onCompleted: (data) => {},
  });
  const categoriesRes = useQuery<ResearchCategories>(RESEARCH_CATEGORIES);
  const activityPosRes = useQuery<ResearchActivityPositions>(
    RESEARCH_ACTIVITY_POSITIONS
  );

  const [visible, setVisible] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState(false);
  const [selectedCategoryCredit, setSelectedCategoryCredit] =
    React.useState<number>(0);
  const [isTotalCreditError, setIsTotalCreditError] =
    React.useState<boolean>(true);
  const [form] = Form.useForm();

  const [insertResearches] =
    useMutation<InsertResearches, InsertResearchesVariables>(INSERT_RESEARCHES);
  const [insertResearchMember] = useMutation<
    InsertResearchMember,
    InsertResearchMemberVariables
  >(INSERT_RESEARCH_MEMBER, {
    onCompleted: () => {
      form.resetFields();
      setCurrentStep(0);
      setIsTotalCreditError(true);
      setProposalFileList([]);
      setFinalReportFileList([]);
      setCertificationFileList([]);
      setDecreeFileList([]);
      setLoading(false);
      setVisible(false);
      props.onFinish();
    },
    onError: () => {
      setLoading(false);
      setVisible(false);
    },
  });

  const categories = categoriesRes?.data?.research_categories || [];
  const activityPoses = activityPosRes?.data?.research_activity_positions || [];
  const searchedEmployees = searchEmployeesRes?.data?.employees || [];
  const [selectedEmployeeIDs, setSelectedEmployeeIDs] = React.useState<
    number[]
  >([]);

  const filteredEmployees = searchedEmployees
    ? searchedEmployees.filter(
        (emp) => !selectedEmployeeIDs.includes(emp.value)
      )
    : [];

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    if (isTotalCreditError) {
      return;
    }

    form.submit();
  };
  const onFinish = (values: Store) => {
    setLoading(true);

    const vals = form.getFieldsValue(flatten(validateList));

    const insertResearchesPayload = {
      ...vals,
      started_at: vals.started_at.format("YYYY-MM-DD"),
      finished_at: vals.finished_at
        ? vals.finished_at.format("YYYY-MM-DD")
        : undefined,
      category_id: Number(vals.category_id),

      proposal_file: get(
        vals,
        `proposal_file[${
          vals.proposal_file ? vals.proposal_file.length - 1 : 0
        }].url`,
        null
      ),

      final_report_file: get(
        vals,
        `final_report_file[${
          vals.final_report_file ? vals.final_report_file.length - 1 : 0
        }].url`,
        null
      ),

      certificate_file: get(
        vals,
        `certificate_file[${
          vals.certificate_file ? vals.certificate_file.length - 1 : 0
        }].url`,
        null
      ),

      decree_file: get(
        vals,
        `decree_file[${
          vals.decree_file ? vals.decree_file.length - 1 : 0
        }].url`,
        null
      ),

      credit_multiplier: selectedCategoryCredit,
    };

    insertResearches({
      variables: {
        object: insertResearchesPayload,
      },
      // update: (cache, { data }) => {
      //   if (data?.insert_researches_one) {
      //     cache.modify({
      //       fields: {
      //         researches(existingResearches = []) {
      //           const newResearches = cache.writeFragment({
      //             data: data.insert_researches_one,
      //             fragment: gql`
      //               fragment researches on researches {
      //                 id
      //                 title
      //                 fund_amount
      //                 created_at
      //                 updated_at
      //                 started_at
      //                 finished_at
      //                 research_category {
      //                   id
      //                   name
      //                 }
      //                 proposal_file
      //                 final_report_file
      //                 certificate_file
      //                 decree_file
      //               }
      //             `,
      //           });
      //           return [...existingResearches, newResearches];
      //         },
      //       },
      //     });
      //   }
      // },
    }).then((result) => {
      const researchId = result?.data?.insert_researches_one?.id;

      if (researchId) {
        const insertResearchMembersPayload = normalizeMembers(
          researchId,
          "research_id",
          values.internal_members,
          values.external_members,
          selectedCategoryCredit
        );

        insertResearchMember({
          variables: {
            objects: insertResearchMembersPayload,
          },
        });
      }
    });
  };

  const onFundAmtChange = (value: any) => {
    const reg = /^-?\d*(\.\d*)?$/;

    if ((!isNaN(value) && reg.test(value)) || value === "" || value === "-") {
      form.setFieldsValue({ fund_amount: value });
    }
  };

  // Uploads
  const [proposalFileList, setProposalFileList] = React.useState([]);
  const handleProposalUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setProposalFileList(fileList);
  };

  const [finalReportFileList, setFinalReportFileList] = React.useState([]);
  const handleFinalReportUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setFinalReportFileList(fileList);
  };

  const [certificationFileList, setCertificationFileList] = React.useState([]);
  const handleCertificationUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setCertificationFileList(fileList);
  };

  const [decreeFileList, setDecreeFileList] = React.useState([]);
  const handleDecreeUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setDecreeFileList(fileList);
  };

  const [currentStep, setCurrentStep] = React.useState(0);
  const validateList = [
    ["title", "category_id", "fund_amount", "started_at", "finished_at"],
    ["proposal_file", "final_report_file", "certificate_file", "decree_file"],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 2 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
  const steps = [
    {
      title: "Data Penelitian",
      content: (
        <>
          <Form.Item name={"title"} label="Judul" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"category_id"}
            label="Kategori"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Pilih Kategori Penelitian"
              loading={categories.length <= 0}
            >
              {categories.map((ctg) => (
                <Option key={ctg.id} value={ctg?.id?.toString() || ""}>
                  {ctg.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"fund_amount"}
            label="Besar Dana"
            rules={[{ required: true }]}
          >
            <InputNumber
              formatter={(value) =>
                `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
              }
              parser={(value: any) => value.replace(/Rp\s?|(\.*)/g, "")}
              style={{ width: "100%" }}
              onChange={onFundAmtChange}
            />
          </Form.Item>
          <Form.Item
            name={"started_at"}
            label="Tanggal Mulai"
            rules={[{ required: true }]}
          >
            <DatePicker
              format="DD/MM/YYYY"
              showToday={false}
              onChange={(startDateValue) => {
                const endDate: moment.Moment =
                  form.getFieldValue("finished_at");
                if (startDateValue && endDate) {
                  if (endDate.isBefore(startDateValue)) {
                    form.setFieldsValue({ finished_at: startDateValue });
                  }
                }
              }}
            />
          </Form.Item>
          <Form.Item name={"finished_at"} label="Tanggal Selesai">
            <DatePicker
              format="DD/MM/YYYY"
              disabledDate={(current) => {
                const startedAt = form.getFieldValue("started_at");
                // Can not select days after today and today
                if (startedAt) {
                  // return current && current <= startedAt.endOf("day");
                  return current.isBefore(startedAt);
                } else {
                  return false;
                }
              }}
              showToday={false}
              onChange={(endDateValue) => {
                const startDate: moment.Moment =
                  form.getFieldValue("started_at");
                if (endDateValue && startDate) {
                  if (startDate.isAfter(endDateValue)) {
                    form.setFieldsValue({ started_at: endDateValue });
                  }
                }
              }}
            />
          </Form.Item>
          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Berkas",
      content: (
        <>
          <Form.Item
            name={"proposal_file"}
            label="Proposal"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
          >
            <Upload
              customRequest={createCustomRequest("research_proposal_file")}
              onChange={handleProposalUploadChange}
              fileList={proposalFileList}
              onRemove={() => setProposalFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {proposalFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"final_report_file"}
            label="Laporan Akhir"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Upload
              customRequest={createCustomRequest("research_final_report_file")}
              onChange={handleFinalReportUploadChange}
              fileList={finalReportFileList}
              onRemove={() => setFinalReportFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {finalReportFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"certificate_file"}
            label="Sertifikat"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Upload
              customRequest={createCustomRequest("research_certification_file")}
              onChange={handleCertificationUploadChange}
              fileList={certificationFileList}
              onRemove={() => setCertificationFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {certificationFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"decree_file"}
            label="SK / Surat Tugas"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Upload
              customRequest={createCustomRequest("research_decree_file")}
              onChange={handleDecreeUploadChange}
              fileList={decreeFileList}
              onRemove={() => setDecreeFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {decreeFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Penulis",
      content: (
        <>
          <Divider orientation="center" plain>
            Penulis Internal
          </Divider>
          <Row justify="center" style={{ paddingBottom: 16 }}>
            <Typography.Text>Angka Kredit Terbagi:&nbsp;</Typography.Text>
            <Typography.Text strong>{selectedCategoryCredit}</Typography.Text>
          </Row>
          <Form.List
            name="internal_members"
            rules={[
              {
                validator: async (_, names) => {
                  if (!names || names.length < 1) {
                    return Promise.reject(new Error("Minimal 1 penulis"));
                  }
                },
              },
            ]}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => {
                  return (
                    <>
                      <div>
                        <Typography.Text type="secondary" underline>
                          Kredit didapatkan:{" "}
                          {Number(
                            form.getFieldValue([
                              "internal_members",
                              field.name,
                              "credit_percentage",
                            ]) * selectedCategoryCredit
                          ) / 100}
                        </Typography.Text>
                      </div>
                      <div
                        key={field.key}
                        style={{
                          display: "flex",
                          alignItems: "baseline",
                          marginBottom: 8,
                        }}
                      >
                        <Form.Item
                          {...field}
                          name={[field.name, "activity_position_id"]}
                          fieldKey={[field.fieldKey, "activity_position_id"]}
                          rules={[
                            { required: true, message: "Posisi wajib diisi" },
                          ]}
                          style={{
                            marginRight: 8,
                            marginBottom: 8,
                            width: 200,
                          }}
                        >
                          <Select
                            placeholder="Posisi"
                            onChange={() => {}}
                            loading={activityPoses.length <= 0}
                            allowClear
                          >
                            {activityPoses.map((activity) => (
                              <Option
                                key={activity.id}
                                value={activity?.id?.toString() || ""}
                              >
                                {activity.name}
                              </Option>
                            ))}
                          </Select>
                        </Form.Item>
                        <Form.Item
                          {...field}
                          name={[field.name, "internal_member"]}
                          fieldKey={[field.fieldKey, "internal_member"]}
                          rules={[
                            { required: true, message: "Nama wajib diisi" },
                          ]}
                          style={{
                            width: "100%",
                            marginRight: 8,
                            marginBottom: 8,
                          }}
                        >
                          <Select
                            maxTagCount={1}
                            labelInValue
                            mode="multiple"
                            value={form.getFieldValue([
                              "internal_members",
                              field.fieldKey,
                              "internal_member",
                            ])}
                            placeholder="Cari nama penulis internal"
                            notFoundContent={
                              searchEmployeesRes.loading ? (
                                <Spin size="small" />
                              ) : null
                            }
                            filterOption={false}
                            onChange={(value) => {
                              const newValue = value[value.length - 1];

                              form.setFieldsValue({
                                internal_members: form
                                  .getFieldValue("internal_members")
                                  .map(
                                    (
                                      x: Array<{
                                        internal_member: Array<{
                                          value: string | number;
                                          label: string;
                                          key: string;
                                        }>;
                                      }>,
                                      i: number
                                    ) => {
                                      if (i === field.name) {
                                        return {
                                          ...x,
                                          internal_member: newValue
                                            ? [newValue]
                                            : [],
                                        };
                                      }
                                      return x;
                                    }
                                  ),
                              });
                            }}
                            onSearch={debounce((value) => {
                              searchEmployees({
                                variables: {
                                  term: `%${value}%`,
                                },
                              });
                            }, 400)}
                          >
                            {filteredEmployees.map((d) => (
                              <Option key={`${d.value}a`} value={d.value}>
                                {d.text}
                              </Option>
                            ))}
                          </Select>
                        </Form.Item>
                        <Form.Item
                          {...field}
                          name={[field.name, "credit_percentage"]}
                          fieldKey={[field.fieldKey, "credit_percentage"]}
                          initialValue={0}
                          rules={[
                            {
                              required: true,
                              message: "Persentase Angka Kredit wajib diisi",
                            },
                          ]}
                          style={{
                            marginRight: 8,
                            marginBottom: 8,
                            width: 240,
                          }}
                          validateStatus={
                            isTotalCreditError ? "warning" : undefined
                          }
                          help={
                            isTotalCreditError ? "Total harus 100%" : undefined
                          }
                        >
                          <InputNumber
                            min={0}
                            max={100}
                            style={{ width: "100%" }}
                            formatter={(value) => `${value}%`}
                            parser={(value: any) => value.replace("%", "")}
                          />
                        </Form.Item>
                        <MinusCircleOutlined
                          onClick={() => remove(field.name)}
                        />
                      </div>
                    </>
                  );
                })}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Internal
                  </Button>
                  <Form.ErrorList errors={errors} />
                </Form.Item>
              </>
            )}
          </Form.List>

          {/* external */}
          <Divider orientation="center" plain>
            Penulis Eksternal
          </Divider>
          <Form.List name="external_members">
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <div
                    key={field.key}
                    style={{
                      display: "flex",
                      alignItems: "baseline",
                      marginBottom: 8,
                    }}
                  >
                    <Form.Item
                      {...field}
                      name={[field.name, "activity_position_id"]}
                      fieldKey={[field.fieldKey, "activity_position_id"]}
                      rules={[
                        { required: true, message: "Posisi wajib diisi" },
                      ]}
                      style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                    >
                      <Select
                        placeholder="Posisi"
                        onChange={() => {}}
                        loading={activityPoses.length <= 0}
                        allowClear
                      >
                        {activityPoses.map((activity) => (
                          <Option
                            key={activity.id}
                            value={activity?.id?.toString() || ""}
                          >
                            {activity.name}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      {...field}
                      name={[field.name, "external_members"]}
                      fieldKey={[field.fieldKey, "external_members"]}
                      rules={[{ required: true, message: "Nama wajib diisi" }]}
                      style={{
                        width: "100%",
                        marginRight: 8,
                        marginBottom: 8,
                      }}
                    >
                      <Input placeholder="Nama" />
                    </Form.Item>

                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                  </div>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Penulis Eksternal
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          {stepNavigator}
        </>
      ),
    },
  ];

  return (
    <>
      <Modal
        title="Tambah Penelitian"
        visible={visible}
        onCancel={handleCancel}
        width={768}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            disabled={currentStep !== steps.length - 1}
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={onFinish}
          validateMessages={validateMessages}
          onValuesChange={(changed, all) => {
            console.log({
              changed,
              all,
            });
            if (changed.category_id && all.category_id) {
              const selectedCtg = categories.find(
                (ctg) => ctg.id === Number(changed.category_id)
              );
              setSelectedCategoryCredit(
                selectedCtg?.credit ? selectedCtg?.credit : 0
              );
            }

            if (changed.internal_members && all.internal_members) {
              const truthyInternalMembers =
                all.internal_members.filter(Boolean);

              const creditPercentages = truthyInternalMembers.reduce(
                (acc: number, curr: any) => {
                  return acc + Number(curr.credit_percentage);
                },
                0
              );

              if (creditPercentages !== 100) {
                setIsTotalCreditError(true);
              } else {
                setIsTotalCreditError(false);
              }
            }

            // This will trigger the filters so that user cant pick the same employee twice
            if (changed.internal_members && all.internal_members) {
              const truthyInternalMembers =
                all.internal_members.filter(Boolean);

              const selectedEmployeeIds = truthyInternalMembers
                .map((member: any) => {
                  const id =
                    (member.internal_member &&
                      member.internal_member.length > 0 &&
                      member.internal_member[0]?.value) ||
                    0;

                  return id;
                })
                .filter(Boolean);

              setSelectedEmployeeIDs(selectedEmployeeIds);
            }
          }}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>
      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Penelitian
      </Button>
    </>
  );
};

export default ResearchAddButton;
