import React from "react";
import {
  Button,
  Modal,
  Typography,
  Space,
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  Upload,
  Select,
  Steps,
  InputNumber,
  Divider,
  Spin,
} from "antd";
import moment from "moment";
import { flatten, debounce } from "lodash-es";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  MinusCircleOutlined,
  RightOutlined,
  UploadOutlined,
  LeftOutlined,
} from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";

import { createCustomRequest } from "../Common/uploadFile";
import {
  ActivityPositions,
  ResearchByPK_researches_by_pk,
  ResearchByPK_researches_by_pk_research_members,
  ResearchCategories,
  Researches,
  SearchEmployees,
  SearchEmployeesVariables,
  SoftDeleteResearchByPK,
  SoftDeleteResearchByPKVariables,
  UpdateResearch,
  UpdateResearchVariables,
  DeleteResearchMembers,
  DeleteResearchMembersVariables,
  InsertResearchMember,
  InsertResearchMemberVariables,
  ResearchActivityPositions,
} from "../globalTypes";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";

import { useHistory } from "react-router-dom";
import {
  DELETE_RESEARCH_MEMBERS,
  INSERT_RESEARCH_MEMBER,
  RESEARCHES,
  SOFT_DELETE_RESEARCH_BY_PK,
  UPDATE_RESEARCH,
} from "./gql/researchGql";
import { RESEARCH_CATEGORIES } from "../ResearchCategories/gql/researchCategoriesGql";
import { normFile } from "./AddForm";
import { SEARCH_EMPLOYEES } from "../Employees/gql/employeesGql";
import normalizeMembers from "../utils/normalizeMembers";
import { RESEARCH_ACTIVITY_POSITIONS } from "../ResearchCategories/gql/researchPositionsGql";

const { Text } = Typography;
const { Step } = Steps;
const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 },
};
const validateMessages = {
  required: "${label} wajib diisi!",
};

type MutationActionModalProps = {
  record: ResearchByPK_researches_by_pk;
  onEditFinish?: (id: number) => void;
};

const getExternalMemberInitialValues = (
  members: ResearchByPK_researches_by_pk_research_members[]
) => {
  return members
    .filter((member) => !member.employee)
    .map((extMember) => ({
      activity_position_id: extMember.research_activity_position?.id.toString(),
      external_members: extMember.external_author,
    }));
};

const getInternalMemberInitialValues = (
  members: ResearchByPK_researches_by_pk_research_members[]
) => {
  return members
    .filter((mem) => Boolean(mem.employee))
    .map((intMember) => ({
      activity_position_id: intMember.research_activity_position?.id.toString(),
      internal_member: [
        {
          value: intMember.employee?.id,
          key: intMember.employee?.id,
          label: intMember.employee?.name,
        },
      ],
    }));
};

const MutationActionsModalResearch = (props: MutationActionModalProps) => {
  const history = useHistory();

  // deleting
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const [deleteLoading, setDeleteLoading] = React.useState(false);
  const [del] = useMutation<
    SoftDeleteResearchByPK,
    SoftDeleteResearchByPKVariables
  >(SOFT_DELETE_RESEARCH_BY_PK, {
    onCompleted: () => {
      setDeleteLoading(false);
      history.push("/dashboard/manage/researches");
    },
  });
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };
  const handleDeleteOk = () => {
    setDeleteLoading(true);
    del({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<Researches>({
            query: RESEARCHES,
          });

          if (existing) {
            const newResearches = existing.researches.filter(
              (data) => data.id !== props.record.id
            );

            cache.writeQuery({
              query: RESEARCHES,
              data: { researches: newResearches },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // editing
  const [searchEmployees, searchEmployeesRes] = useLazyQuery<
    SearchEmployees,
    SearchEmployeesVariables
  >(SEARCH_EMPLOYEES, {
    onCompleted: (data) => {},
  });
  const activityPosRes = useQuery<ResearchActivityPositions>(
    RESEARCH_ACTIVITY_POSITIONS
  );

  const categoriesRes = useQuery<ResearchCategories>(RESEARCH_CATEGORIES);
  const categories = categoriesRes?.data?.research_categories || [];
  const activityPoses = activityPosRes?.data?.research_activity_positions || [];
  const searchedEmployees = searchEmployeesRes?.data?.employees || [];
  const [selectedEmployeeIDs, setSelectedEmployeeIDs] = React.useState<
    number[]
  >(
    props.record.research_members.map((mem) => mem.employee?.id).filter(Boolean)
  );
  const filteredEmployees = searchedEmployees
    ? searchedEmployees.filter(
        (emp) => !selectedEmployeeIDs.includes(emp.value)
      )
    : [];

  const selectedCategoryCredit = props.record?.credit_multiplier || 0;
  const [isTotalCreditError, setIsTotalCreditError] =
    React.useState<boolean>(true);

  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const [editLoading, setEditLoading] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    if (isTotalCreditError) {
      return;
    }

    form.submit();
  };

  const validateList = [
    ["title", "category_id", "fund_amount", "started_at", "finished_at"],
    ["proposal_file", "final_report_file", "certificate_file", "decree_file"],
  ];
  const handleNextStep = () => {
    const validate = form.validateFields(validateList[currentStep]);
    validate
      .then((res) => {
        setCurrentStep((prev) => prev + 1);
      })
      .catch((err) => {});
  };
  const handlePreviousStep = () => {
    setCurrentStep((prev) => prev - 1);
  };
  const [currentStep, setCurrentStep] = React.useState(0);
  const stepNavigator = (
    <>
      <Row justify="space-between">
        <Col span={2}>
          {currentStep > 0 && (
            <Button type="default" onClick={handlePreviousStep}>
              <LeftOutlined />
            </Button>
          )}
        </Col>
        <Col span={2}>
          {currentStep < 2 && (
            <Button type="default" onClick={handleNextStep}>
              <RightOutlined />
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
  const onFundAmtChange = (value: any) => {
    const reg = /^-?\d*(\.\d*)?$/;

    if ((!isNaN(value) && reg.test(value)) || value === "" || value === "-") {
      form.setFieldsValue({ fund_amount: value });
    }
  };
  // Uploads
  const [proposalFileList, setProposalFileList] = React.useState([
    {
      name: "cert.jpg",
      type: "image/jpeg",
      uid: "1",
      url: props.record.proposal_file || "",
      size: 0,
    },
  ]);
  const handleProposalUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setProposalFileList(fileList);
  };

  const [finalReportFileList, setFinalReportFileList] = React.useState(
    props.record.final_report_file
      ? [
          {
            name: "cert.jpg",
            type: "image/jpeg",
            uid: "1",
            url: props.record.final_report_file || "",
            size: 0,
          },
        ]
      : []
  );
  const handleFinalReportUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setFinalReportFileList(fileList);
  };

  const [certificationFileList, setCertificationFileList] = React.useState(
    props.record.certificate_file
      ? [
          {
            name: "cert.jpg",
            type: "image/jpeg",
            uid: "1",
            url: props.record.certificate_file || "",
            size: 0,
          },
        ]
      : []
  );
  const handleCertificationUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setCertificationFileList(fileList);
  };

  const [decreeFileList, setDecreeFileList] = React.useState(
    props.record.decree_file
      ? [
          {
            name: "cert.jpg",
            type: "image/jpeg",
            uid: "1",
            url: props.record.decree_file || "",
            size: 0,
          },
        ]
      : []
  );
  const handleDecreeUploadChange = (info: any) => {
    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);

    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        const eager = file.response?.eager;
        file.url = eager && eager.length > 0 ? eager[0].secure_url : "";
      }
      return file;
    });

    // @ts-ignore
    setDecreeFileList(fileList);
  };

  const steps = [
    {
      title: "Data Penelitian",
      content: (
        <>
          <Form.Item
            name={"title"}
            label="Judul"
            rules={[{ required: true }]}
            initialValue={props.record.title}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"category_id"}
            label="Kategori"
            rules={[{ required: true }]}
            initialValue={props.record.research_category?.id.toString()}
            extra="Kategori tidak dapat diubah karena akan memengaruhi angka kredit."
          >
            <Select
              placeholder="Pilih Kategori Penelitian"
              loading={categories.length <= 0}
              disabled
            >
              {categories.map((ctg) => (
                <Option key={ctg.id} value={ctg?.id?.toString() || ""}>
                  {ctg.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"fund_amount"}
            label="Besar Dana"
            rules={[{ required: true }]}
            initialValue={props.record.fund_amount}
          >
            <InputNumber
              formatter={(value) =>
                `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
              }
              parser={(value: any) => value.replace(/Rp\s?|(\.*)/g, "")}
              style={{ width: "100%" }}
              onChange={onFundAmtChange}
            />
          </Form.Item>
          <Form.Item
            name={"started_at"}
            label="Tanggal Mulai"
            rules={[{ required: true }]}
            initialValue={moment(props.record.started_at, "YYYY-MM-DD")}
          >
            <DatePicker
              format="DD/MM/YYYY"
              showToday={false}
              onChange={(startDateValue) => {
                const endDate: moment.Moment =
                  form.getFieldValue("finished_at");
                if (startDateValue && endDate) {
                  if (endDate.isBefore(startDateValue)) {
                    form.setFieldsValue({ finished_at: startDateValue });
                  }
                }
              }}
            />
          </Form.Item>

          <Form.Item
            name={"finished_at"}
            label="Tanggal Selesai"
            initialValue={
              props.record.finished_at
                ? moment(props.record.finished_at, "YYYY-MM-DD")
                : undefined
            }
          >
            <DatePicker
              format="DD/MM/YYYY"
              disabledDate={(current) => {
                const startedAt = form.getFieldValue("started_at");
                // Can not select days after today and today
                if (startedAt) {
                  return current.isBefore(startedAt);
                } else {
                  return false;
                }
              }}
              showToday={false}
              onChange={(endDateValue) => {
                const startDate: moment.Moment =
                  form.getFieldValue("started_at");
                if (endDateValue && startDate) {
                  if (startDate.isAfter(endDateValue)) {
                    form.setFieldsValue({ started_at: endDateValue });
                  }
                }
              }}
            />
          </Form.Item>
          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Berkas",
      content: (
        <>
          <Form.Item
            name={"proposal_file"}
            label="Proposal"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true }]}
            initialValue={[
              {
                name: "File",
                type: "image.jpg",
                uid: "1",
                // @ts-ignore
                status: "done",
                url: props.record.proposal_file,
                size: 0,
              },
            ]}
          >
            <Upload
              customRequest={createCustomRequest("research_proposal_file")}
              onChange={handleProposalUploadChange}
              fileList={proposalFileList}
              onRemove={() => setProposalFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {proposalFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"final_report_file"}
            label="Laporan Akhir"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            initialValue={
              props.record.final_report_file
                ? [
                    {
                      name: "File",
                      type: "image.jpg",
                      uid: "1",
                      // @ts-ignore
                      status: "done",
                      url: props.record.final_report_file,
                      size: 0,
                    },
                  ]
                : undefined
            }
          >
            <Upload
              customRequest={createCustomRequest("research_final_report_file")}
              onChange={handleFinalReportUploadChange}
              fileList={finalReportFileList}
              onRemove={() => setFinalReportFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {finalReportFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"certificate_file"}
            label="Sertifikat"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            initialValue={
              props.record.certificate_file
                ? [
                    {
                      name: "File",
                      type: "image.jpg",
                      uid: "1",
                      // @ts-ignore
                      status: "done",
                      url: props.record.certificate_file,
                      size: 0,
                    },
                  ]
                : undefined
            }
          >
            <Upload
              customRequest={createCustomRequest("research_certification_file")}
              onChange={handleCertificationUploadChange}
              fileList={certificationFileList}
              onRemove={() => setCertificationFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {certificationFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            name={"decree_file"}
            label="SK / Surat Tugas"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            initialValue={
              props.record.decree_file
                ? [
                    {
                      name: "File",
                      type: "image.jpg",
                      uid: "1",
                      // @ts-ignore
                      status: "done",
                      url: props.record.decree_file,
                      size: 0,
                    },
                  ]
                : undefined
            }
          >
            <Upload
              customRequest={createCustomRequest("research_decree_file")}
              onChange={handleDecreeUploadChange}
              fileList={decreeFileList}
              onRemove={() => setDecreeFileList([])}
              accept=".pdf,.jpeg,.jpg,.png"
            >
              {decreeFileList.length <= 0 && (
                <Button>
                  <UploadOutlined /> Upload File
                </Button>
              )}
            </Upload>
          </Form.Item>

          {stepNavigator}
        </>
      ),
    },
    {
      title: "Data Anggota",
      content: (
        <>
          <Divider orientation="center" plain>
            Penulis Internal
          </Divider>
          <Row justify="center" style={{ paddingBottom: 16 }}>
            <Typography.Text>Angka Kredit Terbagi:&nbsp;</Typography.Text>
            <Typography.Text strong>{selectedCategoryCredit}</Typography.Text>
          </Row>
          <Form.List
            name="internal_members"
            rules={[
              {
                validator: async (_, names) => {
                  if (!names || names.length < 1) {
                    return Promise.reject(new Error("Minimal 1 anggota"));
                  }
                },
              },
            ]}
            initialValue={getInternalMemberInitialValues(
              props.record.research_members
            )}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <>
                    <div>
                      <Typography.Text type="secondary" underline>
                        Kredit didapatkan:{" "}
                        {Number(
                          form.getFieldValue([
                            "internal_members",
                            field.name,
                            "credit_percentage",
                          ]) * selectedCategoryCredit
                        ) / 100}
                      </Typography.Text>
                    </div>
                    <div
                      key={field.key}
                      style={{
                        display: "flex",
                        alignItems: "baseline",
                        marginBottom: 8,
                      }}
                    >
                      <Form.Item
                        {...field}
                        name={[field.name, "activity_position_id"]}
                        fieldKey={[field.fieldKey, "activity_position_id"]}
                        rules={[
                          { required: true, message: "Posisi wajib diisi" },
                        ]}
                        style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                      >
                        <Select
                          placeholder="Posisi"
                          onChange={() => {}}
                          loading={activityPoses.length <= 0}
                          allowClear
                        >
                          {activityPoses.map((activity) => (
                            <Option
                              key={activity.id}
                              value={activity?.id?.toString() || ""}
                            >
                              {activity.name}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "internal_member"]}
                        fieldKey={[field.fieldKey, "internal_member"]}
                        rules={[
                          { required: true, message: "Nama wajib diisi" },
                        ]}
                        style={{
                          width: "100%",
                          marginRight: 8,
                          marginBottom: 8,
                        }}
                      >
                        <Select
                          maxTagCount={1}
                          labelInValue
                          mode="multiple"
                          value={form.getFieldValue([
                            "internal_members",
                            field.fieldKey,
                            "internal_member",
                          ])}
                          placeholder="Cari nama anggota internal"
                          notFoundContent={
                            searchEmployeesRes.loading ? (
                              <Spin size="small" />
                            ) : null
                          }
                          filterOption={false}
                          onChange={(value) => {
                            const newValue = value[value.length - 1];

                            form.setFieldsValue({
                              internal_members: form
                                .getFieldValue("internal_members")
                                .map(
                                  (
                                    x: Array<{
                                      internal_member: Array<{
                                        value: string | number;
                                        label: string;
                                        key: string;
                                      }>;
                                    }>,
                                    i: number
                                  ) => {
                                    if (i === field.name) {
                                      return {
                                        ...x,
                                        internal_member: newValue
                                          ? [newValue]
                                          : [],
                                      };
                                    }
                                    return x;
                                  }
                                ),
                            });
                          }}
                          onSearch={debounce((value) => {
                            searchEmployees({
                              variables: {
                                term: `%${value}%`,
                              },
                            });
                          }, 400)}
                        >
                          {filteredEmployees.map((d) => (
                            <Option key={`${d.value}a`} value={d.value}>
                              {d.text}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "credit_percentage"]}
                        fieldKey={[field.fieldKey, "credit_percentage"]}
                        initialValue={0}
                        rules={[
                          {
                            required: true,
                            message: "Persentase Angka Kredit wajib diisi",
                          },
                        ]}
                        style={{
                          marginRight: 8,
                          marginBottom: 8,
                          width: 240,
                        }}
                        validateStatus={
                          isTotalCreditError ? "warning" : undefined
                        }
                        help={
                          isTotalCreditError ? "Total harus 100%" : undefined
                        }
                      >
                        <InputNumber
                          min={0}
                          max={100}
                          style={{ width: "100%" }}
                          formatter={(value) => `${value}%`}
                          parser={(value: any) => value.replace("%", "")}
                        />
                      </Form.Item>
                      <MinusCircleOutlined onClick={() => remove(field.name)} />
                    </div>
                  </>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Anggota Internal
                  </Button>
                  <Form.ErrorList errors={errors} />
                </Form.Item>
              </>
            )}
          </Form.List>

          {/* external */}
          <Divider orientation="center" plain>
            Anggota Eksternal
          </Divider>
          <Form.List
            name="external_members"
            initialValue={getExternalMemberInitialValues(
              props.record.research_members
            )}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field) => (
                  <div
                    key={field.key}
                    style={{
                      display: "flex",
                      alignItems: "baseline",
                      marginBottom: 8,
                    }}
                  >
                    <Form.Item
                      {...field}
                      name={[field.name, "activity_position_id"]}
                      fieldKey={[field.fieldKey, "activity_position_id"]}
                      rules={[
                        { required: true, message: "Posisi wajib diisi" },
                      ]}
                      style={{ marginRight: 8, marginBottom: 8, width: 200 }}
                    >
                      <Select
                        placeholder="Posisi"
                        onChange={() => {}}
                        loading={activityPoses.length <= 0}
                        allowClear
                      >
                        {activityPoses.map((activity) => (
                          <Option
                            key={activity.id}
                            value={activity?.id?.toString() || ""}
                          >
                            {activity.name}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      {...field}
                      name={[field.name, "external_members"]}
                      fieldKey={[field.fieldKey, "external_members"]}
                      rules={[{ required: true, message: "Nama wajib diisi" }]}
                      style={{
                        width: "100%",
                        marginRight: 8,
                        marginBottom: 8,
                      }}
                    >
                      <Input placeholder="Nama" />
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                  </div>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Tambah Anggota Eksternal
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          {stepNavigator}
        </>
      ),
    },
  ];

  const [updateResearch, { loading: updateResearchLoading }] =
    useMutation<UpdateResearch, UpdateResearchVariables>(UPDATE_RESEARCH);
  const [deleteResearchMembers, { loading: deleteResMemLoading }] = useMutation<
    DeleteResearchMembers,
    DeleteResearchMembersVariables
  >(DELETE_RESEARCH_MEMBERS);
  const [insertResearchMember, { loading: insertResMemLoading }] = useMutation<
    InsertResearchMember,
    InsertResearchMemberVariables
  >(INSERT_RESEARCH_MEMBER);
  const handleEditFinish = (values: Store) => {
    const vals = form.getFieldsValue(flatten(validateList));

    const proposalFileVal = vals.proposal_file;
    const finalReportFileVal = vals.final_report_file;
    const certificateFileVal = vals.certificate_file;
    const decreeFileVal = vals.decree_file;

    let proposalFile = "";
    let finalRepFile = "";
    let certFile = "";
    let decreeFile = "";

    try {
      if (proposalFileVal && proposalFileVal.length > 0) {
        // berarti ganti file
        proposalFile = proposalFileVal[proposalFileVal.length - 1].url;
      }
      if (finalReportFileVal && finalReportFileVal.length > 0) {
        // berarti ganti file
        finalRepFile = finalReportFileVal[finalReportFileVal.length - 1].url;
      }
      if (certificateFileVal && certificateFileVal.length > 0) {
        // berarti ganti file
        certFile = certificateFileVal[certificateFileVal.length - 1].url;
      }
      if (decreeFileVal && decreeFileVal.length > 0) {
        // berarti ganti file
        decreeFile = decreeFileVal[decreeFileVal.length - 1].url;
      }
    } catch (err) {
      console.log("err", err);
    }

    const updateResearchPayload = {
      ...vals,
      started_at: vals.started_at.format("YYYY-MM-DD"),
      finished_at: vals.finished_at
        ? vals.finished_at.format("YYYY-MM-DD")
        : undefined,
      category_id: Number(vals.category_id),
      proposal_file: proposalFile,
      final_report_file: finalRepFile,
      certificate_file: certFile,
      decree_file: decreeFile,
    };

    Promise.all([
      updateResearch({
        variables: {
          id: props.record.id,
          _set: {
            ...updateResearchPayload,
          },
        },
      }),
      new Promise((resolve, reject) => {
        deleteResearchMembers({
          variables: {
            researchId: props.record.id,
          },
        })
          .then(() => {
            const insertResearchMembersPayload = normalizeMembers(
              props.record.id,
              "research_id",
              values.internal_members,
              values.external_members,
              selectedCategoryCredit
            );

            insertResearchMember({
              variables: {
                objects: insertResearchMembersPayload,
              },
            }).then(() => {
              resolve(true);
            });
          })
          .catch((err) => {
            reject(err);
          });
      }),
    ])
      .then(() => {
        setCurrentStep(0);
        form.resetFields();
        setIsTotalCreditError(true);
        setEditLoading(false);
        setEditModalVisible(false);
        props.onEditFinish && props.onEditFinish(props.record.id);
      })
      .catch((err) => {
        setEditLoading(false);
        setEditModalVisible(false);
      });
  };

  return (
    <>
      <Modal
        title="Hapus Penelitian"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">Judul: {props.record.title}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Penelitian"
        visible={editModalvisible}
        width={768}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            disabled={
              currentStep !== steps.length - 1 ||
              updateResearchLoading ||
              deleteResMemLoading ||
              insertResMemLoading
            }
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-employees"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
          onValuesChange={(changed, all) => {
            // Commented bcs category id cant be changed
            // if (changed.category_id && all.category_id) {
            //   const selectedCtg = categories.find(
            //     (ctg) => ctg.id === Number(changed.category_id)
            //   );
            //   setSelectedCategoryCredit(
            //     selectedCtg?.credit ? selectedCtg?.credit : 0
            //   );
            // }
            if (changed.internal_members && all.internal_members) {
              const truthyInternalMembers =
                all.internal_members.filter(Boolean);

              const creditPercentages = truthyInternalMembers.reduce(
                (acc: number, curr: any) => {
                  return acc + Number(curr.credit_percentage);
                },
                0
              );

              if (creditPercentages !== 100) {
                setIsTotalCreditError(true);
              } else {
                setIsTotalCreditError(false);
              }
            }

            // This will trigger the filters so that user cant pick the same employee twice
            if (changed.internal_members && all.internal_members) {
              const selectedEmployeeIds = all.internal_members
                .filter(Boolean)
                .map((member: any) => {
                  const id =
                    (member.internal_member &&
                      member.internal_member.length > 0 &&
                      member.internal_member[0]?.value) ||
                    0;

                  return id;
                })
                .filter(Boolean);

              setSelectedEmployeeIDs(selectedEmployeeIds);
            }
          }}
        >
          <Steps progressDot size="small" current={currentStep}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div style={{ marginTop: 24 }}>{steps[currentStep].content}</div>
        </Form>
      </Modal>

      <Row justify="space-around" style={{ marginTop: 16 }}>
        <Col span={14}>
          <Button block onClick={handleEditClick}>
            <EditOutlined /> Ubah
          </Button>
        </Col>
        <Col span={14} style={{ marginTop: 16 }}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined /> Hapus
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionsModalResearch;
