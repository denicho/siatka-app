import React from "react";
import { Table, Row, Col, Space, Button, Input } from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  InfoCircleTwoTone,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

import ManagementHeading from "../Common/ManagementHeading";
// import StaffAddButton from "./AddFormStaff";
// import LecturerAddButton from "./AddFormLecturer";
import stringTableSorter from "../utils/stringTableSorter";
import { useQuery } from "@apollo/client";
import { RESEARCHES } from "./gql/researchGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";
import { Researches, Researches_researches } from "../globalTypes";
import ResearchAddButton from "./AddForm";
import { get } from "lodash-es";

type Props = {
  id: string;
};

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: `/dashboard/manage/researches`,
    content: `Pengelolaan Penelitian`,
  },
];

const EmployeeListPage = () => {
  const history = useHistory();
  const researchesRes = useQuery<Researches>(RESEARCHES);
  const [searchText, setSearchText] = React.useState("");
  const [searchedCol, setSearchedCol] = React.useState("");
  const searchInput = React.useRef<Input>(null);

  if (
    researchesRes.loading ||
    !researchesRes.data ||
    !researchesRes.data.researches
  ) {
    return <LoadingPlaceholder />;
  }

  const researches = researchesRes.data.researches;

  // Filtering

  const handleSearch = (
    selectedKeys: string,
    confirm: () => void,
    dataIndex: string
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedCol(dataIndex);
  };
  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (
    dataIndex: string | string[],
    label: string
  ) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            if (searchInput !== null) {
              // @ts-ignore
              searchInput.current = node;
            }
          }}
          placeholder={`Cari ${label}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(
              selectedKeys,
              confirm,
              Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
            )
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(
                selectedKeys,
                confirm,
                Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
              )
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Cari
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (
      value: string | number | boolean,
      record: Researches_researches
    ) => {
      const searched = get(
        record,
        Array.isArray(dataIndex) ? dataIndex.join(".") : dataIndex
      );

      // @ts-ignore
      return searched
        ? // @ts-ignore
          searched
            .toString()
            .toLowerCase()
            // @ts-ignore
            .includes(value.toLowerCase())
        : "";
    },
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => {
          if (searchInput.current) {
            searchInput.current.select();
          }
        }, 100);
      }
    },
    render: (text: string) => {
      const shouldHighlight = Array.isArray(dataIndex)
        ? searchedCol === dataIndex.join(".")
        : searchedCol === dataIndex;

      return shouldHighlight ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      );
    },
  });

  const columns = [
    {
      title: "Judul",
      dataIndex: "title",
      key: "title",
      sorter: (a: Researches_researches, b: Researches_researches) => {
        return stringTableSorter(a.title || "", b.title || "");
      },
      ...getColumnSearchProps("title", "Judul"),
    },
    {
      title: "Kategori",
      dataIndex: ["research_category", "name"],
      key: "research_category.name",
      sorter: (a: Researches_researches, b: Researches_researches) => {
        return stringTableSorter(
          a.research_category?.name || "",
          b.research_category?.name || ""
        );
      },
      ...getColumnSearchProps("research_category.name", "Kategori"),
    },
    {
      title: "Detail",
      key: "detail",
      dataIndex: "detail",
      width: 140,
      render: (text: any, record: any, index: any) => {
        return <InfoCircleTwoTone onClick={() => {}} />;
      },
    },
  ];

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Penelitian"
        breadcrumbs={breadcrumbs}
        rightAddon={<ResearchAddButton onFinish={researchesRes.refetch} />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  history.push(`/dashboard/manage/researches/${record.id}`);
                },
              };
            }}
            size="middle"
            rowKey="id"
            columns={columns}
            dataSource={researches}
          />
        </Col>
      </Row>
    </>
  );
};

export default EmployeeListPage;
