import React from "react";
import { Table, Row, Col } from "antd";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import { HomeOutlined } from "@ant-design/icons";
import ManagementHeading from "../Common/ManagementHeading";
import DepartmentAddButton from "./AddForm";
import MutationActionsModal from "./MutationActionsModal";
import { useQuery } from "@apollo/client";
import { StructuralPositions } from "../globalTypes";
import { STRUCTURAL_POS } from "./gql/structuralPositionGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/structural-positions",
    content: "Pengelolaan Jabatan Struktural",
  },
];

const StructuralPositionsListPage = () => {
  // const deps = useResource(StructuralPositionsResource.listShape(), {});
  const structPosRes = useQuery<StructuralPositions>(STRUCTURAL_POS);

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (
    structPosRes.loading ||
    !structPosRes.data ||
    !structPosRes.data.structural_positions
  ) {
    return <LoadingPlaceholder />;
  }

  const structPoses = structPosRes.data.structural_positions;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Jabatan Struktural"
        breadcrumbs={breadcrumbs}
        rightAddon={<DepartmentAddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={structPoses}
          />
        </Col>
      </Row>
    </>
  );
};

export default StructuralPositionsListPage;
