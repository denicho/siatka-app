import React from "react";
import { Button, Modal, Typography, Space, Row, Col, Form, Input } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Store } from "antd/lib/form/interface";
import { useMutation } from "@apollo/client";
import {
  DeleteStructuralPosition,
  DeleteStructuralPositionVariables,
  StructuralPositions,
  UpdateStructuralPosition,
  UpdateStructuralPositionVariables,
} from "../globalTypes";
import {
  DELETE_STRUCTURAL_POS,
  STRUCTURAL_POS,
  UPDATE_STRUCTURAL_POS,
} from "./gql/structuralPositionGql";

const { Text } = Typography;

type MutationActionModalProps = {
  record: any;
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const MutationActionModal = (props: MutationActionModalProps) => {
  // Deleting
  const [deleteModalvisible, setDeleteModalVisible] = React.useState(false);
  const handleDeleteClick = () => {
    setDeleteModalVisible(true);
  };
  const handleDeleteCancel = () => {
    setDeleteModalVisible(false);
  };

  const [deleteStructPos, { loading: deleteLoading }] = useMutation<
    DeleteStructuralPosition,
    DeleteStructuralPositionVariables
  >(DELETE_STRUCTURAL_POS, {
    onCompleted: () => {
      setDeleteModalVisible(false);
    },
    onError: () => {
      setDeleteModalVisible(false);
    },
  });
  const handleDeleteOk = () => {
    deleteStructPos({
      variables: {
        id: props.record.id,
      },
      update: (cache) => {
        try {
          const existing = cache.readQuery<StructuralPositions>({
            query: STRUCTURAL_POS,
          });

          if (existing) {
            const newFuncPos = existing.structural_positions.filter(
              (g: any) => g.id !== props.record.id
            );
            cache.writeQuery({
              query: STRUCTURAL_POS,
              data: { structural_positions: newFuncPos },
            });
          }
        } catch (err) {
          console.log(err);
        }
      },
    });
  };

  // Editing
  const [form] = Form.useForm();
  const [editModalvisible, setEditModalVisible] = React.useState(false);
  const handleEditClick = () => {
    setEditModalVisible(true);
  };
  const handleEditCancel = () => {
    setEditModalVisible(false);
  };
  const handleEditOk = () => {
    form.submit();
  };

  const [update, { loading: editLoading }] = useMutation<
    UpdateStructuralPosition,
    UpdateStructuralPositionVariables
  >(UPDATE_STRUCTURAL_POS, {
    onCompleted: () => {
      setEditModalVisible(false);
    },
  });
  const handleEditFinish = (values: Store) => {
    update({
      variables: {
        pk_columns: {
          id: props.record.id,
        },
        _set: {
          name: values.name,
        },
      },
    });
  };

  return (
    <>
      <Modal
        title="Hapus Jabatan Struktural"
        visible={deleteModalvisible}
        onCancel={handleDeleteCancel}
        footer={[
          <Button key="back" onClick={handleDeleteCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            danger
            loading={deleteLoading}
            onClick={handleDeleteOk}
          >
            Hapus
          </Button>,
        ]}
      >
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini?</Text>
          <Text type="danger">ID: {props.record.id}</Text>
          <Text type="danger">Nama: {props.record.name}</Text>
        </Space>
      </Modal>

      <Modal
        title="Ubah Jabatan Struktural"
        visible={editModalvisible}
        onCancel={handleEditCancel}
        footer={[
          <Button key="back" onClick={handleEditCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={editLoading}
            onClick={handleEditOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-structuralPosition"
          onFinish={handleEditFinish}
          validateMessages={validateMessages}
        >
          <Form.Item
            name={"id"}
            label="ID"
            rules={[{ required: true }]}
            initialValue={props.record.id}
          >
            <Input disabled />
          </Form.Item>
          <Form.Item
            name={"name"}
            label="Nama"
            rules={[{ required: true }]}
            initialValue={props.record.name}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      <Row justify="space-around">
        <Col span={10}>
          <Button block onClick={handleEditClick}>
            <EditOutlined />
          </Button>
        </Col>
        <Col span={10}>
          <Button danger block onClick={handleDeleteClick}>
            <DeleteOutlined />
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default MutationActionModal;
