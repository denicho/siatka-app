import { gql } from "@apollo/client";

export const DELETE_STRUCTURAL_POS = gql`
  mutation DeleteStructuralPosition($id: bigint!) {
    delete_structural_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const STRUCTURAL_POS = gql`
  query StructuralPositions {
    structural_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_STRUCTURAL_POS = gql`
  mutation InsertStructuralPosition(
    $object: structural_positions_insert_input!
  ) {
    insert_structural_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_STRUCTURAL_POS = gql`
  mutation UpdateStructuralPosition(
    $pk_columns: structural_positions_pk_columns_input!
    $_set: structural_positions_set_input
  ) {
    update_structural_positions_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      created_at
      updated_at
    }
  }
`;
