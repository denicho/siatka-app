/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteActivityPositions
// ====================================================

export interface DeleteActivityPositions_delete_activity_positions_by_pk {
  __typename: "activity_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteActivityPositions {
  /**
   * delete single row from the table: "activity_positions"
   */
  delete_activity_positions_by_pk: DeleteActivityPositions_delete_activity_positions_by_pk | null;
}

export interface DeleteActivityPositionsVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ActivityPositions
// ====================================================

export interface ActivityPositions_activity_positions {
  __typename: "activity_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface ActivityPositions {
  /**
   * fetch data from the table: "activity_positions"
   */
  activity_positions: ActivityPositions_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertActivityPositions
// ====================================================

export interface InsertActivityPositions_insert_activity_positions_one {
  __typename: "activity_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertActivityPositions {
  /**
   * insert a single row into the table: "activity_positions"
   */
  insert_activity_positions_one: InsertActivityPositions_insert_activity_positions_one | null;
}

export interface InsertActivityPositionsVariables {
  object: activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateActivityPositions
// ====================================================

export interface UpdateActivityPositions_update_activity_positions_by_pk {
  __typename: "activity_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateActivityPositions {
  /**
   * update single row of the table: "activity_positions"
   */
  update_activity_positions_by_pk: UpdateActivityPositions_update_activity_positions_by_pk | null;
}

export interface UpdateActivityPositionsVariables {
  pk_columns: activity_positions_pk_columns_input;
  _set?: activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: BookPublications
// ====================================================

export interface BookPublications_book_publications_book_publication_category {
  __typename: "book_publication_categories";
  id: number;
  credit: any;
  name: string;
}

export interface BookPublications_book_publications_book_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface BookPublications_book_publications_book_publication_members_book_publication_activity_position {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
}

export interface BookPublications_book_publications_book_publication_members {
  __typename: "book_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: BookPublications_book_publications_book_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  book_publication_activity_position: BookPublications_book_publications_book_publication_members_book_publication_activity_position;
}

export interface BookPublications_book_publications {
  __typename: "book_publications";
  id: any;
  title: string;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  book_publication_category: BookPublications_book_publications_book_publication_category | null;
  /**
   * An array relationship
   */
  book_publication_members: BookPublications_book_publications_book_publication_members[];
  publisher_name: string;
  publish_year: number;
  city: string;
  book_file: string;
}

export interface BookPublications {
  /**
   * fetch data from the table: "book_publications"
   */
  book_publications: BookPublications_book_publications[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: BookPublicationByPK
// ====================================================

export interface BookPublicationByPK_book_publications_by_pk_book_publication_category {
  __typename: "book_publication_categories";
  id: number;
  credit: any;
  name: string;
}

export interface BookPublicationByPK_book_publications_by_pk_book_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface BookPublicationByPK_book_publications_by_pk_book_publication_members_book_publication_activity_position {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
}

export interface BookPublicationByPK_book_publications_by_pk_book_publication_members {
  __typename: "book_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: BookPublicationByPK_book_publications_by_pk_book_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  book_publication_activity_position: BookPublicationByPK_book_publications_by_pk_book_publication_members_book_publication_activity_position;
}

export interface BookPublicationByPK_book_publications_by_pk {
  __typename: "book_publications";
  id: any;
  title: string;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  book_publication_category: BookPublicationByPK_book_publications_by_pk_book_publication_category | null;
  /**
   * An array relationship
   */
  book_publication_members: BookPublicationByPK_book_publications_by_pk_book_publication_members[];
  publisher_name: string;
  publish_year: number;
  city: string;
  book_file: string;
}

export interface BookPublicationByPK {
  /**
   * fetch data from the table: "book_publications" using primary key columns
   */
  book_publications_by_pk: BookPublicationByPK_book_publications_by_pk | null;
}

export interface BookPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertBookPublications
// ====================================================

export interface InsertBookPublications_insert_book_publications_one_book_publication_category {
  __typename: "book_publication_categories";
  id: number;
  credit: any;
  name: string;
}

export interface InsertBookPublications_insert_book_publications_one_book_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertBookPublications_insert_book_publications_one_book_publication_members_book_publication_activity_position {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
}

export interface InsertBookPublications_insert_book_publications_one_book_publication_members {
  __typename: "book_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: InsertBookPublications_insert_book_publications_one_book_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  book_publication_activity_position: InsertBookPublications_insert_book_publications_one_book_publication_members_book_publication_activity_position;
}

export interface InsertBookPublications_insert_book_publications_one {
  __typename: "book_publications";
  id: any;
  title: string;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  book_publication_category: InsertBookPublications_insert_book_publications_one_book_publication_category | null;
  /**
   * An array relationship
   */
  book_publication_members: InsertBookPublications_insert_book_publications_one_book_publication_members[];
  publisher_name: string;
  publish_year: number;
  city: string;
  book_file: string;
}

export interface InsertBookPublications {
  /**
   * insert a single row into the table: "book_publications"
   */
  insert_book_publications_one: InsertBookPublications_insert_book_publications_one | null;
}

export interface InsertBookPublicationsVariables {
  object: book_publications_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertBookPublicationMember
// ====================================================

export interface InsertBookPublicationMember_insert_book_publication_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertBookPublicationMember_insert_book_publication_members_returning_book_publication_activity_position {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
}

export interface InsertBookPublicationMember_insert_book_publication_members_returning {
  __typename: "book_publication_members";
  id: any;
  credit_gain: any | null;
  book_publication_id: any;
  /**
   * An object relationship
   */
  employee: InsertBookPublicationMember_insert_book_publication_members_returning_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  book_publication_activity_position: InsertBookPublicationMember_insert_book_publication_members_returning_book_publication_activity_position;
}

export interface InsertBookPublicationMember_insert_book_publication_members {
  __typename: "book_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertBookPublicationMember_insert_book_publication_members_returning[];
}

export interface InsertBookPublicationMember {
  /**
   * insert data into the table: "book_publication_members"
   */
  insert_book_publication_members: InsertBookPublicationMember_insert_book_publication_members | null;
}

export interface InsertBookPublicationMemberVariables {
  objects: book_publication_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateBookPublication
// ====================================================

export interface UpdateBookPublication_update_book_publications_by_pk_book_publication_category {
  __typename: "book_publication_categories";
  id: number;
  credit: any;
  name: string;
}

export interface UpdateBookPublication_update_book_publications_by_pk {
  __typename: "book_publications";
  id: any;
  title: string;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  book_publication_category: UpdateBookPublication_update_book_publications_by_pk_book_publication_category | null;
  book_file: string;
}

export interface UpdateBookPublication {
  /**
   * update single row of the table: "book_publications"
   */
  update_book_publications_by_pk: UpdateBookPublication_update_book_publications_by_pk | null;
}

export interface UpdateBookPublicationVariables {
  id: any;
  _set?: book_publications_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteBookPublicationMembers
// ====================================================

export interface DeleteBookPublicationMembers_delete_book_publication_members {
  __typename: "book_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeleteBookPublicationMembers {
  /**
   * delete data from the table: "book_publication_members"
   */
  delete_book_publication_members: DeleteBookPublicationMembers_delete_book_publication_members | null;
}

export interface DeleteBookPublicationMembersVariables {
  book_publication_id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteBookPublicationByPK
// ====================================================

export interface DeleteBookPublicationByPK_delete_book_publications_by_pk {
  __typename: "book_publications";
  id: any;
}

export interface DeleteBookPublicationByPK {
  /**
   * delete single row from the table: "book_publications"
   */
  delete_book_publications_by_pk: DeleteBookPublicationByPK_delete_book_publications_by_pk | null;
}

export interface DeleteBookPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeleteBookPublicationByPK
// ====================================================

export interface SoftDeleteBookPublicationByPK_update_book_publications_by_pk {
  __typename: "book_publications";
  id: any;
}

export interface SoftDeleteBookPublicationByPK {
  /**
   * update single row of the table: "book_publications"
   */
  update_book_publications_by_pk: SoftDeleteBookPublicationByPK_update_book_publications_by_pk | null;
}

export interface SoftDeleteBookPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteBookPublicationCategories
// ====================================================

export interface DeleteBookPublicationCategories_delete_book_publication_categories_by_pk {
  __typename: "book_publication_categories";
  id: number;
  name: string;
  credit: any;
  created_at: any;
  updated_at: any;
}

export interface DeleteBookPublicationCategories {
  /**
   * delete single row from the table: "book_publication_categories"
   */
  delete_book_publication_categories_by_pk: DeleteBookPublicationCategories_delete_book_publication_categories_by_pk | null;
}

export interface DeleteBookPublicationCategoriesVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: BookPublicationCategories
// ====================================================

export interface BookPublicationCategories_book_publication_categories {
  __typename: "book_publication_categories";
  id: number;
  name: string;
  credit: any;
  created_at: any;
  updated_at: any;
}

export interface BookPublicationCategories {
  /**
   * fetch data from the table: "book_publication_categories"
   */
  book_publication_categories: BookPublicationCategories_book_publication_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertBookPublicationCategories
// ====================================================

export interface InsertBookPublicationCategories_insert_book_publication_categories_one {
  __typename: "book_publication_categories";
  id: number;
  name: string;
  credit: any;
  created_at: any;
  updated_at: any;
}

export interface InsertBookPublicationCategories {
  /**
   * insert a single row into the table: "book_publication_categories"
   */
  insert_book_publication_categories_one: InsertBookPublicationCategories_insert_book_publication_categories_one | null;
}

export interface InsertBookPublicationCategoriesVariables {
  object: book_publication_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateBookPublicationCategories
// ====================================================

export interface UpdateBookPublicationCategories_update_book_publication_categories_by_pk {
  __typename: "book_publication_categories";
  id: number;
  name: string;
  credit: any;
  created_at: any;
  updated_at: any;
}

export interface UpdateBookPublicationCategories {
  /**
   * update single row of the table: "book_publication_categories"
   */
  update_book_publication_categories_by_pk: UpdateBookPublicationCategories_update_book_publication_categories_by_pk | null;
}

export interface UpdateBookPublicationCategoriesVariables {
  pk_columns: book_publication_categories_pk_columns_input;
  _set?: book_publication_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteBookPublicationActivityPositions
// ====================================================

export interface DeleteBookPublicationActivityPositions_delete_book_publication_activity_positions_by_pk {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeleteBookPublicationActivityPositions {
  /**
   * delete single row from the table: "book_publication_activity_positions"
   */
  delete_book_publication_activity_positions_by_pk: DeleteBookPublicationActivityPositions_delete_book_publication_activity_positions_by_pk | null;
}

export interface DeleteBookPublicationActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: BookPublicationActivityPositions
// ====================================================

export interface BookPublicationActivityPositions_book_publication_activity_positions {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface BookPublicationActivityPositions {
  /**
   * fetch data from the table: "book_publication_activity_positions"
   */
  book_publication_activity_positions: BookPublicationActivityPositions_book_publication_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertBookPublicationActivityPositions
// ====================================================

export interface InsertBookPublicationActivityPositions_insert_book_publication_activity_positions_one {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertBookPublicationActivityPositions {
  /**
   * insert a single row into the table: "book_publication_activity_positions"
   */
  insert_book_publication_activity_positions_one: InsertBookPublicationActivityPositions_insert_book_publication_activity_positions_one | null;
}

export interface InsertBookPublicationActivityPositionsVariables {
  object: book_publication_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateBookPublicationActivityPositions
// ====================================================

export interface UpdateBookPublicationActivityPositions_update_book_publication_activity_positions_by_pk {
  __typename: "book_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdateBookPublicationActivityPositions {
  /**
   * update single row of the table: "book_publication_activity_positions"
   */
  update_book_publication_activity_positions_by_pk: UpdateBookPublicationActivityPositions_update_book_publication_activity_positions_by_pk | null;
}

export interface UpdateBookPublicationActivityPositionsVariables {
  pk_columns: book_publication_activity_positions_pk_columns_input;
  _set?: book_publication_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CommitteeLevel
// ====================================================

export interface CommitteeLevel_committee_level {
  __typename: "committee_level";
  level: string;
}

export interface CommitteeLevel {
  /**
   * fetch data from the table: "committee_level"
   */
  committee_level: CommitteeLevel_committee_level[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Committees
// ====================================================

export interface Committees_committees_committee_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface Committees_committees_committee_members {
  __typename: "committee_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: Committees_committees_committee_members_employee | null;
  external_author: string | null;
}

export interface Committees_committees_committee_category {
  __typename: "committee_categories";
  id: any;
  name: string | null;
}

export interface Committees_committees {
  __typename: "committees";
  id: any;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  /**
   * An array relationship
   */
  committee_members: Committees_committees_committee_members[];
  /**
   * An object relationship
   */
  committee_category: Committees_committees_committee_category | null;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: committee_level_enum;
}

export interface Committees {
  /**
   * fetch data from the table: "committees"
   */
  committees: Committees_committees[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CommitteeByPK
// ====================================================

export interface CommitteeByPK_committees_by_pk_committee_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface CommitteeByPK_committees_by_pk_committee_members {
  __typename: "committee_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: CommitteeByPK_committees_by_pk_committee_members_employee | null;
  external_author: string | null;
}

export interface CommitteeByPK_committees_by_pk_committee_category {
  __typename: "committee_categories";
  id: any;
  name: string | null;
}

export interface CommitteeByPK_committees_by_pk {
  __typename: "committees";
  id: any;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  /**
   * An array relationship
   */
  committee_members: CommitteeByPK_committees_by_pk_committee_members[];
  /**
   * An object relationship
   */
  committee_category: CommitteeByPK_committees_by_pk_committee_category | null;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: committee_level_enum;
}

export interface CommitteeByPK {
  /**
   * fetch data from the table: "committees" using primary key columns
   */
  committees_by_pk: CommitteeByPK_committees_by_pk | null;
}

export interface CommitteeByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertCommittees
// ====================================================

export interface InsertCommittees_insert_committees_one_committee_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertCommittees_insert_committees_one_committee_members {
  __typename: "committee_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: InsertCommittees_insert_committees_one_committee_members_employee | null;
  external_author: string | null;
}

export interface InsertCommittees_insert_committees_one_committee_category {
  __typename: "committee_categories";
  id: any;
  name: string | null;
}

export interface InsertCommittees_insert_committees_one {
  __typename: "committees";
  id: any;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  /**
   * An array relationship
   */
  committee_members: InsertCommittees_insert_committees_one_committee_members[];
  /**
   * An object relationship
   */
  committee_category: InsertCommittees_insert_committees_one_committee_category | null;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: committee_level_enum;
}

export interface InsertCommittees {
  /**
   * insert a single row into the table: "committees"
   */
  insert_committees_one: InsertCommittees_insert_committees_one | null;
}

export interface InsertCommitteesVariables {
  object: committees_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertCommitteeMember
// ====================================================

export interface InsertCommitteeMember_insert_committee_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertCommitteeMember_insert_committee_members_returning {
  __typename: "committee_members";
  id: any;
  committee_id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: InsertCommitteeMember_insert_committee_members_returning_employee | null;
  external_author: string | null;
}

export interface InsertCommitteeMember_insert_committee_members {
  __typename: "committee_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertCommitteeMember_insert_committee_members_returning[];
}

export interface InsertCommitteeMember {
  /**
   * insert data into the table: "committee_members"
   */
  insert_committee_members: InsertCommitteeMember_insert_committee_members | null;
}

export interface InsertCommitteeMemberVariables {
  objects: committee_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateCommittee
// ====================================================

export interface UpdateCommittee_update_committees_by_pk_committee_category {
  __typename: "committee_categories";
  id: any;
  name: string | null;
}

export interface UpdateCommittee_update_committees_by_pk {
  __typename: "committees";
  id: any;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  /**
   * An object relationship
   */
  committee_category: UpdateCommittee_update_committees_by_pk_committee_category | null;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: committee_level_enum;
}

export interface UpdateCommittee {
  /**
   * update single row of the table: "committees"
   */
  update_committees_by_pk: UpdateCommittee_update_committees_by_pk | null;
}

export interface UpdateCommitteeVariables {
  id: any;
  _set?: committees_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteCommitteeMembers
// ====================================================

export interface DeleteCommitteeMembers_delete_committee_members {
  __typename: "committee_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeleteCommitteeMembers {
  /**
   * delete data from the table: "committee_members"
   */
  delete_committee_members: DeleteCommitteeMembers_delete_committee_members | null;
}

export interface DeleteCommitteeMembersVariables {
  committee_id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteCommitteeByPK
// ====================================================

export interface DeleteCommitteeByPK_delete_committees_by_pk {
  __typename: "committees";
  id: any;
}

export interface DeleteCommitteeByPK {
  /**
   * delete single row from the table: "committees"
   */
  delete_committees_by_pk: DeleteCommitteeByPK_delete_committees_by_pk | null;
}

export interface DeleteCommitteeByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeleteCommitteeByPK
// ====================================================

export interface SoftDeleteCommitteeByPK_update_committees_by_pk {
  __typename: "committees";
  id: any;
}

export interface SoftDeleteCommitteeByPK {
  /**
   * update single row of the table: "committees"
   */
  update_committees_by_pk: SoftDeleteCommitteeByPK_update_committees_by_pk | null;
}

export interface SoftDeleteCommitteeByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteCommitteeCategories
// ====================================================

export interface DeleteCommitteeCategories_delete_committee_categories_by_pk {
  __typename: "committee_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteCommitteeCategories {
  /**
   * delete single row from the table: "committee_categories"
   */
  delete_committee_categories_by_pk: DeleteCommitteeCategories_delete_committee_categories_by_pk | null;
}

export interface DeleteCommitteeCategoriesVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CommitteeCategories
// ====================================================

export interface CommitteeCategories_committee_categories {
  __typename: "committee_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface CommitteeCategories {
  /**
   * fetch data from the table: "committee_categories"
   */
  committee_categories: CommitteeCategories_committee_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertCommitteeCategories
// ====================================================

export interface InsertCommitteeCategories_insert_committee_categories_one {
  __typename: "committee_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertCommitteeCategories {
  /**
   * insert a single row into the table: "committee_categories"
   */
  insert_committee_categories_one: InsertCommitteeCategories_insert_committee_categories_one | null;
}

export interface InsertCommitteeCategoriesVariables {
  object: committee_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateCommitteeCategories
// ====================================================

export interface UpdateCommitteeCategories_update_committee_categories_by_pk {
  __typename: "committee_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateCommitteeCategories {
  /**
   * update single row of the table: "committee_categories"
   */
  update_committee_categories_by_pk: UpdateCommitteeCategories_update_committee_categories_by_pk | null;
}

export interface UpdateCommitteeCategoriesVariables {
  pk_columns: committee_categories_pk_columns_input;
  _set?: committee_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteCommitteeActivityPositions
// ====================================================

export interface DeleteCommitteeActivityPositions_delete_committee_activity_positions_by_pk {
  __typename: "committee_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeleteCommitteeActivityPositions {
  /**
   * delete single row from the table: "committee_activity_positions"
   */
  delete_committee_activity_positions_by_pk: DeleteCommitteeActivityPositions_delete_committee_activity_positions_by_pk | null;
}

export interface DeleteCommitteeActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CommitteeActivityPositions
// ====================================================

export interface CommitteeActivityPositions_committee_activity_positions {
  __typename: "committee_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface CommitteeActivityPositions {
  /**
   * fetch data from the table: "committee_activity_positions"
   */
  committee_activity_positions: CommitteeActivityPositions_committee_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertCommitteeActivityPositions
// ====================================================

export interface InsertCommitteeActivityPositions_insert_committee_activity_positions_one {
  __typename: "committee_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertCommitteeActivityPositions {
  /**
   * insert a single row into the table: "committee_activity_positions"
   */
  insert_committee_activity_positions_one: InsertCommitteeActivityPositions_insert_committee_activity_positions_one | null;
}

export interface InsertCommitteeActivityPositionsVariables {
  object: committee_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateCommitteeActivityPositions
// ====================================================

export interface UpdateCommitteeActivityPositions_update_committee_activity_positions_by_pk {
  __typename: "committee_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdateCommitteeActivityPositions {
  /**
   * update single row of the table: "committee_activity_positions"
   */
  update_committee_activity_positions_by_pk: UpdateCommitteeActivityPositions_update_committee_activity_positions_by_pk | null;
}

export interface UpdateCommitteeActivityPositionsVariables {
  pk_columns: committee_activity_positions_pk_columns_input;
  _set?: committee_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteDepartments
// ====================================================

export interface DeleteDepartments_delete_departments_by_pk {
  __typename: "departments";
  id: string;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteDepartments {
  /**
   * delete single row from the table: "departments"
   */
  delete_departments_by_pk: DeleteDepartments_delete_departments_by_pk | null;
}

export interface DeleteDepartmentsVariables {
  id: string;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Departments
// ====================================================

export interface Departments_departments_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface Departments_departments {
  __typename: "departments";
  id: string;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
  /**
   * An object relationship
   */
  faculty: Departments_departments_faculty | null;
}

export interface Departments {
  /**
   * fetch data from the table: "departments"
   */
  departments: Departments_departments[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertDepartments
// ====================================================

export interface InsertDepartments_insert_departments_one_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface InsertDepartments_insert_departments_one {
  __typename: "departments";
  id: string;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
  /**
   * An object relationship
   */
  faculty: InsertDepartments_insert_departments_one_faculty | null;
}

export interface InsertDepartments {
  /**
   * insert a single row into the table: "departments"
   */
  insert_departments_one: InsertDepartments_insert_departments_one | null;
}

export interface InsertDepartmentsVariables {
  object: departments_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateDepartments
// ====================================================

export interface UpdateDepartments_update_departments_by_pk_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface UpdateDepartments_update_departments_by_pk {
  __typename: "departments";
  id: string;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
  /**
   * An object relationship
   */
  faculty: UpdateDepartments_update_departments_by_pk_faculty | null;
}

export interface UpdateDepartments {
  /**
   * update single row of the table: "departments"
   */
  update_departments_by_pk: UpdateDepartments_update_departments_by_pk | null;
}

export interface UpdateDepartmentsVariables {
  pk_columns: departments_pk_columns_input;
  _set?: departments_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EmployeeActivites
// ====================================================

export interface EmployeeActivites_researches {
  __typename: "researches";
  id: any;
  title: string | null;
}

export interface EmployeeActivites_services {
  __typename: "services";
  id: any;
  title: string | null;
}

export interface EmployeeActivites_journal_publications {
  __typename: "journal_publications";
  id: any;
  title: string;
}

export interface EmployeeActivites_seminar_publications {
  __typename: "seminar_publications";
  id: any;
  title: string;
}

export interface EmployeeActivites_book_publications {
  __typename: "book_publications";
  id: any;
  title: string;
}

export interface EmployeeActivites_patent_publications {
  __typename: "patent_publications";
  id: any;
  title: string;
}

export interface EmployeeActivites_self_developments {
  __typename: "self_developments";
  id: any;
  name: string;
}

export interface EmployeeActivites_committees {
  __typename: "committees";
  id: any;
  name: string;
}

export interface EmployeeActivites {
  /**
   * fetch data from the table: "researches"
   */
  researches: EmployeeActivites_researches[];
  /**
   * fetch data from the table: "services"
   */
  services: EmployeeActivites_services[];
  /**
   * fetch data from the table: "journal_publications"
   */
  journal_publications: EmployeeActivites_journal_publications[];
  /**
   * fetch data from the table: "seminar_publications"
   */
  seminar_publications: EmployeeActivites_seminar_publications[];
  /**
   * fetch data from the table: "book_publications"
   */
  book_publications: EmployeeActivites_book_publications[];
  /**
   * fetch data from the table: "patent_publications"
   */
  patent_publications: EmployeeActivites_patent_publications[];
  /**
   * fetch data from the table: "self_developments"
   */
  self_developments: EmployeeActivites_self_developments[];
  /**
   * fetch data from the table: "committees"
   */
  committees: EmployeeActivites_committees[];
}

export interface EmployeeActivitesVariables {
  employeeId: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EmployeeGains
// ====================================================

export interface EmployeeGains_employee_gains_employee_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface EmployeeGains_employee_gains_employee_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: EmployeeGains_employee_gains_employee_department_faculty | null;
}

export interface EmployeeGains_employee_gains_employee {
  __typename: "employees";
  id: any;
  employee_type: employee_type_enum;
  name: string | null;
  /**
   * An object relationship
   */
  department: EmployeeGains_employee_gains_employee_department | null;
}

export interface EmployeeGains_employee_gains {
  __typename: "employees_gains";
  /**
   * An object relationship
   */
  employee: EmployeeGains_employee_gains_employee | null;
  id: any;
  name: string;
  all_gains: any | null;
  research_gains: any | null;
  service_gains: any | null;
  journal_publication_gains: any | null;
  seminar_publication_gains: any | null;
  book_publication_gains: any | null;
  patent_publication_gains: any | null;
  self_development_gains: any | null;
  committee_gains: any | null;
}

export interface EmployeeGains {
  /**
   * execute function "employee_gains" which returns "employees_gains"
   */
  employee_gains: EmployeeGains_employee_gains[];
}

export interface EmployeeGainsVariables {
  where?: employees_gains_bool_exp | null;
  limit?: number | null;
  order_by?: employees_gains_order_by[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EmployeeYearlyGains
// ====================================================

export interface EmployeeYearlyGains_employees_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface EmployeeYearlyGains_employees_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: EmployeeYearlyGains_employees_department_faculty | null;
}

export interface EmployeeYearlyGains_employees_yearly_research_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees_yearly_service_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees_yearly_journal_publication_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees_yearly_seminar_publication_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees_yearly_book_publication_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees_yearly_patent_publication_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees_yearly_self_development_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees_yearly_committee_gains {
  __typename: "emp_yearly_research_gains";
  gains: any;
  year: any;
}

export interface EmployeeYearlyGains_employees {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
  /**
   * An object relationship
   */
  department: EmployeeYearlyGains_employees_department | null;
  /**
   * A computed field, executes function "employee_yearly_research_gains"
   */
  yearly_research_gains: EmployeeYearlyGains_employees_yearly_research_gains[] | null;
  /**
   * A computed field, executes function "employee_yearly_service_gains"
   */
  yearly_service_gains: EmployeeYearlyGains_employees_yearly_service_gains[] | null;
  /**
   * A computed field, executes function "employee_yearly_journal_publication_gains"
   */
  yearly_journal_publication_gains: EmployeeYearlyGains_employees_yearly_journal_publication_gains[] | null;
  /**
   * A computed field, executes function "employee_yearly_seminar_publication_gains"
   */
  yearly_seminar_publication_gains: EmployeeYearlyGains_employees_yearly_seminar_publication_gains[] | null;
  /**
   * A computed field, executes function "employee_yearly_book_publication_gains"
   */
  yearly_book_publication_gains: EmployeeYearlyGains_employees_yearly_book_publication_gains[] | null;
  /**
   * A computed field, executes function "employee_yearly_patent_publication_gains"
   */
  yearly_patent_publication_gains: EmployeeYearlyGains_employees_yearly_patent_publication_gains[] | null;
  /**
   * A computed field, executes function "employee_yearly_self_development_gains"
   */
  yearly_self_development_gains: EmployeeYearlyGains_employees_yearly_self_development_gains[] | null;
  /**
   * A computed field, executes function "employee_yearly_committee_gains"
   */
  yearly_committee_gains: EmployeeYearlyGains_employees_yearly_committee_gains[] | null;
}

export interface EmployeeYearlyGains {
  /**
   * fetch data from the table: "employees"
   */
  employees: EmployeeYearlyGains_employees[];
}

export interface EmployeeYearlyGainsVariables {
  year?: any | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Top10EmployeesByResearchCredit
// ====================================================

export interface Top10EmployeesByResearchCredit_employees_research_members {
  __typename: "research_members";
  id: any;
}

export interface Top10EmployeesByResearchCredit_employees_research_members_aggregate_aggregate_sum {
  __typename: "research_members_sum_fields";
  credit_gain: any | null;
}

export interface Top10EmployeesByResearchCredit_employees_research_members_aggregate_aggregate {
  __typename: "research_members_aggregate_fields";
  sum: Top10EmployeesByResearchCredit_employees_research_members_aggregate_aggregate_sum | null;
}

export interface Top10EmployeesByResearchCredit_employees_research_members_aggregate {
  __typename: "research_members_aggregate";
  aggregate: Top10EmployeesByResearchCredit_employees_research_members_aggregate_aggregate | null;
}

export interface Top10EmployeesByResearchCredit_employees {
  __typename: "employees";
  id: any;
  name: string | null;
  /**
   * An array relationship
   */
  research_members: Top10EmployeesByResearchCredit_employees_research_members[];
  /**
   * An aggregated array relationship
   */
  research_members_aggregate: Top10EmployeesByResearchCredit_employees_research_members_aggregate;
}

export interface Top10EmployeesByResearchCredit {
  /**
   * fetch data from the table: "employees"
   */
  employees: Top10EmployeesByResearchCredit_employees[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Top10EmployeesByServiceCredit
// ====================================================

export interface Top10EmployeesByServiceCredit_employees_service_members {
  __typename: "service_members";
  id: any;
}

export interface Top10EmployeesByServiceCredit_employees_service_members_aggregate_aggregate_sum {
  __typename: "service_members_sum_fields";
  credit_gain: any | null;
}

export interface Top10EmployeesByServiceCredit_employees_service_members_aggregate_aggregate {
  __typename: "service_members_aggregate_fields";
  sum: Top10EmployeesByServiceCredit_employees_service_members_aggregate_aggregate_sum | null;
}

export interface Top10EmployeesByServiceCredit_employees_service_members_aggregate {
  __typename: "service_members_aggregate";
  aggregate: Top10EmployeesByServiceCredit_employees_service_members_aggregate_aggregate | null;
}

export interface Top10EmployeesByServiceCredit_employees {
  __typename: "employees";
  id: any;
  name: string | null;
  /**
   * An array relationship
   */
  service_members: Top10EmployeesByServiceCredit_employees_service_members[];
  /**
   * An aggregated array relationship
   */
  service_members_aggregate: Top10EmployeesByServiceCredit_employees_service_members_aggregate;
}

export interface Top10EmployeesByServiceCredit {
  /**
   * fetch data from the table: "employees"
   */
  employees: Top10EmployeesByServiceCredit_employees[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Top10EmployeesByJournalPublicationCredit
// ====================================================

export interface Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members {
  __typename: "journal_publication_members";
  id: any;
}

export interface Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members_aggregate_aggregate_sum {
  __typename: "journal_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members_aggregate_aggregate {
  __typename: "journal_publication_members_aggregate_fields";
  sum: Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members_aggregate_aggregate_sum | null;
}

export interface Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members_aggregate {
  __typename: "journal_publication_members_aggregate";
  aggregate: Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members_aggregate_aggregate | null;
}

export interface Top10EmployeesByJournalPublicationCredit_employees {
  __typename: "employees";
  id: any;
  name: string | null;
  /**
   * An array relationship
   */
  journal_publication_members: Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members[];
  /**
   * An aggregated array relationship
   */
  journal_publication_members_aggregate: Top10EmployeesByJournalPublicationCredit_employees_journal_publication_members_aggregate;
}

export interface Top10EmployeesByJournalPublicationCredit {
  /**
   * fetch data from the table: "employees"
   */
  employees: Top10EmployeesByJournalPublicationCredit_employees[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteEmployeesAndUser
// ====================================================

export interface DeleteEmployeesAndUser_delete_employees_by_pk_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface DeleteEmployeesAndUser_delete_employees_by_pk_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: DeleteEmployeesAndUser_delete_employees_by_pk_department_faculty | null;
}

export interface DeleteEmployeesAndUser_delete_employees_by_pk_functional_position {
  __typename: "functional_positions";
  id: any;
  name: string | null;
}

export interface DeleteEmployeesAndUser_delete_employees_by_pk_structural_position {
  __typename: "structural_positions";
  id: any;
  name: string | null;
}

export interface DeleteEmployeesAndUser_delete_employees_by_pk_group {
  __typename: "groups";
  id: any;
  name: string;
}

export interface DeleteEmployeesAndUser_delete_employees_by_pk_user_role {
  __typename: "roles";
  id: any;
  name: string | null;
  key: string;
}

export interface DeleteEmployeesAndUser_delete_employees_by_pk_user {
  __typename: "users";
  id: any;
  username: string | null;
  /**
   * An object relationship
   */
  role: DeleteEmployeesAndUser_delete_employees_by_pk_user_role | null;
}

export interface DeleteEmployeesAndUser_delete_employees_by_pk {
  __typename: "employees";
  employee_type: employee_type_enum;
  id: any;
  n_id_n: string | null;
  npp: string | null;
  email: string | null;
  name: string | null;
  address: string | null;
  date_of_birth: any | null;
  place_of_birth: string | null;
  status: employee_status_enum | null;
  base_credit: number | null;
  initial: string | null;
  phone: string | null;
  picture_file: string | null;
  lecturer_certification_number: string | null;
  lecturer_certification_file: string | null;
  /**
   * An object relationship
   */
  department: DeleteEmployeesAndUser_delete_employees_by_pk_department | null;
  /**
   * An object relationship
   */
  functional_position: DeleteEmployeesAndUser_delete_employees_by_pk_functional_position | null;
  /**
   * An object relationship
   */
  structural_position: DeleteEmployeesAndUser_delete_employees_by_pk_structural_position | null;
  /**
   * An object relationship
   */
  group: DeleteEmployeesAndUser_delete_employees_by_pk_group | null;
  /**
   * An object relationship
   */
  user: DeleteEmployeesAndUser_delete_employees_by_pk_user;
}

export interface DeleteEmployeesAndUser_delete_users_by_pk_role {
  __typename: "roles";
  id: any;
  name: string | null;
  key: string;
}

export interface DeleteEmployeesAndUser_delete_users_by_pk {
  __typename: "users";
  id: any;
  username: string | null;
  /**
   * An object relationship
   */
  role: DeleteEmployeesAndUser_delete_users_by_pk_role | null;
}

export interface DeleteEmployeesAndUser {
  /**
   * delete single row from the table: "employees"
   */
  delete_employees_by_pk: DeleteEmployeesAndUser_delete_employees_by_pk | null;
  /**
   * delete single row from the table: "users"
   */
  delete_users_by_pk: DeleteEmployeesAndUser_delete_users_by_pk | null;
}

export interface DeleteEmployeesAndUserVariables {
  employee_id: any;
  user_id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EmployeeStatuses
// ====================================================

export interface EmployeeStatuses_employee_status {
  __typename: "employee_status";
  description: string | null;
  status: string;
}

export interface EmployeeStatuses {
  /**
   * fetch data from the table: "employee_status"
   */
  employee_status: EmployeeStatuses_employee_status[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Employees
// ====================================================

export interface Employees_employees_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface Employees_employees_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: Employees_employees_department_faculty | null;
}

export interface Employees_employees_functional_position {
  __typename: "functional_positions";
  id: any;
  name: string | null;
}

export interface Employees_employees_structural_position {
  __typename: "structural_positions";
  id: any;
  name: string | null;
}

export interface Employees_employees_group {
  __typename: "groups";
  id: any;
  name: string;
}

export interface Employees_employees_user_role {
  __typename: "roles";
  id: any;
  name: string | null;
  key: string;
}

export interface Employees_employees_user {
  __typename: "users";
  id: any;
  username: string | null;
  /**
   * An object relationship
   */
  role: Employees_employees_user_role | null;
}

export interface Employees_employees {
  __typename: "employees";
  employee_type: employee_type_enum;
  id: any;
  n_id_n: string | null;
  npp: string | null;
  email: string | null;
  name: string | null;
  address: string | null;
  date_of_birth: any | null;
  place_of_birth: string | null;
  status: employee_status_enum | null;
  base_credit: number | null;
  initial: string | null;
  phone: string | null;
  picture_file: string | null;
  lecturer_certification_file: string | null;
  lecturer_certification_number: string | null;
  /**
   * An object relationship
   */
  department: Employees_employees_department | null;
  /**
   * An object relationship
   */
  functional_position: Employees_employees_functional_position | null;
  /**
   * An object relationship
   */
  structural_position: Employees_employees_structural_position | null;
  /**
   * An object relationship
   */
  group: Employees_employees_group | null;
  /**
   * An object relationship
   */
  user: Employees_employees_user;
}

export interface Employees {
  /**
   * fetch data from the table: "employees"
   */
  employees: Employees_employees[];
}

export interface EmployeesVariables {
  employee_type?: employee_type_enum | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SearchEmployees
// ====================================================

export interface SearchEmployees_employees {
  __typename: "employees";
  value: any;
  text: string | null;
}

export interface SearchEmployees {
  /**
   * fetch data from the table: "employees"
   */
  employees: SearchEmployees_employees[];
}

export interface SearchEmployeesVariables {
  term?: string | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EmployeesByPK
// ====================================================

export interface EmployeesByPK_employees_by_pk_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface EmployeesByPK_employees_by_pk_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: EmployeesByPK_employees_by_pk_department_faculty | null;
}

export interface EmployeesByPK_employees_by_pk_functional_position {
  __typename: "functional_positions";
  id: any;
  name: string | null;
}

export interface EmployeesByPK_employees_by_pk_structural_position {
  __typename: "structural_positions";
  id: any;
  name: string | null;
}

export interface EmployeesByPK_employees_by_pk_group {
  __typename: "groups";
  id: any;
  name: string;
}

export interface EmployeesByPK_employees_by_pk_user_role {
  __typename: "roles";
  id: any;
  name: string | null;
  key: string;
}

export interface EmployeesByPK_employees_by_pk_user {
  __typename: "users";
  id: any;
  username: string | null;
  /**
   * An object relationship
   */
  role: EmployeesByPK_employees_by_pk_user_role | null;
}

export interface EmployeesByPK_employees_by_pk {
  __typename: "employees";
  employee_type: employee_type_enum;
  id: any;
  n_id_n: string | null;
  npp: string | null;
  email: string | null;
  name: string | null;
  address: string | null;
  date_of_birth: any | null;
  place_of_birth: string | null;
  status: employee_status_enum | null;
  base_credit: number | null;
  initial: string | null;
  phone: string | null;
  picture_file: string | null;
  lecturer_certification_file: string | null;
  lecturer_certification_number: string | null;
  /**
   * An object relationship
   */
  department: EmployeesByPK_employees_by_pk_department | null;
  /**
   * An object relationship
   */
  functional_position: EmployeesByPK_employees_by_pk_functional_position | null;
  /**
   * An object relationship
   */
  structural_position: EmployeesByPK_employees_by_pk_structural_position | null;
  /**
   * An object relationship
   */
  group: EmployeesByPK_employees_by_pk_group | null;
  /**
   * An object relationship
   */
  user: EmployeesByPK_employees_by_pk_user;
}

export interface EmployeesByPK {
  /**
   * fetch data from the table: "employees" using primary key columns
   */
  employees_by_pk: EmployeesByPK_employees_by_pk | null;
}

export interface EmployeesByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EmployeeCreditGainsGraph
// ====================================================

export interface EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_aggregate_sum {
  __typename: "research_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_aggregate {
  __typename: "research_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_nodes_research {
  __typename: "researches";
  id: any;
  title: string | null;
  started_at: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_nodes {
  __typename: "research_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  research: EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_nodes_research | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate {
  __typename: "research_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_aggregate_sum {
  __typename: "service_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_aggregate {
  __typename: "service_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_nodes_service {
  __typename: "services";
  id: any;
  title: string | null;
  start_date: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_nodes {
  __typename: "service_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  service: EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_nodes_service;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate {
  __typename: "service_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_aggregate_sum {
  __typename: "journal_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_aggregate {
  __typename: "journal_publication_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_nodes_journal_publication {
  __typename: "journal_publications";
  id: any;
  title: string;
  created_at: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_nodes {
  __typename: "journal_publication_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  journal_publication: EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_nodes_journal_publication;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate {
  __typename: "journal_publication_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_aggregate_sum {
  __typename: "seminar_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_aggregate {
  __typename: "seminar_publication_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_nodes_seminar_publication {
  __typename: "seminar_publications";
  id: any;
  title: string;
  created_at: any;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_nodes {
  __typename: "seminar_publication_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  seminar_publication: EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_nodes_seminar_publication;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate {
  __typename: "seminar_publication_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_aggregate_sum {
  __typename: "book_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_aggregate {
  __typename: "book_publication_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_nodes_book_publication {
  __typename: "book_publications";
  id: any;
  title: string;
  created_at: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_nodes {
  __typename: "book_publication_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  book_publication: EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_nodes_book_publication;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate {
  __typename: "book_publication_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_aggregate_sum {
  __typename: "patent_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_aggregate {
  __typename: "patent_publication_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_nodes_patent_publication {
  __typename: "patent_publications";
  id: any;
  title: string;
  created_at: any;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_nodes {
  __typename: "patent_publication_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  patent_publication: EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_nodes_patent_publication;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate {
  __typename: "patent_publication_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_aggregate_sum {
  __typename: "self_development_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_aggregate {
  __typename: "self_development_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_nodes_self_development {
  __typename: "self_developments";
  id: any;
  name: string;
  start_date: any;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_nodes {
  __typename: "self_development_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  self_development: EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_nodes_self_development;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate {
  __typename: "self_development_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_aggregate_sum {
  __typename: "committee_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_aggregate {
  __typename: "committee_members_aggregate_fields";
  sum: EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_nodes_committee {
  __typename: "committees";
  id: any;
  name: string;
  start_date: any;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_nodes {
  __typename: "committee_members";
  credit_gain: any | null;
  /**
   * An object relationship
   */
  committee: EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_nodes_committee;
}

export interface EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate {
  __typename: "committee_members_aggregate";
  aggregate: EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_aggregate | null;
  nodes: EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate_nodes[];
}

export interface EmployeeCreditGainsGraph_employees_by_pk {
  __typename: "employees";
  base_credit: number | null;
  /**
   * An aggregated array relationship
   */
  research_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_research_members_aggregate;
  /**
   * An aggregated array relationship
   */
  service_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_service_members_aggregate;
  /**
   * An aggregated array relationship
   */
  journal_publication_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_journal_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  seminar_publication_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_seminar_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  book_publication_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_book_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  patent_publication_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_patent_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  self_development_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_self_development_members_aggregate;
  /**
   * An aggregated array relationship
   */
  committee_members_aggregate: EmployeeCreditGainsGraph_employees_by_pk_committee_members_aggregate;
}

export interface EmployeeCreditGainsGraph {
  /**
   * fetch data from the table: "employees" using primary key columns
   */
  employees_by_pk: EmployeeCreditGainsGraph_employees_by_pk | null;
}

export interface EmployeeCreditGainsGraphVariables {
  id: any;
  start?: any | null;
  end?: any | null;
  startAsDate?: any | null;
  endAsDate?: any | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EmployeeCreditGains
// ====================================================

export interface EmployeeCreditGains_employees_by_pk_research_members_aggregate_aggregate_sum {
  __typename: "research_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_research_members_aggregate_aggregate {
  __typename: "research_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_research_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_research_members_aggregate {
  __typename: "research_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_research_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk_service_members_aggregate_aggregate_sum {
  __typename: "service_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_service_members_aggregate_aggregate {
  __typename: "service_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_service_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_service_members_aggregate {
  __typename: "service_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_service_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk_journal_publication_members_aggregate_aggregate_sum {
  __typename: "journal_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_journal_publication_members_aggregate_aggregate {
  __typename: "journal_publication_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_journal_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_journal_publication_members_aggregate {
  __typename: "journal_publication_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_journal_publication_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk_seminar_publication_members_aggregate_aggregate_sum {
  __typename: "seminar_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_seminar_publication_members_aggregate_aggregate {
  __typename: "seminar_publication_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_seminar_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_seminar_publication_members_aggregate {
  __typename: "seminar_publication_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_seminar_publication_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk_book_publication_members_aggregate_aggregate_sum {
  __typename: "book_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_book_publication_members_aggregate_aggregate {
  __typename: "book_publication_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_book_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_book_publication_members_aggregate {
  __typename: "book_publication_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_book_publication_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk_patent_publication_members_aggregate_aggregate_sum {
  __typename: "patent_publication_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_patent_publication_members_aggregate_aggregate {
  __typename: "patent_publication_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_patent_publication_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_patent_publication_members_aggregate {
  __typename: "patent_publication_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_patent_publication_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk_self_development_members_aggregate_aggregate_sum {
  __typename: "self_development_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_self_development_members_aggregate_aggregate {
  __typename: "self_development_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_self_development_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_self_development_members_aggregate {
  __typename: "self_development_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_self_development_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk_committee_members_aggregate_aggregate_sum {
  __typename: "committee_members_sum_fields";
  credit_gain: any | null;
}

export interface EmployeeCreditGains_employees_by_pk_committee_members_aggregate_aggregate {
  __typename: "committee_members_aggregate_fields";
  sum: EmployeeCreditGains_employees_by_pk_committee_members_aggregate_aggregate_sum | null;
}

export interface EmployeeCreditGains_employees_by_pk_committee_members_aggregate {
  __typename: "committee_members_aggregate";
  aggregate: EmployeeCreditGains_employees_by_pk_committee_members_aggregate_aggregate | null;
}

export interface EmployeeCreditGains_employees_by_pk {
  __typename: "employees";
  base_credit: number | null;
  /**
   * An aggregated array relationship
   */
  research_members_aggregate: EmployeeCreditGains_employees_by_pk_research_members_aggregate;
  /**
   * An aggregated array relationship
   */
  service_members_aggregate: EmployeeCreditGains_employees_by_pk_service_members_aggregate;
  /**
   * An aggregated array relationship
   */
  journal_publication_members_aggregate: EmployeeCreditGains_employees_by_pk_journal_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  seminar_publication_members_aggregate: EmployeeCreditGains_employees_by_pk_seminar_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  book_publication_members_aggregate: EmployeeCreditGains_employees_by_pk_book_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  patent_publication_members_aggregate: EmployeeCreditGains_employees_by_pk_patent_publication_members_aggregate;
  /**
   * An aggregated array relationship
   */
  self_development_members_aggregate: EmployeeCreditGains_employees_by_pk_self_development_members_aggregate;
  /**
   * An aggregated array relationship
   */
  committee_members_aggregate: EmployeeCreditGains_employees_by_pk_committee_members_aggregate;
}

export interface EmployeeCreditGains {
  /**
   * fetch data from the table: "employees" using primary key columns
   */
  employees_by_pk: EmployeeCreditGains_employees_by_pk | null;
}

export interface EmployeeCreditGainsVariables {
  id: any;
  start?: any | null;
  end?: any | null;
  startAsDate?: any | null;
  endAsDate?: any | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertEmployees
// ====================================================

export interface InsertEmployees_insert_employees_one {
  __typename: "employees";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertEmployees {
  /**
   * insert a single row into the table: "employees"
   */
  insert_employees_one: InsertEmployees_insert_employees_one | null;
}

export interface InsertEmployeesVariables {
  object: employees_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateEmployees
// ====================================================

export interface UpdateEmployees_update_employees_by_pk {
  __typename: "employees";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateEmployees {
  /**
   * update single row of the table: "employees"
   */
  update_employees_by_pk: UpdateEmployees_update_employees_by_pk | null;
}

export interface UpdateEmployeesVariables {
  pk_columns: employees_pk_columns_input;
  _set?: employees_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateEmployeesAndUser
// ====================================================

export interface UpdateEmployeesAndUser_update_employees_by_pk_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface UpdateEmployeesAndUser_update_employees_by_pk_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: UpdateEmployeesAndUser_update_employees_by_pk_department_faculty | null;
}

export interface UpdateEmployeesAndUser_update_employees_by_pk_functional_position {
  __typename: "functional_positions";
  id: any;
  name: string | null;
}

export interface UpdateEmployeesAndUser_update_employees_by_pk_structural_position {
  __typename: "structural_positions";
  id: any;
  name: string | null;
}

export interface UpdateEmployeesAndUser_update_employees_by_pk_group {
  __typename: "groups";
  id: any;
  name: string;
}

export interface UpdateEmployeesAndUser_update_employees_by_pk_user_role {
  __typename: "roles";
  id: any;
  name: string | null;
  key: string;
}

export interface UpdateEmployeesAndUser_update_employees_by_pk_user {
  __typename: "users";
  id: any;
  username: string | null;
  /**
   * An object relationship
   */
  role: UpdateEmployeesAndUser_update_employees_by_pk_user_role | null;
}

export interface UpdateEmployeesAndUser_update_employees_by_pk {
  __typename: "employees";
  employee_type: employee_type_enum;
  id: any;
  n_id_n: string | null;
  npp: string | null;
  email: string | null;
  name: string | null;
  address: string | null;
  date_of_birth: any | null;
  place_of_birth: string | null;
  status: employee_status_enum | null;
  base_credit: number | null;
  initial: string | null;
  phone: string | null;
  picture_file: string | null;
  lecturer_certification_file: string | null;
  lecturer_certification_number: string | null;
  /**
   * An object relationship
   */
  department: UpdateEmployeesAndUser_update_employees_by_pk_department | null;
  /**
   * An object relationship
   */
  functional_position: UpdateEmployeesAndUser_update_employees_by_pk_functional_position | null;
  /**
   * An object relationship
   */
  structural_position: UpdateEmployeesAndUser_update_employees_by_pk_structural_position | null;
  /**
   * An object relationship
   */
  group: UpdateEmployeesAndUser_update_employees_by_pk_group | null;
  /**
   * An object relationship
   */
  user: UpdateEmployeesAndUser_update_employees_by_pk_user;
}

export interface UpdateEmployeesAndUser_update_users_by_pk_role {
  __typename: "roles";
  id: any;
  name: string | null;
  key: string;
}

export interface UpdateEmployeesAndUser_update_users_by_pk {
  __typename: "users";
  id: any;
  username: string | null;
  /**
   * An object relationship
   */
  role: UpdateEmployeesAndUser_update_users_by_pk_role | null;
}

export interface UpdateEmployeesAndUser {
  /**
   * update single row of the table: "employees"
   */
  update_employees_by_pk: UpdateEmployeesAndUser_update_employees_by_pk | null;
  /**
   * update single row of the table: "users"
   */
  update_users_by_pk: UpdateEmployeesAndUser_update_users_by_pk | null;
}

export interface UpdateEmployeesAndUserVariables {
  emp_pk_columns: employees_pk_columns_input;
  emp_set?: employees_set_input | null;
  us_pk_columns: users_pk_columns_input;
  us_set?: users_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Roles
// ====================================================

export interface Roles_roles {
  __typename: "roles";
  id: any;
  name: string | null;
}

export interface Roles {
  /**
   * fetch data from the table: "roles"
   */
  roles: Roles_roles[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteFaculties
// ====================================================

export interface DeleteFaculties_delete_faculties_by_pk {
  __typename: "faculties";
  id: number;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteFaculties {
  /**
   * delete single row from the table: "faculties"
   */
  delete_faculties_by_pk: DeleteFaculties_delete_faculties_by_pk | null;
}

export interface DeleteFacultiesVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Faculties
// ====================================================

export interface Faculties_faculties {
  __typename: "faculties";
  id: number;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface Faculties {
  /**
   * fetch data from the table: "faculties"
   */
  faculties: Faculties_faculties[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertFaculties
// ====================================================

export interface InsertFaculties_insert_faculties_one {
  __typename: "faculties";
  id: number;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertFaculties {
  /**
   * insert a single row into the table: "faculties"
   */
  insert_faculties_one: InsertFaculties_insert_faculties_one | null;
}

export interface InsertFacultiesVariables {
  object: faculties_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateFaculties
// ====================================================

export interface UpdateFaculties_update_faculties_by_pk {
  __typename: "faculties";
  id: number;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateFaculties {
  /**
   * update single row of the table: "faculties"
   */
  update_faculties_by_pk: UpdateFaculties_update_faculties_by_pk | null;
}

export interface UpdateFacultiesVariables {
  pk_columns: faculties_pk_columns_input;
  _set?: faculties_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteFunctionalPosition
// ====================================================

export interface DeleteFunctionalPosition_delete_functional_positions_by_pk {
  __typename: "functional_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteFunctionalPosition {
  /**
   * delete single row from the table: "functional_positions"
   */
  delete_functional_positions_by_pk: DeleteFunctionalPosition_delete_functional_positions_by_pk | null;
}

export interface DeleteFunctionalPositionVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FunctionalPositions
// ====================================================

export interface FunctionalPositions_functional_positions {
  __typename: "functional_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface FunctionalPositions {
  /**
   * fetch data from the table: "functional_positions"
   */
  functional_positions: FunctionalPositions_functional_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertFunctionalPosition
// ====================================================

export interface InsertFunctionalPosition_insert_functional_positions_one {
  __typename: "functional_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertFunctionalPosition {
  /**
   * insert a single row into the table: "functional_positions"
   */
  insert_functional_positions_one: InsertFunctionalPosition_insert_functional_positions_one | null;
}

export interface InsertFunctionalPositionVariables {
  object: functional_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateFunctionalPosition
// ====================================================

export interface UpdateFunctionalPosition_update_functional_positions_by_pk {
  __typename: "functional_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateFunctionalPosition {
  /**
   * update single row of the table: "functional_positions"
   */
  update_functional_positions_by_pk: UpdateFunctionalPosition_update_functional_positions_by_pk | null;
}

export interface UpdateFunctionalPositionVariables {
  pk_columns: functional_positions_pk_columns_input;
  _set?: functional_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteGroups
// ====================================================

export interface DeleteGroups_delete_groups_by_pk {
  __typename: "groups";
  id: any;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteGroups {
  /**
   * delete single row from the table: "groups"
   */
  delete_groups_by_pk: DeleteGroups_delete_groups_by_pk | null;
}

export interface DeleteGroupsVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Groups
// ====================================================

export interface Groups_groups {
  __typename: "groups";
  id: any;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface Groups {
  /**
   * fetch data from the table: "groups"
   */
  groups: Groups_groups[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertGroups
// ====================================================

export interface InsertGroups_insert_groups_one {
  __typename: "groups";
  id: any;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertGroups {
  /**
   * insert a single row into the table: "groups"
   */
  insert_groups_one: InsertGroups_insert_groups_one | null;
}

export interface InsertGroupsVariables {
  object: groups_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateGroups
// ====================================================

export interface UpdateGroups_update_groups_by_pk {
  __typename: "groups";
  id: any;
  name: string;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateGroups {
  /**
   * update single row of the table: "groups"
   */
  update_groups_by_pk: UpdateGroups_update_groups_by_pk | null;
}

export interface UpdateGroupsVariables {
  pk_columns: groups_pk_columns_input;
  _set?: groups_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: JournalPublications
// ====================================================

export interface JournalPublications_journal_publications_journal_publication_category {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
}

export interface JournalPublications_journal_publications_journal_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface JournalPublications_journal_publications_journal_publication_members_journal_publication_activity_position {
  __typename: "journal_publication_activity_positions";
  id: number;
  name: string;
}

export interface JournalPublications_journal_publications_journal_publication_members {
  __typename: "journal_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: JournalPublications_journal_publications_journal_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  journal_publication_activity_position: JournalPublications_journal_publications_journal_publication_members_journal_publication_activity_position | null;
}

export interface JournalPublications_journal_publications {
  __typename: "journal_publications";
  id: any;
  title: string;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  journal_publication_category: JournalPublications_journal_publications_journal_publication_category;
  journal_cover_file: string;
  journal_toc_file: string;
  certificate_file: string | null;
  paper_file: string;
  decree_file: string | null;
  /**
   * An array relationship
   */
  journal_publication_members: JournalPublications_journal_publications_journal_publication_members[];
  isbn: string | null;
  issn: string | null;
  journal_no: string;
  journal_name: string;
  journal_volume: string;
  journal_year: number;
  journal_page_no: string;
}

export interface JournalPublications {
  /**
   * fetch data from the table: "journal_publications"
   */
  journal_publications: JournalPublications_journal_publications[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: JournalPublicationByPK
// ====================================================

export interface JournalPublicationByPK_journal_publications_by_pk_journal_publication_category {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
}

export interface JournalPublicationByPK_journal_publications_by_pk_journal_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface JournalPublicationByPK_journal_publications_by_pk_journal_publication_members_journal_publication_activity_position {
  __typename: "journal_publication_activity_positions";
  id: number;
  name: string;
}

export interface JournalPublicationByPK_journal_publications_by_pk_journal_publication_members {
  __typename: "journal_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: JournalPublicationByPK_journal_publications_by_pk_journal_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  journal_publication_activity_position: JournalPublicationByPK_journal_publications_by_pk_journal_publication_members_journal_publication_activity_position | null;
}

export interface JournalPublicationByPK_journal_publications_by_pk {
  __typename: "journal_publications";
  id: any;
  title: string;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  journal_publication_category: JournalPublicationByPK_journal_publications_by_pk_journal_publication_category;
  certificate_file: string | null;
  journal_cover_file: string;
  journal_toc_file: string;
  paper_file: string;
  decree_file: string | null;
  /**
   * An array relationship
   */
  journal_publication_members: JournalPublicationByPK_journal_publications_by_pk_journal_publication_members[];
  isbn: string | null;
  issn: string | null;
  journal_no: string;
  journal_name: string;
  journal_volume: string;
  journal_year: number;
  journal_page_no: string;
}

export interface JournalPublicationByPK {
  /**
   * fetch data from the table: "journal_publications" using primary key columns
   */
  journal_publications_by_pk: JournalPublicationByPK_journal_publications_by_pk | null;
}

export interface JournalPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertJournalPublications
// ====================================================

export interface InsertJournalPublications_insert_journal_publications_one_journal_publication_category {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
}

export interface InsertJournalPublications_insert_journal_publications_one {
  __typename: "journal_publications";
  id: any;
  title: string;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  journal_publication_category: InsertJournalPublications_insert_journal_publications_one_journal_publication_category;
  certificate_file: string | null;
  journal_cover_file: string;
  journal_toc_file: string;
  paper_file: string;
  decree_file: string | null;
}

export interface InsertJournalPublications {
  /**
   * insert a single row into the table: "journal_publications"
   */
  insert_journal_publications_one: InsertJournalPublications_insert_journal_publications_one | null;
}

export interface InsertJournalPublicationsVariables {
  object: journal_publications_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertJournalPublicationMember
// ====================================================

export interface InsertJournalPublicationMember_insert_journal_publication_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertJournalPublicationMember_insert_journal_publication_members_returning_journal_publication_activity_position {
  __typename: "journal_publication_activity_positions";
  id: number;
  name: string;
}

export interface InsertJournalPublicationMember_insert_journal_publication_members_returning {
  __typename: "journal_publication_members";
  id: any;
  credit_gain: any | null;
  journal_publication_id: any;
  /**
   * An object relationship
   */
  employee: InsertJournalPublicationMember_insert_journal_publication_members_returning_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  journal_publication_activity_position: InsertJournalPublicationMember_insert_journal_publication_members_returning_journal_publication_activity_position | null;
}

export interface InsertJournalPublicationMember_insert_journal_publication_members {
  __typename: "journal_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertJournalPublicationMember_insert_journal_publication_members_returning[];
}

export interface InsertJournalPublicationMember {
  /**
   * insert data into the table: "journal_publication_members"
   */
  insert_journal_publication_members: InsertJournalPublicationMember_insert_journal_publication_members | null;
}

export interface InsertJournalPublicationMemberVariables {
  objects: journal_publication_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateJournalPublication
// ====================================================

export interface UpdateJournalPublication_update_journal_publications_by_pk_journal_publication_category {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
}

export interface UpdateJournalPublication_update_journal_publications_by_pk {
  __typename: "journal_publications";
  id: any;
  title: string;
  credit_multiplier: any | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  /**
   * An object relationship
   */
  journal_publication_category: UpdateJournalPublication_update_journal_publications_by_pk_journal_publication_category;
  certificate_file: string | null;
  journal_cover_file: string;
  journal_toc_file: string;
  paper_file: string;
  decree_file: string | null;
}

export interface UpdateJournalPublication {
  /**
   * update single row of the table: "journal_publications"
   */
  update_journal_publications_by_pk: UpdateJournalPublication_update_journal_publications_by_pk | null;
}

export interface UpdateJournalPublicationVariables {
  id: any;
  _set?: journal_publications_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteJournalPublicationMembers
// ====================================================

export interface DeleteJournalPublicationMembers_delete_journal_publication_members {
  __typename: "journal_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeleteJournalPublicationMembers {
  /**
   * delete data from the table: "journal_publication_members"
   */
  delete_journal_publication_members: DeleteJournalPublicationMembers_delete_journal_publication_members | null;
}

export interface DeleteJournalPublicationMembersVariables {
  journal_publication_id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteJournalPublicationByPK
// ====================================================

export interface DeleteJournalPublicationByPK_delete_journal_publications_by_pk {
  __typename: "journal_publications";
  id: any;
}

export interface DeleteJournalPublicationByPK {
  /**
   * delete single row from the table: "journal_publications"
   */
  delete_journal_publications_by_pk: DeleteJournalPublicationByPK_delete_journal_publications_by_pk | null;
}

export interface DeleteJournalPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeleteJournalPublicationByPK
// ====================================================

export interface SoftDeleteJournalPublicationByPK_update_journal_publications_by_pk {
  __typename: "journal_publications";
  id: any;
}

export interface SoftDeleteJournalPublicationByPK {
  /**
   * update single row of the table: "journal_publications"
   */
  update_journal_publications_by_pk: SoftDeleteJournalPublicationByPK_update_journal_publications_by_pk | null;
}

export interface SoftDeleteJournalPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteJournalPublicationCategories
// ====================================================

export interface DeleteJournalPublicationCategories_delete_journal_publication_categories_by_pk {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteJournalPublicationCategories {
  /**
   * delete single row from the table: "journal_publication_categories"
   */
  delete_journal_publication_categories_by_pk: DeleteJournalPublicationCategories_delete_journal_publication_categories_by_pk | null;
}

export interface DeleteJournalPublicationCategoriesVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: JournalPublicationCategories
// ====================================================

export interface JournalPublicationCategories_journal_publication_categories {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface JournalPublicationCategories {
  /**
   * fetch data from the table: "journal_publication_categories"
   */
  journal_publication_categories: JournalPublicationCategories_journal_publication_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertJournalPublicationCategories
// ====================================================

export interface InsertJournalPublicationCategories_insert_journal_publication_categories_one {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertJournalPublicationCategories {
  /**
   * insert a single row into the table: "journal_publication_categories"
   */
  insert_journal_publication_categories_one: InsertJournalPublicationCategories_insert_journal_publication_categories_one | null;
}

export interface InsertJournalPublicationCategoriesVariables {
  object: journal_publication_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateJournalPublicationCategories
// ====================================================

export interface UpdateJournalPublicationCategories_update_journal_publication_categories_by_pk {
  __typename: "journal_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateJournalPublicationCategories {
  /**
   * update single row of the table: "journal_publication_categories"
   */
  update_journal_publication_categories_by_pk: UpdateJournalPublicationCategories_update_journal_publication_categories_by_pk | null;
}

export interface UpdateJournalPublicationCategoriesVariables {
  pk_columns: journal_publication_categories_pk_columns_input;
  _set?: journal_publication_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteJournalPublicationActivityPositions
// ====================================================

export interface DeleteJournalPublicationActivityPositions_delete_journal_publication_activity_positions_by_pk {
  __typename: "journal_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeleteJournalPublicationActivityPositions {
  /**
   * delete single row from the table: "journal_publication_activity_positions"
   */
  delete_journal_publication_activity_positions_by_pk: DeleteJournalPublicationActivityPositions_delete_journal_publication_activity_positions_by_pk | null;
}

export interface DeleteJournalPublicationActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: JournalPublicationActivityPositions
// ====================================================

export interface JournalPublicationActivityPositions_journal_publication_activity_positions {
  __typename: "journal_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface JournalPublicationActivityPositions {
  /**
   * fetch data from the table: "journal_publication_activity_positions"
   */
  journal_publication_activity_positions: JournalPublicationActivityPositions_journal_publication_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertJournalPublicationActivityPositions
// ====================================================

export interface InsertJournalPublicationActivityPositions_insert_journal_publication_activity_positions_one {
  __typename: "journal_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertJournalPublicationActivityPositions {
  /**
   * insert a single row into the table: "journal_publication_activity_positions"
   */
  insert_journal_publication_activity_positions_one: InsertJournalPublicationActivityPositions_insert_journal_publication_activity_positions_one | null;
}

export interface InsertJournalPublicationActivityPositionsVariables {
  object: journal_publication_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateJournalPublicationActivityPositions
// ====================================================

export interface UpdateJournalPublicationActivityPositions_update_journal_publication_activity_positions_by_pk {
  __typename: "journal_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdateJournalPublicationActivityPositions {
  /**
   * update single row of the table: "journal_publication_activity_positions"
   */
  update_journal_publication_activity_positions_by_pk: UpdateJournalPublicationActivityPositions_update_journal_publication_activity_positions_by_pk | null;
}

export interface UpdateJournalPublicationActivityPositionsVariables {
  pk_columns: journal_publication_activity_positions_pk_columns_input;
  _set?: journal_publication_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PatentPublications
// ====================================================

export interface PatentPublications_patent_publications_patent_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface PatentPublications_patent_publications_patent_publication_members_patent_publication_activity_position {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
}

export interface PatentPublications_patent_publications_patent_publication_members {
  __typename: "patent_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: PatentPublications_patent_publications_patent_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  patent_publication_activity_position: PatentPublications_patent_publications_patent_publication_members_patent_publication_activity_position;
}

export interface PatentPublications_patent_publications_patent_publication_category {
  __typename: "patent_publication_categories";
  id: any;
  credit: any | null;
  name: string | null;
}

export interface PatentPublications_patent_publications {
  __typename: "patent_publications";
  id: any;
  title: string;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An array relationship
   */
  patent_publication_members: PatentPublications_patent_publications_patent_publication_members[];
  /**
   * An object relationship
   */
  patent_publication_category: PatentPublications_patent_publications_patent_publication_category | null;
  certificate_no: string;
  first_announcement_date: any;
  certificate_file: string;
}

export interface PatentPublications {
  /**
   * fetch data from the table: "patent_publications"
   */
  patent_publications: PatentPublications_patent_publications[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PatentPublicationByPK
// ====================================================

export interface PatentPublicationByPK_patent_publications_by_pk_patent_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface PatentPublicationByPK_patent_publications_by_pk_patent_publication_members_patent_publication_activity_position {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
}

export interface PatentPublicationByPK_patent_publications_by_pk_patent_publication_members {
  __typename: "patent_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: PatentPublicationByPK_patent_publications_by_pk_patent_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  patent_publication_activity_position: PatentPublicationByPK_patent_publications_by_pk_patent_publication_members_patent_publication_activity_position;
}

export interface PatentPublicationByPK_patent_publications_by_pk_patent_publication_category {
  __typename: "patent_publication_categories";
  id: any;
  credit: any | null;
  name: string | null;
}

export interface PatentPublicationByPK_patent_publications_by_pk {
  __typename: "patent_publications";
  id: any;
  title: string;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An array relationship
   */
  patent_publication_members: PatentPublicationByPK_patent_publications_by_pk_patent_publication_members[];
  /**
   * An object relationship
   */
  patent_publication_category: PatentPublicationByPK_patent_publications_by_pk_patent_publication_category | null;
  certificate_no: string;
  first_announcement_date: any;
  certificate_file: string;
}

export interface PatentPublicationByPK {
  /**
   * fetch data from the table: "patent_publications" using primary key columns
   */
  patent_publications_by_pk: PatentPublicationByPK_patent_publications_by_pk | null;
}

export interface PatentPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertPatentPublications
// ====================================================

export interface InsertPatentPublications_insert_patent_publications_one_patent_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertPatentPublications_insert_patent_publications_one_patent_publication_members_patent_publication_activity_position {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
}

export interface InsertPatentPublications_insert_patent_publications_one_patent_publication_members {
  __typename: "patent_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: InsertPatentPublications_insert_patent_publications_one_patent_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  patent_publication_activity_position: InsertPatentPublications_insert_patent_publications_one_patent_publication_members_patent_publication_activity_position;
}

export interface InsertPatentPublications_insert_patent_publications_one_patent_publication_category {
  __typename: "patent_publication_categories";
  id: any;
  credit: any | null;
  name: string | null;
}

export interface InsertPatentPublications_insert_patent_publications_one {
  __typename: "patent_publications";
  id: any;
  title: string;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An array relationship
   */
  patent_publication_members: InsertPatentPublications_insert_patent_publications_one_patent_publication_members[];
  /**
   * An object relationship
   */
  patent_publication_category: InsertPatentPublications_insert_patent_publications_one_patent_publication_category | null;
  certificate_no: string;
  first_announcement_date: any;
  certificate_file: string;
}

export interface InsertPatentPublications {
  /**
   * insert a single row into the table: "patent_publications"
   */
  insert_patent_publications_one: InsertPatentPublications_insert_patent_publications_one | null;
}

export interface InsertPatentPublicationsVariables {
  object: patent_publications_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertPatentPublicationMember
// ====================================================

export interface InsertPatentPublicationMember_insert_patent_publication_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertPatentPublicationMember_insert_patent_publication_members_returning_patent_publication_activity_position {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
}

export interface InsertPatentPublicationMember_insert_patent_publication_members_returning {
  __typename: "patent_publication_members";
  id: any;
  patent_publication_id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: InsertPatentPublicationMember_insert_patent_publication_members_returning_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  patent_publication_activity_position: InsertPatentPublicationMember_insert_patent_publication_members_returning_patent_publication_activity_position;
}

export interface InsertPatentPublicationMember_insert_patent_publication_members {
  __typename: "patent_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertPatentPublicationMember_insert_patent_publication_members_returning[];
}

export interface InsertPatentPublicationMember {
  /**
   * insert data into the table: "patent_publication_members"
   */
  insert_patent_publication_members: InsertPatentPublicationMember_insert_patent_publication_members | null;
}

export interface InsertPatentPublicationMemberVariables {
  objects: patent_publication_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdatePatentPublication
// ====================================================

export interface UpdatePatentPublication_update_patent_publications_by_pk {
  __typename: "patent_publications";
  id: any;
  title: string;
  credit_multiplier: any | null;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
}

export interface UpdatePatentPublication {
  /**
   * update single row of the table: "patent_publications"
   */
  update_patent_publications_by_pk: UpdatePatentPublication_update_patent_publications_by_pk | null;
}

export interface UpdatePatentPublicationVariables {
  id: any;
  _set?: patent_publications_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePatentPublicationMembers
// ====================================================

export interface DeletePatentPublicationMembers_delete_patent_publication_members {
  __typename: "patent_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeletePatentPublicationMembers {
  /**
   * delete data from the table: "patent_publication_members"
   */
  delete_patent_publication_members: DeletePatentPublicationMembers_delete_patent_publication_members | null;
}

export interface DeletePatentPublicationMembersVariables {
  patent_publication_id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePatentPublicationByPK
// ====================================================

export interface DeletePatentPublicationByPK_delete_patent_publications_by_pk {
  __typename: "patent_publications";
  id: any;
}

export interface DeletePatentPublicationByPK {
  /**
   * delete single row from the table: "patent_publications"
   */
  delete_patent_publications_by_pk: DeletePatentPublicationByPK_delete_patent_publications_by_pk | null;
}

export interface DeletePatentPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeletePatentPublicationByPK
// ====================================================

export interface SoftDeletePatentPublicationByPK_update_patent_publications_by_pk {
  __typename: "patent_publications";
  id: any;
}

export interface SoftDeletePatentPublicationByPK {
  /**
   * update single row of the table: "patent_publications"
   */
  update_patent_publications_by_pk: SoftDeletePatentPublicationByPK_update_patent_publications_by_pk | null;
}

export interface SoftDeletePatentPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePatentPublicationCategories
// ====================================================

export interface DeletePatentPublicationCategories_delete_patent_publication_categories_by_pk {
  __typename: "patent_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeletePatentPublicationCategories {
  /**
   * delete single row from the table: "patent_publication_categories"
   */
  delete_patent_publication_categories_by_pk: DeletePatentPublicationCategories_delete_patent_publication_categories_by_pk | null;
}

export interface DeletePatentPublicationCategoriesVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PatentPublicationCategories
// ====================================================

export interface PatentPublicationCategories_patent_publication_categories {
  __typename: "patent_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface PatentPublicationCategories {
  /**
   * fetch data from the table: "patent_publication_categories"
   */
  patent_publication_categories: PatentPublicationCategories_patent_publication_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertPatentPublicationCategories
// ====================================================

export interface InsertPatentPublicationCategories_insert_patent_publication_categories_one {
  __typename: "patent_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertPatentPublicationCategories {
  /**
   * insert a single row into the table: "patent_publication_categories"
   */
  insert_patent_publication_categories_one: InsertPatentPublicationCategories_insert_patent_publication_categories_one | null;
}

export interface InsertPatentPublicationCategoriesVariables {
  object: patent_publication_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdatePatentPublicationCategories
// ====================================================

export interface UpdatePatentPublicationCategories_update_patent_publication_categories_by_pk {
  __typename: "patent_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdatePatentPublicationCategories {
  /**
   * update single row of the table: "patent_publication_categories"
   */
  update_patent_publication_categories_by_pk: UpdatePatentPublicationCategories_update_patent_publication_categories_by_pk | null;
}

export interface UpdatePatentPublicationCategoriesVariables {
  pk_columns: patent_publication_categories_pk_columns_input;
  _set?: patent_publication_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePatentPublicationActivityPositions
// ====================================================

export interface DeletePatentPublicationActivityPositions_delete_patent_publication_activity_positions_by_pk {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeletePatentPublicationActivityPositions {
  /**
   * delete single row from the table: "patent_publication_activity_positions"
   */
  delete_patent_publication_activity_positions_by_pk: DeletePatentPublicationActivityPositions_delete_patent_publication_activity_positions_by_pk | null;
}

export interface DeletePatentPublicationActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PatentPublicationActivityPositions
// ====================================================

export interface PatentPublicationActivityPositions_patent_publication_activity_positions {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface PatentPublicationActivityPositions {
  /**
   * fetch data from the table: "patent_publication_activity_positions"
   */
  patent_publication_activity_positions: PatentPublicationActivityPositions_patent_publication_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertPatentPublicationActivityPositions
// ====================================================

export interface InsertPatentPublicationActivityPositions_insert_patent_publication_activity_positions_one {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertPatentPublicationActivityPositions {
  /**
   * insert a single row into the table: "patent_publication_activity_positions"
   */
  insert_patent_publication_activity_positions_one: InsertPatentPublicationActivityPositions_insert_patent_publication_activity_positions_one | null;
}

export interface InsertPatentPublicationActivityPositionsVariables {
  object: patent_publication_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdatePatentPublicationActivityPositions
// ====================================================

export interface UpdatePatentPublicationActivityPositions_update_patent_publication_activity_positions_by_pk {
  __typename: "patent_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdatePatentPublicationActivityPositions {
  /**
   * update single row of the table: "patent_publication_activity_positions"
   */
  update_patent_publication_activity_positions_by_pk: UpdatePatentPublicationActivityPositions_update_patent_publication_activity_positions_by_pk | null;
}

export interface UpdatePatentPublicationActivityPositionsVariables {
  pk_columns: patent_publication_activity_positions_pk_columns_input;
  _set?: patent_publication_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PublicEmployeesList
// ====================================================

export interface PublicEmployeesList_employees_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface PublicEmployeesList_employees_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: PublicEmployeesList_employees_department_faculty | null;
}

export interface PublicEmployeesList_employees {
  __typename: "employees";
  id: any;
  name: string | null;
  picture_file: string | null;
  /**
   * An object relationship
   */
  department: PublicEmployeesList_employees_department | null;
}

export interface PublicEmployeesList_employees_aggregate_aggregate {
  __typename: "employees_aggregate_fields";
  count: number | null;
}

export interface PublicEmployeesList_employees_aggregate {
  __typename: "employees_aggregate";
  aggregate: PublicEmployeesList_employees_aggregate_aggregate | null;
}

export interface PublicEmployeesList {
  /**
   * fetch data from the table: "employees"
   */
  employees: PublicEmployeesList_employees[];
  /**
   * fetch aggregated fields from the table: "employees"
   */
  employees_aggregate: PublicEmployeesList_employees_aggregate;
}

export interface PublicEmployeesListVariables {
  limit?: number | null;
  offset?: number | null;
  where?: employees_bool_exp | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PublicDepartments
// ====================================================

export interface PublicDepartments_departments_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface PublicDepartments_departments {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: PublicDepartments_departments_faculty | null;
}

export interface PublicDepartments {
  /**
   * fetch data from the table: "departments"
   */
  departments: PublicDepartments_departments[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PublicEmployeesByPK
// ====================================================

export interface PublicEmployeesByPK_employees_by_pk_department_faculty {
  __typename: "faculties";
  id: number;
  name: string;
}

export interface PublicEmployeesByPK_employees_by_pk_department {
  __typename: "departments";
  id: string;
  name: string | null;
  /**
   * An object relationship
   */
  faculty: PublicEmployeesByPK_employees_by_pk_department_faculty | null;
}

export interface PublicEmployeesByPK_employees_by_pk_functional_position {
  __typename: "functional_positions";
  id: any;
  name: string | null;
}

export interface PublicEmployeesByPK_employees_by_pk_structural_position {
  __typename: "structural_positions";
  id: any;
  name: string | null;
}

export interface PublicEmployeesByPK_employees_by_pk_group {
  __typename: "groups";
  id: any;
  name: string;
}

export interface PublicEmployeesByPK_employees_by_pk {
  __typename: "employees";
  employee_type: employee_type_enum;
  id: any;
  n_id_n: string | null;
  npp: string | null;
  email: string | null;
  name: string | null;
  address: string | null;
  date_of_birth: any | null;
  place_of_birth: string | null;
  status: employee_status_enum | null;
  initial: string | null;
  phone: string | null;
  picture_file: string | null;
  /**
   * An object relationship
   */
  department: PublicEmployeesByPK_employees_by_pk_department | null;
  /**
   * An object relationship
   */
  functional_position: PublicEmployeesByPK_employees_by_pk_functional_position | null;
  /**
   * An object relationship
   */
  structural_position: PublicEmployeesByPK_employees_by_pk_structural_position | null;
  /**
   * An object relationship
   */
  group: PublicEmployeesByPK_employees_by_pk_group | null;
}

export interface PublicEmployeesByPK {
  /**
   * fetch data from the table: "employees" using primary key columns
   */
  employees_by_pk: PublicEmployeesByPK_employees_by_pk | null;
}

export interface PublicEmployeesByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PublicEmployeeActivites
// ====================================================

export interface PublicEmployeeActivites_researches {
  __typename: "researches";
  id: any;
  title: string | null;
  finished_at: any | null;
}

export interface PublicEmployeeActivites_services {
  __typename: "services";
  id: any;
  title: string | null;
  end_date: any | null;
}

export interface PublicEmployeeActivites_journal_publications {
  __typename: "journal_publications";
  id: any;
  title: string;
  journal_year: number;
}

export interface PublicEmployeeActivites_seminar_publications {
  __typename: "seminar_publications";
  id: any;
  title: string;
  seminar_date: any;
}

export interface PublicEmployeeActivites_book_publications {
  __typename: "book_publications";
  id: any;
  title: string;
  publish_year: number;
}

export interface PublicEmployeeActivites_patent_publications {
  __typename: "patent_publications";
  id: any;
  title: string;
  first_announcement_date: any;
}

export interface PublicEmployeeActivites_self_developments {
  __typename: "self_developments";
  id: any;
  name: string;
  end_date: any | null;
}

export interface PublicEmployeeActivites_committees {
  __typename: "committees";
  id: any;
  name: string;
  end_date: any | null;
}

export interface PublicEmployeeActivites {
  /**
   * fetch data from the table: "researches"
   */
  researches: PublicEmployeeActivites_researches[];
  /**
   * fetch data from the table: "services"
   */
  services: PublicEmployeeActivites_services[];
  /**
   * fetch data from the table: "journal_publications"
   */
  journal_publications: PublicEmployeeActivites_journal_publications[];
  /**
   * fetch data from the table: "seminar_publications"
   */
  seminar_publications: PublicEmployeeActivites_seminar_publications[];
  /**
   * fetch data from the table: "book_publications"
   */
  book_publications: PublicEmployeeActivites_book_publications[];
  /**
   * fetch data from the table: "patent_publications"
   */
  patent_publications: PublicEmployeeActivites_patent_publications[];
  /**
   * fetch data from the table: "self_developments"
   */
  self_developments: PublicEmployeeActivites_self_developments[];
  /**
   * fetch data from the table: "committees"
   */
  committees: PublicEmployeeActivites_committees[];
}

export interface PublicEmployeeActivitesVariables {
  employeeId: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Researches
// ====================================================

export interface Researches_researches_research_category {
  __typename: "research_categories";
  id: any;
  credit: any | null;
  name: string | null;
}

export interface Researches_researches_research_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface Researches_researches_research_members_research_activity_position {
  __typename: "research_activity_positions";
  id: number;
  name: string;
}

export interface Researches_researches_research_members {
  __typename: "research_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: Researches_researches_research_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  research_activity_position: Researches_researches_research_members_research_activity_position | null;
}

export interface Researches_researches {
  __typename: "researches";
  id: any;
  title: string | null;
  fund_amount: any | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  started_at: any | null;
  finished_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  research_category: Researches_researches_research_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
  /**
   * An array relationship
   */
  research_members: Researches_researches_research_members[];
}

export interface Researches {
  /**
   * fetch data from the table: "researches"
   */
  researches: Researches_researches[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ResearchByPK
// ====================================================

export interface ResearchByPK_researches_by_pk_research_category {
  __typename: "research_categories";
  id: any;
  credit: any | null;
  name: string | null;
}

export interface ResearchByPK_researches_by_pk_research_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface ResearchByPK_researches_by_pk_research_members_research_activity_position {
  __typename: "research_activity_positions";
  id: number;
  name: string;
}

export interface ResearchByPK_researches_by_pk_research_members {
  __typename: "research_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: ResearchByPK_researches_by_pk_research_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  research_activity_position: ResearchByPK_researches_by_pk_research_members_research_activity_position | null;
}

export interface ResearchByPK_researches_by_pk {
  __typename: "researches";
  id: any;
  title: string | null;
  fund_amount: any | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  started_at: any | null;
  finished_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  research_category: ResearchByPK_researches_by_pk_research_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
  /**
   * An array relationship
   */
  research_members: ResearchByPK_researches_by_pk_research_members[];
}

export interface ResearchByPK {
  /**
   * fetch data from the table: "researches" using primary key columns
   */
  researches_by_pk: ResearchByPK_researches_by_pk | null;
}

export interface ResearchByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertResearches
// ====================================================

export interface InsertResearches_insert_researches_one_research_category {
  __typename: "research_categories";
  id: any;
  credit: any | null;
  name: string | null;
}

export interface InsertResearches_insert_researches_one {
  __typename: "researches";
  id: any;
  title: string | null;
  fund_amount: any | null;
  credit_multiplier: any | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  started_at: any | null;
  finished_at: any | null;
  /**
   * An object relationship
   */
  research_category: InsertResearches_insert_researches_one_research_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
}

export interface InsertResearches {
  /**
   * insert a single row into the table: "researches"
   */
  insert_researches_one: InsertResearches_insert_researches_one | null;
}

export interface InsertResearchesVariables {
  object: researches_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertResearchMember
// ====================================================

export interface InsertResearchMember_insert_research_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertResearchMember_insert_research_members_returning_research_activity_position {
  __typename: "research_activity_positions";
  id: number;
  name: string;
}

export interface InsertResearchMember_insert_research_members_returning {
  __typename: "research_members";
  id: any;
  credit_gain: any | null;
  research_id: any | null;
  /**
   * An object relationship
   */
  employee: InsertResearchMember_insert_research_members_returning_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  research_activity_position: InsertResearchMember_insert_research_members_returning_research_activity_position | null;
}

export interface InsertResearchMember_insert_research_members {
  __typename: "research_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertResearchMember_insert_research_members_returning[];
}

export interface InsertResearchMember {
  /**
   * insert data into the table: "research_members"
   */
  insert_research_members: InsertResearchMember_insert_research_members | null;
}

export interface InsertResearchMemberVariables {
  objects: research_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateResearch
// ====================================================

export interface UpdateResearch_update_researches_by_pk_research_category {
  __typename: "research_categories";
  id: any;
  name: string | null;
  credit: any | null;
}

export interface UpdateResearch_update_researches_by_pk {
  __typename: "researches";
  id: any;
  title: string | null;
  fund_amount: any | null;
  credit_multiplier: any | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  started_at: any | null;
  finished_at: any | null;
  /**
   * An object relationship
   */
  research_category: UpdateResearch_update_researches_by_pk_research_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
}

export interface UpdateResearch {
  /**
   * update single row of the table: "researches"
   */
  update_researches_by_pk: UpdateResearch_update_researches_by_pk | null;
}

export interface UpdateResearchVariables {
  id: any;
  _set?: researches_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteResearchMembers
// ====================================================

export interface DeleteResearchMembers_delete_research_members {
  __typename: "research_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeleteResearchMembers {
  /**
   * delete data from the table: "research_members"
   */
  delete_research_members: DeleteResearchMembers_delete_research_members | null;
}

export interface DeleteResearchMembersVariables {
  researchId: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteResearchByPK
// ====================================================

export interface DeleteResearchByPK_delete_researches_by_pk {
  __typename: "researches";
  id: any;
}

export interface DeleteResearchByPK {
  /**
   * delete single row from the table: "researches"
   */
  delete_researches_by_pk: DeleteResearchByPK_delete_researches_by_pk | null;
}

export interface DeleteResearchByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeleteResearchByPK
// ====================================================

export interface SoftDeleteResearchByPK_update_researches_by_pk {
  __typename: "researches";
  id: any;
}

export interface SoftDeleteResearchByPK {
  /**
   * update single row of the table: "researches"
   */
  update_researches_by_pk: SoftDeleteResearchByPK_update_researches_by_pk | null;
}

export interface SoftDeleteResearchByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteResearchCategories
// ====================================================

export interface DeleteResearchCategories_delete_research_categories_by_pk {
  __typename: "research_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteResearchCategories {
  /**
   * delete single row from the table: "research_categories"
   */
  delete_research_categories_by_pk: DeleteResearchCategories_delete_research_categories_by_pk | null;
}

export interface DeleteResearchCategoriesVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ResearchCategories
// ====================================================

export interface ResearchCategories_research_categories {
  __typename: "research_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface ResearchCategories {
  /**
   * fetch data from the table: "research_categories"
   */
  research_categories: ResearchCategories_research_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertResearchCategories
// ====================================================

export interface InsertResearchCategories_insert_research_categories_one {
  __typename: "research_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertResearchCategories {
  /**
   * insert a single row into the table: "research_categories"
   */
  insert_research_categories_one: InsertResearchCategories_insert_research_categories_one | null;
}

export interface InsertResearchCategoriesVariables {
  object: research_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateResearchCategories
// ====================================================

export interface UpdateResearchCategories_update_research_categories_by_pk {
  __typename: "research_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateResearchCategories {
  /**
   * update single row of the table: "research_categories"
   */
  update_research_categories_by_pk: UpdateResearchCategories_update_research_categories_by_pk | null;
}

export interface UpdateResearchCategoriesVariables {
  pk_columns: research_categories_pk_columns_input;
  _set?: research_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteResearchActivityPositions
// ====================================================

export interface DeleteResearchActivityPositions_delete_research_activity_positions_by_pk {
  __typename: "research_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeleteResearchActivityPositions {
  /**
   * delete single row from the table: "research_activity_positions"
   */
  delete_research_activity_positions_by_pk: DeleteResearchActivityPositions_delete_research_activity_positions_by_pk | null;
}

export interface DeleteResearchActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ResearchActivityPositions
// ====================================================

export interface ResearchActivityPositions_research_activity_positions {
  __typename: "research_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface ResearchActivityPositions {
  /**
   * fetch data from the table: "research_activity_positions"
   */
  research_activity_positions: ResearchActivityPositions_research_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertResearchActivityPositions
// ====================================================

export interface InsertResearchActivityPositions_insert_research_activity_positions_one {
  __typename: "research_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertResearchActivityPositions {
  /**
   * insert a single row into the table: "research_activity_positions"
   */
  insert_research_activity_positions_one: InsertResearchActivityPositions_insert_research_activity_positions_one | null;
}

export interface InsertResearchActivityPositionsVariables {
  object: research_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateResearchActivityPositions
// ====================================================

export interface UpdateResearchActivityPositions_update_research_activity_positions_by_pk {
  __typename: "research_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdateResearchActivityPositions {
  /**
   * update single row of the table: "research_activity_positions"
   */
  update_research_activity_positions_by_pk: UpdateResearchActivityPositions_update_research_activity_positions_by_pk | null;
}

export interface UpdateResearchActivityPositionsVariables {
  pk_columns: research_activity_positions_pk_columns_input;
  _set?: research_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SelfDevelopmentLevel
// ====================================================

export interface SelfDevelopmentLevel_self_development_level {
  __typename: "self_development_level";
  level: string;
}

export interface SelfDevelopmentLevel {
  /**
   * fetch data from the table: "self_development_level"
   */
  self_development_level: SelfDevelopmentLevel_self_development_level[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SelfDevelopments
// ====================================================

export interface SelfDevelopments_self_developments_self_development_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface SelfDevelopments_self_developments_self_development_members {
  __typename: "self_development_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: SelfDevelopments_self_developments_self_development_members_employee | null;
  external_author: string | null;
}

export interface SelfDevelopments_self_developments_self_development_category {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
}

export interface SelfDevelopments_self_developments {
  __typename: "self_developments";
  id: any;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  /**
   * An array relationship
   */
  self_development_members: SelfDevelopments_self_developments_self_development_members[];
  /**
   * An object relationship
   */
  self_development_category: SelfDevelopments_self_developments_self_development_category;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: self_development_level_enum;
}

export interface SelfDevelopments {
  /**
   * fetch data from the table: "self_developments"
   */
  self_developments: SelfDevelopments_self_developments[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SelfDevelopmentByPK
// ====================================================

export interface SelfDevelopmentByPK_self_developments_by_pk_self_development_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface SelfDevelopmentByPK_self_developments_by_pk_self_development_members {
  __typename: "self_development_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: SelfDevelopmentByPK_self_developments_by_pk_self_development_members_employee | null;
  external_author: string | null;
}

export interface SelfDevelopmentByPK_self_developments_by_pk_self_development_category {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
}

export interface SelfDevelopmentByPK_self_developments_by_pk {
  __typename: "self_developments";
  id: any;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  /**
   * An array relationship
   */
  self_development_members: SelfDevelopmentByPK_self_developments_by_pk_self_development_members[];
  /**
   * An object relationship
   */
  self_development_category: SelfDevelopmentByPK_self_developments_by_pk_self_development_category;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: self_development_level_enum;
}

export interface SelfDevelopmentByPK {
  /**
   * fetch data from the table: "self_developments" using primary key columns
   */
  self_developments_by_pk: SelfDevelopmentByPK_self_developments_by_pk | null;
}

export interface SelfDevelopmentByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSelfDevelopments
// ====================================================

export interface InsertSelfDevelopments_insert_self_developments_one_self_development_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertSelfDevelopments_insert_self_developments_one_self_development_members {
  __typename: "self_development_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: InsertSelfDevelopments_insert_self_developments_one_self_development_members_employee | null;
  external_author: string | null;
}

export interface InsertSelfDevelopments_insert_self_developments_one_self_development_category {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
}

export interface InsertSelfDevelopments_insert_self_developments_one {
  __typename: "self_developments";
  id: any;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  /**
   * An array relationship
   */
  self_development_members: InsertSelfDevelopments_insert_self_developments_one_self_development_members[];
  /**
   * An object relationship
   */
  self_development_category: InsertSelfDevelopments_insert_self_developments_one_self_development_category;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: self_development_level_enum;
}

export interface InsertSelfDevelopments {
  /**
   * insert a single row into the table: "self_developments"
   */
  insert_self_developments_one: InsertSelfDevelopments_insert_self_developments_one | null;
}

export interface InsertSelfDevelopmentsVariables {
  object: self_developments_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSelfDevelopmentMember
// ====================================================

export interface InsertSelfDevelopmentMember_insert_self_development_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertSelfDevelopmentMember_insert_self_development_members_returning {
  __typename: "self_development_members";
  id: any;
  self_development_id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: InsertSelfDevelopmentMember_insert_self_development_members_returning_employee | null;
  external_author: string | null;
}

export interface InsertSelfDevelopmentMember_insert_self_development_members {
  __typename: "self_development_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertSelfDevelopmentMember_insert_self_development_members_returning[];
}

export interface InsertSelfDevelopmentMember {
  /**
   * insert data into the table: "self_development_members"
   */
  insert_self_development_members: InsertSelfDevelopmentMember_insert_self_development_members | null;
}

export interface InsertSelfDevelopmentMemberVariables {
  objects: self_development_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateSelfDevelopment
// ====================================================

export interface UpdateSelfDevelopment_update_self_developments_by_pk_self_development_category {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
}

export interface UpdateSelfDevelopment_update_self_developments_by_pk {
  __typename: "self_developments";
  id: any;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  /**
   * An object relationship
   */
  self_development_category: UpdateSelfDevelopment_update_self_developments_by_pk_self_development_category;
  name: string;
  start_date: any;
  end_date: any | null;
  venue: string;
  decree_file: string | null;
  certificate_file: string;
  level: self_development_level_enum;
}

export interface UpdateSelfDevelopment {
  /**
   * update single row of the table: "self_developments"
   */
  update_self_developments_by_pk: UpdateSelfDevelopment_update_self_developments_by_pk | null;
}

export interface UpdateSelfDevelopmentVariables {
  id: any;
  _set?: self_developments_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSelfDevelopmentMembers
// ====================================================

export interface DeleteSelfDevelopmentMembers_delete_self_development_members {
  __typename: "self_development_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeleteSelfDevelopmentMembers {
  /**
   * delete data from the table: "self_development_members"
   */
  delete_self_development_members: DeleteSelfDevelopmentMembers_delete_self_development_members | null;
}

export interface DeleteSelfDevelopmentMembersVariables {
  self_development_id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSelfDevelopmentByPK
// ====================================================

export interface DeleteSelfDevelopmentByPK_delete_self_developments_by_pk {
  __typename: "self_developments";
  id: any;
}

export interface DeleteSelfDevelopmentByPK {
  /**
   * delete single row from the table: "self_developments"
   */
  delete_self_developments_by_pk: DeleteSelfDevelopmentByPK_delete_self_developments_by_pk | null;
}

export interface DeleteSelfDevelopmentByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeleteSelfDevelopmentByPK
// ====================================================

export interface SoftDeleteSelfDevelopmentByPK_update_self_developments_by_pk {
  __typename: "self_developments";
  id: any;
}

export interface SoftDeleteSelfDevelopmentByPK {
  /**
   * update single row of the table: "self_developments"
   */
  update_self_developments_by_pk: SoftDeleteSelfDevelopmentByPK_update_self_developments_by_pk | null;
}

export interface SoftDeleteSelfDevelopmentByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSelfDevelopmentCategories
// ====================================================

export interface DeleteSelfDevelopmentCategories_delete_self_development_categories_by_pk {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteSelfDevelopmentCategories {
  /**
   * delete single row from the table: "self_development_categories"
   */
  delete_self_development_categories_by_pk: DeleteSelfDevelopmentCategories_delete_self_development_categories_by_pk | null;
}

export interface DeleteSelfDevelopmentCategoriesVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SelfDevelopmentCategories
// ====================================================

export interface SelfDevelopmentCategories_self_development_categories {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface SelfDevelopmentCategories {
  /**
   * fetch data from the table: "self_development_categories"
   */
  self_development_categories: SelfDevelopmentCategories_self_development_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSelfDevelopmentCategories
// ====================================================

export interface InsertSelfDevelopmentCategories_insert_self_development_categories_one {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertSelfDevelopmentCategories {
  /**
   * insert a single row into the table: "self_development_categories"
   */
  insert_self_development_categories_one: InsertSelfDevelopmentCategories_insert_self_development_categories_one | null;
}

export interface InsertSelfDevelopmentCategoriesVariables {
  object: self_development_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateSelfDevelopmentCategories
// ====================================================

export interface UpdateSelfDevelopmentCategories_update_self_development_categories_by_pk {
  __typename: "self_development_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateSelfDevelopmentCategories {
  /**
   * update single row of the table: "self_development_categories"
   */
  update_self_development_categories_by_pk: UpdateSelfDevelopmentCategories_update_self_development_categories_by_pk | null;
}

export interface UpdateSelfDevelopmentCategoriesVariables {
  pk_columns: self_development_categories_pk_columns_input;
  _set?: self_development_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSelfDevelopmentActivityPositions
// ====================================================

export interface DeleteSelfDevelopmentActivityPositions_delete_self_development_activity_positions_by_pk {
  __typename: "self_development_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeleteSelfDevelopmentActivityPositions {
  /**
   * delete single row from the table: "self_development_activity_positions"
   */
  delete_self_development_activity_positions_by_pk: DeleteSelfDevelopmentActivityPositions_delete_self_development_activity_positions_by_pk | null;
}

export interface DeleteSelfDevelopmentActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SelfDevelopmentActivityPositions
// ====================================================

export interface SelfDevelopmentActivityPositions_self_development_activity_positions {
  __typename: "self_development_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface SelfDevelopmentActivityPositions {
  /**
   * fetch data from the table: "self_development_activity_positions"
   */
  self_development_activity_positions: SelfDevelopmentActivityPositions_self_development_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSelfDevelopmentActivityPositions
// ====================================================

export interface InsertSelfDevelopmentActivityPositions_insert_self_development_activity_positions_one {
  __typename: "self_development_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertSelfDevelopmentActivityPositions {
  /**
   * insert a single row into the table: "self_development_activity_positions"
   */
  insert_self_development_activity_positions_one: InsertSelfDevelopmentActivityPositions_insert_self_development_activity_positions_one | null;
}

export interface InsertSelfDevelopmentActivityPositionsVariables {
  object: self_development_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateSelfDevelopmentActivityPositions
// ====================================================

export interface UpdateSelfDevelopmentActivityPositions_update_self_development_activity_positions_by_pk {
  __typename: "self_development_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdateSelfDevelopmentActivityPositions {
  /**
   * update single row of the table: "self_development_activity_positions"
   */
  update_self_development_activity_positions_by_pk: UpdateSelfDevelopmentActivityPositions_update_self_development_activity_positions_by_pk | null;
}

export interface UpdateSelfDevelopmentActivityPositionsVariables {
  pk_columns: self_development_activity_positions_pk_columns_input;
  _set?: self_development_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SeminarPublications
// ====================================================

export interface SeminarPublications_seminar_publications_seminar_publication_category {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
}

export interface SeminarPublications_seminar_publications_seminar_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface SeminarPublications_seminar_publications_seminar_publication_members_seminar_publication_activity_position {
  __typename: "seminar_publication_activity_positions";
  id: number;
  name: string;
}

export interface SeminarPublications_seminar_publications_seminar_publication_members {
  __typename: "seminar_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: SeminarPublications_seminar_publications_seminar_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  seminar_publication_activity_position: SeminarPublications_seminar_publications_seminar_publication_members_seminar_publication_activity_position;
}

export interface SeminarPublications_seminar_publications {
  __typename: "seminar_publications";
  id: any;
  title: string;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  seminar_publication_category: SeminarPublications_seminar_publications_seminar_publication_category;
  proceeding_cover_file: string;
  proceeding_toc_file: string;
  certificate_file: string;
  paper_file: string | null;
  decree_file: string | null;
  /**
   * An array relationship
   */
  seminar_publication_members: SeminarPublications_seminar_publications_seminar_publication_members[];
  isbn: string | null;
  issn: string | null;
  seminar_name: string;
  seminar_venue: string;
  seminar_year: number;
  seminar_page_no: string | null;
  seminar_date: any;
}

export interface SeminarPublications {
  /**
   * fetch data from the table: "seminar_publications"
   */
  seminar_publications: SeminarPublications_seminar_publications[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SeminarPublicationByPK
// ====================================================

export interface SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_category {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
}

export interface SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members_seminar_publication_activity_position {
  __typename: "seminar_publication_activity_positions";
  id: number;
  name: string;
}

export interface SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members {
  __typename: "seminar_publication_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  seminar_publication_activity_position: SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members_seminar_publication_activity_position;
}

export interface SeminarPublicationByPK_seminar_publications_by_pk {
  __typename: "seminar_publications";
  id: any;
  title: string;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  seminar_publication_category: SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_category;
  certificate_file: string;
  proceeding_cover_file: string;
  proceeding_toc_file: string;
  paper_file: string | null;
  decree_file: string | null;
  /**
   * An array relationship
   */
  seminar_publication_members: SeminarPublicationByPK_seminar_publications_by_pk_seminar_publication_members[];
  isbn: string | null;
  issn: string | null;
  seminar_name: string;
  seminar_venue: string;
  seminar_year: number;
  seminar_page_no: string | null;
  seminar_date: any;
}

export interface SeminarPublicationByPK {
  /**
   * fetch data from the table: "seminar_publications" using primary key columns
   */
  seminar_publications_by_pk: SeminarPublicationByPK_seminar_publications_by_pk | null;
}

export interface SeminarPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSeminarPublications
// ====================================================

export interface InsertSeminarPublications_insert_seminar_publications_one_seminar_publication_category {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
}

export interface InsertSeminarPublications_insert_seminar_publications_one {
  __typename: "seminar_publications";
  id: any;
  title: string;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  seminar_publication_category: InsertSeminarPublications_insert_seminar_publications_one_seminar_publication_category;
  certificate_file: string;
  proceeding_cover_file: string;
  proceeding_toc_file: string;
  paper_file: string | null;
  decree_file: string | null;
}

export interface InsertSeminarPublications {
  /**
   * insert a single row into the table: "seminar_publications"
   */
  insert_seminar_publications_one: InsertSeminarPublications_insert_seminar_publications_one | null;
}

export interface InsertSeminarPublicationsVariables {
  object: seminar_publications_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSeminarPublicationMember
// ====================================================

export interface InsertSeminarPublicationMember_insert_seminar_publication_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertSeminarPublicationMember_insert_seminar_publication_members_returning_seminar_publication_activity_position {
  __typename: "seminar_publication_activity_positions";
  id: number;
  name: string;
}

export interface InsertSeminarPublicationMember_insert_seminar_publication_members_returning {
  __typename: "seminar_publication_members";
  id: any;
  credit_gain: any | null;
  seminar_publication_id: any;
  /**
   * An object relationship
   */
  employee: InsertSeminarPublicationMember_insert_seminar_publication_members_returning_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  seminar_publication_activity_position: InsertSeminarPublicationMember_insert_seminar_publication_members_returning_seminar_publication_activity_position;
}

export interface InsertSeminarPublicationMember_insert_seminar_publication_members {
  __typename: "seminar_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertSeminarPublicationMember_insert_seminar_publication_members_returning[];
}

export interface InsertSeminarPublicationMember {
  /**
   * insert data into the table: "seminar_publication_members"
   */
  insert_seminar_publication_members: InsertSeminarPublicationMember_insert_seminar_publication_members | null;
}

export interface InsertSeminarPublicationMemberVariables {
  objects: seminar_publication_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateSeminarPublication
// ====================================================

export interface UpdateSeminarPublication_update_seminar_publications_by_pk_seminar_publication_category {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
}

export interface UpdateSeminarPublication_update_seminar_publications_by_pk {
  __typename: "seminar_publications";
  id: any;
  title: string;
  created_at: any;
  updated_at: any;
  deleted_at: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  seminar_publication_category: UpdateSeminarPublication_update_seminar_publications_by_pk_seminar_publication_category;
  certificate_file: string;
  proceeding_cover_file: string;
  proceeding_toc_file: string;
  paper_file: string | null;
  decree_file: string | null;
}

export interface UpdateSeminarPublication {
  /**
   * update single row of the table: "seminar_publications"
   */
  update_seminar_publications_by_pk: UpdateSeminarPublication_update_seminar_publications_by_pk | null;
}

export interface UpdateSeminarPublicationVariables {
  id: any;
  _set?: seminar_publications_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSeminarPublicationMembers
// ====================================================

export interface DeleteSeminarPublicationMembers_delete_seminar_publication_members {
  __typename: "seminar_publication_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeleteSeminarPublicationMembers {
  /**
   * delete data from the table: "seminar_publication_members"
   */
  delete_seminar_publication_members: DeleteSeminarPublicationMembers_delete_seminar_publication_members | null;
}

export interface DeleteSeminarPublicationMembersVariables {
  seminar_publication_id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSeminarPublicationByPK
// ====================================================

export interface DeleteSeminarPublicationByPK_delete_seminar_publications_by_pk {
  __typename: "seminar_publications";
  id: any;
}

export interface DeleteSeminarPublicationByPK {
  /**
   * delete single row from the table: "seminar_publications"
   */
  delete_seminar_publications_by_pk: DeleteSeminarPublicationByPK_delete_seminar_publications_by_pk | null;
}

export interface DeleteSeminarPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeleteSeminarPublicationByPK
// ====================================================

export interface SoftDeleteSeminarPublicationByPK_update_seminar_publications_by_pk {
  __typename: "seminar_publications";
  id: any;
}

export interface SoftDeleteSeminarPublicationByPK {
  /**
   * update single row of the table: "seminar_publications"
   */
  update_seminar_publications_by_pk: SoftDeleteSeminarPublicationByPK_update_seminar_publications_by_pk | null;
}

export interface SoftDeleteSeminarPublicationByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSeminarPublicationCategories
// ====================================================

export interface DeleteSeminarPublicationCategories_delete_seminar_publication_categories_by_pk {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteSeminarPublicationCategories {
  /**
   * delete single row from the table: "seminar_publication_categories"
   */
  delete_seminar_publication_categories_by_pk: DeleteSeminarPublicationCategories_delete_seminar_publication_categories_by_pk | null;
}

export interface DeleteSeminarPublicationCategoriesVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SeminarPublicationCategories
// ====================================================

export interface SeminarPublicationCategories_seminar_publication_categories {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface SeminarPublicationCategories {
  /**
   * fetch data from the table: "seminar_publication_categories"
   */
  seminar_publication_categories: SeminarPublicationCategories_seminar_publication_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSeminarPublicationCategories
// ====================================================

export interface InsertSeminarPublicationCategories_insert_seminar_publication_categories_one {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertSeminarPublicationCategories {
  /**
   * insert a single row into the table: "seminar_publication_categories"
   */
  insert_seminar_publication_categories_one: InsertSeminarPublicationCategories_insert_seminar_publication_categories_one | null;
}

export interface InsertSeminarPublicationCategoriesVariables {
  object: seminar_publication_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateSeminarPublicationCategories
// ====================================================

export interface UpdateSeminarPublicationCategories_update_seminar_publication_categories_by_pk {
  __typename: "seminar_publication_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateSeminarPublicationCategories {
  /**
   * update single row of the table: "seminar_publication_categories"
   */
  update_seminar_publication_categories_by_pk: UpdateSeminarPublicationCategories_update_seminar_publication_categories_by_pk | null;
}

export interface UpdateSeminarPublicationCategoriesVariables {
  pk_columns: seminar_publication_categories_pk_columns_input;
  _set?: seminar_publication_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteSeminarPublicationActivityPositions
// ====================================================

export interface DeleteSeminarPublicationActivityPositions_delete_seminar_publication_activity_positions_by_pk {
  __typename: "seminar_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeleteSeminarPublicationActivityPositions {
  /**
   * delete single row from the table: "seminar_publication_activity_positions"
   */
  delete_seminar_publication_activity_positions_by_pk: DeleteSeminarPublicationActivityPositions_delete_seminar_publication_activity_positions_by_pk | null;
}

export interface DeleteSeminarPublicationActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SeminarPublicationActivityPositions
// ====================================================

export interface SeminarPublicationActivityPositions_seminar_publication_activity_positions {
  __typename: "seminar_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface SeminarPublicationActivityPositions {
  /**
   * fetch data from the table: "seminar_publication_activity_positions"
   */
  seminar_publication_activity_positions: SeminarPublicationActivityPositions_seminar_publication_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertSeminarPublicationActivityPositions
// ====================================================

export interface InsertSeminarPublicationActivityPositions_insert_seminar_publication_activity_positions_one {
  __typename: "seminar_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertSeminarPublicationActivityPositions {
  /**
   * insert a single row into the table: "seminar_publication_activity_positions"
   */
  insert_seminar_publication_activity_positions_one: InsertSeminarPublicationActivityPositions_insert_seminar_publication_activity_positions_one | null;
}

export interface InsertSeminarPublicationActivityPositionsVariables {
  object: seminar_publication_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateSeminarPublicationActivityPositions
// ====================================================

export interface UpdateSeminarPublicationActivityPositions_update_seminar_publication_activity_positions_by_pk {
  __typename: "seminar_publication_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdateSeminarPublicationActivityPositions {
  /**
   * update single row of the table: "seminar_publication_activity_positions"
   */
  update_seminar_publication_activity_positions_by_pk: UpdateSeminarPublicationActivityPositions_update_seminar_publication_activity_positions_by_pk | null;
}

export interface UpdateSeminarPublicationActivityPositionsVariables {
  pk_columns: seminar_publication_activity_positions_pk_columns_input;
  _set?: seminar_publication_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Services
// ====================================================

export interface Services_services_service_category {
  __typename: "service_categories";
  id: any;
  name: string | null;
}

export interface Services_services_service_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface Services_services_service_members_service_activity_position {
  __typename: "service_activity_positions";
  id: number;
  name: string;
}

export interface Services_services_service_members {
  __typename: "service_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: Services_services_service_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  service_activity_position: Services_services_service_members_service_activity_position;
}

export interface Services_services {
  __typename: "services";
  id: any;
  title: string | null;
  fund_amount: number | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  start_date: any | null;
  end_date: any | null;
  credit_multiplier: any | null;
  /**
   * An object relationship
   */
  service_category: Services_services_service_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
  /**
   * An array relationship
   */
  service_members: Services_services_service_members[];
}

export interface Services {
  /**
   * fetch data from the table: "services"
   */
  services: Services_services[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ServiceByPK
// ====================================================

export interface ServiceByPK_services_by_pk_service_category {
  __typename: "service_categories";
  id: any;
  name: string | null;
}

export interface ServiceByPK_services_by_pk_service_members_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface ServiceByPK_services_by_pk_service_members_service_activity_position {
  __typename: "service_activity_positions";
  id: number;
  name: string;
}

export interface ServiceByPK_services_by_pk_service_members {
  __typename: "service_members";
  id: any;
  credit_gain: any | null;
  /**
   * An object relationship
   */
  employee: ServiceByPK_services_by_pk_service_members_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  service_activity_position: ServiceByPK_services_by_pk_service_members_service_activity_position;
}

export interface ServiceByPK_services_by_pk {
  __typename: "services";
  id: any;
  title: string | null;
  fund_amount: number | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  credit_multiplier: any | null;
  start_date: any | null;
  end_date: any | null;
  /**
   * An object relationship
   */
  service_category: ServiceByPK_services_by_pk_service_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
  /**
   * An array relationship
   */
  service_members: ServiceByPK_services_by_pk_service_members[];
}

export interface ServiceByPK {
  /**
   * fetch data from the table: "services" using primary key columns
   */
  services_by_pk: ServiceByPK_services_by_pk | null;
}

export interface ServiceByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertServices
// ====================================================

export interface InsertServices_insert_services_one_service_category {
  __typename: "service_categories";
  id: any;
  name: string | null;
}

export interface InsertServices_insert_services_one {
  __typename: "services";
  id: any;
  title: string | null;
  fund_amount: number | null;
  credit_multiplier: any | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  start_date: any | null;
  end_date: any | null;
  /**
   * An object relationship
   */
  service_category: InsertServices_insert_services_one_service_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
}

export interface InsertServices {
  /**
   * insert a single row into the table: "services"
   */
  insert_services_one: InsertServices_insert_services_one | null;
}

export interface InsertServicesVariables {
  object: services_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertServiceMember
// ====================================================

export interface InsertServiceMember_insert_service_members_returning_employee {
  __typename: "employees";
  id: any;
  name: string | null;
  employee_type: employee_type_enum;
}

export interface InsertServiceMember_insert_service_members_returning_service_activity_position {
  __typename: "service_activity_positions";
  id: number;
  name: string;
}

export interface InsertServiceMember_insert_service_members_returning {
  __typename: "service_members";
  id: any;
  credit_gain: any | null;
  service_id: any;
  /**
   * An object relationship
   */
  employee: InsertServiceMember_insert_service_members_returning_employee | null;
  external_author: string | null;
  /**
   * An object relationship
   */
  service_activity_position: InsertServiceMember_insert_service_members_returning_service_activity_position;
}

export interface InsertServiceMember_insert_service_members {
  __typename: "service_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
  /**
   * data of the affected rows by the mutation
   */
  returning: InsertServiceMember_insert_service_members_returning[];
}

export interface InsertServiceMember {
  /**
   * insert data into the table: "service_members"
   */
  insert_service_members: InsertServiceMember_insert_service_members | null;
}

export interface InsertServiceMemberVariables {
  objects: service_members_insert_input[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateService
// ====================================================

export interface UpdateService_update_services_by_pk_service_category {
  __typename: "service_categories";
  id: any;
  name: string | null;
}

export interface UpdateService_update_services_by_pk {
  __typename: "services";
  id: any;
  title: string | null;
  fund_amount: number | null;
  credit_multiplier: any | null;
  created_at: any | null;
  updated_at: any | null;
  deleted_at: any | null;
  start_date: any | null;
  end_date: any | null;
  /**
   * An object relationship
   */
  service_category: UpdateService_update_services_by_pk_service_category | null;
  proposal_file: string | null;
  final_report_file: string | null;
  certificate_file: string | null;
  decree_file: string | null;
}

export interface UpdateService {
  /**
   * update single row of the table: "services"
   */
  update_services_by_pk: UpdateService_update_services_by_pk | null;
}

export interface UpdateServiceVariables {
  id: any;
  _set?: services_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteServiceMembers
// ====================================================

export interface DeleteServiceMembers_delete_service_members {
  __typename: "service_members_mutation_response";
  /**
   * number of affected rows by the mutation
   */
  affected_rows: number;
}

export interface DeleteServiceMembers {
  /**
   * delete data from the table: "service_members"
   */
  delete_service_members: DeleteServiceMembers_delete_service_members | null;
}

export interface DeleteServiceMembersVariables {
  serviceId: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteServiceByPK
// ====================================================

export interface DeleteServiceByPK_delete_services_by_pk {
  __typename: "services";
  id: any;
}

export interface DeleteServiceByPK {
  /**
   * delete single row from the table: "services"
   */
  delete_services_by_pk: DeleteServiceByPK_delete_services_by_pk | null;
}

export interface DeleteServiceByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SoftDeleteServiceByPK
// ====================================================

export interface SoftDeleteServiceByPK_update_services_by_pk {
  __typename: "services";
  id: any;
}

export interface SoftDeleteServiceByPK {
  /**
   * update single row of the table: "services"
   */
  update_services_by_pk: SoftDeleteServiceByPK_update_services_by_pk | null;
}

export interface SoftDeleteServiceByPKVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteServiceCategories
// ====================================================

export interface DeleteServiceCategories_delete_service_categories_by_pk {
  __typename: "service_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteServiceCategories {
  /**
   * delete single row from the table: "service_categories"
   */
  delete_service_categories_by_pk: DeleteServiceCategories_delete_service_categories_by_pk | null;
}

export interface DeleteServiceCategoriesVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ServiceCategories
// ====================================================

export interface ServiceCategories_service_categories {
  __typename: "service_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface ServiceCategories {
  /**
   * fetch data from the table: "service_categories"
   */
  service_categories: ServiceCategories_service_categories[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertServiceCategories
// ====================================================

export interface InsertServiceCategories_insert_service_categories_one {
  __typename: "service_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertServiceCategories {
  /**
   * insert a single row into the table: "service_categories"
   */
  insert_service_categories_one: InsertServiceCategories_insert_service_categories_one | null;
}

export interface InsertServiceCategoriesVariables {
  object: service_categories_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateServiceCategories
// ====================================================

export interface UpdateServiceCategories_update_service_categories_by_pk {
  __typename: "service_categories";
  id: any;
  name: string | null;
  credit: any | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateServiceCategories {
  /**
   * update single row of the table: "service_categories"
   */
  update_service_categories_by_pk: UpdateServiceCategories_update_service_categories_by_pk | null;
}

export interface UpdateServiceCategoriesVariables {
  pk_columns: service_categories_pk_columns_input;
  _set?: service_categories_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteServiceActivityPositions
// ====================================================

export interface DeleteServiceActivityPositions_delete_service_activity_positions_by_pk {
  __typename: "service_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface DeleteServiceActivityPositions {
  /**
   * delete single row from the table: "service_activity_positions"
   */
  delete_service_activity_positions_by_pk: DeleteServiceActivityPositions_delete_service_activity_positions_by_pk | null;
}

export interface DeleteServiceActivityPositionsVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ServiceActivityPositions
// ====================================================

export interface ServiceActivityPositions_service_activity_positions {
  __typename: "service_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface ServiceActivityPositions {
  /**
   * fetch data from the table: "service_activity_positions"
   */
  service_activity_positions: ServiceActivityPositions_service_activity_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertServiceActivityPositions
// ====================================================

export interface InsertServiceActivityPositions_insert_service_activity_positions_one {
  __typename: "service_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface InsertServiceActivityPositions {
  /**
   * insert a single row into the table: "service_activity_positions"
   */
  insert_service_activity_positions_one: InsertServiceActivityPositions_insert_service_activity_positions_one | null;
}

export interface InsertServiceActivityPositionsVariables {
  object: service_activity_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateServiceActivityPositions
// ====================================================

export interface UpdateServiceActivityPositions_update_service_activity_positions_by_pk {
  __typename: "service_activity_positions";
  id: number;
  name: string;
  created_at: any;
  updated_at: any;
}

export interface UpdateServiceActivityPositions {
  /**
   * update single row of the table: "service_activity_positions"
   */
  update_service_activity_positions_by_pk: UpdateServiceActivityPositions_update_service_activity_positions_by_pk | null;
}

export interface UpdateServiceActivityPositionsVariables {
  pk_columns: service_activity_positions_pk_columns_input;
  _set?: service_activity_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteStructuralPosition
// ====================================================

export interface DeleteStructuralPosition_delete_structural_positions_by_pk {
  __typename: "structural_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface DeleteStructuralPosition {
  /**
   * delete single row from the table: "structural_positions"
   */
  delete_structural_positions_by_pk: DeleteStructuralPosition_delete_structural_positions_by_pk | null;
}

export interface DeleteStructuralPositionVariables {
  id: any;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: StructuralPositions
// ====================================================

export interface StructuralPositions_structural_positions {
  __typename: "structural_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface StructuralPositions {
  /**
   * fetch data from the table: "structural_positions"
   */
  structural_positions: StructuralPositions_structural_positions[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InsertStructuralPosition
// ====================================================

export interface InsertStructuralPosition_insert_structural_positions_one {
  __typename: "structural_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface InsertStructuralPosition {
  /**
   * insert a single row into the table: "structural_positions"
   */
  insert_structural_positions_one: InsertStructuralPosition_insert_structural_positions_one | null;
}

export interface InsertStructuralPositionVariables {
  object: structural_positions_insert_input;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateStructuralPosition
// ====================================================

export interface UpdateStructuralPosition_update_structural_positions_by_pk {
  __typename: "structural_positions";
  id: any;
  name: string | null;
  created_at: any | null;
  updated_at: any | null;
}

export interface UpdateStructuralPosition {
  /**
   * update single row of the table: "structural_positions"
   */
  update_structural_positions_by_pk: UpdateStructuralPosition_update_structural_positions_by_pk | null;
}

export interface UpdateStructuralPositionVariables {
  pk_columns: structural_positions_pk_columns_input;
  _set?: structural_positions_set_input | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * unique or primary key constraints on table "book_publication_activity_positions"
 */
export enum book_publication_activity_positions_constraint {
  book_activity_positions_pkey = "book_activity_positions_pkey",
}

/**
 * update columns of table "book_publication_activity_positions"
 */
export enum book_publication_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "book_publication_categories"
 */
export enum book_publication_categories_constraint {
  book_publication_categories_pkey = "book_publication_categories_pkey",
}

/**
 * update columns of table "book_publication_categories"
 */
export enum book_publication_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "book_publication_members"
 */
export enum book_publication_members_constraint {
  book_publication_members_pkey = "book_publication_members_pkey",
}

/**
 * update columns of table "book_publication_members"
 */
export enum book_publication_members_update_column {
  activity_position_id = "activity_position_id",
  book_publication_id = "book_publication_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
}

/**
 * unique or primary key constraints on table "book_publications"
 */
export enum book_publications_constraint {
  book_publications_pkey = "book_publications_pkey",
}

/**
 * update columns of table "book_publications"
 */
export enum book_publications_update_column {
  book_file = "book_file",
  category_id = "category_id",
  city = "city",
  created_at = "created_at",
  credit_multiplier = "credit_multiplier",
  deleted_at = "deleted_at",
  id = "id",
  publish_year = "publish_year",
  publisher_name = "publisher_name",
  title = "title",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "committee_activity_positions"
 */
export enum committee_activity_positions_constraint {
  committee_activity_positions_pkey = "committee_activity_positions_pkey",
}

/**
 * update columns of table "committee_activity_positions"
 */
export enum committee_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "committee_categories"
 */
export enum committee_categories_constraint {
  idx_16781_primary = "idx_16781_primary",
}

/**
 * update columns of table "committee_categories"
 */
export enum committee_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "committee_level"
 */
export enum committee_level_constraint {
  committee_level_pkey = "committee_level_pkey",
}

export enum committee_level_enum {
  Internasional = "Internasional",
  Nasional = "Nasional",
  Regional = "Regional",
}

/**
 * update columns of table "committee_level"
 */
export enum committee_level_update_column {
  level = "level",
}

/**
 * unique or primary key constraints on table "committee_members"
 */
export enum committee_members_constraint {
  committee_members_pkey = "committee_members_pkey",
}

/**
 * update columns of table "committee_members"
 */
export enum committee_members_update_column {
  activity_position_id = "activity_position_id",
  committee_id = "committee_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
}

/**
 * unique or primary key constraints on table "committees"
 */
export enum committees_constraint {
  committees_pkey = "committees_pkey",
}

/**
 * update columns of table "committees"
 */
export enum committees_update_column {
  category_id = "category_id",
  certificate_file = "certificate_file",
  created_at = "created_at",
  decree_file = "decree_file",
  deleted_at = "deleted_at",
  end_date = "end_date",
  id = "id",
  level = "level",
  name = "name",
  start_date = "start_date",
  updated_at = "updated_at",
  venue = "venue",
}

/**
 * unique or primary key constraints on table "departments"
 */
export enum departments_constraint {
  idx_16785_primary = "idx_16785_primary",
}

/**
 * update columns of table "departments"
 */
export enum departments_update_column {
  created_at = "created_at",
  faculty_id = "faculty_id",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "employee_education_histories"
 */
export enum employee_education_histories_constraint {
  idx_16793_primary = "idx_16793_primary",
}

/**
 * update columns of table "employee_education_histories"
 */
export enum employee_education_histories_update_column {
  created_at = "created_at",
  department = "department",
  employee_id = "employee_id",
  finishing_year = "finishing_year",
  id = "id",
  stage = "stage",
  starting_year = "starting_year",
  thesis = "thesis",
  university = "university",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "employee_functional_position_histories"
 */
export enum employee_functional_position_histories_constraint {
  idx_16802_primary = "idx_16802_primary",
}

/**
 * update columns of table "employee_functional_position_histories"
 */
export enum employee_functional_position_histories_update_column {
  created_at = "created_at",
  date_of_decree = "date_of_decree",
  decree_file = "decree_file",
  employee_id = "employee_id",
  functional_position_id = "functional_position_id",
  id = "id",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "employee_status"
 */
export enum employee_status_constraint {
  employee_status_pkey = "employee_status_pkey",
}

export enum employee_status_enum {
  Aktif = "Aktif",
  Keluar = "Keluar",
  Meninggal = "Meninggal",
  Studi = "Studi",
}

/**
 * update columns of table "employee_status"
 */
export enum employee_status_update_column {
  description = "description",
  status = "status",
}

/**
 * unique or primary key constraints on table "employee_type"
 */
export enum employee_type_constraint {
  employee_type_pkey = "employee_type_pkey",
}

export enum employee_type_enum {
  LECTURER = "LECTURER",
  STAFF = "STAFF",
}

/**
 * update columns of table "employee_type"
 */
export enum employee_type_update_column {
  type = "type",
}

/**
 * unique or primary key constraints on table "employees"
 */
export enum employees_constraint {
  employees_npp_key = "employees_npp_key",
  employees_pkey = "employees_pkey",
}

/**
 * update columns of table "employees"
 */
export enum employees_update_column {
  address = "address",
  base_credit = "base_credit",
  created_at = "created_at",
  date_of_birth = "date_of_birth",
  deleted_at = "deleted_at",
  department_id = "department_id",
  email = "email",
  employee_type = "employee_type",
  functional_position_id = "functional_position_id",
  group_id = "group_id",
  id = "id",
  initial = "initial",
  lecturer_certification_file = "lecturer_certification_file",
  lecturer_certification_number = "lecturer_certification_number",
  n_id_n = "n_id_n",
  name = "name",
  npp = "npp",
  phone = "phone",
  picture_file = "picture_file",
  place_of_birth = "place_of_birth",
  status = "status",
  structural_position_id = "structural_position_id",
  updated_at = "updated_at",
  user_id = "user_id",
}

/**
 * unique or primary key constraints on table "faculties"
 */
export enum faculties_constraint {
  faculties_pkey = "faculties_pkey",
}

/**
 * update columns of table "faculties"
 */
export enum faculties_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "functional_positions"
 */
export enum functional_positions_constraint {
  idx_16817_primary = "idx_16817_primary",
}

/**
 * update columns of table "functional_positions"
 */
export enum functional_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "groups"
 */
export enum groups_constraint {
  idx_16823_primary = "idx_16823_primary",
}

/**
 * update columns of table "groups"
 */
export enum groups_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "journal_publication_activity_positions"
 */
export enum journal_publication_activity_positions_constraint {
  journal_publication_activity_positions_pkey = "journal_publication_activity_positions_pkey",
}

/**
 * update columns of table "journal_publication_activity_positions"
 */
export enum journal_publication_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "journal_publication_categories"
 */
export enum journal_publication_categories_constraint {
  idx_16829_primary = "idx_16829_primary",
}

/**
 * update columns of table "journal_publication_categories"
 */
export enum journal_publication_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "journal_publication_members"
 */
export enum journal_publication_members_constraint {
  journal_publication_members_pkey = "journal_publication_members_pkey",
}

/**
 * update columns of table "journal_publication_members"
 */
export enum journal_publication_members_update_column {
  activity_position_id = "activity_position_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
  journal_publication_id = "journal_publication_id",
}

/**
 * unique or primary key constraints on table "journal_publications"
 */
export enum journal_publications_constraint {
  journal_publications_pkey = "journal_publications_pkey",
}

/**
 * update columns of table "journal_publications"
 */
export enum journal_publications_update_column {
  category_id = "category_id",
  certificate_file = "certificate_file",
  created_at = "created_at",
  credit_multiplier = "credit_multiplier",
  decree_file = "decree_file",
  deleted_at = "deleted_at",
  id = "id",
  isbn = "isbn",
  issn = "issn",
  journal_cover_file = "journal_cover_file",
  journal_name = "journal_name",
  journal_no = "journal_no",
  journal_page_no = "journal_page_no",
  journal_toc_file = "journal_toc_file",
  journal_volume = "journal_volume",
  journal_year = "journal_year",
  paper_file = "paper_file",
  title = "title",
  updated_at = "updated_at",
}

/**
 * column ordering options
 */
export enum order_by {
  asc = "asc",
  asc_nulls_first = "asc_nulls_first",
  asc_nulls_last = "asc_nulls_last",
  desc = "desc",
  desc_nulls_first = "desc_nulls_first",
  desc_nulls_last = "desc_nulls_last",
}

/**
 * unique or primary key constraints on table "patent_publication_activity_positions"
 */
export enum patent_publication_activity_positions_constraint {
  patent_publication_activity_positions_pkey = "patent_publication_activity_positions_pkey",
}

/**
 * update columns of table "patent_publication_activity_positions"
 */
export enum patent_publication_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "patent_publication_categories"
 */
export enum patent_publication_categories_constraint {
  idx_16859_primary = "idx_16859_primary",
}

/**
 * update columns of table "patent_publication_categories"
 */
export enum patent_publication_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "patent_publication_members"
 */
export enum patent_publication_members_constraint {
  patent_publication_members_pkey = "patent_publication_members_pkey",
}

/**
 * update columns of table "patent_publication_members"
 */
export enum patent_publication_members_update_column {
  activity_position_id = "activity_position_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
  patent_publication_id = "patent_publication_id",
}

/**
 * unique or primary key constraints on table "patent_publications"
 */
export enum patent_publications_constraint {
  patent_publications_pkey = "patent_publications_pkey",
}

/**
 * update columns of table "patent_publications"
 */
export enum patent_publications_update_column {
  category_id = "category_id",
  certificate_file = "certificate_file",
  certificate_no = "certificate_no",
  created_at = "created_at",
  credit_multiplier = "credit_multiplier",
  deleted_at = "deleted_at",
  first_announcement_date = "first_announcement_date",
  id = "id",
  title = "title",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "research_activity_positions"
 */
export enum research_activity_positions_constraint {
  research_activity_positions_pkey = "research_activity_positions_pkey",
}

/**
 * update columns of table "research_activity_positions"
 */
export enum research_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "research_categories"
 */
export enum research_categories_constraint {
  idx_16865_primary = "idx_16865_primary",
}

/**
 * update columns of table "research_categories"
 */
export enum research_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "research_members"
 */
export enum research_members_constraint {
  research_members_id_key = "research_members_id_key",
  research_members_pkey = "research_members_pkey",
}

/**
 * update columns of table "research_members"
 */
export enum research_members_update_column {
  activity_position_id = "activity_position_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
  research_id = "research_id",
}

/**
 * unique or primary key constraints on table "researches"
 */
export enum researches_constraint {
  idx_16874_primary = "idx_16874_primary",
}

/**
 * update columns of table "researches"
 */
export enum researches_update_column {
  category_id = "category_id",
  certificate_file = "certificate_file",
  created_at = "created_at",
  credit_multiplier = "credit_multiplier",
  decree_file = "decree_file",
  deleted_at = "deleted_at",
  final_report_file = "final_report_file",
  finished_at = "finished_at",
  fund_amount = "fund_amount",
  id = "id",
  proposal_file = "proposal_file",
  started_at = "started_at",
  title = "title",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "roles"
 */
export enum roles_constraint {
  idx_16883_primary = "idx_16883_primary",
}

/**
 * update columns of table "roles"
 */
export enum roles_update_column {
  id = "id",
  key = "key",
  name = "name",
}

/**
 * unique or primary key constraints on table "self_development_activity_positions"
 */
export enum self_development_activity_positions_constraint {
  self_development_activity_positions_pkey = "self_development_activity_positions_pkey",
}

/**
 * update columns of table "self_development_activity_positions"
 */
export enum self_development_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "self_development_categories"
 */
export enum self_development_categories_constraint {
  idx_16889_primary = "idx_16889_primary",
}

/**
 * update columns of table "self_development_categories"
 */
export enum self_development_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "self_development_level"
 */
export enum self_development_level_constraint {
  self_development_level_pkey = "self_development_level_pkey",
}

export enum self_development_level_enum {
  Internasional = "Internasional",
  Nasional = "Nasional",
  Regional = "Regional",
}

/**
 * update columns of table "self_development_level"
 */
export enum self_development_level_update_column {
  level = "level",
}

/**
 * unique or primary key constraints on table "self_development_members"
 */
export enum self_development_members_constraint {
  self_development_members_pkey = "self_development_members_pkey",
}

/**
 * update columns of table "self_development_members"
 */
export enum self_development_members_update_column {
  activity_position_id = "activity_position_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
  self_development_id = "self_development_id",
}

/**
 * unique or primary key constraints on table "self_developments"
 */
export enum self_developments_constraint {
  self_developments_pkey = "self_developments_pkey",
}

/**
 * update columns of table "self_developments"
 */
export enum self_developments_update_column {
  category_id = "category_id",
  certificate_file = "certificate_file",
  created_at = "created_at",
  decree_file = "decree_file",
  deleted_at = "deleted_at",
  end_date = "end_date",
  id = "id",
  level = "level",
  name = "name",
  start_date = "start_date",
  updated_at = "updated_at",
  venue = "venue",
}

/**
 * unique or primary key constraints on table "seminar_publication_activity_positions"
 */
export enum seminar_publication_activity_positions_constraint {
  seminar_publication_activity_positions_pkey = "seminar_publication_activity_positions_pkey",
}

/**
 * update columns of table "seminar_publication_activity_positions"
 */
export enum seminar_publication_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "seminar_publication_categories"
 */
export enum seminar_publication_categories_constraint {
  idx_16895_primary = "idx_16895_primary",
}

/**
 * update columns of table "seminar_publication_categories"
 */
export enum seminar_publication_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "seminar_publication_members"
 */
export enum seminar_publication_members_constraint {
  seminar_publication_members_pkey = "seminar_publication_members_pkey",
}

/**
 * update columns of table "seminar_publication_members"
 */
export enum seminar_publication_members_update_column {
  activity_position_id = "activity_position_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
  seminar_publication_id = "seminar_publication_id",
}

/**
 * unique or primary key constraints on table "seminar_publications"
 */
export enum seminar_publications_constraint {
  seminar_publications_pkey = "seminar_publications_pkey",
}

/**
 * update columns of table "seminar_publications"
 */
export enum seminar_publications_update_column {
  category_id = "category_id",
  certificate_file = "certificate_file",
  created_at = "created_at",
  credit_multiplier = "credit_multiplier",
  decree_file = "decree_file",
  deleted_at = "deleted_at",
  id = "id",
  isbn = "isbn",
  issn = "issn",
  paper_file = "paper_file",
  proceeding_cover_file = "proceeding_cover_file",
  proceeding_toc_file = "proceeding_toc_file",
  seminar_date = "seminar_date",
  seminar_name = "seminar_name",
  seminar_page_no = "seminar_page_no",
  seminar_venue = "seminar_venue",
  seminar_year = "seminar_year",
  title = "title",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "service_activity_positions"
 */
export enum service_activity_positions_constraint {
  service_activity_positions_pkey = "service_activity_positions_pkey",
}

/**
 * update columns of table "service_activity_positions"
 */
export enum service_activity_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "service_categories"
 */
export enum service_categories_constraint {
  idx_16901_primary = "idx_16901_primary",
}

/**
 * update columns of table "service_categories"
 */
export enum service_categories_update_column {
  created_at = "created_at",
  credit = "credit",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "service_members"
 */
export enum service_members_constraint {
  service_members_pkey = "service_members_pkey",
}

/**
 * update columns of table "service_members"
 */
export enum service_members_update_column {
  activity_position_id = "activity_position_id",
  credit_gain = "credit_gain",
  employee_id = "employee_id",
  external_author = "external_author",
  id = "id",
  service_id = "service_id",
}

/**
 * unique or primary key constraints on table "services"
 */
export enum services_constraint {
  services_pkey = "services_pkey",
}

/**
 * update columns of table "services"
 */
export enum services_update_column {
  certificate_file = "certificate_file",
  created_at = "created_at",
  credit_multiplier = "credit_multiplier",
  decree_file = "decree_file",
  deleted_at = "deleted_at",
  end_date = "end_date",
  final_report_file = "final_report_file",
  fund_amount = "fund_amount",
  id = "id",
  proposal_file = "proposal_file",
  service_category_id = "service_category_id",
  start_date = "start_date",
  title = "title",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "structural_positions"
 */
export enum structural_positions_constraint {
  idx_16907_primary = "idx_16907_primary",
}

/**
 * update columns of table "structural_positions"
 */
export enum structural_positions_update_column {
  created_at = "created_at",
  id = "id",
  name = "name",
  updated_at = "updated_at",
}

/**
 * unique or primary key constraints on table "users"
 */
export enum users_constraint {
  idx_16913_primary = "idx_16913_primary",
}

/**
 * update columns of table "users"
 */
export enum users_update_column {
  created_at = "created_at",
  id = "id",
  password = "password",
  role_id = "role_id",
  token = "token",
  updated_at = "updated_at",
  username = "username",
}

/**
 * expression to compare columns of type Int. All fields are combined with logical 'AND'.
 */
export interface Int_comparison_exp {
  _eq?: number | null;
  _gt?: number | null;
  _gte?: number | null;
  _in?: number[] | null;
  _is_null?: boolean | null;
  _lt?: number | null;
  _lte?: number | null;
  _neq?: number | null;
  _nin?: number[] | null;
}

/**
 * expression to compare columns of type String. All fields are combined with logical 'AND'.
 */
export interface String_comparison_exp {
  _eq?: string | null;
  _gt?: string | null;
  _gte?: string | null;
  _ilike?: string | null;
  _in?: string[] | null;
  _is_null?: boolean | null;
  _like?: string | null;
  _lt?: string | null;
  _lte?: string | null;
  _neq?: string | null;
  _nilike?: string | null;
  _nin?: string[] | null;
  _nlike?: string | null;
  _nsimilar?: string | null;
  _similar?: string | null;
}

/**
 * input type for inserting data into table "activity_positions"
 */
export interface activity_positions_insert_input {
  created_at?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * primary key columns input for table: "activity_positions"
 */
export interface activity_positions_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "activity_positions"
 */
export interface activity_positions_set_input {
  created_at?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * expression to compare columns of type bigint. All fields are combined with logical 'AND'.
 */
export interface bigint_comparison_exp {
  _eq?: any | null;
  _gt?: any | null;
  _gte?: any | null;
  _in?: any[] | null;
  _is_null?: boolean | null;
  _lt?: any | null;
  _lte?: any | null;
  _neq?: any | null;
  _nin?: any[] | null;
}

/**
 * Boolean expression to filter rows from the table "book_publication_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface book_publication_activity_positions_bool_exp {
  _and?: (book_publication_activity_positions_bool_exp | null)[] | null;
  _not?: book_publication_activity_positions_bool_exp | null;
  _or?: (book_publication_activity_positions_bool_exp | null)[] | null;
  book_publication_members?: book_publication_members_bool_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "book_publication_activity_positions"
 */
export interface book_publication_activity_positions_insert_input {
  book_publication_members?: book_publication_members_arr_rel_insert_input | null;
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "book_publication_activity_positions"
 */
export interface book_publication_activity_positions_obj_rel_insert_input {
  data: book_publication_activity_positions_insert_input;
  on_conflict?: book_publication_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "book_publication_activity_positions"
 */
export interface book_publication_activity_positions_on_conflict {
  constraint: book_publication_activity_positions_constraint;
  update_columns: book_publication_activity_positions_update_column[];
  where?: book_publication_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "book_publication_activity_positions"
 */
export interface book_publication_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "book_publication_activity_positions"
 */
export interface book_publication_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "book_publication_categories". All fields are combined with a logical 'AND'.
 */
export interface book_publication_categories_bool_exp {
  _and?: (book_publication_categories_bool_exp | null)[] | null;
  _not?: book_publication_categories_bool_exp | null;
  _or?: (book_publication_categories_bool_exp | null)[] | null;
  book_publications?: book_publications_bool_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "book_publication_categories"
 */
export interface book_publication_categories_insert_input {
  book_publications?: book_publications_arr_rel_insert_input | null;
  created_at?: any | null;
  credit?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "book_publication_categories"
 */
export interface book_publication_categories_obj_rel_insert_input {
  data: book_publication_categories_insert_input;
  on_conflict?: book_publication_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "book_publication_categories"
 */
export interface book_publication_categories_on_conflict {
  constraint: book_publication_categories_constraint;
  update_columns: book_publication_categories_update_column[];
  where?: book_publication_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "book_publication_categories"
 */
export interface book_publication_categories_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "book_publication_categories"
 */
export interface book_publication_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by aggregate values of table "book_publication_members"
 */
export interface book_publication_members_aggregate_order_by {
  avg?: book_publication_members_avg_order_by | null;
  count?: order_by | null;
  max?: book_publication_members_max_order_by | null;
  min?: book_publication_members_min_order_by | null;
  stddev?: book_publication_members_stddev_order_by | null;
  stddev_pop?: book_publication_members_stddev_pop_order_by | null;
  stddev_samp?: book_publication_members_stddev_samp_order_by | null;
  sum?: book_publication_members_sum_order_by | null;
  var_pop?: book_publication_members_var_pop_order_by | null;
  var_samp?: book_publication_members_var_samp_order_by | null;
  variance?: book_publication_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "book_publication_members"
 */
export interface book_publication_members_arr_rel_insert_input {
  data: book_publication_members_insert_input[];
  on_conflict?: book_publication_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "book_publication_members"
 */
export interface book_publication_members_avg_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "book_publication_members". All fields are combined with a logical 'AND'.
 */
export interface book_publication_members_bool_exp {
  _and?: (book_publication_members_bool_exp | null)[] | null;
  _not?: book_publication_members_bool_exp | null;
  _or?: (book_publication_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  book_publication?: book_publications_bool_exp | null;
  book_publication_activity_position?: book_publication_activity_positions_bool_exp | null;
  book_publication_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "book_publication_members"
 */
export interface book_publication_members_insert_input {
  activity_position_id?: any | null;
  book_publication?: book_publications_obj_rel_insert_input | null;
  book_publication_activity_position?: book_publication_activity_positions_obj_rel_insert_input | null;
  book_publication_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
}

/**
 * order by max() on columns of table "book_publication_members"
 */
export interface book_publication_members_max_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
}

/**
 * order by min() on columns of table "book_publication_members"
 */
export interface book_publication_members_min_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
}

/**
 * on conflict condition type for table "book_publication_members"
 */
export interface book_publication_members_on_conflict {
  constraint: book_publication_members_constraint;
  update_columns: book_publication_members_update_column[];
  where?: book_publication_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "book_publication_members"
 */
export interface book_publication_members_stddev_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "book_publication_members"
 */
export interface book_publication_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "book_publication_members"
 */
export interface book_publication_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by sum() on columns of table "book_publication_members"
 */
export interface book_publication_members_sum_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "book_publication_members"
 */
export interface book_publication_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "book_publication_members"
 */
export interface book_publication_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by variance() on columns of table "book_publication_members"
 */
export interface book_publication_members_variance_order_by {
  activity_position_id?: order_by | null;
  book_publication_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * input type for inserting array relation for remote table "book_publications"
 */
export interface book_publications_arr_rel_insert_input {
  data: book_publications_insert_input[];
  on_conflict?: book_publications_on_conflict | null;
}

/**
 * Boolean expression to filter rows from the table "book_publications". All fields are combined with a logical 'AND'.
 */
export interface book_publications_bool_exp {
  _and?: (book_publications_bool_exp | null)[] | null;
  _not?: book_publications_bool_exp | null;
  _or?: (book_publications_bool_exp | null)[] | null;
  book_file?: String_comparison_exp | null;
  book_publication_category?: book_publication_categories_bool_exp | null;
  book_publication_members?: book_publication_members_bool_exp | null;
  category_id?: Int_comparison_exp | null;
  city?: String_comparison_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  credit_multiplier?: numeric_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  publish_year?: Int_comparison_exp | null;
  publisher_name?: String_comparison_exp | null;
  title?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "book_publications"
 */
export interface book_publications_insert_input {
  book_file?: string | null;
  book_publication_category?: book_publication_categories_obj_rel_insert_input | null;
  book_publication_members?: book_publication_members_arr_rel_insert_input | null;
  category_id?: number | null;
  city?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  deleted_at?: any | null;
  id?: any | null;
  publish_year?: number | null;
  publisher_name?: string | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "book_publications"
 */
export interface book_publications_obj_rel_insert_input {
  data: book_publications_insert_input;
  on_conflict?: book_publications_on_conflict | null;
}

/**
 * on conflict condition type for table "book_publications"
 */
export interface book_publications_on_conflict {
  constraint: book_publications_constraint;
  update_columns: book_publications_update_column[];
  where?: book_publications_bool_exp | null;
}

/**
 * input type for updating data in table "book_publications"
 */
export interface book_publications_set_input {
  book_file?: string | null;
  category_id?: number | null;
  city?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  deleted_at?: any | null;
  id?: any | null;
  publish_year?: number | null;
  publisher_name?: string | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "committee_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface committee_activity_positions_bool_exp {
  _and?: (committee_activity_positions_bool_exp | null)[] | null;
  _not?: committee_activity_positions_bool_exp | null;
  _or?: (committee_activity_positions_bool_exp | null)[] | null;
  committee_members?: committee_members_bool_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "committee_activity_positions"
 */
export interface committee_activity_positions_insert_input {
  committee_members?: committee_members_arr_rel_insert_input | null;
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "committee_activity_positions"
 */
export interface committee_activity_positions_obj_rel_insert_input {
  data: committee_activity_positions_insert_input;
  on_conflict?: committee_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "committee_activity_positions"
 */
export interface committee_activity_positions_on_conflict {
  constraint: committee_activity_positions_constraint;
  update_columns: committee_activity_positions_update_column[];
  where?: committee_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "committee_activity_positions"
 */
export interface committee_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "committee_activity_positions"
 */
export interface committee_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "committee_categories". All fields are combined with a logical 'AND'.
 */
export interface committee_categories_bool_exp {
  _and?: (committee_categories_bool_exp | null)[] | null;
  _not?: committee_categories_bool_exp | null;
  _or?: (committee_categories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "committee_categories"
 */
export interface committee_categories_insert_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "committee_categories"
 */
export interface committee_categories_obj_rel_insert_input {
  data: committee_categories_insert_input;
  on_conflict?: committee_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "committee_categories"
 */
export interface committee_categories_on_conflict {
  constraint: committee_categories_constraint;
  update_columns: committee_categories_update_column[];
  where?: committee_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "committee_categories"
 */
export interface committee_categories_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "committee_categories"
 */
export interface committee_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "committee_level". All fields are combined with a logical 'AND'.
 */
export interface committee_level_bool_exp {
  _and?: (committee_level_bool_exp | null)[] | null;
  _not?: committee_level_bool_exp | null;
  _or?: (committee_level_bool_exp | null)[] | null;
  level?: String_comparison_exp | null;
}

/**
 * expression to compare columns of type committee_level_enum. All fields are combined with logical 'AND'.
 */
export interface committee_level_enum_comparison_exp {
  _eq?: committee_level_enum | null;
  _in?: committee_level_enum[] | null;
  _is_null?: boolean | null;
  _neq?: committee_level_enum | null;
  _nin?: committee_level_enum[] | null;
}

/**
 * input type for inserting data into table "committee_level"
 */
export interface committee_level_insert_input {
  level?: string | null;
}

/**
 * input type for inserting object relation for remote table "committee_level"
 */
export interface committee_level_obj_rel_insert_input {
  data: committee_level_insert_input;
  on_conflict?: committee_level_on_conflict | null;
}

/**
 * on conflict condition type for table "committee_level"
 */
export interface committee_level_on_conflict {
  constraint: committee_level_constraint;
  update_columns: committee_level_update_column[];
  where?: committee_level_bool_exp | null;
}

/**
 * order by aggregate values of table "committee_members"
 */
export interface committee_members_aggregate_order_by {
  avg?: committee_members_avg_order_by | null;
  count?: order_by | null;
  max?: committee_members_max_order_by | null;
  min?: committee_members_min_order_by | null;
  stddev?: committee_members_stddev_order_by | null;
  stddev_pop?: committee_members_stddev_pop_order_by | null;
  stddev_samp?: committee_members_stddev_samp_order_by | null;
  sum?: committee_members_sum_order_by | null;
  var_pop?: committee_members_var_pop_order_by | null;
  var_samp?: committee_members_var_samp_order_by | null;
  variance?: committee_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "committee_members"
 */
export interface committee_members_arr_rel_insert_input {
  data: committee_members_insert_input[];
  on_conflict?: committee_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "committee_members"
 */
export interface committee_members_avg_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "committee_members". All fields are combined with a logical 'AND'.
 */
export interface committee_members_bool_exp {
  _and?: (committee_members_bool_exp | null)[] | null;
  _not?: committee_members_bool_exp | null;
  _or?: (committee_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  committee?: committees_bool_exp | null;
  committee_activity_position?: committee_activity_positions_bool_exp | null;
  committee_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "committee_members"
 */
export interface committee_members_insert_input {
  activity_position_id?: any | null;
  committee?: committees_obj_rel_insert_input | null;
  committee_activity_position?: committee_activity_positions_obj_rel_insert_input | null;
  committee_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
}

/**
 * order by max() on columns of table "committee_members"
 */
export interface committee_members_max_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
}

/**
 * order by min() on columns of table "committee_members"
 */
export interface committee_members_min_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
}

/**
 * on conflict condition type for table "committee_members"
 */
export interface committee_members_on_conflict {
  constraint: committee_members_constraint;
  update_columns: committee_members_update_column[];
  where?: committee_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "committee_members"
 */
export interface committee_members_stddev_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "committee_members"
 */
export interface committee_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "committee_members"
 */
export interface committee_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by sum() on columns of table "committee_members"
 */
export interface committee_members_sum_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "committee_members"
 */
export interface committee_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "committee_members"
 */
export interface committee_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by variance() on columns of table "committee_members"
 */
export interface committee_members_variance_order_by {
  activity_position_id?: order_by | null;
  committee_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "committees". All fields are combined with a logical 'AND'.
 */
export interface committees_bool_exp {
  _and?: (committees_bool_exp | null)[] | null;
  _not?: committees_bool_exp | null;
  _or?: (committees_bool_exp | null)[] | null;
  category_id?: bigint_comparison_exp | null;
  certificate_file?: String_comparison_exp | null;
  committee_category?: committee_categories_bool_exp | null;
  committee_level?: committee_level_bool_exp | null;
  committee_members?: committee_members_bool_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  decree_file?: String_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  end_date?: date_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  level?: committee_level_enum_comparison_exp | null;
  name?: String_comparison_exp | null;
  start_date?: date_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
  venue?: String_comparison_exp | null;
}

/**
 * input type for inserting data into table "committees"
 */
export interface committees_insert_input {
  category_id?: any | null;
  certificate_file?: string | null;
  committee_category?: committee_categories_obj_rel_insert_input | null;
  committee_level?: committee_level_obj_rel_insert_input | null;
  committee_members?: committee_members_arr_rel_insert_input | null;
  created_at?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  end_date?: any | null;
  id?: any | null;
  level?: committee_level_enum | null;
  name?: string | null;
  start_date?: any | null;
  updated_at?: any | null;
  venue?: string | null;
}

/**
 * input type for inserting object relation for remote table "committees"
 */
export interface committees_obj_rel_insert_input {
  data: committees_insert_input;
  on_conflict?: committees_on_conflict | null;
}

/**
 * on conflict condition type for table "committees"
 */
export interface committees_on_conflict {
  constraint: committees_constraint;
  update_columns: committees_update_column[];
  where?: committees_bool_exp | null;
}

/**
 * input type for updating data in table "committees"
 */
export interface committees_set_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  end_date?: any | null;
  id?: any | null;
  level?: committee_level_enum | null;
  name?: string | null;
  start_date?: any | null;
  updated_at?: any | null;
  venue?: string | null;
}

/**
 * expression to compare columns of type date. All fields are combined with logical 'AND'.
 */
export interface date_comparison_exp {
  _eq?: any | null;
  _gt?: any | null;
  _gte?: any | null;
  _in?: any[] | null;
  _is_null?: boolean | null;
  _lt?: any | null;
  _lte?: any | null;
  _neq?: any | null;
  _nin?: any[] | null;
}

/**
 * order by aggregate values of table "departments"
 */
export interface departments_aggregate_order_by {
  avg?: departments_avg_order_by | null;
  count?: order_by | null;
  max?: departments_max_order_by | null;
  min?: departments_min_order_by | null;
  stddev?: departments_stddev_order_by | null;
  stddev_pop?: departments_stddev_pop_order_by | null;
  stddev_samp?: departments_stddev_samp_order_by | null;
  sum?: departments_sum_order_by | null;
  var_pop?: departments_var_pop_order_by | null;
  var_samp?: departments_var_samp_order_by | null;
  variance?: departments_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "departments"
 */
export interface departments_arr_rel_insert_input {
  data: departments_insert_input[];
  on_conflict?: departments_on_conflict | null;
}

/**
 * order by avg() on columns of table "departments"
 */
export interface departments_avg_order_by {
  faculty_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "departments". All fields are combined with a logical 'AND'.
 */
export interface departments_bool_exp {
  _and?: (departments_bool_exp | null)[] | null;
  _not?: departments_bool_exp | null;
  _or?: (departments_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  employees?: employees_bool_exp | null;
  faculty?: faculties_bool_exp | null;
  faculty_id?: Int_comparison_exp | null;
  id?: String_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "departments"
 */
export interface departments_insert_input {
  created_at?: any | null;
  employees?: employees_arr_rel_insert_input | null;
  faculty?: faculties_obj_rel_insert_input | null;
  faculty_id?: number | null;
  id?: string | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by max() on columns of table "departments"
 */
export interface departments_max_order_by {
  created_at?: order_by | null;
  faculty_id?: order_by | null;
  id?: order_by | null;
  name?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * order by min() on columns of table "departments"
 */
export interface departments_min_order_by {
  created_at?: order_by | null;
  faculty_id?: order_by | null;
  id?: order_by | null;
  name?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * input type for inserting object relation for remote table "departments"
 */
export interface departments_obj_rel_insert_input {
  data: departments_insert_input;
  on_conflict?: departments_on_conflict | null;
}

/**
 * on conflict condition type for table "departments"
 */
export interface departments_on_conflict {
  constraint: departments_constraint;
  update_columns: departments_update_column[];
  where?: departments_bool_exp | null;
}

/**
 * ordering options when selecting data from "departments"
 */
export interface departments_order_by {
  created_at?: order_by | null;
  employees_aggregate?: employees_aggregate_order_by | null;
  faculty?: faculties_order_by | null;
  faculty_id?: order_by | null;
  id?: order_by | null;
  name?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * primary key columns input for table: "departments"
 */
export interface departments_pk_columns_input {
  id: string;
}

/**
 * input type for updating data in table "departments"
 */
export interface departments_set_input {
  created_at?: any | null;
  faculty_id?: number | null;
  id?: string | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by stddev() on columns of table "departments"
 */
export interface departments_stddev_order_by {
  faculty_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "departments"
 */
export interface departments_stddev_pop_order_by {
  faculty_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "departments"
 */
export interface departments_stddev_samp_order_by {
  faculty_id?: order_by | null;
}

/**
 * order by sum() on columns of table "departments"
 */
export interface departments_sum_order_by {
  faculty_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "departments"
 */
export interface departments_var_pop_order_by {
  faculty_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "departments"
 */
export interface departments_var_samp_order_by {
  faculty_id?: order_by | null;
}

/**
 * order by variance() on columns of table "departments"
 */
export interface departments_variance_order_by {
  faculty_id?: order_by | null;
}

/**
 * order by aggregate values of table "employee_education_histories"
 */
export interface employee_education_histories_aggregate_order_by {
  avg?: employee_education_histories_avg_order_by | null;
  count?: order_by | null;
  max?: employee_education_histories_max_order_by | null;
  min?: employee_education_histories_min_order_by | null;
  stddev?: employee_education_histories_stddev_order_by | null;
  stddev_pop?: employee_education_histories_stddev_pop_order_by | null;
  stddev_samp?: employee_education_histories_stddev_samp_order_by | null;
  sum?: employee_education_histories_sum_order_by | null;
  var_pop?: employee_education_histories_var_pop_order_by | null;
  var_samp?: employee_education_histories_var_samp_order_by | null;
  variance?: employee_education_histories_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "employee_education_histories"
 */
export interface employee_education_histories_arr_rel_insert_input {
  data: employee_education_histories_insert_input[];
  on_conflict?: employee_education_histories_on_conflict | null;
}

/**
 * order by avg() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_avg_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "employee_education_histories". All fields are combined with a logical 'AND'.
 */
export interface employee_education_histories_bool_exp {
  _and?: (employee_education_histories_bool_exp | null)[] | null;
  _not?: employee_education_histories_bool_exp | null;
  _or?: (employee_education_histories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  department?: String_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  finishing_year?: Int_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  stage?: String_comparison_exp | null;
  starting_year?: Int_comparison_exp | null;
  thesis?: String_comparison_exp | null;
  university?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "employee_education_histories"
 */
export interface employee_education_histories_insert_input {
  created_at?: any | null;
  department?: string | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  finishing_year?: number | null;
  id?: any | null;
  stage?: string | null;
  starting_year?: number | null;
  thesis?: string | null;
  university?: string | null;
  updated_at?: any | null;
}

/**
 * order by max() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_max_order_by {
  created_at?: order_by | null;
  department?: order_by | null;
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  stage?: order_by | null;
  starting_year?: order_by | null;
  thesis?: order_by | null;
  university?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * order by min() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_min_order_by {
  created_at?: order_by | null;
  department?: order_by | null;
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  stage?: order_by | null;
  starting_year?: order_by | null;
  thesis?: order_by | null;
  university?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * on conflict condition type for table "employee_education_histories"
 */
export interface employee_education_histories_on_conflict {
  constraint: employee_education_histories_constraint;
  update_columns: employee_education_histories_update_column[];
  where?: employee_education_histories_bool_exp | null;
}

/**
 * order by stddev() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_stddev_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_stddev_pop_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_stddev_samp_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * order by sum() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_sum_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * order by var_pop() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_var_pop_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * order by var_samp() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_var_samp_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * order by variance() on columns of table "employee_education_histories"
 */
export interface employee_education_histories_variance_order_by {
  employee_id?: order_by | null;
  finishing_year?: order_by | null;
  id?: order_by | null;
  starting_year?: order_by | null;
}

/**
 * order by aggregate values of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_aggregate_order_by {
  avg?: employee_functional_position_histories_avg_order_by | null;
  count?: order_by | null;
  max?: employee_functional_position_histories_max_order_by | null;
  min?: employee_functional_position_histories_min_order_by | null;
  stddev?: employee_functional_position_histories_stddev_order_by | null;
  stddev_pop?: employee_functional_position_histories_stddev_pop_order_by | null;
  stddev_samp?: employee_functional_position_histories_stddev_samp_order_by | null;
  sum?: employee_functional_position_histories_sum_order_by | null;
  var_pop?: employee_functional_position_histories_var_pop_order_by | null;
  var_samp?: employee_functional_position_histories_var_samp_order_by | null;
  variance?: employee_functional_position_histories_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_arr_rel_insert_input {
  data: employee_functional_position_histories_insert_input[];
  on_conflict?: employee_functional_position_histories_on_conflict | null;
}

/**
 * order by avg() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_avg_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "employee_functional_position_histories". All fields are combined with a logical 'AND'.
 */
export interface employee_functional_position_histories_bool_exp {
  _and?: (employee_functional_position_histories_bool_exp | null)[] | null;
  _not?: employee_functional_position_histories_bool_exp | null;
  _or?: (employee_functional_position_histories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  date_of_decree?: timestamptz_comparison_exp | null;
  decree_file?: String_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  functional_position?: functional_positions_bool_exp | null;
  functional_position_id?: bigint_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_insert_input {
  created_at?: any | null;
  date_of_decree?: any | null;
  decree_file?: string | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  functional_position?: functional_positions_obj_rel_insert_input | null;
  functional_position_id?: any | null;
  id?: any | null;
  updated_at?: any | null;
}

/**
 * order by max() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_max_order_by {
  created_at?: order_by | null;
  date_of_decree?: order_by | null;
  decree_file?: order_by | null;
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * order by min() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_min_order_by {
  created_at?: order_by | null;
  date_of_decree?: order_by | null;
  decree_file?: order_by | null;
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * on conflict condition type for table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_on_conflict {
  constraint: employee_functional_position_histories_constraint;
  update_columns: employee_functional_position_histories_update_column[];
  where?: employee_functional_position_histories_bool_exp | null;
}

/**
 * order by stddev() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_stddev_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_stddev_pop_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_stddev_samp_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by sum() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_sum_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_var_pop_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_var_samp_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * order by variance() on columns of table "employee_functional_position_histories"
 */
export interface employee_functional_position_histories_variance_order_by {
  employee_id?: order_by | null;
  functional_position_id?: order_by | null;
  id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "employee_status". All fields are combined with a logical 'AND'.
 */
export interface employee_status_bool_exp {
  _and?: (employee_status_bool_exp | null)[] | null;
  _not?: employee_status_bool_exp | null;
  _or?: (employee_status_bool_exp | null)[] | null;
  description?: String_comparison_exp | null;
  status?: String_comparison_exp | null;
}

/**
 * expression to compare columns of type employee_status_enum. All fields are combined with logical 'AND'.
 */
export interface employee_status_enum_comparison_exp {
  _eq?: employee_status_enum | null;
  _in?: employee_status_enum[] | null;
  _is_null?: boolean | null;
  _neq?: employee_status_enum | null;
  _nin?: employee_status_enum[] | null;
}

/**
 * input type for inserting data into table "employee_status"
 */
export interface employee_status_insert_input {
  description?: string | null;
  status?: string | null;
}

/**
 * input type for inserting object relation for remote table "employee_status"
 */
export interface employee_status_obj_rel_insert_input {
  data: employee_status_insert_input;
  on_conflict?: employee_status_on_conflict | null;
}

/**
 * on conflict condition type for table "employee_status"
 */
export interface employee_status_on_conflict {
  constraint: employee_status_constraint;
  update_columns: employee_status_update_column[];
  where?: employee_status_bool_exp | null;
}

/**
 * ordering options when selecting data from "employee_status"
 */
export interface employee_status_order_by {
  description?: order_by | null;
  status?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "employee_type". All fields are combined with a logical 'AND'.
 */
export interface employee_type_bool_exp {
  _and?: (employee_type_bool_exp | null)[] | null;
  _not?: employee_type_bool_exp | null;
  _or?: (employee_type_bool_exp | null)[] | null;
  employees?: employees_bool_exp | null;
  type?: String_comparison_exp | null;
}

/**
 * expression to compare columns of type employee_type_enum. All fields are combined with logical 'AND'.
 */
export interface employee_type_enum_comparison_exp {
  _eq?: employee_type_enum | null;
  _in?: employee_type_enum[] | null;
  _is_null?: boolean | null;
  _neq?: employee_type_enum | null;
  _nin?: employee_type_enum[] | null;
}

/**
 * input type for inserting data into table "employee_type"
 */
export interface employee_type_insert_input {
  employees?: employees_arr_rel_insert_input | null;
  type?: string | null;
}

/**
 * input type for inserting object relation for remote table "employee_type"
 */
export interface employee_type_obj_rel_insert_input {
  data: employee_type_insert_input;
  on_conflict?: employee_type_on_conflict | null;
}

/**
 * on conflict condition type for table "employee_type"
 */
export interface employee_type_on_conflict {
  constraint: employee_type_constraint;
  update_columns: employee_type_update_column[];
  where?: employee_type_bool_exp | null;
}

/**
 * ordering options when selecting data from "employee_type"
 */
export interface employee_type_order_by {
  employees_aggregate?: employees_aggregate_order_by | null;
  type?: order_by | null;
}

/**
 * order by aggregate values of table "employees"
 */
export interface employees_aggregate_order_by {
  avg?: employees_avg_order_by | null;
  count?: order_by | null;
  max?: employees_max_order_by | null;
  min?: employees_min_order_by | null;
  stddev?: employees_stddev_order_by | null;
  stddev_pop?: employees_stddev_pop_order_by | null;
  stddev_samp?: employees_stddev_samp_order_by | null;
  sum?: employees_sum_order_by | null;
  var_pop?: employees_var_pop_order_by | null;
  var_samp?: employees_var_samp_order_by | null;
  variance?: employees_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "employees"
 */
export interface employees_arr_rel_insert_input {
  data: employees_insert_input[];
  on_conflict?: employees_on_conflict | null;
}

/**
 * order by avg() on columns of table "employees"
 */
export interface employees_avg_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "employees". All fields are combined with a logical 'AND'.
 */
export interface employees_bool_exp {
  _and?: (employees_bool_exp | null)[] | null;
  _not?: employees_bool_exp | null;
  _or?: (employees_bool_exp | null)[] | null;
  address?: String_comparison_exp | null;
  base_credit?: Int_comparison_exp | null;
  book_publication_members?: book_publication_members_bool_exp | null;
  committee_members?: committee_members_bool_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  date_of_birth?: date_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  department?: departments_bool_exp | null;
  department_id?: String_comparison_exp | null;
  email?: String_comparison_exp | null;
  employeeStatus?: employee_status_bool_exp | null;
  employeeType?: employee_type_bool_exp | null;
  employee_education_histories?: employee_education_histories_bool_exp | null;
  employee_functional_position_histories?: employee_functional_position_histories_bool_exp | null;
  employee_type?: employee_type_enum_comparison_exp | null;
  functional_position?: functional_positions_bool_exp | null;
  functional_position_id?: bigint_comparison_exp | null;
  group?: groups_bool_exp | null;
  group_id?: bigint_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  initial?: String_comparison_exp | null;
  journal_publication_members?: journal_publication_members_bool_exp | null;
  lecturer_certification_file?: String_comparison_exp | null;
  lecturer_certification_number?: String_comparison_exp | null;
  n_id_n?: String_comparison_exp | null;
  name?: String_comparison_exp | null;
  npp?: String_comparison_exp | null;
  patent_publication_members?: patent_publication_members_bool_exp | null;
  phone?: String_comparison_exp | null;
  picture_file?: String_comparison_exp | null;
  place_of_birth?: String_comparison_exp | null;
  research_members?: research_members_bool_exp | null;
  self_development_members?: self_development_members_bool_exp | null;
  seminar_publication_members?: seminar_publication_members_bool_exp | null;
  service_members?: service_members_bool_exp | null;
  status?: employee_status_enum_comparison_exp | null;
  structural_position?: structural_positions_bool_exp | null;
  structural_position_id?: bigint_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
  user?: users_bool_exp | null;
  user_id?: bigint_comparison_exp | null;
}

/**
 * Boolean expression to filter rows from the table "employees_gains". All fields are combined with a logical 'AND'.
 */
export interface employees_gains_bool_exp {
  _and?: (employees_gains_bool_exp | null)[] | null;
  _not?: employees_gains_bool_exp | null;
  _or?: (employees_gains_bool_exp | null)[] | null;
  all_gains?: numeric_comparison_exp | null;
  book_publication_gains?: numeric_comparison_exp | null;
  committee_gains?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  id?: bigint_comparison_exp | null;
  journal_publication_gains?: numeric_comparison_exp | null;
  name?: String_comparison_exp | null;
  patent_publication_gains?: numeric_comparison_exp | null;
  research_gains?: numeric_comparison_exp | null;
  self_development_gains?: numeric_comparison_exp | null;
  seminar_publication_gains?: numeric_comparison_exp | null;
  service_gains?: numeric_comparison_exp | null;
}

/**
 * ordering options when selecting data from "employees_gains"
 */
export interface employees_gains_order_by {
  all_gains?: order_by | null;
  book_publication_gains?: order_by | null;
  committee_gains?: order_by | null;
  employee?: employees_order_by | null;
  id?: order_by | null;
  journal_publication_gains?: order_by | null;
  name?: order_by | null;
  patent_publication_gains?: order_by | null;
  research_gains?: order_by | null;
  self_development_gains?: order_by | null;
  seminar_publication_gains?: order_by | null;
  service_gains?: order_by | null;
}

/**
 * input type for inserting data into table "employees"
 */
export interface employees_insert_input {
  address?: string | null;
  base_credit?: number | null;
  book_publication_members?: book_publication_members_arr_rel_insert_input | null;
  committee_members?: committee_members_arr_rel_insert_input | null;
  created_at?: any | null;
  date_of_birth?: any | null;
  deleted_at?: any | null;
  department?: departments_obj_rel_insert_input | null;
  department_id?: string | null;
  email?: string | null;
  employeeStatus?: employee_status_obj_rel_insert_input | null;
  employeeType?: employee_type_obj_rel_insert_input | null;
  employee_education_histories?: employee_education_histories_arr_rel_insert_input | null;
  employee_functional_position_histories?: employee_functional_position_histories_arr_rel_insert_input | null;
  employee_type?: employee_type_enum | null;
  functional_position?: functional_positions_obj_rel_insert_input | null;
  functional_position_id?: any | null;
  group?: groups_obj_rel_insert_input | null;
  group_id?: any | null;
  id?: any | null;
  initial?: string | null;
  journal_publication_members?: journal_publication_members_arr_rel_insert_input | null;
  lecturer_certification_file?: string | null;
  lecturer_certification_number?: string | null;
  n_id_n?: string | null;
  name?: string | null;
  npp?: string | null;
  patent_publication_members?: patent_publication_members_arr_rel_insert_input | null;
  phone?: string | null;
  picture_file?: string | null;
  place_of_birth?: string | null;
  research_members?: research_members_arr_rel_insert_input | null;
  self_development_members?: self_development_members_arr_rel_insert_input | null;
  seminar_publication_members?: seminar_publication_members_arr_rel_insert_input | null;
  service_members?: service_members_arr_rel_insert_input | null;
  status?: employee_status_enum | null;
  structural_position?: structural_positions_obj_rel_insert_input | null;
  structural_position_id?: any | null;
  updated_at?: any | null;
  user?: users_obj_rel_insert_input | null;
  user_id?: any | null;
}

/**
 * order by max() on columns of table "employees"
 */
export interface employees_max_order_by {
  address?: order_by | null;
  base_credit?: order_by | null;
  created_at?: order_by | null;
  date_of_birth?: order_by | null;
  deleted_at?: order_by | null;
  department_id?: order_by | null;
  email?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  initial?: order_by | null;
  lecturer_certification_file?: order_by | null;
  lecturer_certification_number?: order_by | null;
  n_id_n?: order_by | null;
  name?: order_by | null;
  npp?: order_by | null;
  phone?: order_by | null;
  picture_file?: order_by | null;
  place_of_birth?: order_by | null;
  structural_position_id?: order_by | null;
  updated_at?: order_by | null;
  user_id?: order_by | null;
}

/**
 * order by min() on columns of table "employees"
 */
export interface employees_min_order_by {
  address?: order_by | null;
  base_credit?: order_by | null;
  created_at?: order_by | null;
  date_of_birth?: order_by | null;
  deleted_at?: order_by | null;
  department_id?: order_by | null;
  email?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  initial?: order_by | null;
  lecturer_certification_file?: order_by | null;
  lecturer_certification_number?: order_by | null;
  n_id_n?: order_by | null;
  name?: order_by | null;
  npp?: order_by | null;
  phone?: order_by | null;
  picture_file?: order_by | null;
  place_of_birth?: order_by | null;
  structural_position_id?: order_by | null;
  updated_at?: order_by | null;
  user_id?: order_by | null;
}

/**
 * input type for inserting object relation for remote table "employees"
 */
export interface employees_obj_rel_insert_input {
  data: employees_insert_input;
  on_conflict?: employees_on_conflict | null;
}

/**
 * on conflict condition type for table "employees"
 */
export interface employees_on_conflict {
  constraint: employees_constraint;
  update_columns: employees_update_column[];
  where?: employees_bool_exp | null;
}

/**
 * ordering options when selecting data from "employees"
 */
export interface employees_order_by {
  address?: order_by | null;
  base_credit?: order_by | null;
  book_publication_members_aggregate?: book_publication_members_aggregate_order_by | null;
  committee_members_aggregate?: committee_members_aggregate_order_by | null;
  created_at?: order_by | null;
  date_of_birth?: order_by | null;
  deleted_at?: order_by | null;
  department?: departments_order_by | null;
  department_id?: order_by | null;
  email?: order_by | null;
  employeeStatus?: employee_status_order_by | null;
  employeeType?: employee_type_order_by | null;
  employee_education_histories_aggregate?: employee_education_histories_aggregate_order_by | null;
  employee_functional_position_histories_aggregate?: employee_functional_position_histories_aggregate_order_by | null;
  employee_type?: order_by | null;
  functional_position?: functional_positions_order_by | null;
  functional_position_id?: order_by | null;
  group?: groups_order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  initial?: order_by | null;
  journal_publication_members_aggregate?: journal_publication_members_aggregate_order_by | null;
  lecturer_certification_file?: order_by | null;
  lecturer_certification_number?: order_by | null;
  n_id_n?: order_by | null;
  name?: order_by | null;
  npp?: order_by | null;
  patent_publication_members_aggregate?: patent_publication_members_aggregate_order_by | null;
  phone?: order_by | null;
  picture_file?: order_by | null;
  place_of_birth?: order_by | null;
  research_members_aggregate?: research_members_aggregate_order_by | null;
  self_development_members_aggregate?: self_development_members_aggregate_order_by | null;
  seminar_publication_members_aggregate?: seminar_publication_members_aggregate_order_by | null;
  service_members_aggregate?: service_members_aggregate_order_by | null;
  status?: order_by | null;
  structural_position?: structural_positions_order_by | null;
  structural_position_id?: order_by | null;
  updated_at?: order_by | null;
  user?: users_order_by | null;
  user_id?: order_by | null;
}

/**
 * primary key columns input for table: "employees"
 */
export interface employees_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "employees"
 */
export interface employees_set_input {
  address?: string | null;
  base_credit?: number | null;
  created_at?: any | null;
  date_of_birth?: any | null;
  deleted_at?: any | null;
  department_id?: string | null;
  email?: string | null;
  employee_type?: employee_type_enum | null;
  functional_position_id?: any | null;
  group_id?: any | null;
  id?: any | null;
  initial?: string | null;
  lecturer_certification_file?: string | null;
  lecturer_certification_number?: string | null;
  n_id_n?: string | null;
  name?: string | null;
  npp?: string | null;
  phone?: string | null;
  picture_file?: string | null;
  place_of_birth?: string | null;
  status?: employee_status_enum | null;
  structural_position_id?: any | null;
  updated_at?: any | null;
  user_id?: any | null;
}

/**
 * order by stddev() on columns of table "employees"
 */
export interface employees_stddev_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "employees"
 */
export interface employees_stddev_pop_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "employees"
 */
export interface employees_stddev_samp_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * order by sum() on columns of table "employees"
 */
export interface employees_sum_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "employees"
 */
export interface employees_var_pop_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "employees"
 */
export interface employees_var_samp_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * order by variance() on columns of table "employees"
 */
export interface employees_variance_order_by {
  base_credit?: order_by | null;
  functional_position_id?: order_by | null;
  group_id?: order_by | null;
  id?: order_by | null;
  structural_position_id?: order_by | null;
  user_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "faculties". All fields are combined with a logical 'AND'.
 */
export interface faculties_bool_exp {
  _and?: (faculties_bool_exp | null)[] | null;
  _not?: faculties_bool_exp | null;
  _or?: (faculties_bool_exp | null)[] | null;
  created_at?: timestamp_comparison_exp | null;
  departments?: departments_bool_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "faculties"
 */
export interface faculties_insert_input {
  created_at?: any | null;
  departments?: departments_arr_rel_insert_input | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "faculties"
 */
export interface faculties_obj_rel_insert_input {
  data: faculties_insert_input;
  on_conflict?: faculties_on_conflict | null;
}

/**
 * on conflict condition type for table "faculties"
 */
export interface faculties_on_conflict {
  constraint: faculties_constraint;
  update_columns: faculties_update_column[];
  where?: faculties_bool_exp | null;
}

/**
 * ordering options when selecting data from "faculties"
 */
export interface faculties_order_by {
  created_at?: order_by | null;
  departments_aggregate?: departments_aggregate_order_by | null;
  id?: order_by | null;
  name?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * primary key columns input for table: "faculties"
 */
export interface faculties_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "faculties"
 */
export interface faculties_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "functional_positions". All fields are combined with a logical 'AND'.
 */
export interface functional_positions_bool_exp {
  _and?: (functional_positions_bool_exp | null)[] | null;
  _not?: functional_positions_bool_exp | null;
  _or?: (functional_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  employee_functional_position_histories?: employee_functional_position_histories_bool_exp | null;
  employees?: employees_bool_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "functional_positions"
 */
export interface functional_positions_insert_input {
  created_at?: any | null;
  employee_functional_position_histories?: employee_functional_position_histories_arr_rel_insert_input | null;
  employees?: employees_arr_rel_insert_input | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "functional_positions"
 */
export interface functional_positions_obj_rel_insert_input {
  data: functional_positions_insert_input;
  on_conflict?: functional_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "functional_positions"
 */
export interface functional_positions_on_conflict {
  constraint: functional_positions_constraint;
  update_columns: functional_positions_update_column[];
  where?: functional_positions_bool_exp | null;
}

/**
 * ordering options when selecting data from "functional_positions"
 */
export interface functional_positions_order_by {
  created_at?: order_by | null;
  employee_functional_position_histories_aggregate?: employee_functional_position_histories_aggregate_order_by | null;
  employees_aggregate?: employees_aggregate_order_by | null;
  id?: order_by | null;
  name?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * primary key columns input for table: "functional_positions"
 */
export interface functional_positions_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "functional_positions"
 */
export interface functional_positions_set_input {
  created_at?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "groups". All fields are combined with a logical 'AND'.
 */
export interface groups_bool_exp {
  _and?: (groups_bool_exp | null)[] | null;
  _not?: groups_bool_exp | null;
  _or?: (groups_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  employees?: employees_bool_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "groups"
 */
export interface groups_insert_input {
  created_at?: any | null;
  employees?: employees_arr_rel_insert_input | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "groups"
 */
export interface groups_obj_rel_insert_input {
  data: groups_insert_input;
  on_conflict?: groups_on_conflict | null;
}

/**
 * on conflict condition type for table "groups"
 */
export interface groups_on_conflict {
  constraint: groups_constraint;
  update_columns: groups_update_column[];
  where?: groups_bool_exp | null;
}

/**
 * ordering options when selecting data from "groups"
 */
export interface groups_order_by {
  created_at?: order_by | null;
  employees_aggregate?: employees_aggregate_order_by | null;
  id?: order_by | null;
  name?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * primary key columns input for table: "groups"
 */
export interface groups_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "groups"
 */
export interface groups_set_input {
  created_at?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "journal_publication_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface journal_publication_activity_positions_bool_exp {
  _and?: (journal_publication_activity_positions_bool_exp | null)[] | null;
  _not?: journal_publication_activity_positions_bool_exp | null;
  _or?: (journal_publication_activity_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  journal_publication_members?: journal_publication_members_bool_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "journal_publication_activity_positions"
 */
export interface journal_publication_activity_positions_insert_input {
  created_at?: any | null;
  id?: number | null;
  journal_publication_members?: journal_publication_members_arr_rel_insert_input | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "journal_publication_activity_positions"
 */
export interface journal_publication_activity_positions_obj_rel_insert_input {
  data: journal_publication_activity_positions_insert_input;
  on_conflict?: journal_publication_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "journal_publication_activity_positions"
 */
export interface journal_publication_activity_positions_on_conflict {
  constraint: journal_publication_activity_positions_constraint;
  update_columns: journal_publication_activity_positions_update_column[];
  where?: journal_publication_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "journal_publication_activity_positions"
 */
export interface journal_publication_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "journal_publication_activity_positions"
 */
export interface journal_publication_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "journal_publication_categories". All fields are combined with a logical 'AND'.
 */
export interface journal_publication_categories_bool_exp {
  _and?: (journal_publication_categories_bool_exp | null)[] | null;
  _not?: journal_publication_categories_bool_exp | null;
  _or?: (journal_publication_categories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "journal_publication_categories"
 */
export interface journal_publication_categories_insert_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "journal_publication_categories"
 */
export interface journal_publication_categories_obj_rel_insert_input {
  data: journal_publication_categories_insert_input;
  on_conflict?: journal_publication_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "journal_publication_categories"
 */
export interface journal_publication_categories_on_conflict {
  constraint: journal_publication_categories_constraint;
  update_columns: journal_publication_categories_update_column[];
  where?: journal_publication_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "journal_publication_categories"
 */
export interface journal_publication_categories_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "journal_publication_categories"
 */
export interface journal_publication_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by aggregate values of table "journal_publication_members"
 */
export interface journal_publication_members_aggregate_order_by {
  avg?: journal_publication_members_avg_order_by | null;
  count?: order_by | null;
  max?: journal_publication_members_max_order_by | null;
  min?: journal_publication_members_min_order_by | null;
  stddev?: journal_publication_members_stddev_order_by | null;
  stddev_pop?: journal_publication_members_stddev_pop_order_by | null;
  stddev_samp?: journal_publication_members_stddev_samp_order_by | null;
  sum?: journal_publication_members_sum_order_by | null;
  var_pop?: journal_publication_members_var_pop_order_by | null;
  var_samp?: journal_publication_members_var_samp_order_by | null;
  variance?: journal_publication_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "journal_publication_members"
 */
export interface journal_publication_members_arr_rel_insert_input {
  data: journal_publication_members_insert_input[];
  on_conflict?: journal_publication_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_avg_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "journal_publication_members". All fields are combined with a logical 'AND'.
 */
export interface journal_publication_members_bool_exp {
  _and?: (journal_publication_members_bool_exp | null)[] | null;
  _not?: journal_publication_members_bool_exp | null;
  _or?: (journal_publication_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  journal_publication?: journal_publications_bool_exp | null;
  journal_publication_activity_position?: journal_publication_activity_positions_bool_exp | null;
  journal_publication_id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "journal_publication_members"
 */
export interface journal_publication_members_insert_input {
  activity_position_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
  journal_publication?: journal_publications_obj_rel_insert_input | null;
  journal_publication_activity_position?: journal_publication_activity_positions_obj_rel_insert_input | null;
  journal_publication_id?: any | null;
}

/**
 * order by max() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_max_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * order by min() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_min_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * on conflict condition type for table "journal_publication_members"
 */
export interface journal_publication_members_on_conflict {
  constraint: journal_publication_members_constraint;
  update_columns: journal_publication_members_update_column[];
  where?: journal_publication_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_stddev_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * order by sum() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_sum_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * order by variance() on columns of table "journal_publication_members"
 */
export interface journal_publication_members_variance_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  journal_publication_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "journal_publications". All fields are combined with a logical 'AND'.
 */
export interface journal_publications_bool_exp {
  _and?: (journal_publications_bool_exp | null)[] | null;
  _not?: journal_publications_bool_exp | null;
  _or?: (journal_publications_bool_exp | null)[] | null;
  category_id?: bigint_comparison_exp | null;
  certificate_file?: String_comparison_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  credit_multiplier?: numeric_comparison_exp | null;
  decree_file?: String_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  isbn?: String_comparison_exp | null;
  issn?: String_comparison_exp | null;
  journal_cover_file?: String_comparison_exp | null;
  journal_name?: String_comparison_exp | null;
  journal_no?: String_comparison_exp | null;
  journal_page_no?: String_comparison_exp | null;
  journal_publication_category?: journal_publication_categories_bool_exp | null;
  journal_publication_members?: journal_publication_members_bool_exp | null;
  journal_toc_file?: String_comparison_exp | null;
  journal_volume?: String_comparison_exp | null;
  journal_year?: Int_comparison_exp | null;
  paper_file?: String_comparison_exp | null;
  title?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "journal_publications"
 */
export interface journal_publications_insert_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  id?: any | null;
  isbn?: string | null;
  issn?: string | null;
  journal_cover_file?: string | null;
  journal_name?: string | null;
  journal_no?: string | null;
  journal_page_no?: string | null;
  journal_publication_category?: journal_publication_categories_obj_rel_insert_input | null;
  journal_publication_members?: journal_publication_members_arr_rel_insert_input | null;
  journal_toc_file?: string | null;
  journal_volume?: string | null;
  journal_year?: number | null;
  paper_file?: string | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "journal_publications"
 */
export interface journal_publications_obj_rel_insert_input {
  data: journal_publications_insert_input;
  on_conflict?: journal_publications_on_conflict | null;
}

/**
 * on conflict condition type for table "journal_publications"
 */
export interface journal_publications_on_conflict {
  constraint: journal_publications_constraint;
  update_columns: journal_publications_update_column[];
  where?: journal_publications_bool_exp | null;
}

/**
 * input type for updating data in table "journal_publications"
 */
export interface journal_publications_set_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  id?: any | null;
  isbn?: string | null;
  issn?: string | null;
  journal_cover_file?: string | null;
  journal_name?: string | null;
  journal_no?: string | null;
  journal_page_no?: string | null;
  journal_toc_file?: string | null;
  journal_volume?: string | null;
  journal_year?: number | null;
  paper_file?: string | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * expression to compare columns of type numeric. All fields are combined with logical 'AND'.
 */
export interface numeric_comparison_exp {
  _eq?: any | null;
  _gt?: any | null;
  _gte?: any | null;
  _in?: any[] | null;
  _is_null?: boolean | null;
  _lt?: any | null;
  _lte?: any | null;
  _neq?: any | null;
  _nin?: any[] | null;
}

/**
 * Boolean expression to filter rows from the table "patent_publication_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface patent_publication_activity_positions_bool_exp {
  _and?: (patent_publication_activity_positions_bool_exp | null)[] | null;
  _not?: patent_publication_activity_positions_bool_exp | null;
  _or?: (patent_publication_activity_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  patent_publication_members?: patent_publication_members_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "patent_publication_activity_positions"
 */
export interface patent_publication_activity_positions_insert_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  patent_publication_members?: patent_publication_members_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "patent_publication_activity_positions"
 */
export interface patent_publication_activity_positions_obj_rel_insert_input {
  data: patent_publication_activity_positions_insert_input;
  on_conflict?: patent_publication_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "patent_publication_activity_positions"
 */
export interface patent_publication_activity_positions_on_conflict {
  constraint: patent_publication_activity_positions_constraint;
  update_columns: patent_publication_activity_positions_update_column[];
  where?: patent_publication_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "patent_publication_activity_positions"
 */
export interface patent_publication_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "patent_publication_activity_positions"
 */
export interface patent_publication_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "patent_publication_categories". All fields are combined with a logical 'AND'.
 */
export interface patent_publication_categories_bool_exp {
  _and?: (patent_publication_categories_bool_exp | null)[] | null;
  _not?: patent_publication_categories_bool_exp | null;
  _or?: (patent_publication_categories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "patent_publication_categories"
 */
export interface patent_publication_categories_insert_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "patent_publication_categories"
 */
export interface patent_publication_categories_obj_rel_insert_input {
  data: patent_publication_categories_insert_input;
  on_conflict?: patent_publication_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "patent_publication_categories"
 */
export interface patent_publication_categories_on_conflict {
  constraint: patent_publication_categories_constraint;
  update_columns: patent_publication_categories_update_column[];
  where?: patent_publication_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "patent_publication_categories"
 */
export interface patent_publication_categories_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "patent_publication_categories"
 */
export interface patent_publication_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by aggregate values of table "patent_publication_members"
 */
export interface patent_publication_members_aggregate_order_by {
  avg?: patent_publication_members_avg_order_by | null;
  count?: order_by | null;
  max?: patent_publication_members_max_order_by | null;
  min?: patent_publication_members_min_order_by | null;
  stddev?: patent_publication_members_stddev_order_by | null;
  stddev_pop?: patent_publication_members_stddev_pop_order_by | null;
  stddev_samp?: patent_publication_members_stddev_samp_order_by | null;
  sum?: patent_publication_members_sum_order_by | null;
  var_pop?: patent_publication_members_var_pop_order_by | null;
  var_samp?: patent_publication_members_var_samp_order_by | null;
  variance?: patent_publication_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "patent_publication_members"
 */
export interface patent_publication_members_arr_rel_insert_input {
  data: patent_publication_members_insert_input[];
  on_conflict?: patent_publication_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_avg_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "patent_publication_members". All fields are combined with a logical 'AND'.
 */
export interface patent_publication_members_bool_exp {
  _and?: (patent_publication_members_bool_exp | null)[] | null;
  _not?: patent_publication_members_bool_exp | null;
  _or?: (patent_publication_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  patent_publication?: patent_publications_bool_exp | null;
  patent_publication_activity_position?: patent_publication_activity_positions_bool_exp | null;
  patent_publication_id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "patent_publication_members"
 */
export interface patent_publication_members_insert_input {
  activity_position_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
  patent_publication?: patent_publications_obj_rel_insert_input | null;
  patent_publication_activity_position?: patent_publication_activity_positions_obj_rel_insert_input | null;
  patent_publication_id?: any | null;
}

/**
 * order by max() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_max_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * order by min() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_min_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * on conflict condition type for table "patent_publication_members"
 */
export interface patent_publication_members_on_conflict {
  constraint: patent_publication_members_constraint;
  update_columns: patent_publication_members_update_column[];
  where?: patent_publication_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_stddev_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * order by sum() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_sum_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * order by variance() on columns of table "patent_publication_members"
 */
export interface patent_publication_members_variance_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  patent_publication_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "patent_publications". All fields are combined with a logical 'AND'.
 */
export interface patent_publications_bool_exp {
  _and?: (patent_publications_bool_exp | null)[] | null;
  _not?: patent_publications_bool_exp | null;
  _or?: (patent_publications_bool_exp | null)[] | null;
  category_id?: bigint_comparison_exp | null;
  certificate_file?: String_comparison_exp | null;
  certificate_no?: String_comparison_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  credit_multiplier?: numeric_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  first_announcement_date?: date_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  patent_publication_category?: patent_publication_categories_bool_exp | null;
  patent_publication_members?: patent_publication_members_bool_exp | null;
  title?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "patent_publications"
 */
export interface patent_publications_insert_input {
  category_id?: any | null;
  certificate_file?: string | null;
  certificate_no?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  deleted_at?: any | null;
  first_announcement_date?: any | null;
  id?: any | null;
  patent_publication_category?: patent_publication_categories_obj_rel_insert_input | null;
  patent_publication_members?: patent_publication_members_arr_rel_insert_input | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "patent_publications"
 */
export interface patent_publications_obj_rel_insert_input {
  data: patent_publications_insert_input;
  on_conflict?: patent_publications_on_conflict | null;
}

/**
 * on conflict condition type for table "patent_publications"
 */
export interface patent_publications_on_conflict {
  constraint: patent_publications_constraint;
  update_columns: patent_publications_update_column[];
  where?: patent_publications_bool_exp | null;
}

/**
 * input type for updating data in table "patent_publications"
 */
export interface patent_publications_set_input {
  category_id?: any | null;
  certificate_file?: string | null;
  certificate_no?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  deleted_at?: any | null;
  first_announcement_date?: any | null;
  id?: any | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "research_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface research_activity_positions_bool_exp {
  _and?: (research_activity_positions_bool_exp | null)[] | null;
  _not?: research_activity_positions_bool_exp | null;
  _or?: (research_activity_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  research_members?: research_members_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "research_activity_positions"
 */
export interface research_activity_positions_insert_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  research_members?: research_members_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "research_activity_positions"
 */
export interface research_activity_positions_obj_rel_insert_input {
  data: research_activity_positions_insert_input;
  on_conflict?: research_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "research_activity_positions"
 */
export interface research_activity_positions_on_conflict {
  constraint: research_activity_positions_constraint;
  update_columns: research_activity_positions_update_column[];
  where?: research_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "research_activity_positions"
 */
export interface research_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "research_activity_positions"
 */
export interface research_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "research_categories". All fields are combined with a logical 'AND'.
 */
export interface research_categories_bool_exp {
  _and?: (research_categories_bool_exp | null)[] | null;
  _not?: research_categories_bool_exp | null;
  _or?: (research_categories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  researches?: researches_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "research_categories"
 */
export interface research_categories_insert_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  researches?: researches_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "research_categories"
 */
export interface research_categories_obj_rel_insert_input {
  data: research_categories_insert_input;
  on_conflict?: research_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "research_categories"
 */
export interface research_categories_on_conflict {
  constraint: research_categories_constraint;
  update_columns: research_categories_update_column[];
  where?: research_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "research_categories"
 */
export interface research_categories_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "research_categories"
 */
export interface research_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by aggregate values of table "research_members"
 */
export interface research_members_aggregate_order_by {
  avg?: research_members_avg_order_by | null;
  count?: order_by | null;
  max?: research_members_max_order_by | null;
  min?: research_members_min_order_by | null;
  stddev?: research_members_stddev_order_by | null;
  stddev_pop?: research_members_stddev_pop_order_by | null;
  stddev_samp?: research_members_stddev_samp_order_by | null;
  sum?: research_members_sum_order_by | null;
  var_pop?: research_members_var_pop_order_by | null;
  var_samp?: research_members_var_samp_order_by | null;
  variance?: research_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "research_members"
 */
export interface research_members_arr_rel_insert_input {
  data: research_members_insert_input[];
  on_conflict?: research_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "research_members"
 */
export interface research_members_avg_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "research_members". All fields are combined with a logical 'AND'.
 */
export interface research_members_bool_exp {
  _and?: (research_members_bool_exp | null)[] | null;
  _not?: research_members_bool_exp | null;
  _or?: (research_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  research?: researches_bool_exp | null;
  research_activity_position?: research_activity_positions_bool_exp | null;
  research_id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "research_members"
 */
export interface research_members_insert_input {
  activity_position_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
  research?: researches_obj_rel_insert_input | null;
  research_activity_position?: research_activity_positions_obj_rel_insert_input | null;
  research_id?: any | null;
}

/**
 * order by max() on columns of table "research_members"
 */
export interface research_members_max_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * order by min() on columns of table "research_members"
 */
export interface research_members_min_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * on conflict condition type for table "research_members"
 */
export interface research_members_on_conflict {
  constraint: research_members_constraint;
  update_columns: research_members_update_column[];
  where?: research_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "research_members"
 */
export interface research_members_stddev_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "research_members"
 */
export interface research_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "research_members"
 */
export interface research_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * order by sum() on columns of table "research_members"
 */
export interface research_members_sum_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "research_members"
 */
export interface research_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "research_members"
 */
export interface research_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * order by variance() on columns of table "research_members"
 */
export interface research_members_variance_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  research_id?: order_by | null;
}

/**
 * input type for inserting array relation for remote table "researches"
 */
export interface researches_arr_rel_insert_input {
  data: researches_insert_input[];
  on_conflict?: researches_on_conflict | null;
}

/**
 * Boolean expression to filter rows from the table "researches". All fields are combined with a logical 'AND'.
 */
export interface researches_bool_exp {
  _and?: (researches_bool_exp | null)[] | null;
  _not?: researches_bool_exp | null;
  _or?: (researches_bool_exp | null)[] | null;
  category_id?: bigint_comparison_exp | null;
  certificate_file?: String_comparison_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  credit_multiplier?: numeric_comparison_exp | null;
  decree_file?: String_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  final_report_file?: String_comparison_exp | null;
  finished_at?: timestamptz_comparison_exp | null;
  fund_amount?: bigint_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  proposal_file?: String_comparison_exp | null;
  research_category?: research_categories_bool_exp | null;
  research_members?: research_members_bool_exp | null;
  started_at?: timestamptz_comparison_exp | null;
  title?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "researches"
 */
export interface researches_insert_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  final_report_file?: string | null;
  finished_at?: any | null;
  fund_amount?: any | null;
  id?: any | null;
  proposal_file?: string | null;
  research_category?: research_categories_obj_rel_insert_input | null;
  research_members?: research_members_arr_rel_insert_input | null;
  started_at?: any | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "researches"
 */
export interface researches_obj_rel_insert_input {
  data: researches_insert_input;
  on_conflict?: researches_on_conflict | null;
}

/**
 * on conflict condition type for table "researches"
 */
export interface researches_on_conflict {
  constraint: researches_constraint;
  update_columns: researches_update_column[];
  where?: researches_bool_exp | null;
}

/**
 * input type for updating data in table "researches"
 */
export interface researches_set_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  final_report_file?: string | null;
  finished_at?: any | null;
  fund_amount?: any | null;
  id?: any | null;
  proposal_file?: string | null;
  started_at?: any | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "roles". All fields are combined with a logical 'AND'.
 */
export interface roles_bool_exp {
  _and?: (roles_bool_exp | null)[] | null;
  _not?: roles_bool_exp | null;
  _or?: (roles_bool_exp | null)[] | null;
  id?: bigint_comparison_exp | null;
  key?: String_comparison_exp | null;
  name?: String_comparison_exp | null;
  users?: users_bool_exp | null;
}

/**
 * input type for inserting data into table "roles"
 */
export interface roles_insert_input {
  id?: any | null;
  key?: string | null;
  name?: string | null;
  users?: users_arr_rel_insert_input | null;
}

/**
 * input type for inserting object relation for remote table "roles"
 */
export interface roles_obj_rel_insert_input {
  data: roles_insert_input;
  on_conflict?: roles_on_conflict | null;
}

/**
 * on conflict condition type for table "roles"
 */
export interface roles_on_conflict {
  constraint: roles_constraint;
  update_columns: roles_update_column[];
  where?: roles_bool_exp | null;
}

/**
 * ordering options when selecting data from "roles"
 */
export interface roles_order_by {
  id?: order_by | null;
  key?: order_by | null;
  name?: order_by | null;
  users_aggregate?: users_aggregate_order_by | null;
}

/**
 * Boolean expression to filter rows from the table "self_development_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface self_development_activity_positions_bool_exp {
  _and?: (self_development_activity_positions_bool_exp | null)[] | null;
  _not?: self_development_activity_positions_bool_exp | null;
  _or?: (self_development_activity_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  self_development_members?: self_development_members_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "self_development_activity_positions"
 */
export interface self_development_activity_positions_insert_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  self_development_members?: self_development_members_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "self_development_activity_positions"
 */
export interface self_development_activity_positions_obj_rel_insert_input {
  data: self_development_activity_positions_insert_input;
  on_conflict?: self_development_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "self_development_activity_positions"
 */
export interface self_development_activity_positions_on_conflict {
  constraint: self_development_activity_positions_constraint;
  update_columns: self_development_activity_positions_update_column[];
  where?: self_development_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "self_development_activity_positions"
 */
export interface self_development_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "self_development_activity_positions"
 */
export interface self_development_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "self_development_categories". All fields are combined with a logical 'AND'.
 */
export interface self_development_categories_bool_exp {
  _and?: (self_development_categories_bool_exp | null)[] | null;
  _not?: self_development_categories_bool_exp | null;
  _or?: (self_development_categories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  self_developments?: self_developments_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "self_development_categories"
 */
export interface self_development_categories_insert_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  self_developments?: self_developments_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "self_development_categories"
 */
export interface self_development_categories_obj_rel_insert_input {
  data: self_development_categories_insert_input;
  on_conflict?: self_development_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "self_development_categories"
 */
export interface self_development_categories_on_conflict {
  constraint: self_development_categories_constraint;
  update_columns: self_development_categories_update_column[];
  where?: self_development_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "self_development_categories"
 */
export interface self_development_categories_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "self_development_categories"
 */
export interface self_development_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "self_development_level". All fields are combined with a logical 'AND'.
 */
export interface self_development_level_bool_exp {
  _and?: (self_development_level_bool_exp | null)[] | null;
  _not?: self_development_level_bool_exp | null;
  _or?: (self_development_level_bool_exp | null)[] | null;
  level?: String_comparison_exp | null;
  self_developments?: self_developments_bool_exp | null;
}

/**
 * expression to compare columns of type self_development_level_enum. All fields are combined with logical 'AND'.
 */
export interface self_development_level_enum_comparison_exp {
  _eq?: self_development_level_enum | null;
  _in?: self_development_level_enum[] | null;
  _is_null?: boolean | null;
  _neq?: self_development_level_enum | null;
  _nin?: self_development_level_enum[] | null;
}

/**
 * input type for inserting data into table "self_development_level"
 */
export interface self_development_level_insert_input {
  level?: string | null;
  self_developments?: self_developments_arr_rel_insert_input | null;
}

/**
 * input type for inserting object relation for remote table "self_development_level"
 */
export interface self_development_level_obj_rel_insert_input {
  data: self_development_level_insert_input;
  on_conflict?: self_development_level_on_conflict | null;
}

/**
 * on conflict condition type for table "self_development_level"
 */
export interface self_development_level_on_conflict {
  constraint: self_development_level_constraint;
  update_columns: self_development_level_update_column[];
  where?: self_development_level_bool_exp | null;
}

/**
 * order by aggregate values of table "self_development_members"
 */
export interface self_development_members_aggregate_order_by {
  avg?: self_development_members_avg_order_by | null;
  count?: order_by | null;
  max?: self_development_members_max_order_by | null;
  min?: self_development_members_min_order_by | null;
  stddev?: self_development_members_stddev_order_by | null;
  stddev_pop?: self_development_members_stddev_pop_order_by | null;
  stddev_samp?: self_development_members_stddev_samp_order_by | null;
  sum?: self_development_members_sum_order_by | null;
  var_pop?: self_development_members_var_pop_order_by | null;
  var_samp?: self_development_members_var_samp_order_by | null;
  variance?: self_development_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "self_development_members"
 */
export interface self_development_members_arr_rel_insert_input {
  data: self_development_members_insert_input[];
  on_conflict?: self_development_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "self_development_members"
 */
export interface self_development_members_avg_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "self_development_members". All fields are combined with a logical 'AND'.
 */
export interface self_development_members_bool_exp {
  _and?: (self_development_members_bool_exp | null)[] | null;
  _not?: self_development_members_bool_exp | null;
  _or?: (self_development_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  self_development?: self_developments_bool_exp | null;
  self_development_activity_position?: self_development_activity_positions_bool_exp | null;
  self_development_id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "self_development_members"
 */
export interface self_development_members_insert_input {
  activity_position_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
  self_development?: self_developments_obj_rel_insert_input | null;
  self_development_activity_position?: self_development_activity_positions_obj_rel_insert_input | null;
  self_development_id?: any | null;
}

/**
 * order by max() on columns of table "self_development_members"
 */
export interface self_development_members_max_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * order by min() on columns of table "self_development_members"
 */
export interface self_development_members_min_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * on conflict condition type for table "self_development_members"
 */
export interface self_development_members_on_conflict {
  constraint: self_development_members_constraint;
  update_columns: self_development_members_update_column[];
  where?: self_development_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "self_development_members"
 */
export interface self_development_members_stddev_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "self_development_members"
 */
export interface self_development_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "self_development_members"
 */
export interface self_development_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * order by sum() on columns of table "self_development_members"
 */
export interface self_development_members_sum_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "self_development_members"
 */
export interface self_development_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "self_development_members"
 */
export interface self_development_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * order by variance() on columns of table "self_development_members"
 */
export interface self_development_members_variance_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  self_development_id?: order_by | null;
}

/**
 * input type for inserting array relation for remote table "self_developments"
 */
export interface self_developments_arr_rel_insert_input {
  data: self_developments_insert_input[];
  on_conflict?: self_developments_on_conflict | null;
}

/**
 * Boolean expression to filter rows from the table "self_developments". All fields are combined with a logical 'AND'.
 */
export interface self_developments_bool_exp {
  _and?: (self_developments_bool_exp | null)[] | null;
  _not?: self_developments_bool_exp | null;
  _or?: (self_developments_bool_exp | null)[] | null;
  category_id?: bigint_comparison_exp | null;
  certificate_file?: String_comparison_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  decree_file?: String_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  end_date?: date_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  level?: self_development_level_enum_comparison_exp | null;
  name?: String_comparison_exp | null;
  self_development_category?: self_development_categories_bool_exp | null;
  self_development_level?: self_development_level_bool_exp | null;
  self_development_members?: self_development_members_bool_exp | null;
  start_date?: date_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
  venue?: String_comparison_exp | null;
}

/**
 * input type for inserting data into table "self_developments"
 */
export interface self_developments_insert_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  end_date?: any | null;
  id?: any | null;
  level?: self_development_level_enum | null;
  name?: string | null;
  self_development_category?: self_development_categories_obj_rel_insert_input | null;
  self_development_level?: self_development_level_obj_rel_insert_input | null;
  self_development_members?: self_development_members_arr_rel_insert_input | null;
  start_date?: any | null;
  updated_at?: any | null;
  venue?: string | null;
}

/**
 * input type for inserting object relation for remote table "self_developments"
 */
export interface self_developments_obj_rel_insert_input {
  data: self_developments_insert_input;
  on_conflict?: self_developments_on_conflict | null;
}

/**
 * on conflict condition type for table "self_developments"
 */
export interface self_developments_on_conflict {
  constraint: self_developments_constraint;
  update_columns: self_developments_update_column[];
  where?: self_developments_bool_exp | null;
}

/**
 * input type for updating data in table "self_developments"
 */
export interface self_developments_set_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  end_date?: any | null;
  id?: any | null;
  level?: self_development_level_enum | null;
  name?: string | null;
  start_date?: any | null;
  updated_at?: any | null;
  venue?: string | null;
}

/**
 * Boolean expression to filter rows from the table "seminar_publication_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface seminar_publication_activity_positions_bool_exp {
  _and?: (seminar_publication_activity_positions_bool_exp | null)[] | null;
  _not?: seminar_publication_activity_positions_bool_exp | null;
  _or?: (seminar_publication_activity_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  seminar_publication_members?: seminar_publication_members_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "seminar_publication_activity_positions"
 */
export interface seminar_publication_activity_positions_insert_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  seminar_publication_members?: seminar_publication_members_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "seminar_publication_activity_positions"
 */
export interface seminar_publication_activity_positions_obj_rel_insert_input {
  data: seminar_publication_activity_positions_insert_input;
  on_conflict?: seminar_publication_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "seminar_publication_activity_positions"
 */
export interface seminar_publication_activity_positions_on_conflict {
  constraint: seminar_publication_activity_positions_constraint;
  update_columns: seminar_publication_activity_positions_update_column[];
  where?: seminar_publication_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "seminar_publication_activity_positions"
 */
export interface seminar_publication_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "seminar_publication_activity_positions"
 */
export interface seminar_publication_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "seminar_publication_categories". All fields are combined with a logical 'AND'.
 */
export interface seminar_publication_categories_bool_exp {
  _and?: (seminar_publication_categories_bool_exp | null)[] | null;
  _not?: seminar_publication_categories_bool_exp | null;
  _or?: (seminar_publication_categories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "seminar_publication_categories"
 */
export interface seminar_publication_categories_insert_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "seminar_publication_categories"
 */
export interface seminar_publication_categories_obj_rel_insert_input {
  data: seminar_publication_categories_insert_input;
  on_conflict?: seminar_publication_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "seminar_publication_categories"
 */
export interface seminar_publication_categories_on_conflict {
  constraint: seminar_publication_categories_constraint;
  update_columns: seminar_publication_categories_update_column[];
  where?: seminar_publication_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "seminar_publication_categories"
 */
export interface seminar_publication_categories_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "seminar_publication_categories"
 */
export interface seminar_publication_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by aggregate values of table "seminar_publication_members"
 */
export interface seminar_publication_members_aggregate_order_by {
  avg?: seminar_publication_members_avg_order_by | null;
  count?: order_by | null;
  max?: seminar_publication_members_max_order_by | null;
  min?: seminar_publication_members_min_order_by | null;
  stddev?: seminar_publication_members_stddev_order_by | null;
  stddev_pop?: seminar_publication_members_stddev_pop_order_by | null;
  stddev_samp?: seminar_publication_members_stddev_samp_order_by | null;
  sum?: seminar_publication_members_sum_order_by | null;
  var_pop?: seminar_publication_members_var_pop_order_by | null;
  var_samp?: seminar_publication_members_var_samp_order_by | null;
  variance?: seminar_publication_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "seminar_publication_members"
 */
export interface seminar_publication_members_arr_rel_insert_input {
  data: seminar_publication_members_insert_input[];
  on_conflict?: seminar_publication_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_avg_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "seminar_publication_members". All fields are combined with a logical 'AND'.
 */
export interface seminar_publication_members_bool_exp {
  _and?: (seminar_publication_members_bool_exp | null)[] | null;
  _not?: seminar_publication_members_bool_exp | null;
  _or?: (seminar_publication_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  seminar_publication?: seminar_publications_bool_exp | null;
  seminar_publication_activity_position?: seminar_publication_activity_positions_bool_exp | null;
  seminar_publication_id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "seminar_publication_members"
 */
export interface seminar_publication_members_insert_input {
  activity_position_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
  seminar_publication?: seminar_publications_obj_rel_insert_input | null;
  seminar_publication_activity_position?: seminar_publication_activity_positions_obj_rel_insert_input | null;
  seminar_publication_id?: any | null;
}

/**
 * order by max() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_max_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * order by min() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_min_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * on conflict condition type for table "seminar_publication_members"
 */
export interface seminar_publication_members_on_conflict {
  constraint: seminar_publication_members_constraint;
  update_columns: seminar_publication_members_update_column[];
  where?: seminar_publication_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_stddev_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * order by sum() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_sum_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * order by variance() on columns of table "seminar_publication_members"
 */
export interface seminar_publication_members_variance_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  seminar_publication_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "seminar_publications". All fields are combined with a logical 'AND'.
 */
export interface seminar_publications_bool_exp {
  _and?: (seminar_publications_bool_exp | null)[] | null;
  _not?: seminar_publications_bool_exp | null;
  _or?: (seminar_publications_bool_exp | null)[] | null;
  category_id?: bigint_comparison_exp | null;
  certificate_file?: String_comparison_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  credit_multiplier?: numeric_comparison_exp | null;
  decree_file?: String_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  isbn?: String_comparison_exp | null;
  issn?: String_comparison_exp | null;
  paper_file?: String_comparison_exp | null;
  proceeding_cover_file?: String_comparison_exp | null;
  proceeding_toc_file?: String_comparison_exp | null;
  seminar_date?: date_comparison_exp | null;
  seminar_name?: String_comparison_exp | null;
  seminar_page_no?: String_comparison_exp | null;
  seminar_publication_category?: seminar_publication_categories_bool_exp | null;
  seminar_publication_members?: seminar_publication_members_bool_exp | null;
  seminar_venue?: String_comparison_exp | null;
  seminar_year?: Int_comparison_exp | null;
  title?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "seminar_publications"
 */
export interface seminar_publications_insert_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  id?: any | null;
  isbn?: string | null;
  issn?: string | null;
  paper_file?: string | null;
  proceeding_cover_file?: string | null;
  proceeding_toc_file?: string | null;
  seminar_date?: any | null;
  seminar_name?: string | null;
  seminar_page_no?: string | null;
  seminar_publication_category?: seminar_publication_categories_obj_rel_insert_input | null;
  seminar_publication_members?: seminar_publication_members_arr_rel_insert_input | null;
  seminar_venue?: string | null;
  seminar_year?: number | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "seminar_publications"
 */
export interface seminar_publications_obj_rel_insert_input {
  data: seminar_publications_insert_input;
  on_conflict?: seminar_publications_on_conflict | null;
}

/**
 * on conflict condition type for table "seminar_publications"
 */
export interface seminar_publications_on_conflict {
  constraint: seminar_publications_constraint;
  update_columns: seminar_publications_update_column[];
  where?: seminar_publications_bool_exp | null;
}

/**
 * input type for updating data in table "seminar_publications"
 */
export interface seminar_publications_set_input {
  category_id?: any | null;
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  id?: any | null;
  isbn?: string | null;
  issn?: string | null;
  paper_file?: string | null;
  proceeding_cover_file?: string | null;
  proceeding_toc_file?: string | null;
  seminar_date?: any | null;
  seminar_name?: string | null;
  seminar_page_no?: string | null;
  seminar_venue?: string | null;
  seminar_year?: number | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "service_activity_positions". All fields are combined with a logical 'AND'.
 */
export interface service_activity_positions_bool_exp {
  _and?: (service_activity_positions_bool_exp | null)[] | null;
  _not?: service_activity_positions_bool_exp | null;
  _or?: (service_activity_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  id?: Int_comparison_exp | null;
  name?: String_comparison_exp | null;
  service_members?: service_members_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "service_activity_positions"
 */
export interface service_activity_positions_insert_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  service_members?: service_members_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "service_activity_positions"
 */
export interface service_activity_positions_obj_rel_insert_input {
  data: service_activity_positions_insert_input;
  on_conflict?: service_activity_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "service_activity_positions"
 */
export interface service_activity_positions_on_conflict {
  constraint: service_activity_positions_constraint;
  update_columns: service_activity_positions_update_column[];
  where?: service_activity_positions_bool_exp | null;
}

/**
 * primary key columns input for table: "service_activity_positions"
 */
export interface service_activity_positions_pk_columns_input {
  id: number;
}

/**
 * input type for updating data in table "service_activity_positions"
 */
export interface service_activity_positions_set_input {
  created_at?: any | null;
  id?: number | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "service_categories". All fields are combined with a logical 'AND'.
 */
export interface service_categories_bool_exp {
  _and?: (service_categories_bool_exp | null)[] | null;
  _not?: service_categories_bool_exp | null;
  _or?: (service_categories_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  credit?: numeric_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  services?: services_bool_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "service_categories"
 */
export interface service_categories_insert_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  services?: services_arr_rel_insert_input | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "service_categories"
 */
export interface service_categories_obj_rel_insert_input {
  data: service_categories_insert_input;
  on_conflict?: service_categories_on_conflict | null;
}

/**
 * on conflict condition type for table "service_categories"
 */
export interface service_categories_on_conflict {
  constraint: service_categories_constraint;
  update_columns: service_categories_update_column[];
  where?: service_categories_bool_exp | null;
}

/**
 * primary key columns input for table: "service_categories"
 */
export interface service_categories_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "service_categories"
 */
export interface service_categories_set_input {
  created_at?: any | null;
  credit?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * order by aggregate values of table "service_members"
 */
export interface service_members_aggregate_order_by {
  avg?: service_members_avg_order_by | null;
  count?: order_by | null;
  max?: service_members_max_order_by | null;
  min?: service_members_min_order_by | null;
  stddev?: service_members_stddev_order_by | null;
  stddev_pop?: service_members_stddev_pop_order_by | null;
  stddev_samp?: service_members_stddev_samp_order_by | null;
  sum?: service_members_sum_order_by | null;
  var_pop?: service_members_var_pop_order_by | null;
  var_samp?: service_members_var_samp_order_by | null;
  variance?: service_members_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "service_members"
 */
export interface service_members_arr_rel_insert_input {
  data: service_members_insert_input[];
  on_conflict?: service_members_on_conflict | null;
}

/**
 * order by avg() on columns of table "service_members"
 */
export interface service_members_avg_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "service_members". All fields are combined with a logical 'AND'.
 */
export interface service_members_bool_exp {
  _and?: (service_members_bool_exp | null)[] | null;
  _not?: service_members_bool_exp | null;
  _or?: (service_members_bool_exp | null)[] | null;
  activity_position_id?: bigint_comparison_exp | null;
  credit_gain?: numeric_comparison_exp | null;
  employee?: employees_bool_exp | null;
  employee_id?: bigint_comparison_exp | null;
  external_author?: String_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  service?: services_bool_exp | null;
  service_activity_position?: service_activity_positions_bool_exp | null;
  service_id?: bigint_comparison_exp | null;
}

/**
 * input type for inserting data into table "service_members"
 */
export interface service_members_insert_input {
  activity_position_id?: any | null;
  credit_gain?: any | null;
  employee?: employees_obj_rel_insert_input | null;
  employee_id?: any | null;
  external_author?: string | null;
  id?: any | null;
  service?: services_obj_rel_insert_input | null;
  service_activity_position?: service_activity_positions_obj_rel_insert_input | null;
  service_id?: any | null;
}

/**
 * order by max() on columns of table "service_members"
 */
export interface service_members_max_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * order by min() on columns of table "service_members"
 */
export interface service_members_min_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  external_author?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * on conflict condition type for table "service_members"
 */
export interface service_members_on_conflict {
  constraint: service_members_constraint;
  update_columns: service_members_update_column[];
  where?: service_members_bool_exp | null;
}

/**
 * order by stddev() on columns of table "service_members"
 */
export interface service_members_stddev_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "service_members"
 */
export interface service_members_stddev_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "service_members"
 */
export interface service_members_stddev_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * order by sum() on columns of table "service_members"
 */
export interface service_members_sum_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "service_members"
 */
export interface service_members_var_pop_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "service_members"
 */
export interface service_members_var_samp_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * order by variance() on columns of table "service_members"
 */
export interface service_members_variance_order_by {
  activity_position_id?: order_by | null;
  credit_gain?: order_by | null;
  employee_id?: order_by | null;
  id?: order_by | null;
  service_id?: order_by | null;
}

/**
 * input type for inserting array relation for remote table "services"
 */
export interface services_arr_rel_insert_input {
  data: services_insert_input[];
  on_conflict?: services_on_conflict | null;
}

/**
 * Boolean expression to filter rows from the table "services". All fields are combined with a logical 'AND'.
 */
export interface services_bool_exp {
  _and?: (services_bool_exp | null)[] | null;
  _not?: services_bool_exp | null;
  _or?: (services_bool_exp | null)[] | null;
  certificate_file?: String_comparison_exp | null;
  created_at?: timestamptz_comparison_exp | null;
  credit_multiplier?: numeric_comparison_exp | null;
  decree_file?: String_comparison_exp | null;
  deleted_at?: timestamptz_comparison_exp | null;
  end_date?: date_comparison_exp | null;
  final_report_file?: String_comparison_exp | null;
  fund_amount?: Int_comparison_exp | null;
  id?: bigint_comparison_exp | null;
  proposal_file?: String_comparison_exp | null;
  service_category?: service_categories_bool_exp | null;
  service_category_id?: bigint_comparison_exp | null;
  service_members?: service_members_bool_exp | null;
  start_date?: date_comparison_exp | null;
  title?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "services"
 */
export interface services_insert_input {
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  end_date?: any | null;
  final_report_file?: string | null;
  fund_amount?: number | null;
  id?: any | null;
  proposal_file?: string | null;
  service_category?: service_categories_obj_rel_insert_input | null;
  service_category_id?: any | null;
  service_members?: service_members_arr_rel_insert_input | null;
  start_date?: any | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "services"
 */
export interface services_obj_rel_insert_input {
  data: services_insert_input;
  on_conflict?: services_on_conflict | null;
}

/**
 * on conflict condition type for table "services"
 */
export interface services_on_conflict {
  constraint: services_constraint;
  update_columns: services_update_column[];
  where?: services_bool_exp | null;
}

/**
 * input type for updating data in table "services"
 */
export interface services_set_input {
  certificate_file?: string | null;
  created_at?: any | null;
  credit_multiplier?: any | null;
  decree_file?: string | null;
  deleted_at?: any | null;
  end_date?: any | null;
  final_report_file?: string | null;
  fund_amount?: number | null;
  id?: any | null;
  proposal_file?: string | null;
  service_category_id?: any | null;
  start_date?: any | null;
  title?: string | null;
  updated_at?: any | null;
}

/**
 * Boolean expression to filter rows from the table "structural_positions". All fields are combined with a logical 'AND'.
 */
export interface structural_positions_bool_exp {
  _and?: (structural_positions_bool_exp | null)[] | null;
  _not?: structural_positions_bool_exp | null;
  _or?: (structural_positions_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  employees?: employees_bool_exp | null;
  id?: bigint_comparison_exp | null;
  name?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
}

/**
 * input type for inserting data into table "structural_positions"
 */
export interface structural_positions_insert_input {
  created_at?: any | null;
  employees?: employees_arr_rel_insert_input | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * input type for inserting object relation for remote table "structural_positions"
 */
export interface structural_positions_obj_rel_insert_input {
  data: structural_positions_insert_input;
  on_conflict?: structural_positions_on_conflict | null;
}

/**
 * on conflict condition type for table "structural_positions"
 */
export interface structural_positions_on_conflict {
  constraint: structural_positions_constraint;
  update_columns: structural_positions_update_column[];
  where?: structural_positions_bool_exp | null;
}

/**
 * ordering options when selecting data from "structural_positions"
 */
export interface structural_positions_order_by {
  created_at?: order_by | null;
  employees_aggregate?: employees_aggregate_order_by | null;
  id?: order_by | null;
  name?: order_by | null;
  updated_at?: order_by | null;
}

/**
 * primary key columns input for table: "structural_positions"
 */
export interface structural_positions_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "structural_positions"
 */
export interface structural_positions_set_input {
  created_at?: any | null;
  id?: any | null;
  name?: string | null;
  updated_at?: any | null;
}

/**
 * expression to compare columns of type timestamp. All fields are combined with logical 'AND'.
 */
export interface timestamp_comparison_exp {
  _eq?: any | null;
  _gt?: any | null;
  _gte?: any | null;
  _in?: any[] | null;
  _is_null?: boolean | null;
  _lt?: any | null;
  _lte?: any | null;
  _neq?: any | null;
  _nin?: any[] | null;
}

/**
 * expression to compare columns of type timestamptz. All fields are combined with logical 'AND'.
 */
export interface timestamptz_comparison_exp {
  _eq?: any | null;
  _gt?: any | null;
  _gte?: any | null;
  _in?: any[] | null;
  _is_null?: boolean | null;
  _lt?: any | null;
  _lte?: any | null;
  _neq?: any | null;
  _nin?: any[] | null;
}

/**
 * order by aggregate values of table "users"
 */
export interface users_aggregate_order_by {
  avg?: users_avg_order_by | null;
  count?: order_by | null;
  max?: users_max_order_by | null;
  min?: users_min_order_by | null;
  stddev?: users_stddev_order_by | null;
  stddev_pop?: users_stddev_pop_order_by | null;
  stddev_samp?: users_stddev_samp_order_by | null;
  sum?: users_sum_order_by | null;
  var_pop?: users_var_pop_order_by | null;
  var_samp?: users_var_samp_order_by | null;
  variance?: users_variance_order_by | null;
}

/**
 * input type for inserting array relation for remote table "users"
 */
export interface users_arr_rel_insert_input {
  data: users_insert_input[];
  on_conflict?: users_on_conflict | null;
}

/**
 * order by avg() on columns of table "users"
 */
export interface users_avg_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

/**
 * Boolean expression to filter rows from the table "users". All fields are combined with a logical 'AND'.
 */
export interface users_bool_exp {
  _and?: (users_bool_exp | null)[] | null;
  _not?: users_bool_exp | null;
  _or?: (users_bool_exp | null)[] | null;
  created_at?: timestamptz_comparison_exp | null;
  employees?: employees_bool_exp | null;
  id?: bigint_comparison_exp | null;
  password?: String_comparison_exp | null;
  role?: roles_bool_exp | null;
  role_id?: bigint_comparison_exp | null;
  token?: String_comparison_exp | null;
  updated_at?: timestamptz_comparison_exp | null;
  username?: String_comparison_exp | null;
}

/**
 * input type for inserting data into table "users"
 */
export interface users_insert_input {
  created_at?: any | null;
  employees?: employees_arr_rel_insert_input | null;
  id?: any | null;
  password?: string | null;
  role?: roles_obj_rel_insert_input | null;
  role_id?: any | null;
  token?: string | null;
  updated_at?: any | null;
  username?: string | null;
}

/**
 * order by max() on columns of table "users"
 */
export interface users_max_order_by {
  created_at?: order_by | null;
  id?: order_by | null;
  password?: order_by | null;
  role_id?: order_by | null;
  token?: order_by | null;
  updated_at?: order_by | null;
  username?: order_by | null;
}

/**
 * order by min() on columns of table "users"
 */
export interface users_min_order_by {
  created_at?: order_by | null;
  id?: order_by | null;
  password?: order_by | null;
  role_id?: order_by | null;
  token?: order_by | null;
  updated_at?: order_by | null;
  username?: order_by | null;
}

/**
 * input type for inserting object relation for remote table "users"
 */
export interface users_obj_rel_insert_input {
  data: users_insert_input;
  on_conflict?: users_on_conflict | null;
}

/**
 * on conflict condition type for table "users"
 */
export interface users_on_conflict {
  constraint: users_constraint;
  update_columns: users_update_column[];
  where?: users_bool_exp | null;
}

/**
 * ordering options when selecting data from "users"
 */
export interface users_order_by {
  created_at?: order_by | null;
  employees_aggregate?: employees_aggregate_order_by | null;
  id?: order_by | null;
  password?: order_by | null;
  role?: roles_order_by | null;
  role_id?: order_by | null;
  token?: order_by | null;
  updated_at?: order_by | null;
  username?: order_by | null;
}

/**
 * primary key columns input for table: "users"
 */
export interface users_pk_columns_input {
  id: any;
}

/**
 * input type for updating data in table "users"
 */
export interface users_set_input {
  created_at?: any | null;
  id?: any | null;
  password?: string | null;
  role_id?: any | null;
  token?: string | null;
  updated_at?: any | null;
  username?: string | null;
}

/**
 * order by stddev() on columns of table "users"
 */
export interface users_stddev_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

/**
 * order by stddev_pop() on columns of table "users"
 */
export interface users_stddev_pop_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

/**
 * order by stddev_samp() on columns of table "users"
 */
export interface users_stddev_samp_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

/**
 * order by sum() on columns of table "users"
 */
export interface users_sum_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

/**
 * order by var_pop() on columns of table "users"
 */
export interface users_var_pop_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

/**
 * order by var_samp() on columns of table "users"
 */
export interface users_var_samp_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

/**
 * order by variance() on columns of table "users"
 */
export interface users_variance_order_by {
  id?: order_by | null;
  role_id?: order_by | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
