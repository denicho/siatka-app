import React from "react";
import { Table, Row, Col } from "antd";
import { format } from "date-fns";
import { id } from "date-fns/locale";
import { HomeOutlined } from "@ant-design/icons";
import ManagementHeading from "../Common/ManagementHeading";
import AddButton from "./AddForm";
import MutationActionsModal from "./MutationActionsModal";
import { useQuery } from "@apollo/client";
import { ServiceCategories } from "../globalTypes";
import { SERVICE_CATEGORIES } from "./gql/serviceCategoriesGql";
import LoadingPlaceholder from "../Common/LoadingPlaceholder";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name",
    width: 400,
  },
  {
    title: "Angka Kredit",
    dataIndex: "credit",
    key: "credit",
  },
  {
    title: "Tanggal Dibuat",
    dataIndex: "created_at",
    key: "created_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
  {
    title: "Tanggal Diperbarui",
    dataIndex: "updated_at",
    key: "updated_at",
    render: (text: string) => {
      const textAsDate = new Date(text);

      return (
        <span>
          {format(textAsDate, "dd MMMM yyyy, HH:mm", {
            locale: id,
          })}
        </span>
      );
    },
  },
];

const breadcrumbs = [
  {
    to: "/dashboard",
    content: <HomeOutlined />,
  },
  {
    to: "/dashboard/manage/service-categories",
    content: "Pengelolaan Jenis Pengabdian",
  },
];

const ServiceCategoriesListPage = () => {
  const listQueryRes = useQuery<ServiceCategories>(SERVICE_CATEGORIES);

  const columnsWithAction = React.useMemo(() => {
    return [
      ...columns,
      {
        title: "Aksi",
        key: "operation",
        dataIndex: "action",
        width: 140,
        render: (text: any, record: any, index: any) => (
          <MutationActionsModal record={record} />
        ),
      },
    ];
  }, []);

  if (
    listQueryRes.loading ||
    !listQueryRes.data ||
    !listQueryRes.data.service_categories
  ) {
    return <LoadingPlaceholder />;
  }

  const serviceCategories = listQueryRes.data.service_categories;

  return (
    <>
      <ManagementHeading
        title="Pengelolaan Jenis Pengabdian"
        breadcrumbs={breadcrumbs}
        rightAddon={<AddButton />}
      />
      <Row style={{ marginTop: 24 }}>
        <Col span={24}>
          <Table
            size="middle"
            rowKey="id"
            columns={columnsWithAction}
            dataSource={serviceCategories}
          />
        </Col>
      </Row>
    </>
  );
};

export default ServiceCategoriesListPage;
