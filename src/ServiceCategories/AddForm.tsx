import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, InputNumber, Modal } from "antd";
import { Store } from "antd/lib/form/interface";
import { gql, useMutation } from "@apollo/client";
import {
  InsertServiceCategories,
  InsertServiceCategoriesVariables,
} from "../globalTypes";
import { INSERT_SERVICE_CATEGORIES } from "./gql/serviceCategoriesGql";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: "${label} wajib diisi!",
};

const ServiceCategoriesAddButton = () => {
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleAddClick = React.useCallback(() => {
    setVisible(true);
  }, []);
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    form.submit();
  };
  const [create] = useMutation<
    InsertServiceCategories,
    InsertServiceCategoriesVariables
  >(INSERT_SERVICE_CATEGORIES, {
    onCompleted: () => {
      form.resetFields();
      setLoading(false);
      setVisible(false);
    },
    onError: () => {
      setLoading(false);
      setVisible(false);
    },
  });
  const onFinish = (values: Store) => {
    setLoading(true);

    create({
      variables: {
        object: {
          name: values.name,
          credit: values.credit,
        },
      },
      update: (cache, { data }) => {
        if (data?.insert_service_categories_one) {
          cache.modify({
            fields: {
              service_categories(existing = []) {
                const newData = cache.writeFragment({
                  data: data.insert_service_categories_one,
                  fragment: gql`
                    fragment service_categories on service_categories {
                      id
                      name
                      credit
                      created_at
                      updated_at
                    }
                  `,
                });
                return [...existing, newData];
              },
            },
          });
        }
      },
    });
  };

  return (
    <>
      <Modal
        title="Tambah Jenis Pengabdian"
        visible={visible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Batal
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={handleOk}
          >
            Simpan
          </Button>,
        ]}
      >
        <Form
          form={form}
          {...layout}
          name="add-serviceCategories"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name={"name"} label="Nama" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            name={"credit"}
            label="Angka Kredit"
            rules={[{ required: true }]}
          >
            <InputNumber min={0} step={0.1} />
          </Form.Item>
        </Form>
      </Modal>

      <Button type="primary" onClick={handleAddClick}>
        <PlusOutlined />
        Tambah Jenis Pengabdian
      </Button>
    </>
  );
};

export default ServiceCategoriesAddButton;
