import { gql } from "@apollo/client";

export const DELETE_SERVICE_ACTIVITY_POSITIONS = gql`
  mutation DeleteServiceActivityPositions($id: Int!) {
    delete_service_activity_positions_by_pk(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const SERVICE_ACTIVITY_POSITIONS = gql`
  query ServiceActivityPositions {
    service_activity_positions(order_by: { created_at: asc }) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const INSERT_SERVICE_ACTIVITY_POSITIONS = gql`
  mutation InsertServiceActivityPositions(
    $object: service_activity_positions_insert_input!
  ) {
    insert_service_activity_positions_one(object: $object) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_SERVICE_ACTIVITY_POSITIONS = gql`
  mutation UpdateServiceActivityPositions(
    $pk_columns: service_activity_positions_pk_columns_input!
    $_set: service_activity_positions_set_input
  ) {
    update_service_activity_positions_by_pk(
      pk_columns: $pk_columns
      _set: $_set
    ) {
      id
      name
      created_at
      updated_at
    }
  }
`;
