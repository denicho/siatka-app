import { gql } from "@apollo/client";

export const DELETE_SERVICE_CATEGORIES = gql`
  mutation DeleteServiceCategories($id: bigint!) {
    delete_service_categories_by_pk(id: $id) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const SERVICE_CATEGORIES = gql`
  query ServiceCategories {
    service_categories(order_by: { created_at: asc }) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const INSERT_SERVICE_CATEGORIES = gql`
  mutation InsertServiceCategories($object: service_categories_insert_input!) {
    insert_service_categories_one(object: $object) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;

export const UPDATE_SERVICE_CATEGORIES = gql`
  mutation UpdateServiceCategories(
    $pk_columns: service_categories_pk_columns_input!
    $_set: service_categories_set_input
  ) {
    update_service_categories_by_pk(pk_columns: $pk_columns, _set: $_set) {
      id
      name
      credit
      created_at
      updated_at
    }
  }
`;
