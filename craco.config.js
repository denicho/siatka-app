const CracoLessPlugin = require("craco-less");
const colors = require("./colors");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          modifyVars: {
            "primary-color": "#0f2b6c",
          },
          javascriptEnabled: true,
        },
      },
    },
  ],
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
};
